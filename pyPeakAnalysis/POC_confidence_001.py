from __future__ import division
import numpy as np 
import random
import matplotlib.pyplot as plt 
from matplotlib.widgets import Slider, Button, RadioButtons
import cv2



path = '/media/morzh/ext4_volume/data/Faces/all_in_one/set_006/'
filename = '0A4xNgYF1Bw.jpg'




def estimate_confidence(poc, t):

    # max = poc[t[0], t[1]]
    conf   = np.max(poc)/ np.sum(poc)

    return conf

def lowpass(img):

    kernel = np.ones((3, 3), np.float32) / 9
    return cv2.filter2D(img, -1, kernel)

def estimate_translation(poc):
    shape = poc.shape
    t0, t1 = np.unravel_index(np.argmax(poc), shape)
    if t0 > shape[0] // 2:
        t0 -= shape[0]
    if t1 > shape[1] // 2:
        t1 -= shape[1]
    return [t0, t1]


def update(val):

    xshift  = slider_xshift.val
    yshift  = slider_yshift.val
    val_blur= 2*int(slider_blur.val)+1

    # print('val_blur', val_blur)

    if val_blur > 0:
        img = cv2.GaussianBlur(img_fixed, (val_blur, val_blur), 0)
    else:
        img = img_fixed


    T = np.float32([[1, 0, xshift], [0, 1, yshift]])
    img_moved = cv2.warpAffine(img, T, (img_fixed.shape[1], img_fixed.shape[0]))

    poc = calc_poc(img_moved, img_fixed)
    poc = lowpass(poc)
    # poc = cv2.Laplacian(poc, -1)
    img_poc = image_stretch(poc)
    t = estimate_translation(poc)

    poc = lowpass(poc)
    conf = estimate_confidence(poc,t)
    print('translation:', t, 'confidence', conf)

    plt.title('shiftX: '+ str(t[0])+' shiftY: '+str(t[1]))
    plt.subplot(1,3,1)
    plt.imshow(img)
    plt.gray()

    plt.subplot(1,3,2)
    plt.imshow(img_moved)
    plt.gray()

    plt.subplot(1,3,3)
    plt.imshow(img_poc)
    plt.gray()

    plt.figure().canvas.draw()




def calc_poc(img_fixed, img_moved):

    blackman_x = np.blackman(img_fixed.shape[0])
    blackman_y = np.blackman(img_fixed.shape[1])

    blackman_x = blackman_x.reshape((blackman_x.shape[0], 1))
    blackman_y = blackman_y.reshape((1, blackman_y.shape[0]))
    blackman   = np.matmul(blackman_x, blackman_y)

    # print('Blackman shape', blackman.shape)

    img_fixed_blackman = np.multiply(blackman, img_fixed)
    img_moved_blackman = np.multiply(blackman, img_moved)
    # img_fixed_blackman = img_fixed
    # img_moved_blackman = img_moved

    fft_fixed = np.fft.fft2(img_fixed_blackman)
    fft_moved = np.fft.fft2(img_moved_blackman)

    R = fft_fixed * np.conj(fft_moved)
    R /= np.absolute(R)

    return np.fft.ifft2(R).astype(np.float32)


def image_stretch(poc):

    img = (poc-poc.min())/(poc.max()-poc.min()) * 255.0
    return img.astype(np.uint8)


img_fixed          = cv2.imread(path+filename, cv2.IMREAD_GRAYSCALE)
shift              = np.array([30.0, 20.0])
translation_matrix = np.float32([[1, 0, shift[0]], [0, 1, shift[1]]])
img_moved          = cv2.warpAffine(img_fixed, translation_matrix, (img_fixed.shape[1], img_fixed.shape[0]))

poc = calc_poc(img_moved, img_fixed)
poc = lowpass(poc)
img_poc = image_stretch(poc)

t = estimate_translation(poc)
conf = estimate_confidence(poc , t)

print('translation', t, 'confidence', conf)

plt.figure(figsize=(20, 12))
plt.subplots_adjust(left=0.05, bottom=0.29)

axcolor   = 'lightgoldenrodyellow'
ax_xshift = plt.axes([0.075, 0.10, 0.8, 0.03], facecolor=axcolor)
ax_yshift = plt.axes([0.075, 0.15, 0.8, 0.03], facecolor=axcolor)
ax_blur   = plt.axes([0.075, 0.20, 0.8, 0.03], facecolor=axcolor)

plt.title('shiftX: '+ str(0.0)+' shiftY: '+str(0.0))

plt.subplot(1,3,1)
plt.imshow(img_fixed)
plt.gray()

plt.subplot(1,3,2)
plt.imshow(img_moved)
plt.gray()

plt.subplot(1,3,3)
plt.imshow(img_poc)
plt.gray()

slider_xshift = Slider(ax_xshift, 'X Shift', 0.0, 30.0, valinit=0)
slider_yshift = Slider(ax_yshift, 'Y Shift', 0.0, 30.0, valinit=0)
slider_blur   = Slider(ax_blur,   'Blur',    0.0, 30.0, valinit=0)

slider_xshift.on_changed(update)
slider_yshift.on_changed(update)
slider_blur.on_changed(update)

plt.show()