from __future__ import division
 
import matplotlib.pyplot as plt
import random 
import numpy as np


amplitude = 10.0
t = 200

random.seed()
x = list(np.arange(t))
y = random.sample(x, t) 
plt.plot(x, y)
plt.show()


##=================================================================================##
 
fL = 0.1  # Cutoff frequency as a fraction of the sampling rate (in (0, 0.5)).
fH = 0.4  # Cutoff frequency as a fraction of the sampling rate (in (0, 0.5)).
b = 0.08  # Transition band, as a fraction of the sampling rate (in (0, 0.5)).
N = int(np.ceil((4 / b)))
if not N % 2: N += 1  # Make sure that N is odd.
n = np.arange(N)
 
############################################# Compute a low-pass filter with cutoff frequency fH.
hlpf = np.sinc(2 * fH * (n - (N - 1) / 2))
plt.plot(np.arange(N), hlpf)
plt.show()

hlpf *= np.blackman(N)
hlpf = hlpf / np.sum(hlpf)
plt.plot(np.arange(N), hlpf)
plt.show()


 
########################################### Compute a high-pass filter with cutoff frequency fL.
hhpf = np.sinc(2 * fL * (n - (N - 1) / 2))
hhpf *= np.blackman(N)
hhpf = hhpf / np.sum(hhpf)
hhpf = -hhpf
hhpf[(N - 1) // 2] += 1

plt.plot(np.arange(N), hhpf)
plt.show()
 
# Convolve both filters.
h = np.convolve(hlpf, hhpf)

plt.plot(np.arange(len(h)), h)
plt.show()

s = np.convolve(y, h)

plt.plot(x, s)
plt.show()