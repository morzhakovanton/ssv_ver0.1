import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


ps_filename = '/home/morzh/temp/powerSpectrum.txt'
ps = np.loadtxt(ps_filename, delimiter=',')

ind_max = np.unravel_index(np.argmax(ps, axis=None), ps.shape)
print('max value: ', np.max(ps), ' at ', ind_max)

print(ps.shape[0], ps.shape[1])

xvalues = np.linspace(0, ps.shape[1]-1, ps.shape[1])
yvalues = np.linspace(0, ps.shape[0]-1, ps.shape[0])

xx, yy = np.meshgrid(xvalues, yvalues)

print(xx.shape[0], xx.shape[1])
print(yy.shape[0], yy.shape[1])


fig = plt.figure()
ax = Axes3D(fig)

#ax.scatter(xx,yy, ps,s=1)
#plt.show()

