from __future__ import division
import numpy as np 
import random
import matplotlib.pyplot as plt 

#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D


ps_filename = '/home/morzh/work/pyPeakAnalysis/powerSpectrum.txt'
ps = np.loadtxt(ps_filename, delimiter=',')
print('shape', ps.shape)

max = np.unravel_index(np.argmax(ps), ps.shape)
print(max)

s = ps[max[0], :]
x = np.arange(len(s))




fL = 0.05  # Cutoff frequency as a fraction of the sampling rate (in (0, 0.5)).
fH = 0.495  # Cutoff frequency as a fraction of the sampling rate (in (0, 0.5)).
b  = 0.08  # Transition band, as a fraction of the sampling rate (in (0, 0.5)).

N = int(np.ceil((4 / b)))
if not N % 2: N += 1  # Make sure that N is odd.
n = np.arange(N)
 
# Compute a low-pass filter with cutoff frequency fL.
lpf = np.sinc(2 * fL * (n - (N - 1) / 2)) # Compute sinc filter.
w = 0.42 - 0.5 * np.cos(2 * np.pi * n / (N - 1)) +   0.08 * np.cos(4 * np.pi * n / (N - 1)) # Compute Blackman window
lpf = lpf * w # Multiply sinc filter with window
lpf = lpf / np.sum(lpf) # Normalize to get unity gain.


# Compute a high-pass filter with cutoff frequency fH.
hpf = np.sinc(2 * fH * (n - (N - 1) / 2))
hpf *= np.blackman(N)
hpf /= np.sum(hpf)
hpf = -hpf
hpf[(N - 1) // 2] += 1


lhpf = lpf + hpf

sg= np.convolve(s, lpf)

fig, ax = plt.subplots()
fig.set_size_inches(19, 10)
plt.subplot(2,1,1)
plt.plot(np.arange(len(s)), s)
plt.subplot(2,1,2)
plt.plot(np.arange(len(sg)), sg)
plt.tight_layout()
plt.show()