
from scipy.io import wavfile
import numpy as np

buffer_len = 256

input_wav = 'sa1.wav'
samp_freq, signal = wavfile.read(input_wav)
signal = signal[:,]  # get first channel
n_buffers = len(signal)//buffer_len
data_type = signal.dtype


