from __future__ import division
import numpy as np 
import random
import matplotlib.pyplot as plt 

#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D

from scipy.signal import savgol_filter


ps_filename = '/home/morzh/work/pyPeakAnalysis/powerSpectrum.txt'
ps = np.loadtxt(ps_filename, delimiter=',')
print('shape', ps.shape)

max = np.unravel_index(np.argmax(ps), ps.shape)
print(max)

s = ps[max[0], :]
x = np.arange(len(s))
sg =savgol_filter(s, 17, 9, mode='constant')
# sg =savgol_filter(sg, 11, 7, mode='constant')

mean_s = np.mean(s)
mean_sg = np.mean(sg)



fig, ax = plt.subplots()
fig.set_size_inches(19, 10)
plt.subplot(2,1,1)
plt.plot(np.arange(len(s)), s)
plt.hlines(mean_s, 0, len(s))
plt.subplot(2,1,2)
plt.plot(np.arange(len(s)), sg)
plt.hlines(mean_sg, 0, len(s))
plt.tight_layout()
plt.show()