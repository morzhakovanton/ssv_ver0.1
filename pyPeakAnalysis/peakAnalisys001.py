import bpy
import bmesh
import numpy as np 
import random
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D


ps_filename = '/home/morzh/work/pyPeakAnalysis/powerSpectrum.txt'
ps = np.loadtxt(ps_filename, delimiter=',')

ind_max = np.unravel_index(np.argmax(ps, axis=None), ps.shape)
print('max value: ', np.max(ps), ' at ', ind_max)

#ps_fft = np.fft.fft2(ps)

bpy.ops.mesh.primitive_grid_add(x_subdivisions=ps.shape[0], y_subdivisions=ps.shape[1])

obj = bpy.context.scene.objects.active
ps = ps.ravel()

for idx in range(0, len(obj.data.vertices)):
    
    v = obj.data.vertices[idx] 
    v.co.z = 0.0001*ps[idx]