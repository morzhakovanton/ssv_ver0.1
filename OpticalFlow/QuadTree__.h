//
// Created by morzh on 16.04.19.
//

#ifndef SSV_VER_0_1_QUADTREE___H
#define SSV_VER_0_1_QUADTREE___H

///https://www.geeksforgeeks.org/quad-tree/

#include "../Common/ImageRectangle.h"

#include <iostream>
#include <cmath>

using namespace std;


struct Point
{
    Point(int _x, int _y): x(_x), y(_y){}
    Point(): x(0), y(0){}
    int x, y;
};


struct QuadTreeNode{

    QuadTreeNode( ImageRectangleI _rect) rect(_rect){}
    QuadTreeNode( Point _pos, int _data) : pos(_pos), data(_data){}

    ImageRectangleI rect;

    Point pos = Point(0,0);
    int data = 0;
};


/// The main quadtree class
class QuadTree__{

    /// Hold details of the boundary of this QuadTreeNode
    Point topLeft;
    Point botRight;

    // Contains details of QuadTreeNode
    QuadTreeNode *n;

    /// Children of this tree
    QuadTree__ *topLeftTree;
    QuadTree__ *topRightTree;
    QuadTree__ *botLeftTree;
    QuadTree__ *botRightTree;

public:
                    QuadTree__        ( );
                    QuadTree__        ( Point topL, Point botR);
    void            insert          ( QuadTreeNode*);
    QuadTreeNode*   search          ( Point);
    bool            inBoundary      ( Point);
};



// Insert a QuadTreeNode into the quadtree
void QuadTree__::insert(QuadTreeNode *QuadTreeNode)
{
    if (QuadTreeNode == NULL)
        return;

    // Current quad cannot contain it
    if (!inBoundary(QuadTreeNode->pos))
        return;

    // We are at a quad of unit calc_area
    // We cannot subdivide this quad further
    if (abs(topLeft.x - botRight.x) <= 1 &&
        abs(topLeft.y - botRight.y) <= 1)
    {
        if (n == NULL)
            n = QuadTreeNode;
        return;
    }

    if ((topLeft.x + botRight.x) / 2 >= QuadTreeNode->pos.x)
    {
        // Indicates topLeftTree
        if ((topLeft.y + botRight.y) / 2 >= QuadTreeNode->pos.y)
        {
            if (topLeftTree == NULL)
                topLeftTree = new QuadTree__( Point(topLeft.x, topLeft.y), Point((topLeft.x + botRight.x) / 2, (topLeft.y + botRight.y) / 2));

            topLeftTree->insert(QuadTreeNode);
        }

            // Indicates botLeftTree
        else
        {
            if (botLeftTree == NULL)
                botLeftTree = new QuadTree__(
                        Point(topLeft.x,
                              (topLeft.y + botRight.y) / 2),
                        Point((topLeft.x + botRight.x) / 2,
                              botRight.y));
            botLeftTree->insert(QuadTreeNode);
        }
    }
    else
    {
        // Indicates topRightTree
        if ((topLeft.y + botRight.y) / 2 >= QuadTreeNode->pos.y)
        {
            if (topRightTree == NULL)
                topRightTree = new QuadTree__(  Point((topLeft.x + botRight.x) / 2, topLeft.y), Point(botRight.x,  (topLeft.y + botRight.y) / 2));

            topRightTree->insert(QuadTreeNode);
        }

            // Indicates botRightTree
        else
        {
            if (botRightTree == NULL)
                botRightTree = new QuadTree__(
                        Point((topLeft.x + botRight.x) / 2,
                              (topLeft.y + botRight.y) / 2),
                        Point(botRight.x, botRight.y));
            botRightTree->insert(QuadTreeNode);
        }
    }
}

// Find a QuadTreeNode in a quadtree
QuadTreeNode* QuadTree__::search(Point p)
{
    // Current quad cannot contain it
    if (!inBoundary(p))
        return NULL;

    // We are at a quad of unit length
    // We cannot subdivide this quad further
    if (n != NULL)
        return n;

    if ((topLeft.x + botRight.x) / 2 >= p.x)
    {
        // Indicates topLeftTree
        if ((topLeft.y + botRight.y) / 2 >= p.y)
        {
            if (topLeftTree == NULL)
                return NULL;
            return topLeftTree->search(p);
        }

            // Indicates botLeftTree
        else
        {
            if (botLeftTree == NULL)
                return NULL;
            return botLeftTree->search(p);
        }
    }
    else
    {
        // Indicates topRightTree
        if ((topLeft.y + botRight.y) / 2 >= p.y)
        {
            if (topRightTree == NULL)
                return NULL;
            return topRightTree->search(p);
        }

            // Indicates botRightTree
        else
        {
            if (botRightTree == NULL)
                return NULL;
            return botRightTree->search(p);
        }
    }
};

// Check if current quadtree contains the point
bool QuadTree__::inBoundary(Point p)
{
    return (p.x >= topLeft.x &&
            p.x <= botRight.x &&
            p.y >= topLeft.y &&
            p.y <= botRight.y);
}

QuadTree__::QuadTree__() {
    topLeft = Point(0, 0);
    botRight = Point(0, 0);
    n = NULL;
    topLeftTree  = NULL;
    topRightTree = NULL;
    botLeftTree  = NULL;
    botRightTree = NULL;
}

QuadTree__::QuadTree__(Point topL, Point botR) {
    n = NULL;
    topLeftTree  = NULL;
    topRightTree = NULL;
    botLeftTree  = NULL;
    botRightTree = NULL;
    topLeft = topL;
    botRight = botR;
}

#endif //SSV_VER_0_1_QUADTREE___H
