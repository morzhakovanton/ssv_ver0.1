//
// Created by morzh on 15.04.19.
//

#ifndef SSV_VER_0_1_OPTICALFLOWPOC_H
#define SSV_VER_0_1_OPTICALFLOWPOC_H

#include    "../Common/ImageRectangle.h"
#include    "../SatellitePatches/PatchesStereoPair.h"
#include    "../Rectification/ImageRegistrationPOC.h"
#include    "../Rectification/ImageRegistrationPOC.h"

#include    "InterpolateFlow.h"
#include    "QuadTree.h"
#include    "OpticalFlowPOCViz.h"



struct OpticalFlowPOCOptions{

    int         min_block_res       = 64;
    int         min_block_area      = 4500;
    float       min_area_ratio      = 0.85;
};


class OpticalFlowPOC{
public:

    void                estimate_flow           ( PatchesStereoPair &pair, const OpticalFlowPOCOptions &options = OpticalFlowPOCOptions() );
    int                 calc_treeDepth          ( const ImageRectangleI &rectangle );
    void                get_imgFixed            (PatchesStereoPair &pair, Quad *quad, cv::Mat &img );
    void                get_imgMoved            ( const PatchesStereoPair &pair, Quad *quad, cv::Mat &img );
    void                get_imgFromRect(const cv::Mat &img_src, const ImageRectangleF &rect, cv::Mat &img_dst);
    Eigen::MatrixXf     get_centroids           ( int lvl );
    Eigen::MatrixXf     get_shifts              ( int lvl );


    int                             tree_depth;
    QuadTree                        tree;
    InterpolateFlow                 interpolate;
    ImageRegistrationPOC            imreg;
    cv::Mat                         flow;
    OpticalFlowPOCOptions           options;
    OpticalFlowPOCViz               viz;
};



void OpticalFlowPOC::estimate_flow(PatchesStereoPair &pair, const OpticalFlowPOCOptions &options) {

    ImageRectangleI                     rect_root_fixed( 0, 0, pair.patch1.imgPNT.cols, pair.patch1.imgPNT.rows );
//    ImageRectangleF                     rect_root_moved( 0, 0, pair.patch2.imgPNT.cols, pair.patch2.imgPNT.rows );
    ConvexTetragon                      tetragon_fixed = pair.patch1.working_area;
    ConvexTetragon                      tetragon_moved = pair.patch2.working_area;
    ImageRegistrationT2POC              imreg_T2;
    ImageRegistrationPOCOptions         options_translate;
    float                               conf=0;


    options_translate.use_Gabor_filter          = false;
    options_translate.calc_confidence           = false;
    options_translate.apply_highpass_filter     = false;
    options_translate.fft_window                = ImageRegistrationPOCOptions::FFTWindow::BLACKMAN;

    tree.root = new Quad();

    cv::Mat             img_fixed, img_moved;
    std::vector<Quad*>  quads = {tree.root};

    tree_depth  = calc_treeDepth(rect_root_fixed);
    tree.root->rect = rect_root_fixed;


    for ( int lvl=0; lvl < tree_depth; ++lvl){
        for (auto quad : quads) {

            get_imgFixed( pair, quad, img_fixed );
            get_imgMoved( pair, quad, img_moved );

            Eigen::Vector2d t = imreg_T2.estimate_translation(img_fixed, img_moved, options_translate, &conf);
            quad->shift = -t;
//            viz.save_image('/home/morzh/Pictures/quad0_lvl0.tiff');
            quad->add_leafs(tetragon_fixed, options.min_area_ratio, options.min_block_area);
        }

        quads = tree.get_childrenActive(quads);
    }

    interpolate.calc_flowData( get_centroids(tree_depth), get_shifts(tree_depth) );
    interpolate.estimate_flow(flow);
}


int OpticalFlowPOC::calc_treeDepth(const ImageRectangleI &rectangle) {

    bool    break_flag  = true;
    int     width       = rectangle.width;
    int     height      = rectangle.height;
    int     depth       = 1;

    while (break_flag){

        width   = width  / 2;
        height  = height / 2;

        if ( width < options.min_block_res || height < options.min_block_res)
            break_flag = false;
        else
            depth += 1;
    }

    return depth;
}

void OpticalFlowPOC::get_imgFixed(PatchesStereoPair &pair, Quad *quad, cv::Mat &img) {

    pair.patch1.imgPNT(quad->rect.get_OpencV()).copyTo(img);

}

void OpticalFlowPOC::get_imgMoved(const PatchesStereoPair &pair, Quad *quad, cv::Mat &img) {

    bool                break_flag  =   true;
    Eigen::Vector2d     shift_aggr  =   Eigen::Vector2d(0,0);
    ImageRectangleF     img_rect;
    Quad*               parent_quad = nullptr;



    while (break_flag){

        parent_quad = quad->parent;

        if ( parent_quad == nullptr)

            break_flag = false;
        else
            shift_aggr += parent_quad->shift;
    }


    img_rect = quad->rect;
    img_rect.shiftBy(shift_aggr.cast<float>());

    get_imgFromRect(pair.patch2.imgPNT, img_rect, img);

}


void OpticalFlowPOC::get_imgFromRect(const cv::Mat &img_src, const ImageRectangleF &rect, cv::Mat &img_dst) {

    cv::Mat  H1_cv = cv::Mat::eye(2,3, CV_64F);

    H1_cv.at<double>(0,2) = rect.x;
    H1_cv.at<double>(1,2) = rect.y;


    cv::warpAffine( img_src, img_dst, H1_cv, cv::Size( std::round(rect.width), std::round(rect.height)), cv::INTER_CUBIC );
}


Eigen::MatrixXf OpticalFlowPOC::get_centroids(int lvl) {

    std::vector<Quad*>      quads;
    int                     centroids_num;
    Eigen::MatrixXf         centroids;

    quads           =   tree.get_quadsAll(lvl);
    centroids_num   =   quads.size();

    centroids.resize(2,centroids_num);

    for( int idx=0; idx < centroids_num; ++idx ){

        Eigen::Vector2i center = quads[idx]->rect.get_center();
        centroids.col(idx) = center.cast<float>();
    }

    return centroids.transpose();
}


Eigen::MatrixXf OpticalFlowPOC::get_shifts(int lvl) {

    std::vector<Quad*>      quads        =   tree.get_quadsAll(lvl);
    int                     shifts_num   =    quads.size();
    Eigen::MatrixXf         shifts;

    shifts.resize(2,shifts_num);

    for( int idx=0; idx < shifts_num; ++idx )

        shifts.col(idx) = quads[idx]->shift.cast<float>();


    return shifts.transpose();
}


#endif //SSV_VER_0_1_OPTICALFLOWPOC_H
