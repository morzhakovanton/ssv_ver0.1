
#ifndef SSV_VER_0_1_QUADTREE_H
#define SSV_VER_0_1_QUADTREE_H


#include <vector>
#include "Common/ImageRectangle.h"
#include "Common/ConvexTetragon.h"


class Quad{
public:

    void add_leafs(const ConvexTetragon &tetragon, double area_ratio_threshold, double area_min);

    std::vector<Quad *>         get_leafsNonNull        (  );
    std::vector<Quad *>         get_leafsActive         (  );
    std::vector<Quad *>         get_leafsAll            (  );

    bool                        check_areLeafsNull      (  );
    void                        order_rectangles        ( const std::vector<ImageRectangleI> &rects_src, std::vector<ImageRectangleI> rects_dst );

    std::array<Quad*, 4>        leafs;
    ImageRectangleI             rect                =   ImageRectangleI();
    Eigen::Vector2d             shift               =   Eigen::Vector2d(0,0);
    float                       shift_confidence    =   0;
    Quad*                       parent              =   nullptr;
    bool                        isActive            =   false;

private:
    template <typename T>
    std::vector<size_t> sort_indexes(const std::vector<T> &v) const;
};



class QuadTree{
public:

    std::vector<Quad*>      get_quadsNonNull        ( int lvl);
    std::vector<Quad*>      get_quadsActive         ( int lvl);
    std::vector<Quad*>      get_quadsAll            ( int lvl);

    std::vector<Quad*>      get_childrenNonNull     ( std::vector<Quad *> quads);
    std::vector<Quad*>      get_childrenActive      ( std::vector<Quad *> quads);
    std::vector<Quad*>      get_childrenAll         ( std::vector<Quad *> quads);

    std::vector<Quad*>      concatenate_vectors     ( std::vector<std::vector<Quad *>> vector) const;

    Quad* root = nullptr;
};

void Quad::add_leafs(const ConvexTetragon &tetragon, double area_ratio_threshold, double area_min) {
    /// leafs order: top_left, top_right, bottom_left, bottom_right (row major oreder)


    std::vector<ImageRectangleI>    leafs_rects = this->rect.subdivide(2);
    std::vector<ImageRectangleI>    ordered_rects;

    order_rectangles(leafs_rects, ordered_rects);


    for (int idx = 0; idx < leafs_rects.size(); ++idx) {

        double intersect_area   = tetragon.intersect_area(leafs_rects[idx]);
        double area_ratio     = intersect_area / leafs_rects[idx].calc_area();

        leafs[idx] = new Quad();

        if ( area_ratio >  area_ratio_threshold || intersect_area > area_min ) {

            leafs[idx]->rect        = ordered_rects[idx];
            leafs[idx]->isActive    = true;
        } else
            leafs[idx]->isActive = false;
    }


    for ( int idx=0; idx<ordered_rects.size(); ++idx ){


        leafs[idx]->rect = ordered_rects[idx];
    }
}


void Quad::order_rectangles(const std::vector<ImageRectangleI> &rects_src, std::vector<ImageRectangleI> rects_dst) {

    /**
     * распихиваем прямоугольники в зависимости от близости углов соответствующим углам  родительского прямоугольника. 4х угольников должно быть ЧЕТЫРЕ
     * */

    if ( rects_src.empty() || rects_src.size() != 4)
        return;

    int                     rects_num       = rects_src.size();
    std::vector<float>      dists_ul,       dists_ur,       dists_bl,       dists_br;
    std::vector<size_t >    dists_ul_idx,   dists_ur_idx,   dists_bl_idx,   dists_br_idx;



    for (int idx = 0; idx < rects_num; ++idx) {

        dists_ul.push_back( (rects_dst[idx].get_ul() - parent->rect.get_ul()).norm() );
        dists_ur.push_back( (rects_dst[idx].get_ur() - parent->rect.get_ur()).norm() );

        dists_bl.push_back( (rects_dst[idx].get_bl() - parent->rect.get_bl()).norm() );
        dists_br.push_back( (rects_dst[idx].get_br() - parent->rect.get_br()).norm() );
    }

    // should be debugged !!!!
    dists_ul_idx = sort_indexes(dists_ul);
    dists_ur_idx = sort_indexes(dists_ur);
    dists_bl_idx = sort_indexes(dists_bl);
    dists_br_idx = sort_indexes(dists_br);


    leafs[0]->rect = rects_src[dists_ul_idx[0]];
    leafs[1]->rect = rects_src[dists_ur_idx[0]];
    leafs[2]->rect = rects_src[dists_bl_idx[0]];
    leafs[3]->rect = rects_src[dists_br_idx[0]];


    for ( int idx=0; idx<4; ++idx){

        if (leafs[idx]->rect.check_isEmpty())
            std::cerr << "Quad::order_rectangles()::Rectangles are not ordered correctly " << std::endl;
    }
}

std::vector<Quad *> Quad::get_leafsNonNull() {

    std::vector<Quad *> leafs_;

    for (auto leaf : this->leafs){

        if (leaf != nullptr)

            leafs_.push_back(leaf);
    }

    return leafs_;
}

bool Quad::check_areLeafsNull() {

    return (leafs[0] == nullptr) && (leafs[1] == nullptr) && (leafs[2] == nullptr) && (leafs[3] == nullptr);
}

template<typename T>
std::vector<size_t> Quad::sort_indexes(const std::vector<T> &v) const {

    // initialize original index locations
    std::vector<size_t> idx(v.size());
    iota(idx.begin(), idx.end(), 0);

    // sort indexes based on comparing values in v
    sort(idx.begin(), idx.end(),
         [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

    return idx;
}


std::vector<Quad *> Quad::get_leafsActive() {

    std::vector<Quad *> leafs_;

    for (auto leaf : this->leafs){

        if (leaf->isActive )

            leafs_.push_back(leaf);
    }

    return leafs_;
}

std::vector<Quad *> Quad::get_leafsAll() {

    std::vector<Quad *> leafs_;

    for (auto leaf : this->leafs)

            leafs_.push_back(leaf);

    return leafs_;
}

std::vector<Quad *> QuadTree::get_quadsAll(int lvl) {

    std::vector<Quad*> leafs = {root};


    for (int idx = 0; idx < lvl; ++idx) {

        leafs = get_childrenAll(leafs);
    }

    return leafs;
}


std::vector<Quad*> QuadTree::get_quadsNonNull(int lvl) {

    std::vector<Quad*> leafs = {root};


    for (int idx = 0; idx < lvl; ++idx) {

        leafs = get_childrenNonNull(leafs);
    }

    return leafs;
}



std::vector<Quad *> QuadTree::get_quadsActive(int lvl) {

    std::vector<Quad*> leafs = {root};


    for (int idx = 0; idx < lvl; ++idx) {

        leafs = get_childrenActive(leafs);
    }

    return leafs;
}


std::vector<Quad*> QuadTree::concatenate_vectors(std::vector<std::vector<Quad*>> vector) const {

    int total_num = 0;

    for (int idx = 0; idx < vector.size(); ++idx)
        total_num += vector[idx].size();



    std::vector<Quad*> out(total_num);

    for (int idx = 0; idx < vector.size(); ++idx)

        out.insert( out.end(), vector[idx].begin(), vector[idx].end() );


    return out;
}

std::vector<Quad*> QuadTree::get_childrenNonNull(std::vector<Quad *> quads) {

    int                                 leafs_num  = quads.size();
    std::vector<std::vector<Quad*>>     vleafs(leafs_num);


    for (int idx=0; idx < quads.size(); ++idx){

        vleafs[idx] = quads[idx]->get_leafsNonNull();
    }

    return concatenate_vectors(vleafs);

}

std::vector<Quad *> QuadTree::get_childrenActive(std::vector<Quad *> quads) {

    int                                 leafs_num  = quads.size();
    std::vector<std::vector<Quad*>>     vleafs(leafs_num);


    for (int idx=0; idx < quads.size(); ++idx){

        vleafs[idx] = quads[idx]->get_leafsActive();
    }

    return concatenate_vectors(vleafs);
}


std::vector<Quad *> QuadTree::get_childrenAll(std::vector<Quad *> quads) {

    int                                 leafs_num  = quads.size();
    std::vector<std::vector<Quad*>>     vleafs(leafs_num);


    for (int idx=0; idx < quads.size(); ++idx){

        vleafs[idx] = quads[idx]->get_leafsAll();
    }

    return concatenate_vectors(vleafs);
}


#endif //SSV_VER_0_1_QUADTREE_H
