//
// Created by morzh on 15.04.19.
//

#ifndef SSV_VER_0_1_INTERPOLATEFLOW_H
#define SSV_VER_0_1_INTERPOLATEFLOW_H


#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>

#ifndef PI
#define PI 3.14159265
#endif


class InterpolateFlow{
public:

    void                    estimate_flow       (cv::Mat& flow );
    void                    calc_flowData(const Eigen::MatrixXf &centroids, const Eigen::MatrixXf &velocity);
    Eigen::Vector2f calc_flowAtPoint(Eigen::Vector2f &r);


    Eigen::MatrixXf         G1, beta, centroids;//, V1;
    float                   lmbda =1e-4;
    float                   sigma =3.5;
};

void InterpolateFlow::calc_flowData(const Eigen::MatrixXf &centroids, const Eigen::MatrixXf &velocity) {

    Eigen::MatrixXf         G, lambda_kronecker;
    bool                    debug           = false;
    int                     num_centroids   =   (int)centroids.rows();

    this->centroids = centroids;

    G.resize(num_centroids, num_centroids);
    G.setZero();

    lambda_kronecker.resize(num_centroids, num_centroids);
    lambda_kronecker.setZero();

    for( int i=0; i< num_centroids; ++i){
        for( int j=0; j<num_centroids; ++j){

            Eigen::Vector2f c_i = centroids.row(i);
            Eigen::Vector2f c_j = centroids.row(j);

            G(i,j) =  1.0 / (2*PI*std::pow(sigma,2))   *   std::exp( (-1.0*(c_i-c_j).norm() ) / (2*std::pow(sigma, 2)) );

            if (i == j)
                lambda_kronecker(i,j) = lmbda;

        }
    }

    G1.resize(num_centroids, num_centroids);

    G1 = G + lambda_kronecker;
//    V1 = lms_vel;
    beta =  G1.inverse()* velocity;

}


Eigen::Vector2f InterpolateFlow::calc_flowAtPoint(Eigen::Vector2f &r) {

    /// ex. denseopticalflow (self, r, sigma, beta, centroids)

    Eigen::Vector2f v_r;

    v_r.setZero();

    for( int i=0; i<  beta.rows(); ++i){

        Eigen::Vector2f c_i  = centroids.row(i);

        v_r += beta.row(i) / (2*PI* std::pow(sigma,2))   *  std::exp(( -1.0*(r - c_i).norm()) / (2*std::pow(sigma,2) ) );
    }

    return  v_r;
}


void InterpolateFlow::estimate_flow(cv::Mat& flow ) {

    if (flow.empty() ){
        std::cerr << "InterpolateFlow::estimate_flow(cv::Mat& flow ) => input matrix (cv::Mat flow) is empty" << std::endl;
        return;
    }

    flow.convertTo(flow, CV_32FC2);

    for (int col = 0; col < flow.cols; ++col) {
        for (int row = 0; row < flow.rows; ++row) {

            Eigen::Vector2f     pt   = Eigen::Vector2f(col, row);
            Eigen::Vector2f     vel  = calc_flowAtPoint(pt);

            flow.at<cv::Vec2f>(row, col) = cv::Vec2f(vel(0), vel(1));
        }
    }
}

#endif //SSV_VER_0_1_INTERPOLATEFLOW_H
