//
// Created by morzh on 04.01.18.
//

#ifndef SSV_VER_0_1_AFFINERECTIFICATIONRPCVIZ_H
#define SSV_VER_0_1_AFFINERECTIFICATIONRPCVIZ_H


#include "SatellitePatches/PatchesStereoPair.h"
#include "AffineRectificationRPCTemp.h"
#include "../Common/TiffWriter.h"

class AffineRectificationRPCViz {
public:

    AffineRectificationRPCViz(AffineRectificationRPCTemp *p_data);

    void             print_regularGeoGrid                   ( );
    void             print_rectificationMatrices            ( );

    void             show_AffineCamEpipolarGeo              ( const PatchesStereoPair &pair);
//    void           show_srcImagesCornersCorrespondeces ( const Frame &kF, const Frame &cF, const cv::Mat& croppedL_src, const cv::Mat& croppedR_src );

    void             show_projRegGridOnPatches              ( const PatchesStereoPair &pair, bool draw_dims_rect = false);
    void             print_AffineCamEpipolarConstraintInfo  ( bool output_all = false);

    void             scale_imageIntensity                   ( const cv::Mat &in_image, cv::Mat &out_image);
    void            show_imagesTransfomed                   ( const PatchesStereoPair &pair, bool show_proj_pts = false, bool show_tetragons = false);

    AffineRectificationRPCTemp* p_data;

    Eigen::IOFormat         HeavyFmt;
    Eigen::IOFormat         OctaveFmt;

    void write_imagesTransformed(const PatchesStereoPair &pair, std::string filename);
};



AffineRectificationRPCViz::AffineRectificationRPCViz(AffineRectificationRPCTemp *p_data) :
        p_data(p_data), HeavyFmt( Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]"),
        OctaveFmt(Eigen::StreamPrecision, 0, ", ", ";\n", "", "", "[", "]") {

}


void AffineRectificationRPCViz::show_projRegGridOnPatches(const PatchesStereoPair &pair, bool draw_dims_rect) {


/*
    cv::Mat im18UC1, im28UC1;
    cv::Mat im18UC3, im28UC3;

    scale_imageIntensity( pair.patch1.imgPN, im18UC1);
    scale_imageIntensity( pair.patch2.imgPN, im28UC1);

    cv::cvtColor(im18UC1,im18UC3, CV_GRAY2RGB);
    cv::cvtColor(im28UC1,im28UC3, CV_GRAY2RGB);
*/


    cv::Mat im1, im2;


    scale_imageIntensity(pair.patch1.imgPN, im1);
    scale_imageIntensity(pair.patch2.imgPN, im2);

    cv::Scalar      white   =   cv::Scalar(255,255,255);
    cv::Scalar      black   =   cv::Scalar(0,0,0);
    cv::Scalar      red     =   cv::Scalar(255,0,0);
    cv::Scalar      green   =   cv::Scalar(0,255,0);
    float           h_step  =   (float) 1/(p_data->num_grid_points(2));

    for (int idx = 0; idx < p_data->patch1_proj_coords.cols(); ++idx) {

        Eigen::Vector2d px1 = p_data->patch1_proj_coords.col(idx);
        Eigen::Vector2d px2 = p_data->patch2_proj_coords.col(idx);

/*
        Eigen::Vector2d px1 = p_data->image1_proj_coords.col(idx) -pair.patch1.rect.get_ul().cast<double>();
        Eigen::Vector2d px2 = p_data->image2_proj_coords.col(idx) -pair.patch2.rect.get_ul().cast<double>();
*/

        float lambda = idx*h_step - std::floor(idx*h_step);
        cv::Scalar color =  lambda*white + (1-lambda)*black;

        cv::circle( im1, cv::Point( std::round(px1(0)), std::round(px1(1)) ),  2,  white, 2 );
        cv::circle( im2, cv::Point( std::round(px2(0)), std::round(px2(1)) ),  2,  white, 2 );
    }

/*
    cv::Point dims = cv::Point( p_data->image_cropped_dimensions(0)-1, p_data->image_cropped_dimensions(1)-1 );

    if ( draw_dims_rect){

        cv::rectangle( im1, cv::Point(0,0) , dims, white, 1 );
        cv::rectangle( im2, cv::Point(0,0) , dims, white, 1 );
    }
*/

    cv::imshow("Projected Geo Grid on Image 1", im1);
    cv::imshow("Projected Geo Grid on Image 2", im2);
    cv::waitKey(-1);


}

void AffineRectificationRPCViz::scale_imageIntensity(const cv::Mat &in_image, cv::Mat &out_image) {

    double min, max;

    cv::minMaxIdx(in_image, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(in_image-min, out_image, 255 / ( max-min) );

}


void AffineRectificationRPCViz::print_regularGeoGrid() {



    int         height_num_elements     =   p_data->regular_geoGrid.size();
    int         planar_num_elements     =   p_data->regular_geoGrid[0].cols();
    int         overall_num_pts         =   height_num_elements * planar_num_elements;
    int         idx_proj                =   0;


    for (int h = 0; h < height_num_elements; ++h) {

        for (int idx = 0; idx < planar_num_elements; ++idx) {

            Eigen::Vector3d  ground_pt              = p_data->regular_geoGrid[h].col(idx);


            std::cout << ground_pt  << " " ;

        }

        std::cout << std::endl;
    }

}

void AffineRectificationRPCViz::print_AffineCamEpipolarConstraintInfo(bool output_all) {

    Eigen::Vector2d         t1, t2;
    int                     cols = p_data->patch1_proj_coords.cols();
    double                  error, total_error(0);


    if (output_all )
        std::cout << "individual errors: "<< std::endl;


    for (int j = 0; j < cols; ++j) {

        t1 = p_data->patch1_proj_coords.col(j);
        t2 = p_data->patch2_proj_coords.col(j);

        Eigen::Vector3d pix1_, pix2_;

        pix1_ << t1, 1;
        pix2_ << t2, 1;


        error = pix2_.transpose() * p_data->AffineFM * pix1_;

        total_error += std::abs(error);

        if (output_all )
            std::cout << error << std::endl;


    }


    std::cout << "total error for " << cols << " points is: " << total_error << std::endl;
    std::cout << "average error for each  point is: " << total_error/(double)cols << std::endl;
    std::cout << "estimated affine matrix: \n" << p_data->AffineFM << std::endl;

}

void AffineRectificationRPCViz::show_AffineCamEpipolarGeo(const PatchesStereoPair &pair) {

    cv::Mat im18UC1, im28UC1;
    cv::Mat im18UC3, im28UC3;

    scale_imageIntensity( pair.patch1.imgPN, im18UC1);
    scale_imageIntensity( pair.patch2.imgPN, im28UC1);

    cv::cvtColor(im18UC1,im18UC3, CV_GRAY2RGB);
    cv::cvtColor(im28UC1,im28UC3, CV_GRAY2RGB);

    cv::RNG rng(12345);


    for (int idx = 0; idx < p_data->patch1_proj_coords.cols(); ++idx) {

        Eigen::Vector2d        px1     = p_data->patch1_proj_coords.col(idx);
        Eigen::Vector2d        px2     = p_data->patch2_proj_coords.col(idx);
        Eigen::Vector3d        px1_homo, px2_homo, im1_line, im2_line;
        Eigen::Vector2d        norm_vec, collinear_vec, start, end;
        double                  norm, d;

        px1_homo << px1, 1;  px2_homo << px2, 1;

        im1_line = ( p_data->AffineFM.transpose() * px2_homo ).transpose();
        im2_line = ( p_data->AffineFM             * px1_homo ).transpose();

//        double check_isEmpty =  (px2_homo.transpose() ) * AffineFM * px1_homo;
        cv::Scalar      color   = cv::Scalar(rng.uniform(80,255), rng.uniform(80, 255), rng.uniform(80, 255));
//        cv::Scalar      color   = cv::Scalar(0, 255, 0);

        cv::Point2i segm_start, segm_end;


        cv::circle( im18UC3, cv::Point( px1(0), px1(1)),  2,  color, 2 );
        cv::circle( im28UC3, cv::Point( px2(0), px2(1)),  2,  color, 2 );


/*
        cv::Point im2_line_pt1 =  cv::Point( 0                                     , std::round( - im2_line(2)/im2_line(1))  );
        cv::Point im2_line_pt2 =  cv::Point( std::round( - im2_line(2)/im2_line(0)), 0                                       );

        cv::Point im1_line_pt1 =  cv::Point( 0                                     , std::round( - im1_line(2)/im1_line(1))  );
        cv::Point im1_line_pt2 =  cv::Point( std::round( - im1_line(2)/im1_line(0)), 0                                       );
        cv::clipLine( cv::Size(  im1.cols, im1.rows ), segm_start, segm_end);
*/

        cv::line   (im18UC3,   cv::Point( 0, std::round( - im1_line(2)/im1_line(1))) , cv::Point( std::round( - im1_line(2)/im1_line(0)), 0) , color);
        cv::line   (im28UC3,   cv::Point( 0, std::round( - im2_line(2)/im2_line(1))) , cv::Point( std::round( - im2_line(2)/im2_line(0)), 0) , color );
    }


    cv::imshow( "First image" , im18UC3 );
    cv::imshow( "Second image", im28UC3 );

    cv::waitKey(-1);


}


void AffineRectificationRPCViz::print_rectificationMatrices() {

    std::cout << "------------------------------------ Rectification matrices ------------" << std::endl;
    std::cout << p_data->H1 << std::endl;
    std::cout << std::endl;
    std::cout << p_data->H2 << std::endl;

    std::cout << "-----------------------------------------------------------------------" << std::endl;

}

void AffineRectificationRPCViz::show_imagesTransfomed(const PatchesStereoPair &pair, bool show_proj_pts, bool show_tetragons) {


    cv::Mat im1, im2;

    cv::Scalar      white   =   cv::Scalar(255,255,255);
    cv::Scalar      black   =   cv::Scalar(0,0,0);
    cv::Scalar      red     =   cv::Scalar(255,0,0);
    cv::Scalar      green   =   cv::Scalar(0,255,0);


    scale_imageIntensity(pair.patch1.imgPNT, im1);
    scale_imageIntensity(pair.patch2.imgPNT, im2);

    cv::cvtColor(im1,im1, CV_GRAY2RGB);
    cv::cvtColor(im2,im2, CV_GRAY2RGB);

    if ( show_proj_pts &&  p_data->patch1_proj_coords_transformed.cols() != 0) {

        for (int idx = 0; idx < p_data->patch1_proj_coords_transformed.cols(); ++idx) {


            Eigen::Vector2d px1 = p_data->patch1_proj_coords_transformed.col(idx);
            Eigen::Vector2d px2 = p_data->patch2_proj_coords_transformed.col(idx);

            cv::circle(im1, cv::Point(px1(0), px1(1)), 2, white, 2);
            cv::circle(im2, cv::Point(px2(0), px2(1)), 2, white, 2);
        }

    }


    if ( show_tetragons){

        cv::Scalar color_tetra = cv::Scalar(180, 90, 90);

        for (int idx = 0; idx < 4; ++idx) {

            cv::Point2f im1_pt1 = cv::Point2f( pair.patch1.working_area.verts[idx](0),        pair.patch1.working_area.verts[idx](1) );
            cv::Point2f im1_pt2 = cv::Point2f( pair.patch1.working_area.verts[(idx+1)%4](0),  pair.patch1.working_area.verts[(idx+1)%4](1) );

            cv::Point2f im2_pt1 = cv::Point2f( pair.patch2.working_area.verts[idx](0),        pair.patch2.working_area.verts[idx](1) );
            cv::Point2f im2_pt2 = cv::Point2f( pair.patch2.working_area.verts[(idx+1)%4](0),  pair.patch2.working_area.verts[(idx+1)%4](1) );

            cv::line(im1, im1_pt1, im1_pt2, color_tetra, 2, CV_AA);
            cv::line(im2, im2_pt1, im2_pt2, color_tetra, 2, CV_AA);
        }
    }

    cv::imshow( "First  Transformed Image", im1 );
    cv::imshow( "Second Transformed Image", im2 );

    cv::waitKey(-1);

}

void AffineRectificationRPCViz::write_imagesTransformed(const PatchesStereoPair &pair, std::string filename) {

    TiffWriter writer;

    std::vector<cv::Mat>    imgs;
    cv::Mat                 img1, img2;

    scale_imageIntensity(pair.patch1.imgPNT, img1);
    scale_imageIntensity(pair.patch2.imgPNT, img2);


    imgs.push_back(img1.clone());
    imgs.push_back(img2.clone());

    writer.write(filename, imgs);
}


#endif //SSV_VER_0_1_AFFINERECTIFICATIONRPCVIZ_H
