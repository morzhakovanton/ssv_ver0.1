
#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONSIM2_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONSIM2_H


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <complex.h>
#include <cmath>
#include <iostream>

#include <Eigen/Core>
#include <fftw3.h>
#include "ImageRegistrationDebug.h"
#include "ImageRegistrationCoordinateFrames.h"
#include "ImageRegistrationT2POC.h"
#include "ImageRegistrationFourierMellin.h"

#include "SatellitePatches/PatchesStereoPair.h"



#define  PI 3.1415926535897932384626433832795f


class ImageRegistrationPOC{
public:

    void                align_T2                                    ( PatchesStereoPair &patches_pair);
    void                align_scales                                ( PatchesStereoPair &patches_pair);
    void                align_scaleRotation                         ( PatchesStereoPair &patches_pair);
    void                align_scalesT2                              ( PatchesStereoPair &patches_pair);
    void                align_SIM2                                  ( PatchesStereoPair &patches_pair);



    ImageRegistrationFourierMellin      Fourier_Mellin;
    ImageRegistrationT2POC              imreg_T2;
    ImageRegistrationDebug              debug;

};




void ImageRegistrationPOC::align_SIM2(PatchesStereoPair &patches_pair) {

    align_scaleRotation   ( patches_pair );
    align_T2              ( patches_pair );
}


void ImageRegistrationPOC::align_scaleRotation(PatchesStereoPair &patches_pair) {

    cv::Mat moved, fixed;
    Eigen::Matrix3d T;
    ImageRegistrationPOCOptions options_FourierMellin, options_logPolarShift;
    //// estimate scale
    patches_pair.patch1.get_smoothedBorderImage( fixed, options_FourierMellin.border_filter_size );
    patches_pair.patch2.get_smoothedBorderImage( moved, options_FourierMellin.border_filter_size );

    T = Fourier_Mellin.estimate_scaleRotation(fixed, moved, options_FourierMellin);

    patches_pair.patch2.update_transform( T );
    patches_pair.patch2.update_transformedImage( patches_pair.im12_rect);

}

void ImageRegistrationPOC::align_scales(PatchesStereoPair &patches_pair) {

    cv::Mat moved, fixed;
    Eigen::Matrix3d T;
    ImageRegistrationPOCOptions options_FourierMellin, options_logPolarShift;
    std::vector<ImageRegistrationPOCOptions> options_vector(2);

    /// options for scales
    options_FourierMellin.use_Gabor_filter = true;
    options_FourierMellin.calc_confidence = false;
    options_FourierMellin.apply_highpass_filter = true;
//    options_FourierMellin.fft_window = ImageRegistrationPOCOptions::FFTWindow::NONE;

    /// options for 2D shift in loglog frame
    options_logPolarShift.use_Gabor_filter = false;
    options_logPolarShift.calc_confidence = false;
    options_logPolarShift.apply_highpass_filter = false;
//    options_logPolarShift.fft_window = ImageRegistrationPOCOptions::FFTWindow::NONE;

    options_vector[0] = options_FourierMellin;
    options_vector[1] = options_logPolarShift;

    /// smooth  sattelite patch border
    patches_pair.patch1.get_smoothedBorderImage( fixed, options_FourierMellin.border_filter_size );
    patches_pair.patch2.get_smoothedBorderImage( moved, options_FourierMellin.border_filter_size );

    T = Fourier_Mellin.estimate_scales(fixed, moved, options_vector);

//    T(1,1) = 0.999;
//    T(1,1) = 1.0;


    std::cout << "scale matrix:" << std::endl;
    std::cout << T << std::endl;

    patches_pair.patch2.update_transform( T );
    patches_pair.patch2.update_transformedImage( patches_pair.im12_rect);

}

void ImageRegistrationPOC::align_T2(PatchesStereoPair &patches_pair) {

    cv::Mat                     moved, fixed;
    Eigen::Vector2d             t;
    Eigen::Matrix3d             T;
    ImageRegistrationPOCOptions options_translate;

    options_translate.use_Gabor_filter = false;
    options_translate.calc_confidence = false;
    options_translate.apply_highpass_filter = false;
    options_translate.fft_window = ImageRegistrationPOCOptions::FFTWindow::BLACKMAN;

    patches_pair.patch1.get_smoothedBorderImage(fixed, options_translate.border_filter_size);
    patches_pair.patch2.get_smoothedBorderImage(moved, options_translate.border_filter_size);


    t = imreg_T2.estimate_translation(fixed, moved, options_translate, nullptr);
    T = imreg_T2.make_transformFromTranslate(-t);
//    std::cout << "POC Translate: " << std::endl;
//    std::cout << T << std::endl;

    patches_pair.patch2.update_transform( T );
    patches_pair.patch2.update_transformedImage( patches_pair.im12_rect);
}


void ImageRegistrationPOC::align_scalesT2(PatchesStereoPair &patches_pair) {

    /**
     * 1) оценивается непропорциональное масштабирование  изображениий относительно друг друга с помощью спектра Фурье-Мелина и применяется к patch2
     * 2) оценивается сдвиг изображениий относительно друг друга после шага 1) и применяется к patch2
     * */

    align_scales          ( patches_pair );
    align_T2              ( patches_pair );
}





#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONSIM2_H
