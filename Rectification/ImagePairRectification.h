#include "SatelliteImagePair/SatelliteImageRPC.h"
#include "Rectification/ImageRegistrationPOC.h"
#include "Rectification/AffineRectificationRPC.h"
#include "PatchesStereo/PatchesStereoPair.h"

/// class ImagePairRectification - 
class ImagePairRectification {
public:

    rectify (PatchesStereoPair stereo_pair&);

    ImageRegistrationPOC        img_ registration;
    AffineRectificationRPC           img_rpc_align;
    SatelliteImageRPC           rpc;

private:

  rectifyUsingRPC (PatchesStereoPair stereo_pair&);
  rectifyUsingImReg (PatchesStereoPair stereo_pair&);

};

