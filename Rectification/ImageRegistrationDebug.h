//
// Created by morzh on 7/1/17.
//

#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONDEBUG_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONDEBUG_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>

#include <complex.h>
#include <fftw3.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>



class ImageRegistrationDebug{

public:

    void        print_fftwf_complex_array_1d            ( fftwf_complex* arr, int size1, int size2 );
    void        print_fftwf_complex_array_2d            ( fftwf_complex* arr, int num_rows, int num_cols );
    void        print_fftwf_complex_array_r2c_2d        ( fftwf_complex* arr, int num_rows, int num_cols );

    void        print_complexf_array_2d                 ( const std::complex<float> *arr, int num_rows, int num_cols);
    void        print_float_array_2d                    ( float *const arr, int num_rows, int num_cols) const;
    void        print_cvMat                             ( const cv::Mat mat);

    void        show_imageScaled                        ( std::string img_name, cv::Mat& img);
    void        show_image                              ( std::string img_name, cv::Mat& img);

    void        write_MatlabDLM                         ( std::string filename, cv::Mat mat);
    void        write_MatlabDLM                         ( std::string filename, float* data, Eigen::Vector2i size);

    void        write_PythonNumpy                       ( std::string filename, float* data, Eigen::Vector2i size) const;
    void        write_python                            ( std::string filename, const cv::Mat &);
};


void ImageRegistrationDebug::print_fftwf_complex_array_2d(fftwf_complex *arr, int num_rows, int num_cols) {

    for (int idx1 = 0; idx1 < num_rows; ++idx1) {
        for (int idx2 = 0; idx2 < num_cols; ++idx2) {

            float* p = (float*)  ( arr + num_rows*idx1 + idx2 );
            std::cout << p[0] <<  std::showpos << p[1] << "i, " ;

        }

        std::cout << std::endl;
    }

}

void ImageRegistrationDebug::print_cvMat(const cv::Mat mat) {

    std::cout << mat << std::endl;

}

void ImageRegistrationDebug::print_fftwf_complex_array_r2c_2d(fftwf_complex *arr, int num_rows, int num_cols) {

    int k=0;

    for (int idx = 0; idx < num_rows*( 0.5*num_cols+1 ); ++idx) {

        float* p = (float*) ( arr + idx );
        std::cout << p[0] <<  std::showpos << p[1] << "i, " ;

        k++;

        if (k == 17){
            k=0;
            std::cout << std::endl;
        }
    }
}

void ImageRegistrationDebug::print_complexf_array_2d(const std::complex<float> *arr, int num_rows, int num_cols) {

    std::complex<float> number;

    std::cout << "[" ;

    for (int idx1 = 0; idx1 < num_rows; ++idx1) {
        for (int idx2 = 0; idx2 < num_cols; ++idx2) {

            number = arr[num_rows*idx1 + idx2];

//            if ( std::abs(number.real() ) > 1e-9  )
                std::cout <<  std::showpos << number.real() << " ";
            if ( std::abs(number.imag() ) > 1e-9  )
                std::cout << number.imag() << "i ";

        }
        if ( idx1 != num_rows-1 )   {   std::cout << ";" << std::endl;   }
        else                        {   std::cout << "];" << std::endl; }
    }
}


void ImageRegistrationDebug::print_float_array_2d(float *const arr, int num_rows, int num_cols) const  {

    std::complex<float> number;

    std::cout << "[" ;

    for (int idx1 = 0; idx1 < num_rows; ++idx1) {
        for (int idx2 = 0; idx2 < num_cols; ++idx2) {


            std::cout <<  std::setprecision(10) << arr[num_cols*idx1 + idx2]  << " ";

        }

        if ( idx1 != num_rows-1 ) {    std::cout << ";" << std::endl; }
    }

    std::cout << "];" << std::endl;
}

void ImageRegistrationDebug::show_imageScaled(std::string img_name, cv::Mat &img ) {

    double min, max;

    cv::minMaxIdx(img, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(img-min, adjMap, 255 / ( max-min) );

    cv::imshow(img_name, adjMap);
    cv::waitKey(-1);

}

void ImageRegistrationDebug::show_image(std::string img_name, cv::Mat &img) {

    cv::imshow(img_name, img);

}

void ImageRegistrationDebug::write_MatlabDLM(std::string filename, cv::Mat mat) {

    std::ofstream out(filename);

    for (       int row = 0; row < mat.rows; ++row) {
        for (   int col = 0; col < mat.cols; ++col) {

                out << mat.at<float>(row, col);

                if ( col != mat.cols-1)

                    out << ", ";

        }

        out << std::endl;
    }

    out.close();
}

void ImageRegistrationDebug::write_MatlabDLM(std::string filename, float *data, Eigen::Vector2i size) {

    /// size is in X-Y coordinates
    std::ofstream out(filename);

    int rows = size(1);
    int cols = size(0);

    for (       int row = 0; row < rows; ++row) {
        for (   int col = 0; col < cols; ++col) {

            out << data[row*cols + col];

            if ( col != cols-1)

                out << ", ";

        }

        out << std::endl;
    }

    out.close();
}

void ImageRegistrationDebug::write_PythonNumpy(std::string filename, float *data, Eigen::Vector2i size) const {

    /// size is in X-Y coordinates
    std::ofstream out(filename);

    int rows = size(1);
    int cols = size(0);

    for (       int row = 0; row < rows; ++row) {
        for (   int col = 0; col < cols; ++col) {

            out << data[row*cols + col];

            if ( col != cols-1)

                out << ", ";

        }

        out << std::endl;
    }

    out << std::endl;

    out.close();

}



void ImageRegistrationDebug::write_python(std::string filename, const cv::Mat &mat) {

    std::ofstream out(filename);

    for (       int row = 0; row < mat.rows; ++row) {
        for (   int col = 0; col < mat.cols; ++col) {

            out << mat.at<float>(row, col);

            if ( col != mat.cols-1)

                out << ", ";

        }

        out << std::endl;
    }

    out << std::endl;

    out.close();
}


#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONDEBUG_H
