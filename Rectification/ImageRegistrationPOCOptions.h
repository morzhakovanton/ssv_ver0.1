//
// Created by morzh on 06.09.18.
//

#ifndef SSV_VER_0_1_IMAGEREGISTRATIONPOCOPTIONS_H
#define SSV_VER_0_1_IMAGEREGISTRATIONPOCOPTIONS_H

#include <algorithm>
#include <iostream>

#define  PI 3.1415926535897932384626433832795f


struct OptionsGaborFilter{

    float       sigma_x     =   12.0;
    float       sigma_y     =   12.0;
    float       theta       =   PI/4.0;
    float       frequency   =   4.0;
    float       bandwidth   =   1;
    int         n_stds      =   3;
    float       offset      =   0.0;


    void    print();

};

void OptionsGaborFilter::print(){

    std::cout << "\033[0;34mOptionsGaborFilter: \033[0m \n";

    std::cout << "\033[0;33m    Sigma X: \033[0m"                        << sigma_x << std::endl;
    std::cout << "\033[0;33m    Sigma Y: \033[0m"                        << sigma_y << std::endl;
    std::cout << "\033[0;33m    Rotation: \033[0m"                       << theta << " radians" << std::endl;
    std::cout << "\033[0;33m    Frequency: \033[0m"                      << frequency <<  std::endl;
    std::cout << "\033[0;33m    Bandwidth: \033[0m"                      << bandwidth <<  std::endl;
    std::cout << "\033[0;33m    Number of standard deviations: \033[0m"  << n_stds <<  std::endl;
    std::cout << "\033[0;33m    Offset: \033[0m"                         << offset <<  std::endl;
}



//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////



struct ImageRegistrationPOCOptions{

    int                     upsample_value = 128;

    bool                    use_Gabor_filter = true;
    OptionsGaborFilter      gabor_options;

    float                   tukey_alpha = 0.5;
    bool                    apply_highpass_filter = false;
    int                     border_filter_size = 20;

    bool                    calc_confidence = false;
    float                   confidence;

    enum class              FFTWindow       { NONE, TUKEY, BLACKMAN, HAMMING};
    enum class              FFTInterpolation   { UPSAMPLING, SIDESINTERP };

    FFTWindow               fft_window = FFTWindow::TUKEY;
    FFTInterpolation        fft_interpolation = FFTInterpolation::UPSAMPLING;

    void                    print();
    std::string             toString_FFTWindow(FFTWindow value) const;
    std::string             toString_FFTIntrerpolation(FFTInterpolation value) const;

};


std::string  ImageRegistrationPOCOptions::toString_FFTWindow(FFTWindow value) const {

    switch(value)
    {
        case FFTWindow ::NONE:
            return "NONE";
            break;
        case FFTWindow ::TUKEY:
            return "TUKEY";
            break;
        case FFTWindow ::BLACKMAN:
            return "BLACKMAN";
            break;
        case FFTWindow ::HAMMING:
            return "HAMMING";
            break;
        default:
            return "<unknown>";
    }

}



std::string  ImageRegistrationPOCOptions::toString_FFTIntrerpolation(FFTInterpolation value) const {

    switch(value)
    {
        case FFTInterpolation::UPSAMPLING:
            return "UPSAMPLING";
            break;
        case FFTInterpolation::SIDESINTERP:
            return "SIDESINTERP";
            break;
        default:
            return "<unknown>";
    }

}


void ImageRegistrationPOCOptions::print() {

    std::cout << "\033[1;34mImageRegistrationOptions: \033[0m \n";

    std::cout  << "\033[0;32mUpsample value:  \033[0m" << upsample_value << std::endl;
    std::cout << "\033[0;32mTukey window alpha value:  \033[0m" << tukey_alpha << std::endl;
    std::cout << "\033[0;32mApply highpass filter:  \033[0m" << apply_highpass_filter << std::endl;
    std::cout << "\033[0;32mBlur border size:  \033[0m" << border_filter_size << std::endl;

    std::cout << "\033[0;32mCalc confidence: \033[0m" << calc_confidence << std::endl;
    if (calc_confidence)    std::cout << "\033[0;32mConfidence value: \033[0m" << confidence << std::endl;

    std::cout << "\033[0;32muse Gabor Filter: \033[0m" << use_Gabor_filter<< std::endl;
    if (use_Gabor_filter)    gabor_options.print();

    if ( toString_FFTWindow(fft_window)  != "NONE") std::cout << "\033[0;32mFFT Window function: \033[0m" << toString_FFTWindow(fft_window) << std::endl;

    std::cout << "\033[0;32mFFT Interpolation: \033[0m" << toString_FFTIntrerpolation(fft_interpolation) << std::endl;
    std::cout << "------------------------------------------------" << std::endl;


/**
    color   foreground background
    black        30         40
    red          31         41
    green        32         42
    yellow       33         43
    blue         34         44
    magenta      35         45
    cyan         36         46
    white        37         47
*/

/**
    reset             0  (everything back to normal)
    bold/bright       1  (often a brighter shade of the same colour)
    underline         4
    inverse           7  (swap foreground and background colours)
    bold/bright off  21
    underline off    24
    inverse off      27
*/


}

#endif //SSV_VER_0_1_IMAGEREGISTRATIONPOCOPTIONS_H
