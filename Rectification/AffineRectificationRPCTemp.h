//
// Created by morzh on 04.01.18.
//

#ifndef SSV_VER_0_1_AFFINERECTIFICATIONRPCTEMP_H
#define SSV_VER_0_1_AFFINERECTIFICATIONRPCTEMP_H


#include "Common/ConvexTetragon.h"


class AffineRectificationRPCTemp {

public:

    AffineRectificationRPCTemp(const Eigen::Vector3i &num_grid_points);

    Eigen::Vector3i                            num_grid_points;
    Eigen::Matrix2Xd                           image1_proj_coords,             image2_proj_coords;


    Eigen::Matrix2Xd                           patch1_proj_coords,     patch2_proj_coords;
    Eigen::Matrix2Xd                           patch1_proj_coords_transformed,     patch2_proj_coords_transformed;
    Eigen::Matrix3d                            AffineFM;

    Eigen::Matrix3d                            H1, H2;
    Eigen::Vector2d                            im12_rectified_size;
    Eigen::Matrix<double, 3,4>                 im1_corners, im2_corners;
    ConvexTetragon                             im1_photoArea,im2_photoArea;
    std::vector<Eigen::Matrix3Xd>              regular_geoGrid;
    ImageRectangleD                            im12_rect;

    Eigen::IOFormat                            HeavyFmt, OctaveFmt;

    bool                                       affineEstimateIsValid = false;

};


AffineRectificationRPCTemp::AffineRectificationRPCTemp(const Eigen::Vector3i &num_grid_points) : num_grid_points(num_grid_points) {


}

#endif //SSV_VER_0_1_AFFINERECTIFICATIONRPCTEMP_H
