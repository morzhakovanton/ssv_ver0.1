//
// Created by morzh on 7/1/17.
//

#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONSTATISTICS_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONSTATISTICS_H


#include <iomanip>

class ImageRegistrationStatistics{
public:

    void                            reset_stats_T2      ( );
    void                            collect_statsT2     ( Eigen::Vector2f T2, Eigen::Vector2f shift_T2, float confidence);
    void                            print_statsT2       ( );
    void                            print_statsFailsT2  ( );


    int                             numOfFails=0;
    std::vector<Eigen::Vector2f>    groud_truth_T2;
    std::vector<Eigen::Vector2f>    estimate_T2;
    std::vector<float>              confidence_T2;
    std::vector<bool>               estimateIsFailed;
    cv::Mat                         error_matrix;
    cv::Mat                         confidence_matrix;

};

void ImageRegistrationStatistics::collect_statsT2(Eigen::Vector2f T2, Eigen::Vector2f shift_T2, float confidence) {

    groud_truth_T2.push_back ( T2         );
    estimate_T2.push_back    ( shift_T2   );
    confidence_T2.push_back  ( confidence );

    if ( ( std::abs( shift_T2(0)-T2(0) ) > 1e-6 ) || ( std::abs( shift_T2(1)-T2(1) ) > 1e-6 ) ) {

        estimateIsFailed.push_back(true);
        numOfFails++;
    }
    else
        estimateIsFailed.push_back(false);

}

void ImageRegistrationStatistics::reset_stats_T2() {

    groud_truth_T2.clear();
    estimate_T2.clear();
    confidence_T2.clear();
    estimateIsFailed.clear();

    numOfFails = 0;

}

void ImageRegistrationStatistics::print_statsT2() {

    std::cout << "ground truth" <<  " " << "estimate T2" << " " << "confidence_T2"  << std::endl;

    for (int idx = 0; idx < groud_truth_T2.size(); ++idx) {

        std::cout << std::setw(15) << groud_truth_T2[idx].transpose()  <<  " | " <<  std::setw(9) << estimate_T2[idx].transpose() << " |" <<
                     std::setw(15) << (groud_truth_T2[idx] - estimate_T2[idx]).norm()   <<  " | "<< std::setw(12)  << confidence_T2[idx]  <<  std::endl;

    }

}

void ImageRegistrationStatistics::print_statsFailsT2() {

    std::cout << "Fails: " << std::endl;
    std::cout << "ground truth" <<  " " << "estimate T2" << " " << "confidence_T2"  << std::endl;

    for (int idx = 0; idx < estimateIsFailed.size(); ++idx) {

        if ( estimateIsFailed[idx] == true)

            std::cout << groud_truth_T2[idx].transpose() <<  " " << estimate_T2[idx].transpose() << " " << confidence_T2[idx]  << std::endl;
    }


}


#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONSTATISTICS_H
