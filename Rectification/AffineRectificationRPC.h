//
// Created by morzh on 19.12.17.
//

#ifndef SSV_VER_0_1_IMAGEPAIRRPCALIGN_H
#define SSV_VER_0_1_IMAGEPAIRRPCALIGN_H


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/SVD>
#include <Eigen/Eigenvalues>
#include <eigen3/unsupported/Eigen/FFT>

#include "boost/multi_array.hpp"
#include "gdal/gdal_priv.h"

#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"
#include <opencv2/core/eigen.hpp>

#include "SatellitePatches/PatchesStereoPair.h"
#include "AffineRectificationRPCTemp.h"
#include "AffineRectificationRPCViz.h"




class AffineRectificationRPC{

public:

                    AffineRectificationRPC                  ( const Eigen::Vector3i &geo_grid_num_pts);

    void            estimate_affineFundamentalMatrix        ( PatchesStereoPair &pair);

    void            project_regularGridOnImages             ( PatchesStereoPair &pair);
    void            project_regularGridOnPatches            ( const PatchesStereoPair &pair);

    void            calc_rectificationsMatrices_old         ( PatchesStereoPair &pair);
    void            calc_rectificationsMatrices             ( PatchesStereoPair &pair);
    void            calc_regularGeoGridForSIM2(PatchesStereoPair &pair);
    void            calc_BBox                               ( Eigen::Matrix<double, 3,4> &col_pts_homo, Eigen::Matrix2d &bbox);
    void            transform_projectedCoords               ( const PatchesStereoPair &pair );

    void            do_affineRectification                  ( PatchesStereoPair &pair);
    void            rectify_SIM2_old                        ( PatchesStereoPair &pair);
    void            rectify_SIM2                            ( PatchesStereoPair &pair);
    void            rectify_SIM1                            ( PatchesStereoPair &pair);
    void            rectify_SL3                             ( PatchesStereoPair &pair);

    void            calc_regularGeoGridForTransformedPatches(PatchesStereoPair &pair);

    double          calc_distanceFromLineToPoint            ( const Eigen::Vector3d &line, const Eigen::Vector3d &point);


    AffineRectificationRPCTemp  data;
    AffineRectificationRPCViz   viz;


    void do_rectificationSL3 (PatchesStereoPair &pair);
    void do_rectificationSIM1(PatchesStereoPair &pair);
    void do_rectificationSIM2(PatchesStereoPair &pair);
};



AffineRectificationRPC::AffineRectificationRPC(const Eigen::Vector3i &geo_grid_num_pts) : data(geo_grid_num_pts), viz(&data)  {

}


void AffineRectificationRPC::estimate_affineFundamentalMatrix(PatchesStereoPair &pair) {

    int                     cols            =  data.patch1_proj_coords.cols();
    Eigen::Vector2d         t1              =  Eigen::Vector2d(0,0);
    Eigen::Vector2d         t2              =  Eigen::Vector2d(0,0);
    Eigen::Vector4d         r_tilda;
    Eigen::Vector4d         centroid;
    Eigen::Matrix4Xd        r;

    r.conservativeResize( Eigen::NoChange, cols );

    centroid.setZero();

    for (int idx = 0; idx < cols; ++idx) {

        Eigen::Vector4d r_i;

        r_i << data.patch2_proj_coords.col(idx), data.patch1_proj_coords.col(idx);

        centroid +=  r_i;

        r.col(idx) = r_i;

//        t1 += patch1_proj_coords.col(idx);
//        t2 += patch2_proj_coords.col(idx);
    }

    t1 /= (double) cols;   t2 /= (double) cols;

    centroid /= double(cols);

    Eigen::Matrix2Xd   im1_centered_coords = data.patch1_proj_coords;
    Eigen::Matrix2Xd   im2_centered_coords = data.patch2_proj_coords;


    for (int idx = 0; idx < cols; ++idx) {

        r.col(idx) -= centroid;
    }

    /**-------------------------------------------------------------------------*/

    Eigen::Matrix4Xd  A_t;
    A_t.conservativeResize( Eigen::NoChange, cols );

    Eigen::Matrix4d W;

    W.setZero();

    for (int idx = 0; idx < cols; ++idx) {

        W += r.col(idx) * r.col(idx).transpose();
    }



    Eigen::EigenSolver<Eigen::Matrix4d> EVD(W);

    Eigen::Matrix4d  N     = (EVD.eigenvectors()).real();
    Eigen::Vector4d  lda   = ((EVD.eigenvalues()).real());

    Eigen::Vector4d::Index min_lda;
    lda.minCoeff(&min_lda);


    Eigen::Vector4d  n = N.col(min_lda);

    double e = -n.transpose()*centroid;


    data.AffineFM.setZero();


    data.AffineFM(2,0)                =   n(2);
    data.AffineFM(2,1)                =   n(3);

    data.AffineFM(0,2)                =   n(0);
    data.AffineFM(1,2)                =   n(1);

    data.AffineFM(2,2)                =   e;


    data.affineEstimateIsValid = true;

}

void AffineRectificationRPC::do_affineRectification(PatchesStereoPair &pair) {

    do_rectificationSIM1( pair);
    do_rectificationSIM2( pair);

}



void AffineRectificationRPC::rectify_SIM2_old(PatchesStereoPair &pair) {

    cv::Mat  H1_cv = cv::Mat::zeros(2,3, CV_64F);
    cv::Mat  H2_cv = cv::Mat::zeros(2,3, CV_64F);

    Eigen::Matrix3d H1 = pair.patch1.T;
    Eigen::Matrix3d H2 = pair.patch2.T;


    H1_cv.at<double>(0,0) = H1(0,0);  H1_cv.at<double>(0,1) = H1(0,1);  H1_cv.at<double>(0,2) = H1(0,2);
    H1_cv.at<double>(1,0) = H1(1,0);  H1_cv.at<double>(1,1) = H1(1,1);  H1_cv.at<double>(1,2) = H1(1,2);

    H2_cv.at<double>(0,0) = H2(0,0);  H2_cv.at<double>(0,1) = H2(0,1);  H2_cv.at<double>(0,2) = H2(0,2);
    H2_cv.at<double>(1,0) = H2(1,0);  H2_cv.at<double>(1,1) = H2(1,1);  H2_cv.at<double>(1,2) = H2(1,2); //+0.1;

//    std::cout << H1_cv << std::endl;      std::cout << H2_cv << std::endl;
    cv::warpAffine( pair.patch1.imgPN, pair.patch1.imgPNT, H1_cv, cv::Size(data.im12_rectified_size(0), data.im12_rectified_size(1) ), cv::INTER_CUBIC );
    cv::warpAffine( pair.patch2.imgPN, pair.patch2.imgPNT, H2_cv, cv::Size(data.im12_rectified_size(0), data.im12_rectified_size(1) ), cv::INTER_CUBIC );
}




void AffineRectificationRPC::rectify_SIM2(PatchesStereoPair &pair) {

    pair.patch1.working_area.transform( pair.patch1.T );
    pair.patch2.working_area.transform( pair.patch2.T );

    pair.patch1.update_transformedImage( pair.im12_rect );
    pair.patch2.update_transformedImage( pair.im12_rect );
}



void AffineRectificationRPC::project_regularGridOnImages(PatchesStereoPair &pair) {


    int         height_num_elements     =   data.regular_geoGrid.size();
    int         planar_num_elements     =   data.regular_geoGrid[0].cols();
    int         overall_num_pts         =   height_num_elements * planar_num_elements;
    int         idx_proj                =   0;


    data.image1_proj_coords.conservativeResize( Eigen::NoChange, overall_num_pts );
    data.image2_proj_coords.conservativeResize( Eigen::NoChange, overall_num_pts );


    for (int h = 0; h < height_num_elements; ++h) {

        for (int idx = 0; idx < planar_num_elements; ++idx) {

            Eigen::Vector3d  ground_pt              = data.regular_geoGrid[h].col(idx);
            data.image1_proj_coords.col(idx_proj)   = pair.patch1.rpc.calc_RPCI2D( ground_pt );
            data.image2_proj_coords.col(idx_proj)   = pair.patch2.rpc.calc_RPCI2D( ground_pt );

            ++idx_proj;
        }
    }

}



void AffineRectificationRPC::project_regularGridOnPatches(const PatchesStereoPair &pair) {


    int         height_num_elements     =   data.regular_geoGrid.size();
    int         planar_num_elements     =   data.regular_geoGrid[0].cols();
    int         overall_num_pts         =   height_num_elements * planar_num_elements;
    int         idx_proj                =   0;


    data.patch1_proj_coords.conservativeResize( Eigen::NoChange, overall_num_pts );
    data.patch2_proj_coords.conservativeResize( Eigen::NoChange, overall_num_pts );


    for (int h = 0; h < height_num_elements; ++h) {

        for (int idx = 0; idx < planar_num_elements; ++idx) {

            Eigen::Vector3d  ground_pt              = data.regular_geoGrid[h].col(idx);
            data.patch1_proj_coords.col(idx_proj)   = pair.patch1.rpc.calc_RPCI2D( ground_pt )  - pair.patch1.rect.get_ul().cast<double>() ;
            data.patch2_proj_coords.col(idx_proj)   = pair.patch2.rpc.calc_RPCI2D( ground_pt )  - pair.patch2.rect.get_ul().cast<double>();

            ++idx_proj;
        }
    }

}


void AffineRectificationRPC::calc_rectificationsMatrices_old(PatchesStereoPair &pair) {


    Eigen::Matrix<double, 3,4>  im1_corners_transformed, im2_corners_transformed;
    int im1_cols, im2_cols,im1_rows, im2_rows;

//    pair.patch1.T.setIdentity(); pair.patch2.T.setIdentity();
    data.H1.setIdentity(); data.H2.setIdentity();

    im1_cols = pair.patch1.imgPN.cols;
    im1_rows = pair.patch1.imgPN.rows;

    im2_cols = pair.patch2.imgPN.cols;
    im2_rows = pair.patch2.imgPN.rows;


    data.im1_corners.col(0) = Eigen::Vector3d( 0       , 0       , 1 );
    data.im1_corners.col(1) = Eigen::Vector3d( im1_cols, 0       , 1 );
    data.im1_corners.col(2) = Eigen::Vector3d( im1_cols, im1_rows, 1 );
    data.im1_corners.col(3) = Eigen::Vector3d( 0       , im1_rows, 1 );



    data.im2_corners.col(0) = Eigen::Vector3d( 0       , 0       , 1 );
    data.im2_corners.col(1) = Eigen::Vector3d( im2_cols, 0       , 1 );
    data.im2_corners.col(2) = Eigen::Vector3d( im2_cols, im2_rows, 1 );
    data.im2_corners.col(3) = Eigen::Vector3d( 0       , im2_rows, 1 );


/*
    std::cout << " before applying sim transform" << std::endl;
    std::cout << im1_corners << std::endl;
    std::cout << std::endl;
    std::cout << im2_corners << std::endl;
*/


    data.H1(0,0) =  data.AffineFM(2,1);   data.H1(0,1) =  -data.AffineFM(2,0);  data.H1(0,2) =  0;
    data.H1(1,0) =  data.AffineFM(2,0);   data.H1(1,1) =   data.AffineFM(2,1);  data.H1(1,2) =  data.AffineFM(2,2);

    data.H2(0,0) = -data.AffineFM(1,2);   data.H2(0,1) =   data.AffineFM(0,2);  data.H2(0,2) =  0;
    data.H2(1,0) = -data.AffineFM(0,2);   data.H2(1,1) =  -data.AffineFM(1,2);  data.H2(1,2) =  0;


    im1_corners_transformed  = data.H1*data.im1_corners;
    im2_corners_transformed  = data.H2*data.im2_corners;

/*
    std::cout << "------------------------------------ \n";
    std::cout << " after applying sim transform" << std::endl;
    std::cout << im1_corners << std::endl;
    std::cout << std::endl;
    std::cout << im2_corners << std::endl;
*/

    Eigen::Matrix2d im1_bbox, im2_bbox;

    calc_BBox( im1_corners_transformed, im1_bbox);
    calc_BBox( im2_corners_transformed, im2_bbox);


/*
    std::cout << "------------------------------------ \n";
    std::cout << " bounding boxes: " << std::endl;
    std::cout << im1_bbox << std::endl;
    std::cout << std::endl;
    std::cout << im2_bbox << std::endl;
*/


    ///  below we should shift both images int a way, they occupy image space evenely.

    double v2_c = std::max( -im2_bbox(1,0), -im1_bbox(1,0) );

    data.H1(0,2) =  -im1_bbox(0,0);
    data.H2(0,2) =  -im2_bbox(0,0);

    data.H1(1,2) =   data.AffineFM(2,2) + v2_c;
    data.H2(1,2) =  v2_c;


    Eigen::Vector2d im1_dims = im1_bbox.col(1) - im1_bbox.col(0);
    Eigen::Vector2d im2_dims = im2_bbox.col(1) - im2_bbox.col(0);

    data.im12_rectified_size(0) = std::max( im1_dims(0), im2_dims(0) );
    data.im12_rectified_size(1) = std::max( im1_dims(1), im2_dims(1) ) + std::abs( im2_bbox(1,0) - im1_bbox(1,0) );


    double sqr_sum        = data.H1(0,0)*data.H1(0,0) + data.H1(1,0)*data.H1(1,0);
    double scale_factor   = std::sqrt(1.0/sqr_sum);

    Eigen::Matrix3d         scale;

    scale.setIdentity();

    scale(0,0) = scale_factor; scale(1,1) = scale_factor;

//    std::cout << scale << std::endl;
//    std::cout << std::endl;

    data.H1 = scale*data.H1;
    data.H2 = scale*data.H2;

    pair.patch1.T = data.H1;
    pair.patch2.T = data.H2;

    data.im12_rectified_size(0) *= scale_factor;
    data.im12_rectified_size(1) *= scale_factor;

}



void AffineRectificationRPC::calc_rectificationsMatrices(PatchesStereoPair &pair) {

    double                  sqr_sum, scale_factor, v2_c;
    Eigen::Matrix3d         scale;
    ImageRectangleD         im1_rect_bbox, im2_rect_bbox;

    data.H1.setIdentity(); data.H2.setIdentity();

    data.im1_photoArea          =       ConvexTetragon(ImageRectangleD(0,0,pair.patch1.imgPN.cols, pair.patch1.imgPN.rows));
    data.im2_photoArea          =       ConvexTetragon(ImageRectangleD(0,0,pair.patch2.imgPN.cols, pair.patch2.imgPN.rows));

    pair.patch1.working_area    =       data.im1_photoArea;
    pair.patch2.working_area    =       data.im2_photoArea;

    data.H1(0,0)                =       data.AffineFM(2,1);   data.H1(0,1) =  -data.AffineFM(2,0);  data.H1(0,2) =  0;
    data.H1(1,0)                =       data.AffineFM(2,0);   data.H1(1,1) =   data.AffineFM(2,1);  data.H1(1,2) =  data.AffineFM(2,2);

    data.H2(0,0)                =       -data.AffineFM(1,2);   data.H2(0,1) =   data.AffineFM(0,2);  data.H2(0,2) =  0;
    data.H2(1,0)                =       -data.AffineFM(0,2);   data.H2(1,1) =  -data.AffineFM(1,2);  data.H2(1,2) =  0;

    data.im1_photoArea.transform(data.H1);
    data.im2_photoArea.transform(data.H2);

    im1_rect_bbox               =       data.im1_photoArea.get_boundingBox<double>();
    im2_rect_bbox               =       data.im1_photoArea.get_boundingBox<double>();
    v2_c                        =       std::max( -im2_rect_bbox.y, -im1_rect_bbox.y );  ///  below we should shift both images int a way, they occupy image space evenely.

    data.H1(0,2)                =       -im1_rect_bbox.x;
    data.H2(0,2)                =       -im2_rect_bbox.x;

    data.H1(1,2)                =       data.AffineFM(2,2) + v2_c;
    data.H2(1,2)                =       v2_c;

    pair.im12_rect              =       im1_rect_bbox.maxSizeTo(im2_rect_bbox);

    sqr_sum                     =       data.H1(0,0)*data.H1(0,0) + data.H1(1,0)*data.H1(1,0);
    scale_factor                =       std::sqrt(1.0/sqr_sum);
    scale                       =       Eigen::Matrix3d::Identity();
    scale(0,0)                  =       scale(1,1) = scale_factor;

    pair.patch1.T               =       scale*data.H1;
    pair.patch2.T               =       scale*data.H2;


    pair.im12_rect.scaleSizeTo(scale_factor);
}


void AffineRectificationRPC::calc_BBox(Eigen::Matrix<double, 3, 4> &col_pts_homo, Eigen::Matrix2d &bbox) {

    /** calculates bounding box from set of homogenious pixels column-coordinates, stqcked into the matrix  3 by number of pixel's coordinates */


    /** first get rid from homogenity */
    Eigen::Vector3d     col;
    Eigen::Matrix2Xd    col_pts;
    col_pts.conservativeResize(Eigen::NoChange, col_pts_homo.cols() );


    for (int idx = 0; idx < col_pts_homo.cols(); ++idx) {

        col                      = col_pts_homo.col(idx);
        col                     /= col(2);
        col_pts.col(idx)         = Eigen::Vector2d( col(0), col(1) );

    }

    /** second is to find min and max values in each dimension  for both images */
    Eigen::Vector2d   minVals  = col_pts.rowwise().minCoeff();
    Eigen::Vector2d   maxVals  = col_pts.rowwise().maxCoeff();


    bbox.col(0) = minVals;
    bbox.col(1) = maxVals;

}


void AffineRectificationRPC::rectify_SL3(PatchesStereoPair &pair) {

    /**
     * имея точки на первом снимке и соответствующие им точки на втором снимке, посчитаем  частный случай преборазования SL3 ( affine homography) переводящее  один набор точек в другой.
     * Таким образом находится  горизонтальный скос, применение которго ещё больше сблизит два данных куска космических снимков
     * Задача нахождения наилучшего преобразования решается с помощью DLT (Direct Linear Transform) с помощью сингулярного разложения (SVD), см, напр,
     * https://www.cs.ubc.ca/grads/resources/thesis/May09/Dubrofsky_Elan.pdf, page 14.
     * или
     * Hartley, Zisserman - Multiple View Geometry in Computer Vision, стр. 130, ALgorithm 4.7
     * Параметры, отвечающие за преобразование кроме перемещения и горизонтального скоса, отбрасываются ( второй скос, перемещенеи по y )
     * */

    int                 points_num = data.patch1_proj_coords_transformed.cols();
    Eigen::MatrixXd     A, V;
    Eigen::Matrix2Xd    X, U;
    Eigen::Matrix2d     B, C, H2x2;
    Eigen::Matrix3d     H_A;
    Eigen::Vector2d     t1, t2;

    A.conservativeResize(points_num, 4);


    /// Для получения устойчивого решения оба набора точек нормируются (см, напр, книгу Hartley, Zisserman)

    X = data.patch1_proj_coords_transformed;
    U = data.patch2_proj_coords_transformed;
    t1.setZero();
    t2.setZero();

    /// центрируем оба набора точек ( Algorithm a) )
    for (int idx = 0; idx < points_num; ++idx) {

        t1 += data.patch1_proj_coords_transformed.col(idx);
        t2 += data.patch2_proj_coords_transformed.col(idx);
    }
    t1 /= points_num;
    t2 /= points_num;

    for (int idx = 0; idx < points_num; ++idx) {

        X.col(idx) -= t1;
        U.col(idx) -= t2;
    }

    /// Form the n×4 matrix A ( Algorithm b) )
        for (int idx = 0; idx < points_num; ++idx) {

        A(idx,  0) = X.col(idx)(0);         A(idx,  1) = X.col(idx)(1);
        A(idx,  2) = U.col(idx)(0);         A(idx,  3) = U.col(idx)(1);
    }


    Eigen::JacobiSVD<Eigen::MatrixXd> svd( A, Eigen::ComputeFullV );
    svd.computeV();

    V       =   svd.matrixV();
    B       =   V.block<2,2>(0,0);
    C       =   V.block<2,2>(2,0);
    H2x2    =   C*B.inverse();



    H_A.setIdentity();
    H_A.block<2,2>(0,0) = H2x2;
    H_A.block<2,1>(0,2) = t2 - H2x2*t1;
    H_A(1,0) = 0;
    H_A(1,2)=0;

/*
    std::cout << "Affine Homography estimate: " << std::endl;
    std::cout << H_A << std::endl;
*/

    pair.patch1.update_transform( H_A );
    pair.patch1.update_transformedImage( pair.im12_rect);

}

void AffineRectificationRPC::rectify_SIM1(PatchesStereoPair &pair) {

    /**
     * сделаем дополнительное выравнивание по горизонтальному смещению и масштабированию. Для  этого решим однмерную задачу:
     * 1) посчитаем  среднее  арифм  X-координат  точек, полученных проекцией регулярной решётки и "преобразованием выравнивания" первого и второго куска (data.patch{1-2}_proj_coords_transformed)
     * 2) центрируем  данные с помощью найденного арифм среднего с шага 1)
     * 3)  посчитаем средне квадратическое отклонение
     * 4) найдём сдвиг как разность средних арифм  и  масштаб как отношение средне квадратических ( вдоль оси X )
     * 5) в зависимости от значения масшатаба (больше или меньше единицы), делаем  преобразование (сдвиг и масштабирование по X ) первого или второго изображени
     * */


    double              mean1(0), mean2(0), dev1(0), dev2(0), shift(0), scale(1);
    int                 points_num = data.patch1_proj_coords_transformed.cols();
    Eigen::VectorXd     set1, set2;
    Eigen::Matrix3d     xform = Eigen::Matrix3d::Identity();

    set1.resize(points_num);
    set2.resize(points_num);


    ///  шаг 1)
    for (int idx = 0; idx < points_num; ++idx) {

        double x1 = data.patch1_proj_coords_transformed.col(idx)(0);
        double x2 = data.patch2_proj_coords_transformed.col(idx)(0);

        mean1 += x1;
        mean2 += x2;

        set1(idx) = x1;
        set2(idx) = x2;
    }

    mean1 /= points_num;
    mean2 /= points_num;

    ///  шаг 2)
    for (int idx = 0; idx < points_num; ++idx) {

        set1(idx) -= mean1;
        set2(idx) -= mean2;

        set1(idx) *= set1(idx);
        set2(idx) *= set2(idx);

    }

    ///  шаг 3)
    dev1    =  std::sqrt(  set1.sum()/(points_num-1) );
    dev2    =  std::sqrt(  set2.sum()/(points_num-1) );


    scale   =   dev1/dev2;
    shift   =   -mean1+mean2;

//    std::cout << "scale: " << scale << "; shift: " << shift << std::endl;

    xform.setIdentity();

    if ( scale > 1){

        scale       = 1.0/scale;
        shift       = -shift;

        xform(0,0) =    scale;
        xform(0,2) =    shift+mean1 - mean1*scale;

        pair.patch1.update_transform( xform);
        pair.patch1.update_transformedImage( pair.im12_rect);
    }
    else{

        xform(0,0) =    scale;
        xform(0,2) =    shift+mean2 - mean2*scale;

        pair.patch2.update_transform( xform);
        pair.patch2.update_transformedImage(pair.im12_rect);
    }



}


void AffineRectificationRPC::calc_regularGeoGridForSIM2(PatchesStereoPair &pair) {

    /**
      * 1. Находим равномерную решётку для  прямоугольника   pair.patch1.rect_src (  рассматриваемого куска  всего  космического снимка )
 `    * 2. Делаем проекцию равномерной решётки с разными высотами в систему координат Земли
 `    * */


    double              height_step         =   2.0 / ( data.num_grid_points(2) - 1);
    Eigen::Vector2i     grid_div_2d         =   Eigen::Vector2i( data.num_grid_points(0), data.num_grid_points(1) );

    Eigen::Matrix2Xd    img_reg_grid; //, test_grid_px, test_grid_px2;
    Eigen::Matrix3Xd    ground_reg_grid; //, test_pt_grid2;


    /// шаг 1.
    pair.patch1.rect_src.get_regularGrid( grid_div_2d,  img_reg_grid );
    pair.patch1.rpc.normalize_imgCoordinatesArray(img_reg_grid);

    data.regular_geoGrid.clear();

    /// шаг 2.
    for (int idx = 0; idx < data.num_grid_points(2); ++idx) {

        pair.patch1.rpc.calc_NRPC3DArray                       ( img_reg_grid, -1 + idx * height_step, ground_reg_grid);
        pair.patch1.rpc.unnormalize_groundCoordinatesArray     ( ground_reg_grid);
        data.regular_geoGrid.push_back                         ( ground_reg_grid );
    }
}


void AffineRectificationRPC::transform_projectedCoords( const PatchesStereoPair &pair ) {

    /**
     *  имея матрицы для "разворота" кусков снимков  к виду стереопары, найдём координаты точек на повёрнутых изобржения, полученных с помощью проекции регулярной решётки.
     *  Регулярная решётка состоит из точек в координатах Земли. То есть мы просто применяем найенные с помощью  calc_rectificationsMatrices(..)  преобразовния.
     * */

    int     points_num  = data.patch1_proj_coords.cols();

    data.patch1_proj_coords_transformed.conservativeResize(Eigen::NoChange, points_num);
    data.patch2_proj_coords_transformed.conservativeResize(Eigen::NoChange, points_num);


    for (int idx = 0; idx < points_num; ++idx) {

        Eigen::Vector3d     px1, px2;

        px1 << data.patch1_proj_coords.col(idx), 1;
        px2 << data.patch2_proj_coords.col(idx), 1;

        px1 = pair.patch1.T *px1;
        px2 = pair.patch2.T *px2;

        px1 /= px1(2);
        px2 /= px2(2);

        data.patch1_proj_coords_transformed.col(idx) = Eigen::Vector2d( px1(0), px1(1) );
        data.patch2_proj_coords_transformed.col(idx) = Eigen::Vector2d( px2(0), px2(1) );
    }


}


void AffineRectificationRPC::calc_regularGeoGridForTransformedPatches(PatchesStereoPair &pair) {

    /**
     *  1. Находится равномерная сетка внутри 4х угольника изображения. Для этого берётся равномерная сетка  ограничивающего прямоугольника (bounding box) 4х-угольника.
     *  2. Полученные точки  "разворачиваются обратно" (т.е. отменяется преобразование приведения к виду стереопары)
     *  3. Далее отсекаются точки,  лежащие вне прямоугольника
     *  4. Точки с шага 3. с  нормированной высотой 0 проецируются на Землю.
     * */


    /// 1. create regular grid inside tetragon on the first image
    Eigen::Matrix2Xd                           tetragon_reg_grid, rect_grid;
    pair.patch1.working_area.get_regularGrid   ( Eigen::Vector2i(35,35), tetragon_reg_grid );
    int                                         num_points =  tetragon_reg_grid.cols();

    /// 2. reverse rectification transform
    rect_grid.conservativeResize ( Eigen::NoChange, num_points);

    for (int idx = 0; idx < num_points; ++idx) {

        Eigen::Vector3d pt;
        pt <<  tetragon_reg_grid.col(idx), 1;
        pt = pair.patch1.T.inverse()*pt;
        pt /= pt(2);

        rect_grid.col(idx) = Eigen::Vector2d( pt(0), pt(1) );
    }

    /// 3. cut point outside rect_src
    Eigen::Matrix2Xd    rect_src_points;
    ImageRectangleI     patch1_rect_src = pair.patch1.rect_src;
    Eigen::Vector2i     shift = pair.patch1.rect_src.get_ul()  - pair.patch1.rect.get_ul();
    Eigen::Vector2i     shift2 = pair.patch1.rect.get_ul();
    Eigen::Matrix3Xd    ground_reg_grid;

    patch1_rect_src.set_ul(shift);
    patch1_rect_src.cutOutsidePoints( rect_grid, rect_src_points);
    rect_grid = rect_src_points;

    for (int idx = 0; idx < rect_grid.cols(); ++idx) {

        rect_grid.col(idx) += shift2.cast<double>();
    }


    /// 4. make 3D grid from rect_grid
    pair.patch1.rpc.normalize_imgCoordinatesArray(rect_grid);
    data.regular_geoGrid.clear();

    int     num_steps = 4;
    double height_step =  (float)2.0/(num_steps-1);

    for (int idx = 0; idx < num_steps; ++idx) {

        pair.patch1.rpc.calc_NRPC3DArray                       ( rect_grid, -1.0 + idx*height_step, ground_reg_grid);
        pair.patch1.rpc.unnormalize_groundCoordinatesArray     ( ground_reg_grid);
        data.regular_geoGrid.push_back                         ( ground_reg_grid );
    }

}

void AffineRectificationRPC::do_rectificationSIM2(PatchesStereoPair &pair) {

    calc_regularGeoGridForSIM2              (pair );
    project_regularGridOnPatches            (pair );
    estimate_affineFundamentalMatrix        (pair );
    calc_rectificationsMatrices             (pair );
    rectify_SIM2                            (pair );
    transform_projectedCoords               (pair );
}

void AffineRectificationRPC::do_rectificationSIM1(PatchesStereoPair &pair) {

    calc_regularGeoGridForTransformedPatches(pair );
    project_regularGridOnPatches            (pair );
    transform_projectedCoords               (pair );
    rectify_SIM1                            (pair );
}


void AffineRectificationRPC::do_rectificationSL3(PatchesStereoPair &pair) {

    calc_regularGeoGridForTransformedPatches(pair);
    project_regularGridOnPatches            (pair );
    transform_projectedCoords               (pair );
    rectify_SL3                             (pair );
}


#endif //SSV_VER_0_1_IMAGEPAIRRPCALIGN_H
