//
// Created by morzh on 06.09.18.
//

#ifndef SSV_VER_0_1_IMAGEREGISTRATIONBASE_H
#define SSV_VER_0_1_IMAGEREGISTRATIONBASE_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <complex.h>
#include <cmath>
#include <iostream>

#include <Eigen/Core>
#include <fftw3.h>
#include "ImageRegistrationPOCOptions.h"
#include "ImageRegistrationDebug.h"

#ifndef PI
#define  PI 3.1415926535897932384626433832795f
#endif

class ImageRegistrationPOCBase {

public:
    virtual ~ImageRegistrationPOCBase();


    void                    calc_AbsFFT                                 ( const cv::Mat &src, cv::Mat &dst);
    void                    calc_FFT                                    ( float *p_src, const Eigen::Vector2i &size, std::complex<float> *p_dst);
    void                    calc_FFT(cv::Mat &src, cv::Mat &dst);
    void                    calc_WFFTFiltered                           ( const cv::Mat &src, cv::Mat &dst, const ImageRegistrationPOCOptions &options);
    void                    calc_FFTShift                               ( cv::Mat &mat, Eigen::Vector2i shift_vals = Eigen::Vector2i(0, 0) );
    void                    calc_IFFTShift1D                            ( cv::Mat &img, int shift_val );

    void                    create_highPassEmphasisFilter               ( cv::Mat &highpass_filter );
    void                    create_BlackmanWindow2D                     ( cv::Mat &blackman_window);
    void                    create_HammingWindow2D                      ( cv::Mat &hamming_window);
    void                    create_TukeyWindow2D                        ( cv::Mat &tukey_window, float alpha = 0.5);
    void                    create_TukeyWindow1D                        ( cv::Mat mat, float alpha=0.5);

    void                    apply_GaborFilter                           ( const cv::Mat &src, cv::Mat &dst, const OptionsGaborFilter &options);
    cv::Mat                 get_GaborKernel                             ( const OptionsGaborFilter &options);
    float                   sigma_prefactor                             ( float bandwidth);
    void                    meshgrid                                    ( const cv::Range &xgv, const cv::Range &ygv, cv::Mat &X, cv::Mat &Y);
    cv::Mat                 cos                                         ( cv::Mat m);
    cv::Mat                 sin                                         ( cv::Mat m);


//    void                    apply_bandPassFilter                        ( float *pc, Eigen::Vector2i size, Eigen::Vector2f passband_normalized);
//    void                    apply_differentiating                       ( float* pc, Eigen::Vector2i size );
//    void                    apply_squaring                              ( float* pc, Eigen::Vector2i size );
//    void                    apply_movingAverageFilter                   ( float* pc );
//    void                    detect_peaks                                ( float* pc,  Eigen::Vector2f *peaks, float *peaks_val );

    Eigen::Vector2f         clamp                                       ( Eigen::Vector2f val, Eigen::Vector2f min, Eigen::Vector2f max);

    ImageRegistrationDebug          debug;
};

ImageRegistrationPOCBase::~ImageRegistrationPOCBase() {

}


void ImageRegistrationPOCBase::calc_AbsFFT(const cv::Mat &src, cv::Mat &dst) {

    int rows = src.rows;
    int cols = src.cols;


    if ( cols < 2 || rows < 2)  {     std::cerr << "input matrix too small" << std::endl;                       return;  }
    if ( src.type() != 5)       {     std::cerr << "input should be float one channel  matrix" << std::endl;    return;  }

    if ( dst.cols != src.cols || dst.rows != src.rows || dst.type() != 5 ){     dst = cv::Mat(rows, cols, CV_32FC1);     }

//    dst = cv::Mat(rows_dst, cols_dst, CV_32FC1);

    float                   *p_src          =  (float*)          src.data;
    float                   *p_dst          =  (float*)          dst.data;
    float                   *p_dft_abs      =  new float[sizeof(float) * rows*(cols/2+1) ];
    fftwf_complex           *p_src_dft      =  (fftwf_complex*)  fftwf_malloc( sizeof( fftwf_complex ) * (cols/2+1)*rows );
    std::complex<float>     *p_src_dft_     =  (std::complex<float>*) p_src_dft;
    fftwf_plan              fftw_plan;

    fftw_plan  =  fftwf_plan_dft_r2c_2d ( rows, cols,  p_src,  p_src_dft, FFTW_ESTIMATE );
    fftwf_execute( fftw_plan);

//    debug.print_cvMat( src);
//    debug.print_fftwf_complex_array_2d( p_src_dft, rows, cols/2+1);

    for ( int idx=0;  idx < (rows)*(cols/2+1); ++idx ){   p_dft_abs[idx] = std::abs( p_src_dft_[idx] );     }
//    debug.print_float_array_2d( p_dft_abs, rows, cols/2+1);

    for ( int idx=0;  idx < rows;            ++idx ){   memcpy( p_dst+idx*cols, p_dft_abs+idx*(cols/2+1), sizeof(float)*(cols/2+1) );   }
//    debug.print_float_array_2d(p_dst, rows, cols);

    for (int idx1 = 1; idx1 < rows; ++idx1) {
        for (int idx2 = 1; idx2 < cols/2; ++idx2) {

            p_dst[(rows*cols - (idx1-1)*cols -idx2)] = p_dst[idx1*cols+idx2];
        }
    }

//    debug.print_float_array_2d(p_dst, rows, cols);

    for (int idx=1; idx < cols/2; ++idx){    p_dst[cols-idx] = p_dst[idx];    }

//    debug.print_float_array_2d(p_dst, rows, cols);
}


void ImageRegistrationPOCBase::calc_WFFTFiltered(const cv::Mat &src, cv::Mat &dst, const ImageRegistrationPOCOptions &options) {
    /**
     * calculate windowed FFT and filter result with highpass filter
     * */

    cv::Mat             window              =   cv::Mat( src.rows, src.cols, CV_32FC1);
    cv::Mat             src_windowed        =   cv::Mat( src.rows, src.cols, CV_32FC1);
    cv::Mat             highpass            =   cv::Mat( src.rows, src.cols, CV_32FC1);
    cv::Mat             src_fft, src_gabor;


    if ( options.use_Gabor_filter){

        apply_GaborFilter(src, src_gabor, options.gabor_options);
    }
    else{
        src_gabor = src;
    }


    switch (options.fft_window){

        case ImageRegistrationPOCOptions::FFTWindow::NONE:
            window.setTo(1.0);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::BLACKMAN:
            create_BlackmanWindow2D(window);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::HAMMING:
            create_HammingWindow2D(window);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::TUKEY:
            create_TukeyWindow2D(window);
            break;
    }


    src_windowed  = window.mul(src_gabor);

    calc_AbsFFT   (src_windowed, src_fft);
    calc_FFTShift (src_fft);


    if ( options.apply_highpass_filter){

        create_highPassEmphasisFilter  ( highpass );
        dst = src_fft.mul(highpass);
    }
    else{

        dst = src_fft;
    }


/*
    dst =  cv::Mat( src.rows, src.cols, CV_32FC1);
//    create_BlackmanWindow2D          ( window );
    create_HammingWindow2D           ( window  );
//    debug.write_MatlabDLM("/home/morzh/ClionProjects/imageregistration/data/blackman_window.txt", window);
    src_windowed      =   src.mul( window);

    calc_AbsFFT(src_windowed, src_fft);
    calc_FFTShift                  ( src_fft );

    create_highPassEmphasisFilter  ( highpass );

//    debug.write_MatlabDLM("/home/morzh/ClionProjects/imageregistration/data/highpass.txt", highpass );
    dst      =   src_fft.mul(highpass);
*/
}


void ImageRegistrationPOCBase::calc_FFTShift(cv::Mat &mat, Eigen::Vector2i shift_vals) {

    /// circular shift in directions X and Y by the amount of width/2 and height/2
    int     width           =   mat.cols;
    int     height          =   mat.rows;
    int     channels_num    =   mat.channels();
    int     h_width,  h_height;

    if ( width ==1 && height == 1 )                         {  return; }
    if ( width ==1 || height == 1 )                         {  std::cerr << "1-D  circular shift is not implemented yet" << std::endl;  return; }
    if ( shift_vals(0) >= width || shift_vals(1) >= height) {  std::cerr << "shift value(s) exseeds image dimensions"<< std::endl;      return; }

    if ( shift_vals == Eigen::Vector2i(0,0) ) {

        h_width     = std::round( (float) width  / 2 );
        h_height    = std::round( (float) height / 2 );
    }
    else{
        h_width     = std::abs( shift_vals(0) );
        h_height    = std::abs( shift_vals(1) );
    }

    cv::Mat temp(height, width, CV_32FC(channels_num) );

    /// circular right shift in X direction by width/2
    mat ( cv::Rect( 0      , 0                , width       , h_height        ) ).copyTo(temp( cv::Rect( 0, height-h_height       ,  width, h_height        ) ) );
    mat ( cv::Rect( 0      , h_height         , width       , height-h_height ) ).copyTo(temp( cv::Rect( 0, 0,  width, height-h_height ) ) );
    /// circular down shift in Y direction by height/2
    temp( cv::Rect( 0      , 0                , h_width      , height         ) ).copyTo( mat( cv::Rect(width-h_width, 0       , h_width, height         ) ) );
    temp( cv::Rect( h_width, 0                , width-h_width, height         ) ).copyTo( mat( cv::Rect(0, 0, width-h_width, height  ) ) );
}


void ImageRegistrationPOCBase::calc_IFFTShift1D(cv::Mat &img, int shift_val) {

    /// circular shift in directions X and Y by the amount of width/2 and height/2

    int         width         =   img.cols;
    int         height        =   img.rows;
    int         channels_num  =   img.channels();
    int         h_length, length;
    cv::Mat     vec, img_t;
    bool        transpose_img;

    if ( width ==1 && height == 1 ){  return; }
    if ( width > 1 && height >  1 ){  std::cout << "use  calc_IFFTShift1D() for 2D  circular shift" << std::endl;    return; }

    if ( width == 1 && height >  1 ){   length = height; transpose_img = true;      }
    if ( width  > 1 && height == 1 ){   length = width;  transpose_img = false;       }


    if ( shift_val == 0 )   {        h_length     = std::round( (float) length / 2 );    }
    else                    {        h_length     = std::abs( shift_val );               }

    vec = cv::Mat( 1,  length, CV_32FC(channels_num) );

    if ( transpose_img)     {    img_t = img.t();         img_t.copyTo(vec);            }
    else                    {    img.copyTo( vec);                                      }

    /**------------------------------------------------------------------------------------------------*/

    cv::Mat temp(1, length, CV_32FC(channels_num) );

    /// circular left shift in X direction by width/2
    vec( cv::Rect( length-h_length, 0, h_length      , 1)     ).copyTo(temp( cv::Rect( 0      , 0,  h_length      , 1) ) );
    vec( cv::Rect( 0            , 0  , length-h_length, 1)     ).copyTo(temp( cv::Rect( h_length, 0,  length-h_length, 1) ) );


    /**-----------------------------------------------------------------------------------------------------*/

    if ( transpose_img) {   img_t = temp.t();    img_t.copyTo(img);     }
    else                {   temp.copyTo(img);                           }

}


void ImageRegistrationPOCBase::create_highPassEmphasisFilter(cv::Mat &highpass_filter) {

    int     height      = highpass_filter.rows;
    int     width       = highpass_filter.cols;


    if ( highpass_filter.type() != 5)       {     std::cerr << "input should be float one channel  matrix" << std::endl;    return;  }
    if ( width < 2 || height < 2)           {     std::cerr << "input matrix too small" << std::endl;                       return;  }

    /// Defines High-Pass emphasis filter used in Reddy and Chatterji

    float delta_x = (float) 1/(width-1),  delta_y = (float) 1/(height-1);

    for (int col =0; col < width; ++col) {
        for (int row = 0; row < height; ++row) {

            float   x_  = -0.5f +  delta_x*col;
            float   y_  = -0.5f +  delta_y*row;
            float   X   = std::cos(PI*x_)*std::cos(PI*y_);

            highpass_filter.at<float>(row, col) = (1-X)*(2-X);
        }
    }
}


void ImageRegistrationPOCBase::create_BlackmanWindow2D(cv::Mat &blackman_window) {


    int         M               =   blackman_window.cols;
    int         N               =   blackman_window.rows;

    float       a0              =   (float) 7938/18608;
    float       a1              =   (float) 9240/18608;
    float       a2              =   (float) 1430/18608;


    cv::Mat     h1              =   cv::Mat::zeros( M, 1, CV_32FC1);
    cv::Mat     h2              =   cv::Mat::zeros( N, 1, CV_32FC1);
    cv::Mat     bWindowC1;


    /// Make outer product degenerate if M or N is equal to 1.
    if ( M==1 || N==1 ){

        blackman_window.setTo(1.0f);
        return;
    }

    if( M > 1){

        for (int m = 0; m < M; ++m)

            h1.at<float>(m,0) = a0 - a1*std::cos(2*PI*m / (M-1)) + a2*std::cos(4*PI*m / (M-1));
    }

/*
    std::cout << h1 << std::endl;
    std::cout << std::endl;
    std::cout << h2 << std::endl;
*/

    if (N > 1){

        for (int n=0; n < N; ++n )

            h2.at<float>(n,0) = a0 - a1*cosf(2*PI*n / (N-1)) + a2*cosf(4*PI*n / (N-1));
    }

    bWindowC1 = h2 * h1.t();

    if ( blackman_window.channels() == 1){     blackman_window = bWindowC1;    }
    if ( blackman_window.channels() == 2){

        cv::Mat bw[2];

        bWindowC1.copyTo(bw[0]);
        bWindowC1.copyTo(bw[1]);

        cv::merge( bw, 2, blackman_window);

    }


}


void ImageRegistrationPOCBase::create_HammingWindow2D(cv::Mat &hamming_window) {


    if ( hamming_window.type() != 5)       {     std::cerr << "input should be float one channel  matrix" << std::endl;    return;  }

///   1D case  w(n) = 0.53836 − 0.46164 cos(2πn/N), 0≤n≤N

    int                 cols = hamming_window.cols;
    int                 rows = hamming_window.rows;


    cv::Mat             hamming_dir_y_ = cv::Mat(rows,    1, CV_32FC1 );
    cv::Mat             hamming_dir_x_ = cv::Mat(   1, cols, CV_32FC1 );


    for (int n = 0; n <= cols-1; ++n) {   hamming_dir_x_.at<float>(0,n) = 0.53836 - 0.46164*std::cos( 2*PI* n/(cols-1) );    }
    for (int m = 0; m <= rows-1; ++m) {   hamming_dir_y_.at<float>(m,0) = 0.53836 - 0.46164*std::cos( 2*PI* m/(rows-1) );    }


    hamming_window = hamming_dir_y_ * hamming_dir_x_;
}


void ImageRegistrationPOCBase::create_TukeyWindow2D(cv::Mat &tukey_window, float alpha) {

    if ( tukey_window.type() != 5)       {     std::cerr << "input should be float one channel  matrix" << std::endl;    return;  }

    int                 N = tukey_window.cols;
    int                 M = tukey_window.rows;


    cv::Mat             tukey_dir_x = cv::Mat(1, N, CV_32FC1 );
    cv::Mat             tukey_dir_y = cv::Mat(1, M, CV_32FC1 );

    create_TukeyWindow1D(tukey_dir_x, alpha );
    create_TukeyWindow1D(tukey_dir_y, alpha );


    tukey_window = tukey_dir_y.t() * tukey_dir_x;
}


void ImageRegistrationPOCBase::create_TukeyWindow1D(cv::Mat mat, float alpha) {

    if ( mat.rows > 1){ std::cerr << "Only matrices 1 by N are accepted" << std::endl;}


    int N = mat.cols;

    for (int n = 0; n < N; ++n) {

        if ( n <   alpha*(N-1)/2 )
            mat.at<float>(0,n) = 0.5  * ( 1 + std::cos( PI*( 2*n / (alpha*(N-1)) -1) ));

        if ( n >= alpha*(N-1)/2 && n <= (N-1)*(1 - alpha/2) )
            mat.at<float>(0,n) =1.0;

        if ( n > (N-1)*(1 - alpha/2) )
            mat.at<float>(0,n) = 0.5  * ( 1 + std::cos( PI*( 2*n / (alpha*(N-1))  - 2/alpha + 1 ) ));

    }
}

cv::Mat ImageRegistrationPOCBase::get_GaborKernel(const OptionsGaborFilter &options) {

    /**
    Return complex 2D Gabor filter kernel (cv::Mat with two channels ).

    Gabor kernel is a Gaussian kernel modulated by a complex harmonic function.   Harmonic function consists of an imaginary sine function and a real
    cosine function. Spatial frequency is inversely proportional to the  wavelength of the harmonic and to the standard deviation of a Gaussian
    kernel. The bandwidth is also inversely proportional to the standard    deviation.


     \frequency : spatial frequency of the harmonic function. Specified in pixels.
    \theta :    Orientation in radians. If 0, the harmonic is in the x-direction.
     \bandwidth :   The bandwidth captured by the filter. For fixed bandwidth, `sigma_x`    and `sigma_y` will decrease with increasing frequency. This value is
    ignored if `sigma_x` and `sigma_y` are set by the user.
    \sigma_x, \sigma_y :  Standard deviation in x- and y-directions. These directions apply to  the kernel *before* rotation. If `theta = pi/2`, then the kernel is
    rotated 90 degrees so that `sigma_x` controls the *vertical* direction.
    \n_stds :     The linear size of the kernel is n_stds (3 by default) standard   deviations
    \offset :     Phase offset of harmonic function in radians.

    [1] http://en.wikipedia.org/wiki/Gabor_filter
    [2] http://mplab.ucsd.edu/tutorials/gabor.pdf
    */

    float sigma_x, sigma_y;

    if (options.sigma_x  == -1 )
        sigma_x = sigma_prefactor(options.bandwidth) / options.frequency;
    else
        sigma_x = options.sigma_x;

    if (options.sigma_y == -1)
        sigma_y = sigma_prefactor(options.bandwidth) / options.frequency;
    else
        sigma_y = options.sigma_y;

    float   theta       = options.theta;
    float   n_stds      = options.n_stds;
    float   frequency   = options.frequency;
    float   offset      = options.offset;

    std::vector<float> x0 = {std::abs(n_stds * sigma_x * std::cos(theta)), std::abs(n_stds * sigma_y * std::sin(theta)), 1};
    std::vector<float> y0 = {std::abs(n_stds * sigma_y * std::cos(theta)), std::abs(n_stds * sigma_x * std::sin(theta)), 1};

    int x0__ = (int) std::ceil( *std::max_element( x0.begin(), x0.end()) );
    int y0__ = (int) std::ceil( *std::max_element( y0.begin(), y0.end()) );
    
    cv::Mat X, Y;

    meshgrid( cv::Range(-y0__, y0__+1), cv::Range(-x0__, x0__+1), X, Y);

    X.convertTo(X, CV_32FC1);
    Y.convertTo(Y, CV_32FC1);

    cv::Mat rotx =  X * std::cos(theta) + Y * std::sin(theta);
    cv::Mat roty = -X * std::sin(theta) + Y * std::cos(theta);

/*
    std::cout << rotx << std::endl;
    std::cout << roty << std::endl;
*/

    cv::Mat rotx_sqr, roty_sqr;


    cv::Mat g = cv::Mat::zeros(Y.rows,Y.cols, CV_32FC1);
    cv::Mat g1 = cv::Mat::zeros(Y.rows,Y.cols, CV_32FC1);
    cv::Mat g2 = cv::Mat::zeros(Y.rows,Y.cols, CV_32FC1);
    cv::Mat g_exp = cv::Mat::zeros(Y.rows,Y.cols, CV_32FC1);

    cv::pow( rotx, 2, rotx_sqr );
    cv::pow( roty, 2, roty_sqr );


    g = -0.5 *( rotx_sqr/std::pow(sigma_x,2)   +  roty_sqr/ std::pow(sigma_y, 2));
    cv::exp(g, g_exp);
    g_exp /= 2 * PI * sigma_x * sigma_y;
//    std::cout << g_exp << std::endl;

    cv::Mat exp_arg = 2 * PI * frequency * rotx + offset;
//    std::cout << exp_arg << std::endl;

    g1 = g_exp.mul(cos(exp_arg));
    g2 = g_exp.mul(sin(exp_arg));

/*
    std::cout << g1 << std::endl;
    std::cout << std::endl;
    std::cout << g2 << std::endl;
*/

    std::vector<cv::Mat> channels(2);

    channels[0] = g1;
    channels[1] = g2;

//    g_exp = cv::Mat::zeros(Y.rows,Y.cols, CV_32FC2);

    cv::merge( channels, g_exp);

    return g_exp;
}


void ImageRegistrationPOCBase::apply_GaborFilter(const cv::Mat &src, cv::Mat &dst, const OptionsGaborFilter &options) {
     /// сворачиваем изображение с ядром Габора
    cv::Mat Gabor_ReIm[2];
    cv::split(get_GaborKernel(options),Gabor_ReIm);

//    debug.show_imageScaled("Gabor", Gabor_ReIm[0]);

//    std::cout << Gabor_ReIm[0] << std::endl;
//    std::cout << src << std::endl;
    cv::filter2D(src, dst, -1, Gabor_ReIm[0] );
//    std::cout << dst << std::endl;
}


void ImageRegistrationPOCBase::meshgrid(const cv::Range &xgv, const cv::Range &ygv, cv::Mat &X, cv::Mat &Y){

    std::vector<int> t_x, t_y;

    for (int i = xgv.start; i < xgv.end; i++) t_x.push_back(i);
    for (int i = ygv.start; i < ygv.end; i++) t_y.push_back(i);

    cv::Mat xgv__(t_x), ygv__(t_y);

    cv::repeat(xgv__.reshape(1,1), ygv__.total(), 1, X);
    cv::repeat(ygv__.reshape(1,1).t(), 1, xgv__.total(), Y);
}

float ImageRegistrationPOCBase::sigma_prefactor(float b) {

    /// See http://www.cs.rug.nl/~imaging/simplecell.html
    return 1.0f / PI * std::sqrt( std::log(2.0f) / 2.0f) *  ( std::pow(2.0f, b) + 1) / ( std::pow(2.0f, b) - 1);
}


cv::Mat ImageRegistrationPOCBase::cos(cv::Mat m){

    if (m.depth() != CV_32F )
    {
        cv::Exception e;
        e.code = -1;
        e.msg = "Mat must be float for Cos function";
        throw e;
    }


    cv::Mat t(m.size(),CV_32FC(m.channels()));

    for (int i = 0; i < m.rows; i++) {

        float *ptr1 = (float*)m.ptr(i);
        float *ptr2 = (float*)t.ptr(i);

        for (int j=0;j<m.cols*m.channels();j++,ptr1++,ptr2++)
            *ptr2 = std::cos(*ptr1);
    }

    return t;
}

cv::Mat ImageRegistrationPOCBase::sin(cv::Mat m){

    if (m.depth() != CV_32F )
    {
        cv::Exception e;
        e.code = -1;
        e.msg = "Mat must be float for Cos function";
        throw e;
    }


    cv::Mat t(m.size(),CV_32FC(m.channels()));

    for (int i = 0; i < m.rows; i++) {

        float *ptr1 = (float*)m.ptr(i);
        float *ptr2 = (float*)t.ptr(i);

        for (int j=0;j<m.cols*m.channels();j++,ptr1++,ptr2++)
            *ptr2 = std::sin(*ptr1);
    }

    return t;
}

/*
void ImageRegistrationPOCBase::apply_bandPassFilter(float *pc, Eigen::Vector2i size, Eigen::Vector2f passband_normalized, float transitionband_N) {

    */
/**
        1. Compute a low-pass filter with cutoff frequency fL and transition band b.
        2. Compute a high-pass filter with cutoff frequency fH  and transition band b
        3. Convolve input signal with fL+fH.
     *//*

}
*/

/*
void ImageRegistrationPOCBase::apply_differentiating(float *pc, Eigen::Vector2i size) {

}

void ImageRegistrationPOCBase::apply_squaring(float *pc, Eigen::Vector2i size) {

}

void ImageRegistrationPOCBase::apply_movingAverageFilter(float *pc) {

}

void ImageRegistrationPOCBase::detect_peaks(float *pc, Eigen::Vector2f *peaks, float *peaks_val) {

}
*/


void ImageRegistrationPOCBase::calc_FFT(float *p_src, const Eigen::Vector2i &size, std::complex<float> *p_dst) {

    int rows = size(1), cols = size(0);

    if ( p_dst == nullptr)      {       std::cerr << "pointer to FFT data is null" << std::endl;    return;  }
    if ( cols < 2 || rows < 2)  {       std::cerr << "input matrix too small"      << std::endl;    return;  }


    fftwf_complex           *p_fftw3      =  (fftwf_complex*)  fftwf_malloc( sizeof( fftwf_complex ) * (cols/2+1)*rows );
    fftwf_plan              fftw_plan;

    fftw_plan  =  fftwf_plan_dft_r2c_2d ( rows, cols,  p_src,  p_fftw3, FFTW_ESTIMATE );
    fftwf_execute( fftw_plan);


    for ( int idx=0;  idx < rows;  ++idx )
        memcpy( p_dst+idx*cols, p_fftw3+idx*(cols/2+1), sizeof(float)*(cols/2+1) );


    for (int idx1 = 1; idx1 < rows; ++idx1) {
        for (int idx2 = 1; idx2 < cols/2; ++idx2)

            p_dst[(rows*cols - (idx1-1)*cols -idx2)] = p_dst[idx1*cols+idx2];
    }


    for (int idx=1; idx < cols/2; ++idx)
        p_dst[cols-idx] = p_dst[idx];
}

void ImageRegistrationPOCBase::calc_FFT(cv::Mat &src, cv::Mat &dst) {

    dst = cv::Mat(src.rows, src.cols, CV_32FC2);

    float*                  p_src   =   (float*)src.data;
    Eigen::Vector2i         size    =   Eigen::Vector2i(src.cols, src.rows);
    std::complex<float>*    p_dst   =   (std::complex<float>*)dst.data;

    calc_FFT(p_src, size, p_dst);
}

Eigen::Vector2f ImageRegistrationPOCBase::clamp(Eigen::Vector2f val, Vector2f min, Vector2f max) {

    return Eigen::Vector2f();
}

#endif //SSV_VER_0_1_IMAGEREGISTRATIONBASE_H
