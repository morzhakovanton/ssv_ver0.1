//
// Created by morzh on 06.09.18.
//

#ifndef SSV_VER_0_1_IMAGEREGISTRATIONFOURIERMELLIN_H
#define SSV_VER_0_1_IMAGEREGISTRATIONFOURIERMELLIN_H


#include <Eigen/Core>
#include <opencv2/core/eigen.hpp>

#include "ImageRegistrationPOCOptions.h"
#include "ImageRegistrationPOCBase.h"
#include "ImageRegistrationDebug.h"
#include "ImageRegistrationCoordinateFrames.h"
#include "ImageRegistrationT2POC.h"


class ImageRegistrationFourierMellin: public ImageRegistrationPOCBase {
public:


    Eigen::Matrix3d         estimate_scaleRotation      ( const cv::Mat &fixed, const cv::Mat &moved, const ImageRegistrationPOCOptions &options = ImageRegistrationPOCOptions());
    Eigen::Matrix3d         estimate_scales             (const cv::Mat &fixed, const cv::Mat &moved, const std::vector<ImageRegistrationPOCOptions> &options );

    ImageRegistrationCoordinateFrames   frames;
    ImageRegistrationT2POC              imreg_shift;
    ImageRegistrationDebug              debug;
};



Eigen::Matrix3d ImageRegistrationFourierMellin::estimate_scaleRotation(const cv::Mat &fixed, const cv::Mat &moved, const ImageRegistrationPOCOptions &options) {

    int                 rows                =   fixed.rows;
    int                 cols                =   fixed.cols;

    float               angle, scale, rotateScale_confidence, T2_shift_confidence;

    Eigen::Vector2i     size_logpolar;
    Eigen::Vector2d     theta_range, logpolar_shift, shift;
    Eigen::Matrix3d     T = Eigen::Matrix3d::Identity();
    Eigen::Matrix2d     sR;// = Eigen::Matrix3d::Identity();

    cv::Mat             fixed_fm, moved_fm;
    cv::Mat             fixed_highpass      =   cv::Mat( rows, cols, CV_32FC1);
    cv::Mat             moved_highpass      =   cv::Mat( rows, cols, CV_32FC1);
    cv::Mat             xform, moved_scaledRotated;


    calc_WFFTFiltered(fixed, fixed_highpass, options);
    calc_WFFTFiltered(moved, moved_highpass, options);

    theta_range         =   Eigen::Vector2d( 0, PI);
    size_logpolar       =   Eigen::Vector2i( fixed.cols, fixed.rows);


    frames.convert_cartesianToLogPolar( fixed_highpass, fixed_fm, size_logpolar, theta_range.cast<float>() );
    frames.convert_cartesianToLogPolar( moved_highpass, moved_fm, size_logpolar, theta_range.cast<float>() );

    logpolar_shift = imreg_shift.estimate_translation(fixed_fm, moved_fm, options, &rotateScale_confidence);

    angle           =       ( logpolar_shift(0)*180.0f ) / cols;
    scale           =       std::pow( frames.log_base, logpolar_shift(1) );
    xform           =       cv::getRotationMatrix2D( cv::Point2d(0,0), angle, scale );

    cv::cv2eigen(xform, sR);
    T.block<2,2>(0,0) = sR;

    return T;
}


Eigen::Matrix3d ImageRegistrationFourierMellin::estimate_scales(const cv::Mat &fixed, const cv::Mat &moved, const std::vector<ImageRegistrationPOCOptions> &options) {

    int                 rows                =   fixed.rows;
    int                 cols                =   fixed.cols;

    float               angle, scale1, scale2, scales_confidence, T2_confidence;

    Eigen::Vector2i     size_loglog = Eigen::Vector2i(2*512, 2*512);
    Eigen::Vector2f     theta_range, loglog_shift, shift;
    Eigen::Matrix3d     T = Eigen::Matrix3d::Identity();

    cv::Mat             fixed_half, moved_half, fixed_fm, moved_fm, xform, moved_scaled;
    cv::Mat             fixed_filtered      =   cv::Mat( rows, cols, CV_32FC1);
    cv::Mat             moved_filtered      =   cv::Mat( rows, cols, CV_32FC1);


    calc_WFFTFiltered(fixed, fixed_filtered, options[0]);
    calc_WFFTFiltered(moved, moved_filtered, options[0]);

    fixed(cv::Rect2i(0,0, fixed_filtered.cols, 0.5*fixed_filtered.rows)).copyTo(fixed_half);
    moved(cv::Rect2i(0,0, moved_filtered.cols, 0.5*moved_filtered.rows)).copyTo(moved_half);

/*
    size_loglog   << fixed.cols, fixed.rows;
    frames.convert_cartesianToLogLog( fixed, fixed_fm, size_loglog);

    cv::imshow("fixed", fixed_fm/255.0);
    cv::waitKey(-1);
*/

    frames.convert_cartesianToLogLog( fixed_half, fixed_fm, size_loglog);
    frames.convert_cartesianToLogLog( moved_half, moved_fm, size_loglog);

    Eigen::Vector2d t_loglog = imreg_shift.estimate_translation(fixed_fm, moved_fm, options[1], nullptr);
//    loglog_shift = imreg_shift.estimate_shift(fixed_fm, moved_fm, true, ImageRegistrationT2POC::Options::UPSAMPLING, opts.upsample_value,  &scales_confidence);

    scale1           =       std::pow( frames.log_base,   -t_loglog(0) );
    scale2           =       std::pow( frames.log_base_2, -t_loglog(1) );

    T(0,0)  = scale1;   T(1,1)  = scale2;

    return T;
}


#endif //SSV_VER_0_1_IMAGEREGISTRATIONFOURIERMELLIN_H
