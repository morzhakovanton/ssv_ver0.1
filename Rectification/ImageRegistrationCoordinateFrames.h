//
// Created by morzh on 7/23/17.
//

#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONCOORDINATEFRAMES_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONCOORDINATEFRAMES_H


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>
#include <iostream>

#define  PI 3.1415926535897932384626433832795f
//#define print_conv_mat

using namespace Eigen;

class ImageRegistrationCoordinateFrames{

public:

    void                convert_cartesianToPolar                    ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_resolution,  Eigen::Vector2f theta_range = Eigen::Vector2f( 0, 2*PI) );
    void                convert_polarToCartesian                    ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2f &img_dst_resolution );
    void                convert_cartesianToLogPolar                 ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_size, Eigen::Vector2f theta_range = Eigen::Vector2f(0, 2*PI)  );
    void                convert_logPolarToCartesian                 ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2f &img_dst_resolution );
    void                convert_cartesianToLogLog                   ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_size );
    void                convert_logLogToCartesian                   ( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2f &img_dst_resolution );


    void                calc_resampledImage                         ( const cv::Mat &im, const Eigen::VectorXf &theta, const Eigen::VectorXf &rho, cv::Mat &im_out);
    void                calc_logPolarParameters                     ( const Eigen::Vector2f &img_dims, Eigen::Vector2f &img_center, Eigen::Vector2f &theta_rho_steps, float &log_base);
    void                calc_logLogParameters                       ( const Eigen::Vector2f &img_dims, float& log_base, float& log_base_2);
    void                calc_logParameters                          ( const Eigen::Vector2f &img_dims, Eigen::Vector2f &img_center, Eigen::Vector2f &theta_rho_steps, float &log_base);

    float               get_colorLinearInterpolation                ( const cv::Mat &img, float x, float y);
    float               get_colorLinearInterpolation                ( const cv::Mat &img, Eigen::Vector2f pt);

    cv::Mat             imPolar, polar2cartesian;
    double              log_base, log_base_2;

};



void ImageRegistrationCoordinateFrames::convert_cartesianToLogLog(const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_resolution) {

    if ( img_src.channels() != 1)                   { std::cout << "function works only on grayscale images "   << std::endl;   return; }
    if ( img_src.rows < 2 ||  img_src.cols < 2  )   { std::cout << "source image is too small"                  << std::endl;   return; }


    float                   half_img_diag, step_rho, step_theta, pt_x, pt_y;
    Eigen::Vector2i         src_size;
    Eigen::Vector2f         src_center, img_pt;
    Eigen::Vector2i         img_src_size   = Eigen::Vector2i( img_src.cols, img_src.rows );


    img_dst     =    cv::Mat::zeros( img_dst_resolution(1), img_dst_resolution(0), CV_32FC1 );
    log_base    =    std::pow( 10.0, std::log10(img_src_size(0))/(img_dst_resolution(0)-1) );
    log_base_2  =    std::pow( 10.0, std::log10(img_src_size(1))/(img_dst_resolution(1)-1) );


    for (       int  x = 0; x < img_dst_resolution(0); x++ ) {
        for (   int  y = 0; y < img_dst_resolution(1); y++ ) {

            pt_x            =           (float) std::pow(log_base,  x)-1;
            pt_y            =           (float) std::pow(log_base_2,y)-1;

            img_dst.at<float>(y, x) =   get_colorLinearInterpolation( img_src, pt_x, pt_y );
        }
    }



}


void ImageRegistrationCoordinateFrames::convert_cartesianToPolar(const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_resolution, Eigen::Vector2f theta_range) {

    ///  image coordinate system is in X-Y format ( columns first, than rows ) ;  img_dst_resolution is  in \theta - \rho coordinate system

    if ( img_src.channels() != 1)                   { std::cout << "function works only on greyscale images "   << std::endl;   return; }
    if ( img_src.rows < 2 ||  img_src.cols < 2  )   { std::cout << "source image is too small"                  << std::endl;   return; }


    float                   half_img_diag, step_rho, step_theta, pt_x, pt_y;
    Eigen::Vector2i         src_size;
    Eigen::Vector2f         src_center, img_pt;


    half_img_diag           =           0.5*std::sqrt( img_src.rows*img_src.rows  +  img_src.cols*img_src.cols );
    step_rho                =           half_img_diag                       /   img_dst_resolution(1);
    step_theta              =           ( theta_range(1)-theta_range(0) )   /   img_dst_resolution(0);

    src_size                =           Eigen::Vector2i( img_src.cols, img_src.rows );
    src_center              =           0.5 * src_size.cast<float>();
    img_dst                 =           cv::Mat::zeros( img_dst_resolution(1), img_dst_resolution(0), CV_32FC1 );


    for (       int  c_theta = 0; c_theta <  img_dst_resolution(0); c_theta++ ) {
#ifdef  print_conv_mat
        std::cout <<  "Mm"<< c_theta <<  "=[ ";
#endif
        for (   int  c_rho   = 0; c_rho   < img_dst_resolution(1); c_rho++ ) {

            pt_x            =           src_center(0)    +      ( /*half_img_diag -*/ step_rho*c_rho ) * std::cos( step_theta*c_theta + theta_range(0)  );
            pt_y            =           src_center(1)    +      ( /*half_img_diag -*/ step_rho*c_rho ) * std::sin( step_theta*c_theta + theta_range(0)  );

#ifdef print_conv_mat
            std::cout << pt_x << ", " << pt_y << "; " ;
#endif
            img_dst.at<float>( c_rho, c_theta )     =   get_colorLinearInterpolation( img_src, pt_x, pt_y );
        }

#ifdef print_conv_mat
        std::cout <<  "]';" <<std::endl;
#endif

    }

}




void ImageRegistrationCoordinateFrames::convert_cartesianToLogPolar(const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2i &img_dst_size, Eigen::Vector2f theta_range) {

    ///  image coordinate system is in X-Y format ( columns first, than rows ) ;  img_dst_resolution is  in \theta - log(\rho) coordinate system

    if ( img_src.channels() != 1)                   { std::cout << "function works only on greyscale images "   << std::endl;   return; }
    if ( img_src.rows < 2 ||  img_src.cols < 2  )   { std::cout << "source image is too small"                  << std::endl;   return; }

    float                   img_diag, half_img_diag, step_rho, step_theta, pt_x, pt_y;
    Eigen::Vector2i         img_src_size;
    Eigen::Vector2f         src_center, img_pt;


    img_diag                =           std::sqrt( img_src.rows*img_src.rows  +  img_src.cols*img_src.cols );
    half_img_diag           =           0.5*img_diag;
    step_rho                =           half_img_diag                        / img_dst_size(1);
    step_theta              =           ( theta_range(1)-theta_range(0) )    / img_dst_size(0);

    img_src_size            =           Eigen::Vector2i( img_src.cols, img_src.rows );
    src_center              =           0.5 * img_src_size.cast<float>();
    img_dst                 =           cv::Mat::zeros( img_dst_size(1), img_dst_size(0), CV_32FC1 );

    log_base                =           std::pow( 10.0f, std::log10(img_diag)/img_src_size(0) );



    for (       int  c_theta = 0; c_theta <  img_dst_size(0); c_theta++ ) {
#ifdef  print_conv_mat
        std::cout <<  "Mm"<< c_theta <<  "=[ ";
#endif
        for (   int  c_rho   = 0; c_rho   < img_dst_size(1); c_rho++ ) {

            pt_x            =           src_center(0)    +      std::pow(log_base, ( /*half_img_diag - */c_rho) ) * std::cos( step_theta*(c_theta) );
            pt_y            =           src_center(1)    +      std::pow(log_base, ( /*half_img_diag - */c_rho) ) * std::sin( step_theta*(c_theta) );

#ifdef print_conv_mat
            std::cout << pt_x << ", " << pt_y << "; " ;
#endif
            img_dst.at<float>( c_rho, c_theta )     =   get_colorLinearInterpolation( img_src, pt_x, pt_y );
        }

#ifdef print_conv_mat
        std::cout <<  "]';" <<std::endl;
#endif

    }

}


void ImageRegistrationCoordinateFrames::calc_resampledImage(const cv::Mat &im, const Eigen::VectorXf &theta, const Eigen::VectorXf &rho, cv::Mat &im_out) {

    im_out = cv::Mat::zeros(   rho.size(),  theta.size(),CV_32FC1);

    cv::Mat im_copy;

    im.convertTo(im_copy, CV_32FC1);

//    show_imageScaled("SSS", im);
//    std::cout << im_copy << std::endl;

    for (int col = 0; col < theta.size(); ++col) {
        for (int row = 0; row < rho.size(); ++row) {

            cv::Vec2f coords            =   polar2cartesian.at<cv::Vec2f>(row, col);
            im_out.at<float>(row, col)  = get_colorLinearInterpolation(im_copy, coords(0), coords(1));
        }
    }
//    std::cout << im_out << std::endl;
}

float ImageRegistrationCoordinateFrames::get_colorLinearInterpolation(const cv::Mat &img, float x, float y) {

    int     width   =   img.cols;
    int     height  =   img.rows;

    if ( (x < 0) || (x > width-1) || (y < 0) || (y > height-1) ) return 0.0f;

    cv::Mat patch;
    cv::getRectSubPix( img, cv::Size(1,1), cv::Point2f(x, y), patch );

    return patch.at<float>(0,0);
}


void ImageRegistrationCoordinateFrames::calc_logLogParameters(const Eigen::Vector2f &img_dims, float &log_base, float &log_base_2) {

    /**
     * log_base is in X direction, log_base_2 in Y
     * */

    log_base                =   std::pow( 10, std::log10(img_dims(0))/(img_dims(0)-1) );
    log_base_2              =   std::pow( 10, std::log10(img_dims(1))/(img_dims(1)-1) );

}



void ImageRegistrationCoordinateFrames::calc_logPolarParameters(const Eigen::Vector2f &img_dims, Eigen::Vector2f &img_center, Eigen::Vector2f &theta_rho_steps, float &log_base) {

/**
 image dimensions are in X-Y format (columns first, rows after ), image center is in X-Y coordinates too
 */
    float d;

    img_center              =   0.5*img_dims;
    d                       =   (img_dims-img_center).norm();

    theta_rho_steps(0)      =   PI / img_dims(1);
    theta_rho_steps(1)      =   d/img_center(0);

    log_base                =   std::pow( 10, std::log10(d)/img_dims(0) );

}

void ImageRegistrationCoordinateFrames::convert_polarToCartesian( const cv::Mat &img_src, cv::Mat &img_dst, const Eigen::Vector2f &img_dst_resolution ) {

    ///  image coordinate system is in X-Y format ( columns first, than rows ) ;  img_dst_resolution is  in \theta - \rho coordinate system

    if ( img_src.channels() != 1)                   { std::cout << "function works only on greyscale images "   << std::endl;   return; }
    if ( img_src.rows < 2 ||  img_src.cols < 2  )   { std::cout << "source image is too small"                  << std::endl;   return; }


    float                   half_img_diag, step_rho, step_theta, pt_x, pt_y;
    Eigen::Vector2i         src_size;
    Eigen::Vector2f         src_center, img_pt;


    half_img_diag           =           0.5*std::sqrt( img_src.rows*img_src.rows  +  img_src.cols*img_src.cols );
    step_rho                =           half_img_diag   / img_dst_resolution(1);
    step_theta              =           2*PI            / img_dst_resolution(0);

    src_size                =           Eigen::Vector2i( img_src.cols, img_src.rows );
    src_center              =           0.5 * src_size.cast<float>();
    img_dst                 =           cv::Mat::zeros( img_dst_resolution(1), img_dst_resolution(0), CV_32FC1 );


}


#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONCOORDINATEFRAMES_H
