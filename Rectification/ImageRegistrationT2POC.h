//
// Created by morzh on 6/29/17.
//

/**
    using papers:
    "Efficient subpixel image registration algorithms"          by Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup,  Opt. Lett. 33,  % 156-158 (2008)
    "Extension of Phase Correlation to Subpixel Registration"   by Hassan Foroosh, Josiane B. Zerubia, Marc Berthod
*/


#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONT2POC_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONT2POC_H

#include <cmath>
#include <algorithm>
#include <cstdint>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>

#include <complex.h>
#include <fftw3.h>
#include <iostream>
#include "ImageRegistrationPOCBase.h"
#include "ImageRegistrationDebug.h"
#include "ImageRegistrationStatistics.h"
#include "ImageRegistrationPOCOptions.h"


#define  errors_check


typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>    MatrixType;
//typedef Eigen::Map<MatrixType>                                  MapType;



struct ImageRegistrationT2POCData{

    Eigen::Matrix3d     T;
    Eigen::Vector2d     t;
    double              conf;

};

class ImageRegistrationT2POC :   ImageRegistrationPOCBase{

public:
    enum class Options { UPSAMPLING, SIDESINTERP };

    Eigen::Vector2d estimate_translation(const cv::Mat &fixed, const cv::Mat &moved, ImageRegistrationPOCOptions options, float *conf=nullptr);
    /// int the following methods size is in  X-Y  coordinates (not row-column coordinates)
    void                    calc_crossPowerSpectrum                 ( const cv::Mat &fixed, const cv::Mat &moved, std::complex<float> *cps, const Eigen::Vector2i &cps_size);

    void                    find_maximum                            ( float *pc, Eigen::Vector2i size, Eigen::Vector2f& argmax, float *valmax= nullptr);

    void                    find_peaksQRS                           ( float *pc, Eigen::Vector2i &size, Eigen::Vector2f *peaks, float *peaks_val);
    void                    find_peak                               ( std::complex<float> *phasecorr, const Eigen::Vector2i &size, Eigen::Vector2i &peak);
    void                    find_peak                               ( float *phase_corr, const Eigen::Vector2i &size, Eigen::Vector2i &peak);

    void                    choose_fromFourPeaks                    ( const cv::Mat &fixed, const cv::Mat &moved, const Eigen::Vector2f& peak, Eigen::Vector2f& subpix_shift );
    void                    refine_shiftByUpsampling                ( std::complex<float> *cps, Eigen::Vector2i &cps_size, Eigen::Vector2f &peak, Eigen::Vector2f &peak_upsampled, int upsampling_factor);
    void                    refine_shiftByInterpolation             ( float *phasecorr, const Eigen::Vector2i &phasecorr_size, Eigen::Vector2f &peak_idx, const cv::Mat &fixed, const cv::Mat &moved);
    void                    calc_IDFT2DUpsampling                   ( std::complex<float> *cps, const Eigen::Vector2i &cps_size, const Eigen::Vector2f &point, int upsampling_factor,
                                                                      float vicinity, std::complex<float> *&cps_upsampled, Eigen::Vector2i &cps_upsampled_size, Eigen::Vector2i &center );
    int                     calc_maxFromFloors                      ( float val1, float val2);
    void                    calc_phaseCorrelation                   ( std::complex<float> *cps, const Eigen::Vector2i &size, float *pc) const;
    float                   calc_conf                               ( float *pc, Eigen::Vector2i pc_size, float* maxval = nullptr);
    float                   calc_clamp                              ( float lower, float upper, float value);

    Matrix3d make_transformFromTranslate(const Eigen::Vector2d &t);
};


void ImageRegistrationT2POC::calc_phaseCorrelation(std::complex<float> *cps, const Eigen::Vector2i &size, float *pc) const {

    fftwf_complex*          fftwf_cps               =   reinterpret_cast<fftwf_complex*> ( cps );
    fftwf_complex*          fftwf_phasecorr         =   (fftwf_complex*)  fftwf_malloc( sizeof( fftwf_complex ) * size(1)*size(0)  );
    std::complex<float>*    phasecorr_              =   reinterpret_cast<std::complex<float>*> ( fftwf_phasecorr );

    fftwf_plan              fftw_plan               =   fftwf_plan_dft_2d                ( size(1), size(0),  fftwf_cps,  fftwf_phasecorr,  FFTW_BACKWARD, FFTW_ESTIMATE );

    fftwf_execute           ( fftw_plan);
    fftwf_destroy_plan      ( fftw_plan);

    for (int i = 0; i < size(0) * size(1); ++i) {

        pc[i] = std::abs( phasecorr_[i]);
    }


    fftwf_free(fftwf_phasecorr);

//    debug.print_float_array_2d( pc, size(0), size(1) );
//    debug.write_PythonNumpy("/home/morzh/temp/powerSpectrum.txt", pc, size);
}



void ImageRegistrationT2POC::find_maximum(float *pc, Eigen::Vector2i size, Eigen::Vector2f& argmax, float *valmax) {

    int         max1_idx(0);
    int         x0, y0;
    float       peak_val = pc[0];

    for (int idx = 1; idx < size(0) * size(1); ++idx) {
        if (   peak_val <  pc[idx]  ){
            max1_idx        =   idx;
            peak_val        =   pc[idx];
        }
    }

    y0 = max1_idx/size(0);
    x0  = max1_idx  -  y0*size(0);

    argmax[0] = x0;
    argmax[1] = y0;

    if (valmax)
        *valmax = peak_val;

//    std::cout << "peak value  is: " << peak_val << " at: " << max1_idx/size(0) << " "  << max1_idx  - y0*size(0) << std::endl;
}


void ImageRegistrationT2POC::find_peaksQRS(float *pc, Eigen::Vector2i &size, Eigen::Vector2f *peaks, float *peaks_val) {
    ///!!! https://dsp.stackexchange.com/questions/1302/peak-detection-approach, using https://en.wikipedia.org/wiki/Daubechies_wavelet !!!///

    int low_freq, high_freq;

//    apply_bandPassFilter(pc, size, low_freq, 0);
//    apply_differentiating     (pc, size );
//    apply_squaring            (pc, size );
//    apply_movingAverageFilter (pc);
//    detect_peaks              (pc, peaks, peaks_val);
}


void ImageRegistrationT2POC::find_peak(std::complex<float> *phasecorr, const Eigen::Vector2i &size, Eigen::Vector2i &peak) {

    int     max_idx(0), y, x;

    for (int idx = 1; idx < size(0) * size(1); ++idx) {

        if (    phasecorr[max_idx].real()  < phasecorr[idx].real() ) {       max_idx =  idx;       }

        std::cout << phasecorr[max_idx].real()  << " " <<  phasecorr[idx].real() << std::endl;
    }

    y       =   max_idx/size(0);
    x       =   max_idx - y*size(0);
    peak    =   Eigen::Vector2i(x,y);
}


void ImageRegistrationT2POC::find_peak( float *phase_corr, const Eigen::Vector2i &size, Eigen::Vector2i &peak) {

    int     max_idx(0);

    for (int idx = 1; idx < size(0) * size(1); ++idx) {

        if (    phase_corr[max_idx]  < phase_corr[idx]) {       max_idx =  idx;       }

//        std::cout << phase_corr[max_idx]  << " " <<  phase_corr[idx] << std::endl;
    }

    int     y   =   max_idx/size(0);
    int     x   =   max_idx -  y*size(0);

    peak = Eigen::Vector2i(x,y);

}


void ImageRegistrationT2POC::refine_shiftByUpsampling(std::complex<float> *cps, Eigen::Vector2i &cps_size, Eigen::Vector2f &peak, Eigen::Vector2f &peak_upsampled,  int upsampling_factor) {

    Eigen::Vector2f         shift_upsampled;
    Eigen::Vector2i         peak_upsampled_int;

    std::complex<float>     *phasecorr_upsampled    =   nullptr;
    float                   *phasecorr_upsampled_abs=   nullptr;
    Eigen::Vector2i         center_upsampled,  phasecorr_upsampled_size;



    calc_IDFT2DUpsampling   ( cps, cps_size, peak, upsampling_factor, 0.75, phasecorr_upsampled, phasecorr_upsampled_size, center_upsampled );

//    debug.print_complexf_array_2d( cps, cps_size(0), cps_size(1) );
//    debug.print_complexf_array_2d( phasecorr_upsampled, phasecorr_upsampled_size(0), phasecorr_upsampled_size(1) );

    phasecorr_upsampled_abs     =   new float [  sizeof( float ) * phasecorr_upsampled_size(0)*phasecorr_upsampled_size(1) ];

    for (int idx = 0; idx < phasecorr_upsampled_size(0)*phasecorr_upsampled_size(1); ++idx) {      phasecorr_upsampled_abs[idx] =  std::abs(  phasecorr_upsampled[idx] );        }

    find_peak               ( phasecorr_upsampled_abs, phasecorr_upsampled_size, peak_upsampled_int );

//    debug.print_complexf_array_2d ( phasecorr_upsampled, phasecorr_upsampled_size(1), phasecorr_upsampled_size(0) );
//    debug.print_float_array_2d ( phasecorr_upsampled_abs, phasecorr_upsampled_size(1), phasecorr_upsampled_size(0) );


    peak_upsampled      =   ( peak_upsampled_int.cast<float>()   - center_upsampled.cast<float>() ) / (float) upsampling_factor;

    if (  peak(0) >  cps_size(0)/2 ){    peak(0) -= cps_size(0);    }
    if (  peak(1) >  cps_size(1)/2 ){    peak(1) -= cps_size(1);    }

    peak += peak_upsampled;

}

void ImageRegistrationT2POC::calc_IDFT2DUpsampling(std::complex<float> *cps, const Eigen::Vector2i &cps_size, const Eigen::Vector2f &point, int upsampling_factor, float vicinity,
                                                   std::complex<float> *&cps_upsampled, Eigen::Vector2i &cps_upsampled_size, Eigen::Vector2i &center) {


//    debug.print_complexf_array_2d(cps, 32,32);

    int                     vicinity_val_upsampled;
    Eigen::Matrix2Xi        vicinity_ups;

//    debug.print_complexf_array_2d(cps, cps_size(1), cps_size(0));

    vicinity_val_upsampled              =       (int) ( vicinity*upsampling_factor ) ;

    vicinity_ups.conservativeResize( Eigen::NoChange, 2*vicinity_val_upsampled+1);

    for (int idx = 0; idx < vicinity_ups.cols(); ++idx) {

        vicinity_ups(0,idx) =  upsampling_factor * point(1) - vicinity_val_upsampled + idx;
        vicinity_ups(1,idx) =  upsampling_factor * point(0) - vicinity_val_upsampled + idx;
    }


//    std::cout << vicinity_ups << std::endl;


    Eigen::Vector2i size_array                          =       cps_size;
    Eigen::Vector2i size_upsampled                      =       upsampling_factor*cps_size;
    Eigen::Vector2i  half_size_array                    =       cps_size/2;
    float mult                                          =       (float)  1 / ( 2*upsampling_factor*size_array(0)*size_array(1) )  ;


    Eigen::VectorXf  half_array_left1, half_array_left2, half_array_right1, half_array_right2;

    half_array_left1.resize (  half_size_array(0) );
    half_array_left2.resize (  half_size_array(0) );

    half_array_right1.resize(  half_size_array(1) );
    half_array_right2.resize(  half_size_array(1) );


    center          =       Eigen::Vector2i ( (int)(0.5*vicinity_ups.cols()), (int )( 0.5*vicinity_ups.cols() ) );

    for (int idx = 0; idx < half_size_array(0); ++idx) {

        half_array_left1(idx) = idx;
        half_array_left2(idx) = half_size_array(0)+size_array(0)*(upsampling_factor-1) + idx;
    }

    for (int idx = 0; idx < half_size_array(1); ++idx) {

        half_array_right1(idx) = idx;
        half_array_right2(idx) = half_size_array(1)+size_array(1)*(upsampling_factor-1) + idx;
    }


    Eigen::MatrixXf arr_left1, arr_left2, arr_right1, arr_right2;

    arr_left1.resize(vicinity_ups.cols(), half_array_left1.size() );
    arr_left2.resize(vicinity_ups.cols(), half_array_left2.size() );

    arr_right1.resize(vicinity_ups.cols(), half_array_right1.size() );
    arr_right2.resize(vicinity_ups.cols(), half_array_right2.size() );


    arr_left1                           =      ( vicinity_ups.row(0).transpose() ).cast<float>() * half_array_left1.transpose();
    arr_left2                           =      ( vicinity_ups.row(0).transpose() ).cast<float>() * half_array_left2.transpose();

    arr_right1                          =      ( vicinity_ups.row(1).transpose() ).cast<float>() * half_array_right1.transpose();
    arr_right2                          =      ( vicinity_ups.row(1).transpose() ).cast<float>() * half_array_right2.transpose();


    Eigen::MatrixXcf  exp_ups_left1, exp_ups_left2,  exp_ups_right1, exp_ups_right2;

    exp_ups_left1.resize  ( arr_left1.rows(),  arr_left1.cols()  );
    exp_ups_left2.resize  ( arr_left2.rows(),  arr_left2.cols()  );
    exp_ups_right1.resize ( arr_right1.rows(), arr_right1.cols() );
    exp_ups_right2.resize ( arr_right2.rows(), arr_right2.cols() );



    for (    int idx1 = 0; idx1 < exp_ups_left1.rows(); ++idx1) {
        for (int idx2 = 0; idx2 < exp_ups_left1.cols(); ++idx2) {

            exp_ups_left1(idx1, idx2) = std::exp( arr_left1(idx1, idx2)  *  std::complex<float>( 0, 2*PI/ size_upsampled(0))  );
        }
    }


    for (    int idx1 = 0; idx1 < exp_ups_left1.rows(); ++idx1) {
        for (int idx2 = 0; idx2 < exp_ups_left1.cols(); ++idx2) {

            exp_ups_left2(idx1, idx2) = std::exp( arr_left2(idx1, idx2)  *  std::complex<float>( 0, 2*PI/ size_upsampled(0))  );
        }
    }

    for (    int idx1 = 0; idx1 < exp_ups_right1.rows(); ++idx1) {
        for (int idx2 = 0; idx2 < exp_ups_right1.cols(); ++idx2) {

            exp_ups_right1(idx1, idx2) = std::exp( arr_right1(idx1, idx2)  *  std::complex<float>( 0, 2*PI/ size_upsampled(1) )  );
        }
    }


    for (    int idx1 = 0; idx1 < exp_ups_right2.rows(); ++idx1) {
        for (int idx2 = 0; idx2 < exp_ups_right2.cols(); ++idx2) {

            exp_ups_right2(idx1, idx2) = std::exp( arr_right2(idx1, idx2)  *  std::complex<float>( 0, 2*PI/ size_upsampled(1) )  );
        }
    }


    float       upsampling_factor_sqr = (float)upsampling_factor*upsampling_factor;

    Eigen::MatrixXcf  arr_left_up,  arr_left_down, arr_right_up, arr_right_down;

    arr_left_up.resize   ( half_size_array(0), half_size_array(1) );
    arr_left_down.resize ( half_size_array(0), half_size_array(1) );
    arr_right_up.resize  ( half_size_array(0), half_size_array(1) );
    arr_right_down.resize( half_size_array(0), half_size_array(1) );

    for (       int idx1 = 0; idx1 < half_size_array(0); ++idx1) {
        for (   int idx2 = 0; idx2 < half_size_array(1); ++idx2) {

            arr_left_up   ( idx1, idx2 )    =   upsampling_factor_sqr  * cps[   idx1*cps_size(0)                           + idx2 ] ;
            arr_left_down ( idx1, idx2 )    =   upsampling_factor_sqr  * cps[ ( idx1+ half_size_array(1) )*cps_size(0)     + idx2 ] ;
            arr_right_up  ( idx1, idx2 )    =   upsampling_factor_sqr  * cps[   idx1*cps_size(0)                           + idx2  + half_size_array(1)] ;
            arr_right_down( idx1, idx2 )    =   upsampling_factor_sqr  * cps[ ( idx1+ half_size_array(1) )*cps_size(0)     + idx2  + half_size_array(1)] ;

        }
    }



    Eigen::MatrixXcf    exp_left1           =       exp_ups_left1* arr_left_up + exp_ups_left2 *  arr_left_down;
    Eigen::MatrixXcf    exp_left2           =       exp_ups_left1* arr_right_up + exp_ups_left2 *  arr_right_down;
    Eigen::MatrixXcf    upsampled           =       mult  * (  exp_left1  * exp_ups_right1.transpose() +  exp_left2 *  exp_ups_right2.transpose()  );
    Eigen::MatrixXcf    upsampled_t         =       upsampled.transpose();


    if ( cps_upsampled == nullptr){

        cps_upsampled =  new std::complex<float> [  sizeof( std::complex<float> ) * upsampled_t.rows()*upsampled_t.cols() ];
    }

    cps_upsampled_size = Eigen::Vector2i( upsampled_t.cols(), upsampled_t.rows() );

    memcpy(  cps_upsampled, upsampled_t.data(), sizeof(std::complex<float>)* upsampled_t.rows()*upsampled_t.cols() );
}



int ImageRegistrationT2POC::calc_maxFromFloors(float val1, float val2) {

    return std::max( (int)  std::floor(val1),  (int)std::floor(val2));
}


void ImageRegistrationT2POC::calc_crossPowerSpectrum(const cv::Mat &fixed, const cv::Mat &moved, std::complex<float> *cps, const Eigen::Vector2i &cps_size) {

    int rows = cps_size(1);
    int cols = cps_size(0);

    float                   abs ;
    float                   *p_moved       =  (float*)          fixed.data;
    float                   *p_fixed       =  (float*)          moved.data;
    fftwf_complex           *p_moved_dft   =  (fftwf_complex*)  fftwf_malloc( sizeof( fftwf_complex ) * rows*(cols/2+1) );
    fftwf_complex           *p_fixed_dft   =  (fftwf_complex*)  fftwf_malloc( sizeof( fftwf_complex ) * rows*(cols/2+1) );

    std::complex<float>*    fft_fixed      =   new std::complex<float>[rows*cols];
    std::complex<float>*    fft_moved      =   new std::complex<float>[rows*cols];

    fftwf_plan              fftw_plan;

    /// calc FFT from moved and fixed images (2D arrays)

    fftw_plan  =  fftwf_plan_dft_r2c_2d ( rows, cols,  p_moved,  p_moved_dft, FFTW_ESTIMATE );
    fftwf_execute( fftw_plan);

    fftw_plan  =  fftwf_plan_dft_r2c_2d ( rows, cols,  p_fixed,  p_fixed_dft, FFTW_ESTIMATE );
    fftwf_execute( fftw_plan);

/*
    debug.print_cvMat(fixed);
    debug.print_cvMat(moved);
    debug.print_fftwf_complex_array_r2c_2d ( p_moved_dft, 32, 32 );
    debug.print_fftwf_complex_array_r2c_2d ( p_fixed_dft, 32, 32 );
    debug.print_fftwf_complex_array_2d ( p_fixed_dft, 32, 32 );
*/


    for (int idx = 0; idx < rows; ++idx) {

        memcpy( fft_fixed+idx*cols, p_fixed_dft+idx*(cols/2+1), sizeof(std::complex<float>)*(cols/2+1) );
        memcpy( fft_moved+idx*cols, p_moved_dft+idx*(cols/2+1), sizeof(std::complex<float>)*(cols/2+1) );
    }


    for (int idx1 = 1; idx1 < rows; ++idx1) {
        for (int idx2 = 1; idx2 < cols/2; ++idx2) {

            fft_moved[(rows*cols - (idx1-1)*cols -idx2)] = std::conj(fft_moved[idx1*cols+idx2]);
            fft_fixed[(rows*cols - (idx1-1)*cols -idx2)] = std::conj(fft_fixed[idx1*cols+idx2]);
        }
    }


    for (int idx=1; idx < cols/2; ++idx){

        fft_moved[cols-idx] = std::conj(fft_moved[idx]);
        fft_fixed[cols-idx] = std::conj(fft_fixed[idx]);
    }

/*
    debug.print_complexf_array_2d(fft_moved, 32,32);
    debug.print_complexf_array_2d(fft_fixed, 32,32);
*/

    for (int idx = 0; idx < rows * cols; ++idx) {

        cps[idx]  =  fft_fixed[idx] * std::conj( fft_moved[idx] );
    }


    for (int idx = 0; idx < rows * cols; ++idx) {

        abs  = std::abs ( cps[idx] );
        if ( abs > 1e-7){    cps[idx] /= abs;  }
    }



    fftwf_destroy_plan(fftw_plan);

    delete [] fft_fixed;
    delete [] fft_moved;

    fftwf_free(p_moved_dft);
    fftwf_free(p_fixed_dft);
}



void
ImageRegistrationT2POC::refine_shiftByInterpolation(float *phasecorr, const Eigen::Vector2i &phasecorr_size, Eigen::Vector2f &peak_idx, const cv::Mat &fixed,     const cv::Mat &moved) {

    /**
     *  uisng paper High-Accuracy Sub-pixel Motion Estimation from Noisy Images in Fourier Domain
     *  by
     *  Jinchang Ren, Jianmin Jiang and Theodore Vlachos
    */


//    debug.print_complexf_array_2d(phasecorr, 32,32);


    Eigen::Vector2i     peak_int,  peak_int_p1,  peak_int_m1;
    Eigen::Vector2i     xm1_y, xp1_y, x_ym1, x_yp1;

    peak_int            =       Eigen::Vector2i( std::round(peak_idx(0)), std::round(peak_idx(1)) );

    xm1_y               =       Eigen::Vector2i( peak_int(0)-1, peak_int(1)   );
    xp1_y               =       Eigen::Vector2i( peak_int(0)+1, peak_int(1)   );
    x_ym1               =       Eigen::Vector2i( peak_int(0)  , peak_int(1)-1 );
    x_yp1               =       Eigen::Vector2i( peak_int(0)  , peak_int(1)+1 );

    peak_int_m1(0)      =       ( peak_int(0)-1+32 ) % 32;
    peak_int_m1(1)      =       ( peak_int(1)-1+32 ) % 32;
    peak_int_p1(0)      =       ( peak_int(0)+1+32 ) % 32;
    peak_int_p1(1)      =       ( peak_int(1)+1+32 ) % 32;


    int a               =       peak_int(1)    * phasecorr_size(0) + peak_int(0)    ;
    int b               =       peak_int_m1(1) * phasecorr_size(0) + peak_int(0)    ;
    int c               =       peak_int_p1(1) * phasecorr_size(0) + peak_int(0)    ;
    int d               =       peak_int(1)    * phasecorr_size(0) + peak_int_m1(0) ;
    int e               =       peak_int(1)    * phasecorr_size(0) + peak_int_p1(0) ;


    float                   peak_val                =       phasecorr[ a ];
    float                   peak_y_m1               =       phasecorr[ b ];
    float                   peak_y_p1               =       phasecorr[ c ];
    float                   peak_x_m1               =       phasecorr[ d ];
    float                   peak_x_p1               =       phasecorr[ e ];


    Eigen::Vector2f         dPeakNeighbours         =       Eigen::Vector2f ( peak_x_p1 - peak_x_m1, peak_y_p1-peak_y_m1 );
    Eigen::Vector2f         dPeakNeighbours_den     =       dPeakNeighbours.cwiseAbs() + Eigen::Vector2f( peak_val, peak_val);
    Eigen::Vector2f         delta                   =       Eigen::Vector2f(  dPeakNeighbours(0)/ dPeakNeighbours_den(0),  dPeakNeighbours(1)/ dPeakNeighbours_den(1) );

    if ( peak_x_p1 / peak_val > 0.85 &&  peak_x_p1 / peak_val < 1 ) delta(0) = 0.5;
    if ( peak_y_p1 / peak_val > 0.85 &&  peak_y_p1 / peak_val < 1 ) delta(1) = 0.5;

    if ( peak_x_m1 / peak_val > 0.85 &&  peak_x_m1 / peak_val < 1 ) delta(0) = -0.5;
    if ( peak_y_m1 / peak_val > 0.85 &&  peak_y_m1 / peak_val < 1 ) delta(1) = -0.5;

    if (  peak_idx(0) >  phasecorr_size(0)/2 ){    peak_idx(0) -= phasecorr_size(0);    }
    if (  peak_idx(1) >  phasecorr_size(1)/2 ){    peak_idx(1) -= phasecorr_size(1);    }

//    std::cout << "subpix delta: " << delta.transpose() << std::endl;
    choose_fromFourPeaks( fixed, moved,  peak_idx, delta);

    peak_idx = delta;
}



void ImageRegistrationT2POC::choose_fromFourPeaks(const cv::Mat &fixed, const cv::Mat &moved, const Eigen::Vector2f& peak, Eigen::Vector2f &subpix_shift) {


#ifdef errors_check
    if ( fixed.rows != moved.rows || fixed.cols != moved.cols ){  std::cout << "matrices are not of the same size" << std::endl; }
#endif

    Eigen::Vector2i             size_image = Eigen::Vector2i( fixed.cols, fixed.rows );

    cv::Mat                     xform1           = cv::Mat::eye( 2, 3, CV_32FC1);
    cv::Mat                     xform2           = cv::Mat::eye( 2, 3, CV_32FC1);
    cv::Mat                     xform3           = cv::Mat::eye( 2, 3, CV_32FC1);
    cv::Mat                     xform4           = cv::Mat::eye( 2, 3, CV_32FC1);
    cv::Mat                     check, mat_ad;
    Eigen::Vector2f             peaks_to_choose[4];
    Eigen::Vector2i             peak_int;
    float                       sad[4];
    int                         max_sad_idx(0);

    if (  peak(0) >  0.5*size_image(0) ){    peak_int(0) = peak(0) - size_image(0);    } else {     peak_int(0) = peak(0);  }
    if (  peak(1) >  0.5*size_image(1) ){    peak_int(1) = peak(1) - size_image(1);    } else {     peak_int(1) = peak(1);  }


    peaks_to_choose[0](0) = peak_int(0) - subpix_shift(0);    peaks_to_choose[0](1) = peak_int(1) - subpix_shift(1);
    peaks_to_choose[1](0) = peak_int(0) - subpix_shift(0);    peaks_to_choose[1](1) = peak_int(1) + subpix_shift(1);
    peaks_to_choose[2](0) = peak_int(0) + subpix_shift(0);    peaks_to_choose[2](1) = peak_int(1) - subpix_shift(1);
    peaks_to_choose[3](0) = peak_int(0) + subpix_shift(0);    peaks_to_choose[3](1) = peak_int(1) + subpix_shift(1);




    cv::Rect                    rect1( calc_maxFromFloors( peaks_to_choose[0](0), 0 ),    calc_maxFromFloors( peaks_to_choose[0](1), 0 ),
                                       fixed.cols - std::abs( peaks_to_choose[0](0) ),    fixed.rows - std::abs( peaks_to_choose[0](1) ) );
    cv::Rect                    rect2( calc_maxFromFloors( peaks_to_choose[1](0), 0 ),    calc_maxFromFloors( peaks_to_choose[1](1), 0 ),
                                       fixed.cols - std::abs( (int)peaks_to_choose[1](0) ),   fixed.rows - std::abs( (int)peaks_to_choose[1](1) ) );
    cv::Rect                    rect3( calc_maxFromFloors( peaks_to_choose[2](0), 0 ),    calc_maxFromFloors( peaks_to_choose[2](1), 0 ),
                                       fixed.cols - std::abs( peaks_to_choose[2](0) ),    fixed.rows - std::abs( peaks_to_choose[2](1) ) );
    cv::Rect                    rect4( calc_maxFromFloors( peaks_to_choose[3](0), 0 ),    calc_maxFromFloors( peaks_to_choose[3](1), 0 ),
                                       fixed.cols - std::abs( (int)peaks_to_choose[3](0) ),   fixed.rows - std::abs( (int)peaks_to_choose[3](1) ) );


    xform1.at<float>(0,2) = peaks_to_choose[0](0);              xform1.at<float>(1,2) = peaks_to_choose[0](1);
    xform2.at<float>(0,2) = peaks_to_choose[1](0);              xform2.at<float>(1,2) = peaks_to_choose[1](1);
    xform3.at<float>(0,2) = peaks_to_choose[2](0);              xform3.at<float>(1,2) = peaks_to_choose[2](1);
    xform4.at<float>(0,2) = peaks_to_choose[3](0);              xform4.at<float>(1,2) = peaks_to_choose[3](1);


    cv::warpAffine  ( fixed, check,  xform1,  fixed.size()  );
    cv::absdiff     ( moved , check, mat_ad);

    sad[0] = (float)1/(rect1.width * rect1.height) * cv::sum( mat_ad(rect1) )[0];

    cv::warpAffine  ( fixed, check,  xform2,  fixed.size()  );
    cv::absdiff     ( moved , check, mat_ad );

    sad[1] = (float)1/(rect2.width * rect2.height) * cv::sum( mat_ad(rect2) )[0];

    cv::warpAffine  ( fixed, check,  xform3,  fixed.size()  );
    cv::absdiff     ( moved , check, mat_ad);

    sad[2] = (float)1/(rect3.width * rect3.height) * cv::sum( mat_ad(rect3) )[0];

    cv::warpAffine  ( fixed, check,  xform4,  fixed.size()  );
    cv::absdiff     ( moved , check, mat_ad );

    sad[3] = (float)1/(rect4.width * rect4.height) * cv::sum( mat_ad(rect4) )[0];


    for ( int idx=1; idx<4; idx++){      if ( sad[max_sad_idx] > sad[idx] ) max_sad_idx = idx;     }


    subpix_shift = peaks_to_choose[max_sad_idx];
}


float ImageRegistrationT2POC::calc_clamp(float lower, float upper, float value) {

    float clamped_value;

    clamped_value   =   value < lower           ?  lower : value;
    clamped_value   =   clamped_value > upper   ?  upper : clamped_value ;

    return  clamped_value;
}



Eigen::Vector2d ImageRegistrationT2POC::estimate_translation(const cv::Mat &fixed, const cv::Mat &moved, ImageRegistrationPOCOptions options, float *conf) {


#ifdef errors_check
    if ( fixed.type() != 5  || moved.type() != 5 )                   { std::cerr << "input images should be float grayscaled" << std::endl;   return  Eigen::Vector2d::Identity();  }
    if ( fixed.rows   != moved.rows || fixed.cols !=  moved.cols )   { std::cerr << "both images should have the same size  " << std::endl;   return  Eigen::Vector2d::Identity();  }
#endif


    Eigen::Vector2f                 argmax;
    float                           valmax;
    Eigen::Vector2f                 peak_upsampled;
    float                           confidence;
    Eigen::Matrix3d                 T =  Eigen::Matrix3d::Identity();


    int                             rows            =       fixed.rows;
    int                             cols            =       fixed.cols;
    Eigen::Vector2i                 sz              =       Eigen::Vector2i(fixed.cols, fixed.rows ); /// size in X-Y coordinates
    std::complex<float>*            cps             =       new std::complex<float>[rows*cols];
    float*                          pc              =       new float[rows*cols];
    cv::Mat                         window          =       cv::Mat(fixed.rows, fixed.cols,  CV_32FC1);
    cv::Mat                         fixed_windowed, moved_windowed;
    cv::Mat                         fixed_gabor, moved_gabor;



    if ( options.use_Gabor_filter){

        apply_GaborFilter(fixed, fixed_gabor, options.gabor_options);
        apply_GaborFilter(moved, moved_gabor, options.gabor_options);
    } else{
        fixed_gabor = fixed;
        moved_gabor = moved;
    }


/*
    std::cout << fixed_gabor << std::endl;
    debug.show_imageScaled("Gabor fixed", fixed_gabor);
    debug.show_imageScaled("Gabor moved", moved_gabor);
*/

    switch (options.fft_window){

        case ImageRegistrationPOCOptions::FFTWindow::NONE:
            window.setTo(1.0);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::BLACKMAN:
            create_BlackmanWindow2D(window);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::HAMMING:
            create_HammingWindow2D(window);
            break;
        case ImageRegistrationPOCOptions::FFTWindow::TUKEY:
            create_TukeyWindow2D(window);
            break;
    }


    fixed_windowed = window.mul(fixed_gabor);
    moved_windowed = window.mul(moved_gabor);

    calc_crossPowerSpectrum         (fixed_windowed, moved_windowed, cps, sz);
    calc_phaseCorrelation           (cps, sz, pc      );
    find_maximum                    (pc, sz, argmax, &valmax);


    switch ( options.fft_interpolation ){
        case ImageRegistrationPOCOptions::FFTInterpolation::UPSAMPLING:
            refine_shiftByUpsampling(cps, sz, argmax, peak_upsampled, options.upsample_value);     /// refine estimate to subpixel precision
            break;
        case ImageRegistrationPOCOptions::FFTInterpolation::SIDESINTERP:
            refine_shiftByInterpolation     (pc, sz, argmax, fixed, moved);           /// refine estimate to subpixel precision
            break;
    }


//    T(0,2) = -argmax(0);
//    T(1,2) = -argmax(1);

    if (  options.calc_confidence && conf)
        *conf = calc_conf(pc, sz, &valmax);

    return argmax.cast<double>();
}

float ImageRegistrationT2POC::calc_conf(float *pc, Eigen::Vector2i pc_size, float* maxval) {

    if (!pc)
        return  0;

    float sum=0;
    int   wh    =  pc_size(0) * pc_size(1);

    for (int idx = 0; idx < wh; ++idx) {
        sum += pc[idx];
    }

    if (maxval)
        return  (*maxval)/sum;


    float max = pc[0];
    for (int idx = 1; idx < wh; ++idx) {
        if (max < pc[idx])
            max = pc[idx];
    }

    return max/sum;
}

Matrix3d ImageRegistrationT2POC::make_transformFromTranslate(const Eigen::Vector2d &t) {

    Eigen::Matrix3d T = Eigen::Matrix3d::Identity();
    T(0,2) = t(0);    T(1,2) = t(1);
    return T;
}


#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONT2POC_H
