
#include "SatelliteImagePair/SatelliteImagePair.h"
#include <Rectification/AffineRectificationRPC.h>
#include <Rectification/ImageRegistrationPOC.h>
#include <Disparity/DisparityBox.h>
#include "OpticalFlow/OpticalFlowPOC.h"

int main(){

    std::string                 path1("/media/morzh/ext4_volume/data/Pleiades_Tristereo_Melbourne/PHR1A_201202250024589_FR1_PX_E144S38_1205_01084/");
    std::string                 path2("/media/morzh/ext4_volume/data/Pleiades_Tristereo_Melbourne/PHR1A_201202250025259_FR1_PX_E144S38_1205_01119/");
    std::string                 path3("/media/morzh/ext4_volume/data/Pleiades_Tristereo_Melbourne/PHR1A_201202250025536_FR1_PX_E144S38_1205_01120/");

    SatelliteImagesPair         sat_pair;
    PatchesStereoPair           patches_pair;
    ImageRegistrationPOC        im_reg;
    AffineRectificationRPC      rect_rpc(Eigen::Vector3i(15,15,3));

    OpticalFlowPOC              flow_poc;
//  DisparityBoxPatchesPair     box_stereo;



    sat_pair.read_imagesData(path1, path2);
//    sat_pair.img1.rpc.rpc.print_vertices();
//    sat_pair.img1.rpc.rpci.print_vertices();
    sat_pair.calc_workingAreas();
    sat_pair.calc_img1PatchSizeNum(0.1, 512, 768);
//    sat_pair.viz.show_imageArea1(Eigen::Vector2i(  1080, 720 ));
//    sat_pair.viz.show_imageArea2(Eigen::Vector2i(  1080, 720 ));
//    sat_pair.viz.show_workingAreas(Eigen::Vector2i(  1080, 720 ));
//    sat_pair.viz.show_patches(Eigen::Vector2i(  1080, 720 ));

//    sat_pair.viz.show_workingArea_2( Eigen::Vector2i(  1080, 720 )   );
//    sat_pair.viz.show_workingAreas( Eigen::Vector2i(  1080, 720 )   );


    sat_pair.get_patchesStereo(365, patches_pair);
//    sat_pair.viz.show_img1PatchOrig(0);
//    sat_pair.viz.show_img1PatchOrig(51);

//    patches_pair.viz.show_rectangles();
//    patches_pair.viz.show_stereopairNormalized();


    std::string imgs_sim2_filename("/home/morzh/Pictures/SSV_SIM2.tiff");
    std::string imgs_sim1_filename("/home/morzh/Pictures/SSV_SIM1.tiff");
    std::string imgs_POC_filename("/home/morzh/Pictures/SSV__POC.tiff");


    rect_rpc.do_rectificationSIM2                ( patches_pair);
    rect_rpc.viz.write_imagesTransformed         ( patches_pair, imgs_sim2_filename );
//    rect_rpc.viz.show_AffineCamEpipolarGeo       ( patches_pair );
//    rect_rpc.viz.show_projRegGridOnPatches       ( patches_pair, false );
//    rect_rpc.viz.show_imagesTransfomed           ( patches_pair, true, true);
    rect_rpc.do_rectificationSL3                ( patches_pair);
    rect_rpc.do_rectificationSIM1                ( patches_pair);
//    rect_rpc.viz.show_projRegGridOnPatches       ( patches_pair, true );
//    rect_rpc.viz.show_imagesTransfomed           ( patches_pair, true, true);

//    im_reg.align_T2(patches_pair);
    rect_rpc.viz.write_imagesTransformed         ( patches_pair, imgs_sim1_filename );


    /// further align transformed images using phase only correlation
/*
    ImageRegistrationT2POC img_reg;
    im_reg.align_T2(patches_pair);
    rect_rpc.viz.write_imagesTransformed         ( patches_pair, imgs_POC_filename );
*/

    OpticalFlowPOCOptions flow_options;
    flow_poc.estimate_flow(patches_pair, flow_options);

//
//    patches_pair.viz.show_stereopairTransformed();

//    box_stereo.estimate_disparityPair(patches_pair);

    return 0;
}


