#ifndef SSV_PATCHESSTEREOPAIR_H
#define SSV_PATCHESSTEREOPAIR_H



#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "PatchesStereoPairVizUtility.h"
#include "ImagePatch.h"


struct DisparityData{

    cv::Mat ref2target, target2ref;
    cv::Mat errors_ref2target, errors_target2ref;
    cv::Mat confidence;
    cv::Mat occlusions;
};


/// class PatchesStereoPair - 
class  PatchesStereoPair {
public:
    PatchesStereoPair();

    ImagePatch                      patch1, patch2;
    DisparityData                   disparity_local;

    cv::Mat                         disparity_global;
    cv::Mat                         flow;

    PatchesStereoPairVizUtility     viz;
    ImageRectangleD                 im12_rect;
};


PatchesStereoPair::PatchesStereoPair() : viz(&patch1, &patch2, &disparity_local.ref2target ) {}

#endif // SSV_PATCHESSTEREOPAIR_H