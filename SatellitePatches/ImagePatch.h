
#ifndef SSV_IMAGEPATCH_H
#define SSV_IMAGEPATCH_H

#include <vector>
#include <numeric>

#include "SatelliteImagePair/SatelliteImageRPC.h"
#include "Common/ImageRectangle.h"
#include "Common/ConvexTetragon.h"

class ImagePatch {

    /**
     * for image
     * P -- means patch
     * N -- means normalized
     * T -- means transformed
     * W -- warped with optical flow
     * */

public:

    cv::Mat             imgP, imgPN,    imgPNT;
    ImageRectangleI     rect_src,   rect;
    ConvexTetragon      working_area;
    Eigen::Vector2d     intensity_range;
    Eigen::Matrix3d     T;
    SatelliteImageRPC   rpc;


    void    update_transform                        ( const Eigen::Matrix3d &xform);
    void    update_transformedImage                 ( const ImageRectangleD &imgs_rect);
    void    get_smoothedBorderImage                 ( cv::Mat &img_out, int filter_size=20);
    void    calc_intensityRange                     ( );
    void    normalize_byTruncatedIntensityRange     ( double truncate_ratio = 0.1);
    void    clamp_image                             ( const cv::Mat &img_src, cv::Mat &img_dst, double min, double max);

};



void ImagePatch::update_transform(const Eigen::Matrix3d &xform) {

    T = xform*T;
}

void ImagePatch::update_transformedImage(const ImageRectangleD &imgs_rect) {

    cv::Mat  H1_cv = cv::Mat::zeros(2,3, CV_64F);


    H1_cv.at<double>(0,0) = T(0,0);  H1_cv.at<double>(0,1) = T(0,1);  H1_cv.at<double>(0,2) = T(0,2);
    H1_cv.at<double>(1,0) = T(1,0);  H1_cv.at<double>(1,1) = T(1,1);  H1_cv.at<double>(1,2) = T(1,2);

    cv::warpAffine( imgPN, imgPNT, H1_cv, imgs_rect.get_whSizeCV(), cv::INTER_CUBIC );

    working_area    =    ConvexTetragon( ImageRectangleD(0,0, imgPN.cols, imgPN.rows) );

    working_area.transform(T);
}


void ImagePatch::get_smoothedBorderImage(cv::Mat &img_out, int filter_size) {

    cv::Mat mask = cv::Mat::zeros( cv::Size( imgPNT.cols, imgPNT.rows  ), CV_32FC1 );

    std::vector<cv::Point> vertices = working_area.offset(-filter_size).get_veticesCV();

    cv::Scalar    color_black   = cv::Scalar(0, 0, 0);
    cv::Scalar    color_white   = cv::Scalar(1, 1, 1);

    cv::fillConvexPoly(mask, vertices,   color_white );
    cv::blur(mask, mask, cv::Size(filter_size, filter_size ) );
    cv::multiply( imgPNT, mask, img_out );
}


void ImagePatch::calc_intensityRange() {

    double  min, max;

    cv::minMaxLoc(imgP, &min, &max);

    intensity_range(0) = min;
    intensity_range(1) = max-min;

}

void ImagePatch::normalize_byTruncatedIntensityRange(double truncate_ratio) {

    int                     num_pixels = imgPN.rows*imgPN.cols;
    int                     num_elements_truncate;
    float                   average;
    std::vector<float>      intensities(num_pixels);
    float                   range1, range2, range;


    double min, max, mean, spread;
    cv::minMaxLoc(imgP, &min, &max);


    spread  =   max - min;
    mean    =   min + 0.5*spread;

    spread *= (1-2*truncate_ratio);

    min = mean - 0.5*spread;
    max = mean + 0.5*spread;

    clamp_image(imgP, imgPN, min, max);


    imgPN -= mean;
    imgPN /= 0.5*spread;



/*
    /// for debigging :
    cv::minMaxLoc(imgPN, &min, &max);
    std::cout<< min << " " << max << std::endl;
*/



/*
    if (imgPN.isContinuous() ){

        memcpy( &intensities[0], (float*)imgPN.data, sizeof(float)*num_pixels);
    }
    else{

        int  img_cols = imgPN.cols;

        for (int row = 0; row < imgPN.rows; ++row) {
            for (int col = 0; col < imgPN.cols; ++col) {

                int idx =row*img_cols + col;

                intensities[idx] = imgPN.at<float>(row,col);
            }
        }
    }


    std::sort( intensities.begin(), intensities.end());

    num_elements_truncate = 0.5*num_pixels * truncate_ratio;

    intensities.erase( intensities.end()-num_elements_truncate, intensities.end());
    intensities.erase( intensities.begin(), intensities.begin()+num_elements_truncate);

    average = (float)std::accumulate( intensities.begin(), intensities.end(), 0.0)/intensities.size();

    range1 = average - *intensities.begin();
    range2 =  *(intensities.end()-1) - average;

    range = std::max(range1, range2);

    imgPN = (imgPN - average)/range;
//    imgPN = (imgPN - average)/(2*range) + 0.5;
*/


}

void ImagePatch::clamp_image(const cv::Mat &img_src, cv::Mat &img_dst, double min, double max) {

    cv::Mat dst;

    cv::threshold( img_src-min, dst, max-min, 0, 2);
    cv::threshold( dst,     img_dst, 0,       0, 3);

//    cv::inRange(mat, cv::Scalar(min), cv::Scalar(max),mask);
//
//    mat.copyTo(dst,mask);

    img_dst += min;
}


#endif // SSV_IMAGEPATCH_H
