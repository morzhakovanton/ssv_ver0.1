


#include "ImagePatch.h"

class PatchesStereoPairVizUtility {
public:

                PatchesStereoPairVizUtility     ( ImagePatch *patch1, ImagePatch *patch2, cv::Mat *d);

    void        show_stereopair_scaled          ( );
    void        show_stereopairNormalized();
    void        show_scaled                     ( std::string window_name, cv::Mat img);
    void        show_rectangles                 ( );
    void        save                            ( );
    void        scaleImage                      ( const cv::Mat& in_image, cv::Mat& out_image);
    void        show_stereopairTransformed      ();

    cv::Mat*        p_disparity;
    ImagePatch*     p_patch1;

    ImagePatch*     p_patch2;
};



PatchesStereoPairVizUtility::PatchesStereoPairVizUtility(ImagePatch *patch1, ImagePatch *patch2, cv::Mat *d) : p_patch1(patch1), p_patch2(patch2), p_disparity(d)  { }


void PatchesStereoPairVizUtility::show_stereopair_scaled() {


    cv::Mat img2show = cv::Mat::zeros( p_patch1->imgPN.rows, 2*p_patch1->imgPN.cols, CV_32FC1);
    cv::hconcat(p_patch1->imgPN, p_patch2->imgPN, img2show);
    show_scaled("Stereo Pair", img2show);
}

void PatchesStereoPairVizUtility::show_stereopairNormalized() {

    cv::Mat img2show = cv::Mat::zeros( p_patch1->imgPN.rows, 2*p_patch1->imgPN.cols, CV_32FC1);

    cv::hconcat(p_patch1->imgPN, p_patch2->imgPN, img2show);

    show_scaled("Stereo Pair Normalized", img2show);
}

void PatchesStereoPairVizUtility::show_scaled(std::string window_name, cv::Mat img) {

    double      min, max;
    cv::Mat     adjMap;

    cv::minMaxIdx(img, &min, &max);
    cv::convertScaleAbs(img-min, adjMap, 255 / ( max-min) );

    cv::imshow( window_name, adjMap);
    cv::waitKey(-1);
}


void PatchesStereoPairVizUtility::show_rectangles() {

    cv::Mat         img1, img2;
    cv::Scalar      white = cv::Scalar(255,255,255);

    scaleImage( p_patch1->imgPN, img1 );
    scaleImage( p_patch2->imgPN, img2 );

    cv::Point2i  patch_rect_src =  p_patch1->rect_src.get_ulOpenCV() - p_patch1->rect.get_ulOpenCV();

    cv::rectangle( img1, patch_rect_src, patch_rect_src+p_patch1->rect_src.get_whOpenCV() - cv::Point2i(1,1),  white, 1 );
    cv::rectangle( img1, cv::Point2i(0,0), p_patch1->rect.get_whOpenCV()- cv::Point2i(1,1),  white, 1 );

    cv::rectangle( img2, cv::Point2i(0,0), p_patch2->rect.get_whOpenCV()- cv::Point2i(1,1),  white, 1 );

    cv::imshow("Image 1 Rectangles", img1);
    cv::imshow("Image 2 Rectangles", img2);

    cv::waitKey(-1);
}

void PatchesStereoPairVizUtility::scaleImage(const cv::Mat &in_image, cv::Mat &out_image) {

    double min, max;

    cv::minMaxIdx(in_image, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(in_image-min, out_image, 255 / ( max-min) );


}

void PatchesStereoPairVizUtility::show_stereopairTransformed() {

    cv::Mat img2show = cv::Mat::zeros( p_patch1->imgPNT.rows, 2*p_patch1->imgPNT.cols, CV_32FC1);

    cv::hconcat(p_patch1->imgPNT, p_patch2->imgPNT, img2show);

    show_scaled("Stereo Pair Transformed", img2show);
}

