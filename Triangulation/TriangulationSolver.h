#include "SatelliteGeometry.h"

#include <Eigen/Core>
#include <Eigen/Dense>
#include "RPC.h"


#define  MAX_ITERATIONS     22
#define  MAX_ITERATIONS2    100
#define  EPSILON            1e-5
#define  HEIGHT_STEP        1



class TriangulationSolver{
public:

    void estimate (PatchStereoPair stereo_images&, SatelliteGeometry geo&);

    Eigen::Vector3d iterative_direct_RFM_height(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, double height_init, int *iter_num= nullptr);
    Eigen::Vector3d iterative_RPC_variance_height(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, double height_init, int *iter_num= nullptr);
    Eigen::Vector3d
    iterative_inverse_RFM_ground(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, const Eigen::Vector3d &init_vals, int *iter_num= nullptr);

    Eigen::Vector2d reproject_pixel_to_pixel(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel, double height = 0) const;

};


Eigen::Vector3d
TriangulationSolver::iterative_direct_RFM_height(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, double height_init, int *iter_num ) {

    double  Z_ = height_init, deltaZ;
    Eigen::Vector3d lXh, rXh;
    Eigen::Vector2d lPartial , rPartial ;
    int  i_num=0;




    for (int i = 0; i < MAX_ITERATIONS2; ++i) {

        Eigen::Vector3d lPt, rPt;

        lPt << pixel1, Z_;
        rPt << pixel2, Z_;

        lXh = rpc1.direct_rpc2( lPt );
        rXh = rpc2.direct_rpc2( rPt );

        lPartial =  rpc1.direct_rpc_partial_by_Height( lPt );
        rPartial =  rpc2.direct_rpc_partial_by_Height( rPt );

        deltaZ = (lXh(0) - rXh(0) + lXh(1) - rXh(1)) / (rPartial(0) - lPartial(0) + rPartial(1) - lPartial(1));

        Z_ += deltaZ;

        if ( abs(deltaZ) <  EPSILON ){ i_num = i; break;}
    }



    Eigen::Vector3d lGroundPt = rpc1.direct_rpc(pixel1, Z_);
    Eigen::Vector3d rGroundPt = rpc2.direct_rpc(pixel2, Z_);

    if ( iter_num != nullptr){ *iter_num = i_num;   }

    return  0.5*( lGroundPt +rGroundPt );
}

/**

double rpc_height( struct rpc *rpca, struct rpc *rpcb,  double xa, double ya, double xb, double yb, double *outerr)
{
    double x[2] = {xa, ya};
    double y[2] = {xb, yb};

    double h = 0;
    for (int t = 0; t < RPCH_MAXIT; t++) {

        double hstep = RPCH_HSTEP;
        double p[2], q[2];

        eval_rpc_pair(p, rpca, rpcb, x[0], x[1], h);
        eval_rpc_pair(q, rpca, rpcb, x[0], x[1], h + hstep);

        double a[2] = {q[0] - p[0], q[1] - p[1]};
        double b[2] = {y[0] - p[0], y[1] - p[1]};
        double a2 = a[0]*a[0] + a[1]*a[1];

        //if (a2 < RPCH_A2MAX) break;

        double lambda = (a[0]*b[0] + a[1]*b[1])/a2;

        // projection of p2 to the line r1-r0
        double z[2] = {p[0] + lambda*a[0], p[1] + lambda*a[1]};

        double err = hypot(z[0] - y[0], z[1] - y[1]);
        if (outerr) *outerr=err;

        h += lambda*hstep;

        if (fabs(lambda) < RPCH_LAMBDA_STOP)
            break;
    }
    return h;
}
*/


Eigen::Vector3d
TriangulationSolver::iterative_RPC_variance_height(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, double height_init, int *iter_num) {

    double h = height_init, err, lambda, hstep;
    Eigen::Vector2d p,q,a,b,z;
    int  i_num=0;

    for (int i = 0; i < MAX_ITERATIONS2; i++) {

        hstep = HEIGHT_STEP;

        p = reproject_pixel_to_pixel  ( rpc1, rpc2, pixel1, h);
        q = reproject_pixel_to_pixel  ( rpc1, rpc2, pixel1, h+hstep);

        a = q       - p;
        b = pixel2  - p;

        a.normalize();

        lambda = a.dot(b);

        z = p + lambda*a;

        err = (z-pixel2).norm();

        h += lambda*hstep;

        if (fabs(lambda) < EPSILON ){ i_num = i;   break;    }

    }



    Eigen::Vector3d lGroundPt = rpc1.direct_rpc(pixel1, h);
    Eigen::Vector3d rGroundPt = rpc2.direct_rpc(pixel2, h);

    if ( iter_num != nullptr){ *iter_num = i_num;   }

    return 0.5*(lGroundPt+rGroundPt);

}


#define EPSILON 1e-5

Eigen::Vector3d
TriangulationSolver::iterative_inverse_RFM_ground(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel1, const Eigen::Vector2d &pixel2, const Eigen::Vector3d &init_vals, int *iter_num) {

    Eigen::Matrix<double,4,3> J;
    Eigen::Matrix<double,4,1> f0;
    Eigen::Matrix<double,3,1> delta;
    Eigen::Matrix<double,3,1> solution;

    Eigen::Vector2d lPix_norm = rpc1.normalize_image_coordinates( pixel1 );
    Eigen::Vector2d rPix_norm = rpc2.normalize_image_coordinates( pixel2 );


    int  i_num=0;

    solution = init_vals;


    for (int i = 0; i < 20; ++i) {

        f0.block<2,1>(0,0) = lPix_norm - rpc1.normalized_inverse_rpc( rpc1.normalize_ground_coordinates(solution) );
        f0.block<2,1>(2,0) = rPix_norm - rpc2.normalized_inverse_rpc( rpc2.normalize_ground_coordinates(solution) );

/*
        std::cout << rpc1.inverse_rpc_partial_by_Longtitude ( solution ) << std::endl;
        std::cout << rpc1.inverse_rpc_partial_by_Latitude   ( solution ) << std::endl;
        std::cout << rpc1.inverse_rpc_partial_by_Height     ( solution ) << std::endl;

        std::cout << rpc2.inverse_rpc_partial_by_Longtitude ( solution ) << std::endl;
        std::cout << rpc2.inverse_rpc_partial_by_Latitude   ( solution ) << std::endl;
        std::cout << rpc2.inverse_rpc_partial_by_Height     ( solution ) << std::endl;
*/


        J.block<2,1>(0,0) = rpc1.inverse_rpc_partial_by_Longtitude ( solution );
        J.block<2,1>(0,1) = rpc1.inverse_rpc_partial_by_Latitude   ( solution );
        J.block<2,1>(0,2) = rpc1.inverse_rpc_partial_by_Height     ( solution );

        J.block<2,1>(2,0) = rpc2.inverse_rpc_partial_by_Longtitude ( solution );
        J.block<2,1>(2,1) = rpc2.inverse_rpc_partial_by_Latitude   ( solution );
        J.block<2,1>(2,2) = rpc2.inverse_rpc_partial_by_Height     ( solution );

//        std::cout << J << std::endl;

        delta = (J.transpose()*J).inverse() *  J.transpose()*f0;

        solution += delta;

        if ( delta.norm() < EPSILON){ i_num = i; break;}

    }

    if ( iter_num != nullptr){ *iter_num = i_num;   }


    return  solution;

}


Eigen::Vector2d TriangulationSolver::reproject_pixel_to_pixel(const RPC &rpc1, const RPC &rpc2, const Eigen::Vector2d &pixel, double height) const{

    return  rpc2.inverse_rpc(  rpc1.direct_rpc( pixel, height)   );

}