/// class SatImgPairVizUtility - 

#ifndef SSV_SATELLITEIMAGEPAIRVIZUTILITY_H
#define SSV_SATELLITEIMAGEPAIRVIZUTILITY_H

#include "SatelliteImage.h"
#include "Common/ImageRectangle.h"
#include "SatteliteImagePairData.h"


class SatelliteImagePairViz {

public:

                SatelliteImagePairViz    ( SatelliteImage *p_img1, SatelliteImage *p_img2, SatteliteImagePairData  *data );


    void        show_workingAreas       ( Eigen::Vector2i out_size);
    void        show_workingArea_1      ( Eigen::Vector2i out_size, bool draw_patches = false);
    void        show_workingArea_2      ( Eigen::Vector2i out_size);
    void        show_scaled             ( std::string window_name, cv::Mat img);
    void        get_scaled              ( const cv::Mat& img_in,cv::Mat& img_out);
    void        show_img1PatchOrig      ( int idx);
    void        show_patches            ( Eigen::Vector2i out_size);
    void        get_img1PatchRect       (int idx, ImageRectangleI &rect);

    void        show_imageArea1         (Eigen::Vector2i out_size, bool draw_working_area = false);
    void        show_imageArea2         (Eigen::Vector2i out_size, bool draw_working_area = false);



private:

    SatelliteImage              *p_img1, *p_img2;
    SatteliteImagePairData      *p_data;
//    ImageRectangleD             *p_working_area1, *p_working_area2;
};





SatelliteImagePairViz::SatelliteImagePairViz( SatelliteImage *p_img1,  SatelliteImage *p_img2,  SatteliteImagePairData  *p_data ): p_img1(p_img1), p_img2(p_img2), p_data(p_data){ }



void SatelliteImagePairViz::show_workingArea_1(Eigen::Vector2i out_size, bool draw_patches) {
    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    ImageRectangleI     working_area            =   p_img1->convert_rectangleD2I (  p_data->img1_working_area);
    Eigen::Vector2i     working_area_dims       =   Eigen::Vector2i( working_area.width,  working_area.height );

    double              scale_width             =   (double)out_size(0) / working_area_dims(0);
    double              scale_height            =   (double)out_size(1) / working_area_dims(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);
    Eigen::Vector2i     img_out_size            =   Eigen::Vector2i(scale*working_area_dims(0), scale*working_area_dims(1) );
    cv::Mat             img2show                =   cv::Mat( img_out_size(0), img_out_size(1), CV_32FC1);

    p_img1->viz.get( img2show, working_area, img_out_size  );

    show_scaled("Working Area 1", img2show);
}



void SatelliteImagePairViz::show_workingArea_2(Eigen::Vector2i out_size) {
    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    ImageRectangleI     working_area            =   p_img2->convert_rectangleD2I (  p_data->img2_working_area);
    Eigen::Vector2i     working_area_dims       =   Eigen::Vector2i( working_area.width,  working_area.height );

    double              scale_width             =   (double)out_size(0) / working_area_dims(0);
    double              scale_height            =   (double)out_size(1) / working_area_dims(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);
    Eigen::Vector2i     img_out_size            =   Eigen::Vector2i(scale*working_area_dims(0), scale*working_area_dims(1) );
    cv::Mat             img2show                =   cv::Mat( img_out_size(0), img_out_size(1), CV_32FC1);

    p_img2->viz.get( img2show, working_area, img_out_size  );

    show_scaled("Working Area 2", img2show);
}


void SatelliteImagePairViz::show_workingAreas(Eigen::Vector2i out_size) {

    /// X-Y (width-height) format !!! Columns first, rows second

    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    ImageRectangleI     img1_working_area       =   p_img1->convert_rectangleD2I (  p_data->img1_working_area);
    ImageRectangleI     img2_working_area       =   p_img2->convert_rectangleD2I (  p_data->img2_working_area);

    Eigen::Vector2i     stacked_working_areas   =   Eigen::Vector2i( std::max(img1_working_area.width, img2_working_area.width) ,  img1_working_area.height + img2_working_area.height);


    double              scale_width             =   (double)out_size(0) / stacked_working_areas(0);
    double              scale_height            =   (double)out_size(1) / stacked_working_areas(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);


    Eigen::Vector2i     img1_out_size           =   Eigen::Vector2i( scale*img1_working_area.width, scale*img1_working_area.height );
    Eigen::Vector2i     img2_out_size           =   Eigen::Vector2i( scale*img2_working_area.width, scale*img2_working_area.height );


    cv::Mat             img1                    =   cv::Mat( img1_out_size(0), img1_out_size(1), CV_32FC1);
    cv::Mat             img2                    =   cv::Mat( img2_out_size(0), img2_out_size(1), CV_32FC1);


    p_img1->viz.get( img1, img1_working_area, img1_out_size  );
    p_img2->viz.get( img2, img2_working_area, img2_out_size  );


    cv::Mat             img_to_show             =   cv::Mat(  img1.rows + img2.rows,  std::max( img1.cols, img2.cols ),  CV_32FC1 );

    cv::Rect            img1_rect               =   cv::Rect( 0, 0,                img1_out_size(0), img1_out_size(1) );
    cv::Rect            img2_rect               =   cv::Rect( 0, img1_out_size(1), img2_out_size(0), img2_out_size(1) );

    img1.copyTo( img_to_show( img1_rect ));
    img2.copyTo( img_to_show( img2_rect ));

    show_scaled("Working Areas", img_to_show);

}

void SatelliteImagePairViz::get_scaled(const cv::Mat& img_in, cv::Mat& img_out) {

    double      min, max;

    cv::minMaxIdx(img_in, &min, &max);
    cv::convertScaleAbs(img_in-min, img_out, 255 / ( max-min) );

}


void SatelliteImagePairViz::show_scaled(std::string window_name, cv::Mat img ) {

    double      min, max;
    cv::Mat     adjMap;

    cv::minMaxIdx(img, &min, &max);
    cv::convertScaleAbs(img-min, adjMap, 255 / ( max-min) );

    cv::imshow( window_name, adjMap);
    cv::waitKey(-1);

}

void SatelliteImagePairViz::show_img1PatchOrig(int idx) {


    cv::Mat             img_to_show;
    ImageRectangleI     rect, img1_wa;
    int                 row_rect, col_rect;

    img1_wa         =   p_img1->convert_rectangleD2I( p_data->img1_working_area);
    idx             =   std::max( 0,  std::min( idx,  p_data->img1_patches_num(0)*p_data->img1_patches_num(1) ) );

    row_rect        =  (float) idx/p_data->img1_patches_num(0);
    col_rect        =   idx - row_rect*p_data->img1_patches_num(0);

    rect.x          =   std::round ( p_data->img1_patch_size(0) * (  (col_rect)*(1-p_data->patches_overlap )  ) );
    rect.y          =   std::round ( p_data->img1_patch_size(1) * (  (row_rect)*(1-p_data->patches_overlap ) ) );

    rect.x         +=   img1_wa.x;
    rect.y         +=   img1_wa.y;


    rect.width      =   std::round ( p_data->img1_patch_size(0) );
    rect.height     =   std::round ( p_data->img1_patch_size(1) );


    p_img1->get_imgFromRect(rect, img_to_show);

    show_scaled("Image 1 Patch Original Number "+idx, img_to_show );

}

void SatelliteImagePairViz::show_imageArea1(Eigen::Vector2i out_size, bool draw_working_area) {

    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    Eigen::Vector2i     pix_unnormed_1          =   Eigen::Vector2i( p_img1->dim.bounding_polygon[0].column, p_img1->dim.bounding_polygon[0].row);
    Eigen::Vector2i     pix_unnormed_3          =   Eigen::Vector2i( p_img1->dim.bounding_polygon[2].column, p_img1->dim.bounding_polygon[2].row);

    ImageRectangleI     image_area              =   ImageRectangleI( pix_unnormed_1(0),pix_unnormed_1(1), pix_unnormed_3(0)-pix_unnormed_1(0), pix_unnormed_3(1)-pix_unnormed_1(1) );
    Eigen::Vector2i     image_area_dims         =   Eigen::Vector2i( image_area.width,  image_area.height );
    ImageRectangleI     working_area            =   p_img1->convert_rectangleD2I (  p_data->img1_working_area);
    Eigen::Vector2i     working_area_dims       =   Eigen::Vector2i( working_area.width,  working_area.height );

    double              scale_width             =   (double)out_size(0) / image_area_dims(0);
    double              scale_height            =   (double)out_size(1) / image_area_dims(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);
    Eigen::Vector2i     img_out_size            =   Eigen::Vector2i(scale*image_area_dims(0), scale*image_area_dims(1) );
    cv::Mat             img2show                =   cv::Mat( img_out_size(0), img_out_size(1), CV_32FC1);

    p_img1->viz.get( img2show, image_area, img_out_size  );


    cv::Mat img2show_scaled, img2show_scaled_rgb;

    get_scaled(img2show,  img2show_scaled);

    if ( draw_working_area ){

        if ( working_area.check_isEmpty() ) std::cerr << "Image working calc_area is empty" << std::endl;

        cv::cvtColor(img2show_scaled, img2show_scaled_rgb, cv::COLOR_GRAY2BGR);

        cv::Point2i     up_left         = scale*working_area.get_ulOpenCV();
        cv::Point2i     bottom_right    = scale*working_area.get_brOpenCV();
        cv::Scalar      color           = cv::Scalar(200,80,80);

        cv::rectangle( img2show_scaled_rgb, up_left, bottom_right, color);
        cv::imshow("Image 1 Area", img2show_scaled_rgb);
    }else

        cv::imshow("Image 1 Area", img2show_scaled);


    cv::waitKey(-1);



}

void SatelliteImagePairViz::show_imageArea2(Eigen::Vector2i out_size, bool draw_working_area) {

    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    Eigen::Vector2i     pix_unnormed_1          =   Eigen::Vector2i( p_img2->dim.bounding_polygon[0].column, p_img2->dim.bounding_polygon[0].row);
    Eigen::Vector2i     pix_unnormed_3          =   Eigen::Vector2i( p_img2->dim.bounding_polygon[2].column, p_img2->dim.bounding_polygon[2].row);

    ImageRectangleI     image_area              =   ImageRectangleI( pix_unnormed_1(0),pix_unnormed_1(1), pix_unnormed_3(0)-pix_unnormed_1(0), pix_unnormed_3(1)-pix_unnormed_1(1) );
    Eigen::Vector2i     image_area_dims         =   Eigen::Vector2i( image_area.width,  image_area.height );
    ImageRectangleI     working_area            =   p_img1->convert_rectangleD2I (  p_data->img2_working_area);
    Eigen::Vector2i     working_area_dims       =   Eigen::Vector2i( working_area.width,  working_area.height );

    double              scale_width             =   (double)out_size(0) / image_area_dims(0);
    double              scale_height            =   (double)out_size(1) / image_area_dims(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);
    Eigen::Vector2i     img_out_size            =   Eigen::Vector2i(scale*image_area_dims(0), scale*image_area_dims(1) );
    cv::Mat             img2show                =   cv::Mat( img_out_size(0), img_out_size(1), CV_32FC1);

    p_img2->viz.get( img2show, image_area, img_out_size  );


    cv::Mat img2show_scaled, img2show_scaled_rgb;

    get_scaled(img2show,  img2show_scaled);

    if ( draw_working_area ){

        if ( working_area.check_isEmpty() ) std::cerr << "Image working calc_area is empty" << std::endl;

        cv::cvtColor(img2show_scaled, img2show_scaled_rgb, cv::COLOR_GRAY2BGR);

        cv::Point2i     up_left         = scale*working_area.get_ulOpenCV();
        cv::Point2i     bottom_right    = scale*working_area.get_brOpenCV();
        cv::Scalar      color           = cv::Scalar(200,80,80);

        cv::rectangle( img2show_scaled_rgb, up_left, bottom_right, color);
        cv::imshow("Image 2 Area", img2show_scaled_rgb);
    }else

        cv::imshow("Image 2 Area", img2show_scaled);


    cv::waitKey(-1);


}

void SatelliteImagePairViz::show_patches(Eigen::Vector2i out_size) {

    /**
     *  out_size(0) задаёт количество столбцов, out_size(1) зввдёт количество строк
     * выходное изображение будет разрешением: строк не больше, чем out_size(1) и столбцов, не больше, чем  out_size(0)
     * */

    Eigen::Vector2i     pix_unnormed_1          =   Eigen::Vector2i( p_img1->dim.bounding_polygon[0].column, p_img1->dim.bounding_polygon[0].row);
    Eigen::Vector2i     pix_unnormed_3          =   Eigen::Vector2i( p_img1->dim.bounding_polygon[2].column, p_img1->dim.bounding_polygon[2].row);

    ImageRectangleI     image_area              =   ImageRectangleI( pix_unnormed_1(0),pix_unnormed_1(1), pix_unnormed_3(0)-pix_unnormed_1(0), pix_unnormed_3(1)-pix_unnormed_1(1) );
    Eigen::Vector2i     image_area_dims         =   Eigen::Vector2i( image_area.width,  image_area.height );
    ImageRectangleI     working_area            =   p_img1->convert_rectangleD2I (  p_data->img1_working_area);
    Eigen::Vector2i     working_area_dims       =   Eigen::Vector2i( working_area.width,  working_area.height );

    double              scale_width             =   (double)out_size(0) / image_area_dims(0);
    double              scale_height            =   (double)out_size(1) / image_area_dims(1);
    double              scale                   =   (double)std::min(scale_width, scale_height);
    Eigen::Vector2i     img_out_size            =   Eigen::Vector2i(scale*image_area_dims(0), scale*image_area_dims(1) );
    cv::Mat             img2show                =   cv::Mat( img_out_size(0), img_out_size(1), CV_32FC1);


    p_img1->viz.get( img2show, image_area, img_out_size  );


    cv::Mat img2show_scaled, img2show_scaled_rgb;
    get_scaled(img2show,  img2show_scaled);
    cv::cvtColor(img2show_scaled, img2show_scaled_rgb, cv::COLOR_GRAY2BGR);


    for (int idx = 0; idx < p_data->img1_patches_num(0)*p_data->img1_patches_num(1); ++idx) {


        ImageRectangleI rect;
        get_img1PatchRect(idx, rect);

        cv::Point2i     up_left         = scale*rect.get_ulOpenCV();
        cv::Point2i     bottom_right    = scale*rect.get_brOpenCV();
        cv::Scalar      color           = cv::Scalar(100,150,100);

        cv::rectangle( img2show_scaled_rgb, up_left, bottom_right, color);

    }

    cv::imshow("Image 1 Patches", img2show_scaled_rgb);
    cv::waitKey(-1);

}


void SatelliteImagePairViz::get_img1PatchRect(int idx, ImageRectangleI &rect) {

    int                 row_rect, col_rect;
    ImageRectangleI     img1_wa;

    /** just idx value clamping */
    idx             =   std::max( 0,  std::min( idx,  p_data->img1_patches_num(0)*p_data->img1_patches_num(1) ) );
    img1_wa         =   p_img1->convert_rectangleD2I( p_data->img1_working_area);

    row_rect        =  (float) idx/p_data->img1_patches_num(0);
    col_rect        =   idx - row_rect*p_data->img1_patches_num(0);

    rect.x          =   std::round ( p_data->img1_patch_size(0) * (  (col_rect)*(1-p_data->patches_overlap ) /*+ 1 */) );
    rect.y          =   std::round ( p_data->img1_patch_size(1) * (  (row_rect)*(1-p_data->patches_overlap ) /*+ 1 */) );

    rect.x         +=   img1_wa.x;
    rect.y         +=   img1_wa.y;


    rect.width      =   std::round ( p_data->img1_patch_size(0) );
    rect.height     =   std::round ( p_data->img1_patch_size(1) );

}

#endif // SSV_SATELLITEIMAGEPAIRVIZUTILITY_H