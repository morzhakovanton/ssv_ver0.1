/**

класс RPCDirect содержит данные, прочитанные из xml файла. 
Они включают в себя:
1) коэффициенты рациональных многочленов для преобразования
из системы  координат "изображение-высота"  в систему координат  
"широта-долгота-высота"
2) погрешности
3) множители для нормировки координат
*/

#ifndef SSV_SATELLITERPCDIRECT_H
#define SSV_SATELLITERPCDIRECT_H

#include <iostream>
#include <iomanip>


class RPCDirect {
  
public:
  std::array< double,20 >       sample_numerator,   sample_denumerator;
  std::array< double,20 >       line_numerator,     line_denumerator;
  double                        error_bias_X,       error_bias_Y;
  double                        sample_scale,       sample_offset;
  double                        line_scale,         line_offset;
  int                           first_row,          last_row;
  int                           first_column,       last_column;
  double                        scale_height,       offset_height;


  void print();
};

void RPCDirect::print() {

    std::cout << "Direct RPC: " << std::endl;


    for (int i = 0; i < 20; ++i) {

        std::cout <<   std::setprecision(17)    << std::setw(25) << sample_numerator[i] << " " << std::setw(25)  << sample_denumerator[i] << " "
                  << std::setw(25) << line_numerator[i] << " " << std::setw(25)  << line_denumerator[i] << std::endl;
    }

    std::cout << std::endl;

    std::cout << "Error Bias X: " << error_bias_X  << ";  Error Bias Y: " << error_bias_Y << std::endl;

    std::cout << "First Row: " << first_row << " ; First Column: " << first_column << "; Lat Row: " << last_row << "; Last Column: " << last_column << std::endl;

    std::cout << "-------------------------------------------------------------------------------------------------------- \n" << std::endl;


}

#endif // SSV_SATELLITERPCDIRECT_H