//
// Created by morzh on 13.11.17.
//

#ifndef SSV_VER_0_1_SATELLITEIMAGEDIM_H
#define SSV_VER_0_1_SATELLITEIMAGEDIM_H

#include <string>

struct SatelliteImageVertex{

    double longtitude, latitude;
    int  column,row;

};


class SatelliteImageDIM{

    /** Данный класс содержит методы чтения файла  и печати   DIM данных. В настоящее время из xml файла читается только  bounding_polygon
     * */

public:

    void                readPleiades                ( std::string xml_file);
    void                shift_rowsColsIndexing      ( int col_shift, int row_shift);

    void                print                       (  );

//private:
    /// section 1 -- General Information
    std::string                     map_name;
    std::string                     geometric_processing_level;
    std::string                     radiometric_processing_level;
    /// section 2 -- Image dimensions
    int                             number_of_columns;
    int                             number_of_rows;
    int                             number_of_spectral_bands;
    /// section 3 -- Dataset framing (4 points)
    std::array<SatelliteImageVertex, 4>  bounding_polygon;
    /// section 4 -- Production
    std::string                     production_date;
    std::string                     job_identification;
    std::string                     dataset_producer_identification;
    std::string                     producer_link;
};



void SatelliteImageDIM::readPleiades(std::string xml_file) {

    /**  В данном методе читается файл с DIM данными, который содержит общие сведения о космическом снимке  */

    rapidxml::xml_document<>    doc;
    rapidxml::xml_node<>        * root_node;
    rapidxml::file<>            xmlFile ( xml_file.c_str());

    doc.parse<0>( xmlFile.data() );
    root_node = doc.first_node("Dimap_Document");



    rapidxml::xml_node<> * Dataset_Content_node                 =    root_node->first_node              ( "Metadata_Identification");

    Dataset_Content_node                                        =     Dataset_Content_node->next_sibling              ();
    Dataset_Content_node                                        =     Dataset_Content_node->next_sibling              ();
//    std::cout << Dataset_Content_node->name() << std::endl;



    rapidxml::xml_node<> * Dataset_Extent_node                  =     Dataset_Content_node->first_node("SURFACE_AREA");

    Dataset_Extent_node                                         =  Dataset_Extent_node->next_sibling();
    Dataset_Extent_node                                         =  Dataset_Extent_node->next_sibling();
//    std::cout << Dataset_Extent_node->name() << std::endl;


    int i = 0;

    for ( rapidxml::xml_node<> * Vertex_node    =    Dataset_Extent_node->first_node ( "Vertex"); i<4; Vertex_node = Vertex_node->next_sibling(), ++i  ) {

        rapidxml::xml_node<> * LON_node                            =    Vertex_node->first_node           ( "LON");
        rapidxml::xml_node<> * LAT_node                            =    Vertex_node->first_node           ( "LAT");
        rapidxml::xml_node<> * COL_node                            =    Vertex_node->first_node           ( "COL");
        rapidxml::xml_node<> * ROW_node                            =    Vertex_node->first_node           ( "ROW");


        bounding_polygon[i].longtitude      = atof(LON_node->value());
        bounding_polygon[i].latitude        = atof(LAT_node->value());
        bounding_polygon[i].column          = atof(COL_node->value());
        bounding_polygon[i].row             = atof(ROW_node->value());
    }


    shift_rowsColsIndexing(-1,-1);


    rapidxml::xml_node<> * Raster_Data_node   =    root_node->first_node              ( "Raster_Data");

//    Raster_Data_node = Raster_Data_node->next_sibling();
//    Raster_Data_node = Raster_Data_node->next_sibling();


    rapidxml::xml_node<> * Data_Access_node                  =     Raster_Data_node->first_node("Data_Access");

    Data_Access_node = Data_Access_node->next_sibling();
//    std::cout << Data_Access_node->name() << std::endl;


    rapidxml::xml_node<> * NROWS_node                       = Data_Access_node->first_node("NROWS");
    rapidxml::xml_node<> * NCOLS_node                       = Data_Access_node->first_node("NCOLS");
    rapidxml::xml_node<> * NBANDS_node                      = Data_Access_node->first_node("NBANDS");
//    std::cout << NROWS_node->name() << std::endl;

    number_of_rows = atoi(NROWS_node->value() );
    number_of_columns = atoi(NCOLS_node->value() );
    number_of_spectral_bands  =  atoi(NBANDS_node->value() );

}

void SatelliteImageDIM::print() {

    std::cout << "vertex #:     " << "longtitude                " << "latitude                              " <<  "Row                        " << "Column" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------------------------------------" << std::endl;

    for (int i = 0; i < 4; ++i) {

        std::cout << "#" << i+1 << "     "  <<  std::setprecision(17)    <<     std::setw(25)   <<    bounding_polygon[i].longtitude     << " "
                                            <<  std::setprecision(17)    <<     std::setw(25)   <<    bounding_polygon[i].latitude       << " "
                                            <<  std::setprecision(17)    <<     std::setw(25)   <<    bounding_polygon[i].row            << " "
                                            <<  std::setprecision(17)    <<     std::setw(25)   <<    bounding_polygon[i].column         << std::endl;
    }


    std::cout << "Number of cloumns:" << number_of_columns          << std::endl;
    std::cout << "Number of rows:   " << number_of_rows             << std::endl;
    std::cout << "Number of bands:  " << number_of_spectral_bands   << std::endl;

}

void SatelliteImageDIM::shift_rowsColsIndexing(int col_shift, int row_shift) {

    for (int i = 0; i < 4; ++i) {

        bounding_polygon[i].column += col_shift;
        bounding_polygon[i].row    += row_shift;
    }

}

#endif //SSV_VER_0_1_SATELLITEIMAGEDIM_H
