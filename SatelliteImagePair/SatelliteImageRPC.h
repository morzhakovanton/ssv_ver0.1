
#ifndef SSV_SATELLITEIMAGERPC_H
#define SSV_SATELLITEIMAGERPC_H

#include <string>
#include <Eigen/Dense>

#include "3rdParty/rapidxml-1.13/rapidxml_utils.hpp"
#include "RPCDirect.h"
#include "RPCInverse.h"


/**
 *
 * Данный класс содержит коэффициенты рациональных многочленов для прямого  и обратного проеобразования "система  координат изображения  <-->  сист коорд Земли".
 * Чтобы получить координаты точки поверхности Земли по координтам точки изображения и высоты точки (на поверхности Земли) применяем обратные методы ( с буквой 'I', напр calc_NRPCI3DAtPoint)
 * Чтобы получить координаты точки на изоюражении по трём координатам точки на  поверхности  Земли, применяем прямые методы (напр calc_NRPC2DAtPoint )
 *
 * Нормировка координат производится для более точного и устойчивого приближения с помощью рациональных многочленов
 *
 *  в имени методов:
 *  N    --  значит  "Normalized" (может отсутствовать)
 *  RPC  --  "Rational Polynomial Coefficients"
 *  I    --  значит "Inverse" (может отсутствовать в имени метода)
 *  2D   --  метод возвращает двумерный вектор
 *  3D   --  метод возвращает трёхмерный вектор
 *
 *  img  -- 'image'
 *
 *  pixel   подразумевает, что это точка изображения,
 *  point   подразумевает, что это точка Земли
 *  height  подразумевает, что это высота Земли в какой либо точке
 *  arg     подразумевает, что это  аргумент функции (в данном классе аргумент многочлена)
 *
 * */


class SatelliteImageRPC {

public:
//    SatelliteImageRPC                                                   ();
//    SatelliteImageRPC                                                   (std::string& path);

    void                    read(std::string xml_file);
    void                    shift_sampleAndLineOffsets                  ( int samp_offset, int line_offset   );
    void                    shift_rowColumnRange                        ( int row_offset,  int column_offset );

    Eigen::Vector2d         calc_NRPC2D                                 ( const Eigen::Vector3d &pixel_height)          const;
    Eigen::Vector3d         calc_NRPC3D                                 ( const Eigen::Vector3d &pixel_height)                 const;
    Eigen::Vector3d         calc_NRPC3D                                 (const Eigen::Vector2d &pixel, double height)  const;

    Eigen::Vector2d         calc_NRPCI2D                                ( const Eigen::Vector3d &point)          const;
    Eigen::Vector3d         calc_NRPCI3D                                ( const Eigen::Vector3d &point) const ;

    Eigen::Vector3d         calc_RPC3D                                  ( const Eigen::Vector3d &pixel_height)          const;
    Eigen::Vector3d         calc_RPC3D                                  ( const Eigen::Vector2d &pixel, double height)  const;
    Eigen::Vector2d         calc_RPCI2D                                 ( const Eigen::Vector3d &ground_point)           const;

    Eigen::Vector3d         normalize_groundCoordinates                 ( const Eigen::Vector3d& point)                 const;
    Eigen::Vector3d         normalize_groundCoordinates                 ( const Eigen::Vector2d&  point, double height) const;
    Eigen::Vector2d         normalize_imgCoordinates                    ( const Eigen::Vector2d &pixel)                 const;
    Eigen::Vector3d         normalize_imageHeightCoordinates            ( const Eigen::Vector3d &pixel_height)          const ;
    Eigen::Vector3d         normalize_imageHeightCoordinates            ( const Eigen::Vector2d& pixel, double height)  const ;
    Eigen::Vector3d         unnormalize_groundCoordinates               ( const Eigen::Vector3d &point)                 const ;
    Eigen::Vector2d         unnormalize_imgCoordinates(Eigen::Vector2d pixel)                        const ;

    void                    calc_NRPC3DArray(const Eigen::Matrix2Xd &array_px, double d, Eigen::Matrix3Xd &array_pt);
    void                    calc_NRPC3DArray                            ( const Eigen::Matrix3Xd &array_pt, Eigen::Matrix2Xd &array_px );
    void                    normalize_imgCoordinatesArray               ( Eigen::Matrix2Xd &pixel_grid)           const;
    void                    normalize_groundCoordinatesArray            ( Eigen::Matrix3Xd &coords_matrix)      const;
    void                    unnormalize_imgCoordinatesArray             ( Eigen::Matrix2Xd &pixel_grid)           const;
    void                    unnormalize_groundCoordinatesArray          ( Eigen::Matrix3Xd &coords_matrix);

    Eigen::Vector2d         partial_NRPCByHeight                        ( const Eigen::Vector3d & pixel_height);
    Eigen::Vector2d         partial_RPCByHeight                         ( const Eigen::Vector3d & pixel_height);
    Eigen::Vector2d         partial_NRPCIByLongtitude                   ( const Eigen::Vector3d& ground_point_normalized);
    Eigen::Vector2d         partial_RPCIByLongtitude                    ( const Eigen::Vector3d& ground_point_unnormalized);
    Eigen::Vector2d         partial_NRPCIByLatitude                     ( const Eigen::Vector3d & ground_point);
    Eigen::Vector2d         partial_RPCIByLatitude                      ( const Eigen::Vector3d & ground_point_unnormalized);


    Eigen::Vector2d         partial_NRPCIByHeight                       ( const Eigen::Vector3d & ground_point);
    Eigen::Vector2d         partial_RPCIByHeight                        ( const Eigen::Vector3d & ground_point_unnormalized);

    RPCDirect               rpc;    /// direct RPC

    RPCInverse              rpci;   /// inverse RPC

private:

    double                  calc_polynom(const std::array<double, 20> &polynom_coeffs, const Eigen::Vector3d &arg) const ;
    double                  partial_polynomByLongtitudeOrSampleAtPoint (const std::array<double, 20>& coeffs, const Eigen::Vector3d& point);
    double                  partial_polynomByLatitudeOrLineAtPoint (const std::array<double, 20>& coeffs, const Eigen::Vector3d& point);
    double                  partial_polynomByHeightAtPoint (const std::array<double, 20>& coeffs, const Eigen::Vector3d& point);
};



/**--------------------------------------------------------------------------------------------------------------------------------*/


void SatelliteImageRPC::read(std::string xml_file) {

    /**
     * В данном методе читается файл с RPC (коэффициентами рационального многочлена) для прямого и обратного преоборазований координат изображения в  координаты Земли.
     * Каждый многочлен кубический и имеет 20 коэффициентов. Соотвественно для определения прямого и обратного проеобразований нужно по 40
     * коэффициентов. Итого 80. Для работы с нормированными координатами необходимы 'scale' и 'offset' (*_SCALE и *_OFF) значения для строк-столбцов-высоты
     * (или линейки-элемента_линейки-высоты) и для долготы-широты-высоты. Погрешности error_bias_* в данной версии  системы не используются, но читаются из xml файла.
     * */


    rapidxml::xml_document<>    doc;
    rapidxml::xml_node<>        * root_node;
    rapidxml::file<>            xmlFile ( xml_file.c_str());

    doc.parse<0>( xmlFile.data() );
    root_node = doc.first_node("Dimap_Document");

    /**------------------------------------------------------------------------------------- DIRECT MODEL ---*/

    rapidxml::xml_node<> * Rational_Function_node         = root_node->first_node                ( "Rational_Function_Model");
    rapidxml::xml_node<> * Global_RFM_node                = Rational_Function_node->first_node   ( "Global_RFM");
    rapidxml::xml_node<> * DirectModel_node               = Global_RFM_node->first_node          ( "Direct_Model");
    rapidxml::xml_node<> * InverseModel_node              = Global_RFM_node->first_node          ( "Inverse_Model");
    rapidxml::xml_node<> * RFMValidity_node               = Global_RFM_node->first_node          ( "RFM_Validity");

    for (int i = 1; i <= 20; ++i) {

        std::string SAMP_NUM_COEFF_str("SAMP_NUM_COEFF_");
        std::string SAMP_DEN_COEFF_str("SAMP_DEN_COEFF_");

        std::string LINE_NUM_COEFF_str("LINE_NUM_COEFF_");
        std::string LINE_DEN_COEFF_str("LINE_DEN_COEFF_");

        rapidxml::xml_node<> *SAMP_NUM_COEFF_node = DirectModel_node->first_node( ( SAMP_NUM_COEFF_str + std::to_string(i) ).c_str() );
        rapidxml::xml_node<> *SAMP_DEN_COEFF_node = DirectModel_node->first_node( ( SAMP_DEN_COEFF_str + std::to_string(i) ).c_str() );

        rapidxml::xml_node<> *LINE_NUM_COEFF_node = DirectModel_node->first_node( ( LINE_NUM_COEFF_str + std::to_string(i) ).c_str() );
        rapidxml::xml_node<> *LINE_DEN_COEFF_node = DirectModel_node->first_node( ( LINE_DEN_COEFF_str + std::to_string(i) ).c_str() );


        rpc.sample_numerator[i-1]       = atof(SAMP_NUM_COEFF_node->value() );
        rpc.sample_denumerator[i-1]     = atof(SAMP_DEN_COEFF_node->value() );
        rpc.line_numerator[i-1]         = atof(LINE_NUM_COEFF_node->value() );
        rpc.line_denumerator[i-1]       = atof(LINE_DEN_COEFF_node->value() );

    }

    rapidxml::xml_node<> * DIR_ERR_BIAS_X = DirectModel_node->first_node("ERR_BIAS_X");
    rapidxml::xml_node<> * DIR_ERR_BIAS_Y = DirectModel_node->first_node("ERR_BIAS_Y");

    rpc.error_bias_X = atof( DIR_ERR_BIAS_X->value());
    rpc.error_bias_Y = atof( DIR_ERR_BIAS_Y->value());

    rapidxml::xml_node<> * DirectModelValidityDomain_node       =  RFMValidity_node->first_node                    ("Direct_Model_Validity_Domain");

    rapidxml::xml_node<> * FIRST_ROW                            =  DirectModelValidityDomain_node->first_node      ("FIRST_ROW");
    rapidxml::xml_node<> * FIRST_COL                            =  DirectModelValidityDomain_node->first_node      ("FIRST_COL");
    rapidxml::xml_node<> * LAST_ROW                             =  DirectModelValidityDomain_node->first_node      ("LAST_ROW");
    rapidxml::xml_node<> * LAST_COL                             =  DirectModelValidityDomain_node->first_node      ("LAST_COL");

    rpc.first_row        = atoi( FIRST_ROW->value() );
    rpc.first_column     = atoi( FIRST_COL->value() );
    rpc.last_row         = atoi( LAST_ROW->value()  );
    rpc.last_column      = atoi( LAST_COL->value()  );



    rapidxml::xml_node<> * SAMP_SCALE       =  RFMValidity_node->first_node                    ("SAMP_SCALE");
    rapidxml::xml_node<> * SAMP_OFF         =  RFMValidity_node->first_node                    ("SAMP_OFF");

    rapidxml::xml_node<> * LINE_SCALE       =  RFMValidity_node->first_node                    ("LINE_SCALE");
    rapidxml::xml_node<> * LINE_OFF         =  RFMValidity_node->first_node                    ("LINE_OFF");

    rpc.sample_scale     = atof( SAMP_SCALE->value() );
    rpc.sample_offset    = atof( SAMP_OFF->value()   );
    rpc.line_scale       = atof( LINE_SCALE->value() );
    rpc.line_offset      = atof( LINE_OFF->value()   );



    for (int i = 1; i <= 20; ++i) {

        std::string SAMP_NUM_COEFF_str("SAMP_NUM_COEFF_");
        std::string SAMP_DEN_COEFF_str("SAMP_DEN_COEFF_");

        std::string LINE_NUM_COEFF_str("LINE_NUM_COEFF_");
        std::string LINE_DEN_COEFF_str("LINE_DEN_COEFF_");

        rapidxml::xml_node<> *SAMP_NUM_COEFF_node = InverseModel_node->first_node( ( SAMP_NUM_COEFF_str + std::to_string(i) ).c_str() );
        rapidxml::xml_node<> *SAMP_DEN_COEFF_node = InverseModel_node->first_node( ( SAMP_DEN_COEFF_str + std::to_string(i) ).c_str() );

        rapidxml::xml_node<> *LINE_NUM_COEFF_node = InverseModel_node->first_node( ( LINE_NUM_COEFF_str + std::to_string(i) ).c_str() );
        rapidxml::xml_node<> *LINE_DEN_COEFF_node = InverseModel_node->first_node( ( LINE_DEN_COEFF_str + std::to_string(i) ).c_str() );


        rpci.sample_numerator[i-1]      =   atof( SAMP_NUM_COEFF_node->value() );
        rpci.sample_denumerator[i-1]    =   atof( SAMP_DEN_COEFF_node->value() );
        rpci.line_numerator[i-1]        =   atof( LINE_NUM_COEFF_node->value() );
        rpci.line_denumerator[i-1]      =   atof( LINE_DEN_COEFF_node->value() );

    }


    rapidxml::xml_node<> *  INV_ERR_BIAS_COL = InverseModel_node->first_node("ERR_BIAS_COL");
    rapidxml::xml_node<> *  INV_ERR_BIAS_ROW = InverseModel_node->first_node("ERR_BIAS_ROW");

    rpci.error_bias_row         = atof( INV_ERR_BIAS_ROW->value());
    rpci.error_bias_column      = atof( INV_ERR_BIAS_COL->value());


    rapidxml::xml_node<> * InverseModelValidityDomain_node       =  RFMValidity_node->first_node                    ("Inverse_Model_Validity_Domain");

    rapidxml::xml_node<> * FIRST_LON                            =  InverseModelValidityDomain_node->first_node      ("FIRST_LON");
    rapidxml::xml_node<> * FIRST_LAT                            =  InverseModelValidityDomain_node->first_node      ("FIRST_LAT");
    rapidxml::xml_node<> * LAST_LON                             =  InverseModelValidityDomain_node->first_node      ("LAST_LON");
    rapidxml::xml_node<> * LAST_LAT                             =  InverseModelValidityDomain_node->first_node      ("LAST_LAT");


    rpci.first_longtitude                   = atof( FIRST_LON->value() );
    rpci.first_latitude                     = atof( FIRST_LAT->value() );
    rpci.last_longtitude                    = atof( LAST_LON->value()  );
    rpci.last_latitude                      = atof( LAST_LAT->value()  );



    rapidxml::xml_node<> * LONG_SCALE       =  RFMValidity_node->first_node                    ("LONG_SCALE");
    rapidxml::xml_node<> * LONG_OFF         =  RFMValidity_node->first_node                    ("LONG_OFF");

    rapidxml::xml_node<> * LAT_SCALE        =  RFMValidity_node->first_node                    ("LAT_SCALE");
    rapidxml::xml_node<> * LAT_OFF          =  RFMValidity_node->first_node                    ("LAT_OFF");

    rapidxml::xml_node<> * HEIGHT_SCALE     =  RFMValidity_node->first_node                    ("HEIGHT_SCALE");
    rapidxml::xml_node<> * HEIGHT_OFF       =  RFMValidity_node->first_node                    ("HEIGHT_OFF");


    rpci.scale_longtitude                   = atof( LONG_SCALE->value()     );
    rpci.offset_longtitude                  = atof( LONG_OFF->value()       );
    rpci.scale_latitude                     = atof( LAT_SCALE->value()      );
    rpci.offset_latitude                    = atof( LAT_OFF->value()        );
    rpci.scale_height                       = atof( HEIGHT_SCALE->value()   );
    rpci.offset_height                      = atof( HEIGHT_OFF->value()     );

    rpc.scale_height                        = atof( HEIGHT_SCALE->value()   );
    rpc.offset_height                       = atof( HEIGHT_OFF->value()     );


    shift_sampleAndLineOffsets  ( -1,-1 );
    shift_rowColumnRange        ( -1,-1 );

}


void SatelliteImageRPC::shift_sampleAndLineOffsets(int samp_offset, int line_offset) {

    rpc.sample_offset    +=  samp_offset;
    rpc.line_offset      +=  line_offset;
}


void SatelliteImageRPC::shift_rowColumnRange( int row_offset,  int col_offset ){


    rpc.first_row       +=  row_offset;       rpc.last_row      += row_offset;
    rpc.first_column    +=  col_offset;       rpc.last_column   += col_offset;

}

/**-------------------------------------- INVERSE RPC METHODS ---------------------------------------- */

Eigen::Vector2d SatelliteImageRPC::calc_NRPCI2D(const Eigen::Vector3d &point) const {

    /// имея нормированные координаты  поверхности Земли , получаем точку на изображении

    double  samp_num    = calc_polynom(rpci.sample_numerator, point);
    double  samp_den    = calc_polynom(rpci.sample_denumerator, point);

    double  line_num    = calc_polynom(rpci.line_numerator, point);
    double  line_den    = calc_polynom(rpci.line_denumerator, point);


    return Eigen::Vector2d( samp_num/samp_den, line_num/line_den);
}



Eigen::Vector3d SatelliteImageRPC::calc_NRPCI3D(const Eigen::Vector3d &point) const {

    /// имея нормированные координаты  поверхности Земли , получаем точку на изображении и высоту  (высота просто копируется для удобства)

    Eigen::Vector2d result = calc_NRPCI2D(Eigen::Vector3d(point(0), point(1), point(2)));

    return Eigen::Vector3d( result(0), result(1), point(2));
}


/**-------------------------------------- DIRECT RPC METHODS ---------------------------------------- */


Eigen::Vector3d SatelliteImageRPC::calc_NRPC3D(const Eigen::Vector3d &pixel_height)  const {


/*
    double samp_num  = calc_polynom(rpc.sample_numerator,   point);
    double samp_den  = calc_polynom(rpc.sample_denumerator, point);

    double line_num  = calc_polynom(rpc.line_numerator,   point);
    double line_den  = calc_polynom(rpc.line_denumerator, point);
*/

    Eigen::Vector2d  pt = calc_NRPC2D(pixel_height);

    /**  supposed that pt(2) ( aka Height )  is already normalized !!! */
    return Eigen::Vector3d( pt(0), pt(1), pixel_height(2) );
//    return Eigen::Vector3d( samp_num/samp_den, line_num/line_den, point(2) );

}


Eigen::Vector2d SatelliteImageRPC::calc_NRPC2D(const Eigen::Vector3d &point) const {

    double samp_num     = calc_polynom(rpc.sample_numerator, point);
    double samp_den     = calc_polynom(rpc.sample_denumerator, point);

    double line_num     = calc_polynom(rpc.line_numerator, point);
    double line_den     = calc_polynom(rpc.line_denumerator, point);


    return Eigen::Vector2d( samp_num/samp_den, line_num/line_den);
}


double SatelliteImageRPC::calc_polynom(const std::array<double, 20> &polynom_coeffs, const Eigen::Vector3d &arg)  const{

    double Long   = arg(0);
    double Lat    = arg(1);
    double Height = arg(2);

    Eigen::VectorXd coords (20);
    Eigen::VectorXd coeffs_(20);

    for (int i=0;i<20;++i){     coeffs_(i) = polynom_coeffs[i];     }


    coords <<    1, Long, Lat, Height,
            Long*Lat, Long*Height, Lat*Height,
            Long*Long, Lat*Lat, Height*Height,
            Lat*Long*Height, Long*Long*Long, Long*Lat*Lat, Long*Height*Height, Long*Long*Lat,
            Lat*Lat*Lat, Lat*Height*Height, Long*Long*Height, Lat*Lat*Height, Height*Height*Height;


    return coords.dot(coeffs_);

}


Eigen::Vector3d SatelliteImageRPC::normalize_imageHeightCoordinates(const Eigen::Vector2d &pixel, double height)  const {


    return Eigen::Vector3d( (pixel(0)-rpc.sample_offset)/rpc.sample_scale,  (pixel(1)-rpc.line_offset)/rpc.line_scale, (height-rpc.offset_height)/rpc.scale_height );
}


Eigen::Vector3d SatelliteImageRPC::normalize_imageHeightCoordinates(const Eigen::Vector3d &pixel_height) const {


    return Eigen::Vector3d(  (pixel_height(0)-rpc.sample_offset)/rpc.sample_scale,  (pixel_height(1)-rpc.line_offset )/rpc.line_scale,  (pixel_height(2)-rpc.offset_height)/rpc.scale_height );
}



void  SatelliteImageRPC::normalize_imgCoordinatesArray(Eigen::Matrix2Xd &pixel_grid) const {

    Eigen::Vector2d pix_normalized;

    for (int i = 0; i < pixel_grid.cols(); ++i) {

        pix_normalized      = normalize_imgCoordinates(pixel_grid.col(i));
        pixel_grid.col(i)   =   pix_normalized;

    }
}

Eigen::Vector2d SatelliteImageRPC::normalize_imgCoordinates(const Eigen::Vector2d &pixel) const {

    /** using standard image coordinate system */
    return  Eigen::Vector2d ( (pixel(0)-rpc.sample_offset)/rpc.sample_scale,  (pixel(1)-rpc.line_offset)/rpc.line_scale );
}

Eigen::Vector2d SatelliteImageRPC::unnormalize_imgCoordinates(Eigen::Vector2d pixel) const {

    /** using standard image coordinate system */
    return  Eigen::Vector2d ( pixel(0)*rpc.sample_scale + rpc.sample_offset, pixel(1)*rpc.line_scale + rpc.line_offset );
}


Eigen::Vector3d SatelliteImageRPC::unnormalize_groundCoordinates(const Eigen::Vector3d &point)  const {

    /** LONGtitude LATitiude HEIGHT order */
    return  Eigen::Vector3d(
            point(0) * rpci.scale_longtitude + rpci.offset_longtitude,
            point(1) * rpci.scale_latitude   + rpci.offset_latitude,
            point(2) * rpci.scale_height     + rpci.offset_height
    );
}

Eigen::Vector3d SatelliteImageRPC::normalize_groundCoordinates(const Eigen::Vector2d &point, double height)  const {

    /** LONGtitude LATitiude HEIGHT order */
    return  Eigen::Vector3d(
            ( point(0) - rpci.offset_longtitude)  / rpci.scale_longtitude,
            ( point(1) - rpci.offset_latitude  )  / rpci.scale_latitude,
            ( height   - rpci.offset_height    )  / rpci.scale_height
    );
}

Eigen::Vector3d SatelliteImageRPC::normalize_groundCoordinates(const Eigen::Vector3d &point) const {

    /** LONGtitude LATitiude HEIGHT order */
    return  Eigen::Vector3d(
            ( point(0) - rpci.offset_longtitude)  / rpci.scale_longtitude,
            ( point(1) - rpci.offset_latitude  )  / rpci.scale_latitude,
            ( point(2) - rpci.offset_height    )  / rpci.scale_height
    );
}

Eigen::Vector3d SatelliteImageRPC::calc_NRPC3D(const Eigen::Vector2d &pixel, double height) const {

    Eigen::Vector3d pt;

    pt << pixel , height;

    double samp_num  = calc_polynom(rpc.sample_numerator, pt);
    double samp_den  = calc_polynom(rpc.sample_denumerator, pt);

    double line_num  = calc_polynom(rpc.line_numerator, pt);
    double line_den  = calc_polynom(rpc.line_denumerator, pt);


    return Eigen::Vector3d( samp_num/samp_den, line_num/line_den, height );
}

Eigen::Vector3d SatelliteImageRPC::calc_RPC3D(const Eigen::Vector3d &pixel_height) const {

    return unnormalize_groundCoordinates(  calc_NRPC3D( normalize_imageHeightCoordinates( pixel_height ) ) );
}


Eigen::Vector3d SatelliteImageRPC::calc_RPC3D(const Eigen::Vector2d &pixel, double height) const {


    return unnormalize_groundCoordinates(  calc_NRPC3D( normalize_imageHeightCoordinates( Eigen::Vector3d( pixel(0), pixel(1), height ))));
}

Eigen::Vector2d SatelliteImageRPC::calc_RPCI2D(const Eigen::Vector3d &ground_point)  const {

    return unnormalize_imgCoordinates(calc_NRPCI2D(normalize_groundCoordinates(ground_point)));
}


void SatelliteImageRPC::unnormalize_groundCoordinatesArray(Eigen::Matrix3Xd &coords_matrix) {


    int                 cols = coords_matrix.cols();
    Eigen::Vector3d     pt;


    for (int idx = 0; idx < cols; ++idx) {

        pt                          =   coords_matrix.col(idx);
        coords_matrix.col(idx)      =   unnormalize_groundCoordinates( pt);
    }
}


void SatelliteImageRPC::calc_NRPC3DArray(const Eigen::Matrix2Xd &array_px, double height, Eigen::Matrix3Xd &array_pt) {


    int                 cols = array_px.cols();
    Eigen::Vector2d     px;
    Eigen::Vector3d     pt;

    array_pt.conservativeResize( Eigen::NoChange, cols );


    for (int idx = 0; idx < cols; ++idx) {

        px                          =   array_px.col(idx);
        pt                          =   calc_NRPC3D(px, height);
        array_pt.col(idx)           =   pt;
    }

}

void SatelliteImageRPC::calc_NRPC3DArray(const Eigen::Matrix3Xd &array_pt, Eigen::Matrix2Xd &array_px) {


    int                 cols = array_pt.cols();
    Eigen::Vector2d     px;
    Eigen::Vector3d     pt;

    array_px.conservativeResize( Eigen::NoChange, cols );


    for (int idx = 0; idx < cols; ++idx) {

        pt                          =   array_pt.col(idx);
        px                          =   calc_NRPC2D(pt);
        array_px.col(idx)           =   px;
    }


}

void SatelliteImageRPC::unnormalize_imgCoordinatesArray(Eigen::Matrix2Xd &pixel_grid) const {


    Eigen::Vector2d px_unnormalized;

    for (int i = 0; i < pixel_grid.cols(); ++i) {

        px_unnormalized      =   unnormalize_imgCoordinates(pixel_grid.col(i));
        pixel_grid.col(i)    =   px_unnormalized;

    }

}


#endif //SSV_SATELLITEIMAGERPC_H