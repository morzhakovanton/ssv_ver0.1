

/// class SatelliteImageInfo - ImageInfo class hold information about path, image name etc


#ifndef SSV_SATELLITEIMAGEINFO_H
#define SSV_SATELLITEIMAGEINFO_H

#include <boost/filesystem.hpp>

#include <string>
#include <vector>

struct DataPleiades{

    std::string     path_DIM ;
    std::string     path_RPC ;
    std::string     path_IMG ;
    std::string     path_PREVIEW;

    void            reset_paths ();
    bool            check_pathsAreNotEmpty() ;

};

void DataPleiades::reset_paths() {

    path_DIM.clear();
    path_RPC.clear();
    path_IMG.clear();
    path_PREVIEW.clear();
}

bool DataPleiades::check_pathsAreNotEmpty() {

    return  path_DIM  != "" && path_RPC != "" && path_PREVIEW != "" && path_IMG != "";
}






class SatelliteImageInfo {
public:

    void                        set_pathPleiades            (std::string &path);

    std::string                 get_pathNameImage           (   );
    std::string                 get_pathNameRPC             (   );
    std::string                 get_pathNameDIM             (   );

    bool                        parse_pathPleiades          ( );


    std::string                 data_path;
    DataPleiades                data;

    struct path_leaf_string {  std::string operator()(const boost::filesystem::directory_entry& entry) const   {      return entry.path().leaf().string();   }     };

private:

    void                read_directory              ( const std::string& name, std::vector<std::string>& v);

};



void SatelliteImageInfo::set_pathPleiades(std::string &path) {

    /// указывается путь, в котором лежат все файлы, относящиеся к одному снимку Pleiades

    data_path = path;


}

void SatelliteImageInfo::read_directory(const std::string &name, std::vector<std::string>& v) {

    boost::filesystem::path                     p       ( name );
    boost::filesystem::directory_iterator       start   ( p    );
    boost::filesystem::directory_iterator       end;

    std::transform( start, end, std::back_inserter(v), path_leaf_string() );
}

bool SatelliteImageInfo::parse_pathPleiades() {

    /**
     * по пути data_path находится папка, содержащая подстроку 'IMG' и её имя прибавляется к data_path.
     * В обновлённом  data_path ищутся файлы с расширением .XML, содержащие подстроки 'RPC' и 'DIM' в имени файла
     * В обновлённом  data_path ищется файл  с расширением .JP2
     * В обновлённом  data_path ищется файл  с расширением .JPG, содержащий подстроку 'PREVIEW' в имени файла
     * Все найденные файлы с путями записываются в поля структуры data типа  DataPleiades
     * */

    std::vector<std::string>        dirs;

    read_directory( data_path, dirs );


    for (auto  dir : dirs ) {

        std::size_t found = dir.find("IMG");

        if (found != std::string::npos) {

            data_path   += dir;
            data_path   += "/";
            break;
        }

    }


    dirs.clear();
    data.reset_paths();

    read_directory( data_path, dirs );

    for (auto  file : dirs ) {

        std::size_t found_xml   =   file.find(".XML");
        std::size_t found_jp2   =   file.find(".JP2");
        std::size_t found_jpg   =   file.find(".JPG");

        if (found_xml != std::string::npos) {

            std::size_t found_dim = file.find("DIM");
            std::size_t found_rpc = file.find("RPC");

            if (found_dim  != std::string::npos)    {   data.path_DIM       =   data_path + file;     }
            if (found_rpc  != std::string::npos)    {   data.path_RPC       =   data_path + file;     }
        }

        if (found_jp2 != std::string::npos)         {

            std::size_t found_img = file.find("IMG");
            std::size_t found_aux = file.find(".aux");

            if (found_img  != std::string::npos &&  found_aux == std::string::npos )    {   data.path_IMG       =   data_path + file;     }

        }

        if (found_jpg != std::string::npos)         {

            std::size_t found_preview = file.find("PREVIEW");

            if (found_preview  != std::string::npos) {   data.path_PREVIEW   =   data_path + file;    }

        }
    }

    return data.check_pathsAreNotEmpty();

}

std::string SatelliteImageInfo::get_pathNameRPC() {

    return data.path_RPC;
}

std::string SatelliteImageInfo::get_pathNameDIM() {

    return data.path_DIM;
}

std::string SatelliteImageInfo::get_pathNameImage() {

    return data.path_IMG;
}


#endif  // SSV_SATELLITEIMAGEINFO_H