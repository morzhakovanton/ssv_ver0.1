
#ifndef SSV_SATELLITEIMAGE_H
#define SSV_SATELLITEIMAGE_H

#include <string>
#include <algorithm>

#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/opencv.hpp>

#include <eigen3/Eigen/Dense>

#include "Common/ImageRectangle.h"
#include "SatelliteImageRPC.h"
#include "SatelliteImageInfo.h"
#include "SatelliteImageViz.h"
#include "SatelliteImageDIM.h"


class SatelliteImage {

public:

    SatelliteImage();

    void                        read_imageData              ( std::string &path);
//    void                        get_imagePatch              ( const ImageRectangle& rect, cv::Mat& patch );
//    void                        get_imageSample             ( const ImageRectangle& roi, const Eigen::Vector2i& size_out,  cv::Mat& img_out );
//    void                        get_imagePreview            ( cv::Mat& imgPN );

    void                        calc_imageArea              ( );

    ImageRectangleI             convert_rectangleD2I        ( const ImageRectangleD& rect );
    ImageRectangleD             convert_rectangleI2D        ( const ImageRectangleI& rect );

    ImageRectangleD             normalize_rectangleI2D(const ImageRectangleI &rect);
    ImageRectangleD             normalize_rectangleD        ( const ImageRectangleD& rect );



    bool                        data_is_loaded;
    SatelliteImageRPC           rpc;
    SatelliteImageDIM           dim;
    SatelliteImageInfo          info;
    SatelliteImageViz            viz;

    ImageRectangleD             img_area;

    void get_imgFromRect(const ImageRectangleI &rectangle, cv::Mat &mat);

    ImageRectangleI unnormalize_rectangleD2I(const ImageRectangleD &rect);
};



SatelliteImage::SatelliteImage() : viz( &rpc, &info) {

}


void SatelliteImage::read_imageData(std::string &path) {

    /**
    * под чтением данных изображения понимается
    * 1) проверка наличия файлов с RPC и DIM данными для пути  path
    * 2) проверка наличия изображения в формате JPEG2000 (расширение .jp2)
    * 3) проверка наличия уменьшенного изображения PREVIEW в формате JPEG (.jpg)
    * Если RPC и DIM данные прочитаны и проверено наличие изображений  по указанному пути, то data_is_loaded присваивается значение true
    */

    data_is_loaded = false;

    info.set_pathPleiades(path);

    if (  info.parse_pathPleiades() )   {    data_is_loaded = true;     }
    else                                {   std::cerr << "Pleiades image read error" << std::endl;         return;     }

    rpc.read         ( info.get_pathNameRPC() );
    dim.readPleiades ( info.get_pathNameDIM());

//    dim.print_vertices();

}

ImageRectangleI SatelliteImage::convert_rectangleD2I(const ImageRectangleD &rect) {

    Eigen::Vector2d     pt1, pt2, pt3, pt4;
    int                 x, y, width, height;

    pt1  = rect.get_ul();

    pt4 = pt2 = pt1;

    pt4(0) += rect.width;
    pt4(1) += rect.height;


    ///test
    pt2 = Eigen::Vector2d(-1,-1);
    pt2         = rpc.unnormalize_imgCoordinates(pt2);
    ///test
    pt3 = Eigen::Vector2d(1,1);
    pt3         = rpc.unnormalize_imgCoordinates(pt3);


    pt1         = rpc.unnormalize_imgCoordinates(pt1);
    pt4         = rpc.unnormalize_imgCoordinates(pt4);


    x           =   std::round(pt1(0));
    y           =   std::round(pt1(1));
    width       =   std::round( pt4(0) - pt1(0));
    height      =   std::round( pt4(1) - pt1(1));

    return ImageRectangleI(x,y,width, height);
}

void SatelliteImage::calc_imageArea() {

    /**
     * берём первую и третью точку из Bounding Polygon  для нахождения нормированного прямоугольника, содержащего изображение
     * 1) берётся верхний левый и нижний правый углы изображения в координатах изображения (координаты не нормированны)
     * 2) нормируем крайние точки из предыдущего шага
     * */

    Eigen::Vector2d         pix_unnormed_1, pix_unnormed_3, pix_normed_1, pix_normed_3;


    pix_unnormed_1      =   Eigen::Vector2d( dim.bounding_polygon[0].column, dim.bounding_polygon[0].row);
    pix_unnormed_3      =   Eigen::Vector2d( dim.bounding_polygon[2].column, dim.bounding_polygon[2].row);

    pix_normed_1        = rpc.normalize_imgCoordinates(pix_unnormed_1);
    pix_normed_3        = rpc.normalize_imgCoordinates(pix_unnormed_3);


    img_area    =   ImageRectangleD( pix_normed_1(0), pix_normed_1(1), pix_normed_3(0)-pix_normed_1(0), pix_normed_3(1)-pix_normed_1(1) );

}

void SatelliteImage::get_imgFromRect(const ImageRectangleI &roi, cv::Mat &img) {

    if ( roi.check_isEmpty()) { std::cerr << "SatelliteImage::get_imgFromRect(); " << " ROI is empty"; return;}


    img = cv::Mat::zeros(roi.height, roi.width, CV_32FC1);

    GDALDataset         *dataset;
    GDALRasterBand      *poBand;
    CPLErr               error;

    GDALAllRegister();
    dataset = (GDALDataset *) GDALOpen( ( info.get_pathNameImage()).c_str(), GA_ReadOnly );

    if( dataset == NULL ) { std::cerr << "GDAL cannot read this f***ing file from the specified  path  \n";      return;     }

    poBand = dataset->GetRasterBand(1);
    error  = poBand->RasterIO(GF_Read, roi.x, roi.y, roi.width, roi.height, img.data, roi.width, roi.height, GDT_Float32 , 0, 0);
}



ImageRectangleD SatelliteImage::normalize_rectangleI2D(const ImageRectangleI &rect) {


    Eigen::Vector2d     ul_normalized       = rpc.normalize_imgCoordinates(rect.get_ul().cast<double>());
    Eigen::Vector2d     br_normalized       = rpc.normalize_imgCoordinates(rect.get_br().cast<double>());

    return ImageRectangleD( ul_normalized, br_normalized);

/*
    double              width_normalized    =  br_normalized(0) - ul_normalized(0);
    double              height_normalized   =  br_normalized(1) - ul_normalized(1);
    return ImageRectangleD( ul_normalized(0),  ul_normalized(1),  width_normalized, height_normalized );
*/
}



ImageRectangleI SatelliteImage::unnormalize_rectangleD2I(const ImageRectangleD &rect) {


    Eigen::Vector2i     ul_unnormalized     = rpc.unnormalize_imgCoordinates(rect.get_ul()).cast<int>();
    Eigen::Vector2i     br_unnormalized     = rpc.unnormalize_imgCoordinates(rect.get_br()).cast<int>();


    return ImageRectangleI(ul_unnormalized, br_unnormalized);
}


#endif // SSV_SATELLITEIMAGE_H
