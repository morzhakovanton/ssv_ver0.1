
#ifndef SSV_SATELLITEIMAGEPAIR_H
#define SSV_SATELLITEIMAGEPAIR_H


#include <algorithm>

#include <eigen3/Eigen/Dense>

#include "Common/ImageRectangle.h"
#include "SatellitePatches/PatchesStereoPair.h"
#include "SatelliteImagePairViz.h"
#include "SatelliteImage.h"

#include "SatteliteImagePairData.h"


class SatelliteImagesPair {

public:
    SatelliteImagesPair();

    void                        read_imagesData                         ( std::string &path1, std::string& path2 );

    void                        calc_workingAreas                       ( );
    void                        calc_img1PatchSizeNum                   ( float overlap, int min_res, int max_res );
    void                        calc_fitResolution                      ( float overlap, int min, int max, int length, int &segments_num, float &out_resolution);

    void                        project_normalizedRectFromImg1ToImg1    ( const ImageRectangleD &src, double height, ImageRectangleD &dst );
    void                        project_normalizedRectFromImg1ToImg2    ( const ImageRectangleD &src_rect, double height, ImageRectangleD &dst_rect );
    void                        project_normalizedRectFromImg2ToImg1    ( const ImageRectangleD &src_rect, double height, ImageRectangleD &dst_rect );
    void                        project_normalizedRectCornersToGroundImg1(const ImageRectangleD &src, double height,  Eigen::Matrix<double, 3,4>& corners);

    void                        get_patchesStereo_old                   ( int idx, PatchesStereoPair &pair);
    void                        get_patchesStereo                       ( int idx, PatchesStereoPair &pair);
    void                        get_img1PatchRect                       ( int idx, ImageRectangleI &rect);
    void                        make_equalRectangles                    ( ImageRectangleI &rect1, ImageRectangleI &rect2);
    Eigen::Vector2d             swap_coordinates2D                      ( const Eigen::Vector2d& v)  const;


    Eigen::Vector3d             swap_coordinates3D                      ( const Eigen::Vector3d& v)  const;
    SatelliteImage              img1, img2;
    SatteliteImagePairData      data;
    SatelliteImagePairViz        viz;

    bool                        ready_for_stereo;
};



SatelliteImagesPair::SatelliteImagesPair(): viz(&img1, &img2, &data) {  }



void SatelliteImagesPair::read_imagesData(  std::string &path1, std::string& path2   ) {

    /**
     * читаем данные для двух снимков по указанным путям и если чтение прошло успешно, присваиваем ready_for_stereo значение true
     * */

    img1.read_imageData( path1 );
    img2.read_imageData( path2 );

    if ( img1.data_is_loaded  && img2.data_is_loaded )      {       ready_for_stereo = true;                                                }
    else                                                    {       std::cerr << " images are not ready for stereo" << std::endl;   return; }


/*
    std::cout << "Image 1" << std::endl;
    img1.dim.print();
    std::cout << "Image 2" << std::endl;
    img2.dim.print_vertices();
*/

/*
    std::cout << "Image 1" << std::endl;
    img1.rpc.rpc.print();
    img1.rpc.rpci.print();
    std::cout << "Image 1" << std::endl;
    img2.rpc.rpc.print();
    img2.rpc.rpci.print_vertices();
*/

}


void SatelliteImagesPair::calc_workingAreas() {

    /**
     *  Нахождение  рабочих областей для двух снимков.  Для первого снимка ищется область, все точки которой ( для всех значений  высоты ) гарантированно  лежат на втором снимке.
     *  Для этого делаем несколько шагов:
     *
     *  1)   Берём нормированый  квадрат всего первого изображения ( вычисленный заранее, используя  DIM данные ) и проецируем его с высотами -1 и +1
     *       на второе изоюражениеж
     *  2)   Находим пересечение  двух полученных квадратов с квадратом всего второго изображения
     *  3)   Находим пересечение  двух квадратов с шага 2)
     *  4)   Найденный квадрат с шага 3) проецируем на первое изображение с высотами -1 и +1
     *  5)   Берём пересечение найденных квадратов  с шага 4) и считаем полученный квадрат искомым
     *  6)   предположително надо подавить погрешности  приближения рациональными многочленами, сжимаем прямоугольную область на маленькое значение
     */


    /// находим нормированный прямоугольник, которому соответствует каждое изображение
    img1.calc_imageArea();     img2.calc_imageArea();


    if ( !ready_for_stereo ){ std::cerr << "Images are not ready for stereo" << std::endl; return;}

    ImageRectangleD         rect_height_m1, rect_height_p1, second_img_overlap, second_img_circumscribe;

    /// шаг 1)
    project_normalizedRectFromImg1ToImg2       ( img1.img_area, -1, rect_height_m1 );
    project_normalizedRectFromImg1ToImg2       ( img1.img_area, +1, rect_height_p1 );

    /// шаг 2)
    rect_height_m1.overlap_inPlace             ( img2.img_area);
    rect_height_p1.overlap_inPlace             ( img2.img_area);

    /// шаг 3)
    second_img_overlap      = rect_height_m1.overlap(rect_height_p1);

    /// шаг 4)
    project_normalizedRectFromImg2ToImg1       ( second_img_overlap, -1, rect_height_m1 );
    project_normalizedRectFromImg2ToImg1       ( second_img_overlap, +1, rect_height_p1 );

    /// шаг 5)
    data.img1_working_area   = rect_height_m1.overlap(rect_height_p1);

    /// шаг 6)
    data.img1_working_area.shrinkBy(0.001);

    /**
     * Для вторго снимка ищется область, такая, что все точки из рабочей области первого изображения отображаются на рабочую область второго.
     *
     * 1. Для этого берётся прямоугольная (рабочая) область первого изображения и  делается проекция на второе изображение с высотами -1 и +1ж
     * 2. Берётся описанный прямоугольник двух полученных с первого  шага прямоугольников
     * */

    /// шаг 1)
    project_normalizedRectFromImg1ToImg2( data.img1_working_area, -1, rect_height_m1 );
    project_normalizedRectFromImg1ToImg2( data.img1_working_area, +1, rect_height_p1 );


    /// шаг 2)
    data.img2_working_area = rect_height_m1.circumscribe(rect_height_p1);
}



void SatelliteImagesPair::project_normalizedRectFromImg1ToImg1(const ImageRectangleD &src, double height, ImageRectangleD &dst) {

    /**
     * Повторная проекция (reprojection) прямоугольной области первого изображения  this->img1 на первое изображение с данной высотой.
     * Предполагается, что прямоугольники src_rect and dst_rect в нормированных координатах, то есть левый верхний угол принадлежит множеству [-1,+1]x[-1,+1], длиные сторон из [0,2]
     * */

    /** делаем простую проверку того, что  длина сторон нормированного прямоугольлника должна быть в окрестности двух*/
    if ( src.width > 2.5 || src.height > 2.5){ std::cout << "Rectangle should be normalized " << std::endl;}

    /** нумерация точек происходит от левого верхнего угла по часовой стрелке */
    Eigen::Vector3d     ground_pt_ul,ground_pt_br;
    Eigen::Vector2d     pix_ul, pix_br;

    /** проецируем  полученные точки левого верхнего  и правого нижнего угля прямоугольника  в систему координат Земли в нормированных координатах первого изображения */
    ground_pt_ul  =   img1.rpc.calc_NRPC3D( src.get_ul(), height);
    ground_pt_br  =   img1.rpc.calc_NRPC3D( src.get_br(), height);

    /** отображаем точки Земли обратно на первое изображение  с данной высотой*/
    pix_ul        =   img1.rpc.calc_NRPCI2D(ground_pt_ul);
    pix_br        =   img1.rpc.calc_NRPCI2D(ground_pt_br);

    /** интересующий нас прямоугол'ник : */
    dst = ImageRectangleD( pix_ul, pix_br );
}


void SatelliteImagesPair::project_normalizedRectFromImg1ToImg2(const ImageRectangleD &src_rect, double height, ImageRectangleD &dst_rect) {

    /**
     * Повторная проекция (reprojection) прямоугольной области первого изображения  this->img1 на второе изображение this->img2.
     * Предполагается, что прямоугольники src_rect and dst_rect в нормированных координатах, то есть левый верхний угол принадлежит множеству [-1,+1]x[-1,+1], длиные сторон из [0,2]
     * */

    /** делаем простую проверку того, что  длина сторон нормированного прямоугольлника должна быть в окрестности двух*/

    if ( src_rect.width > 2.5 || src_rect.height > 2.5){ std::cout << "Rectangle should be normalized " << std::endl;}

    /** нумерация точек происходит от левого верхнего угла по часовой стрелке */

    Eigen::Vector2d src_pix1, src_pix2, src_pix3, src_pix4;
    Eigen::Vector3d dst_pt1, dst_pt2, dst_pt3, dst_pt4;
    Eigen::Vector2d dst_pix1, dst_pix2, dst_pix3, dst_pix4;

    /** получаем нормированные координаты  на изображении первой и третьей точек  прямоугольника  ( левый верх и правый низ) */
//    Eigen::Vector2d test_pix = Eigen::Vector2d(0,0);
//    Eigen::Vector2d test_npix = img1.rpc.normalize_imgCoordinates( test_pix);
//    src_pix1        = Eigen::Vector2d( src_rect.y,                  src_rect.x                  );

    src_pix1        = Eigen::Vector2d( src_rect.x,                  src_rect.y                  );
    src_pix3        = Eigen::Vector2d( src_rect.x+src_rect.width,   src_rect.y+src_rect.height  );

    /** проецируем  полученные точки в систему координат Земли в нормированных координатах первого изображения */
    dst_pt1 = img1.rpc.calc_NRPC3D(src_pix1, height);
    dst_pt3 = img1.rpc.calc_NRPC3D(src_pix3, height);

//    Eigen::Vector3d  test_dst = img1.rpc.calc_RPC3D(test_pix, 0);
//    Eigen::Vector3d  test_dst2= img1.rpc.unnormalize_groundCoordinates(test_dst2);
//
    /** координаты полученных точек в системе координат Долгота-Широта-Высота*/
    dst_pt1 = img1.rpc.unnormalize_groundCoordinates(dst_pt1);
    dst_pt3 = img1.rpc.unnormalize_groundCoordinates(dst_pt3);

    /** координаты точек dst_pt1 and dst_pt2  Земли в нормированной системе координат второго изображения */
    dst_pt1 = img2.rpc.normalize_groundCoordinates(dst_pt1);
    dst_pt3 = img2.rpc.normalize_groundCoordinates(dst_pt3);

    /** отображаем точки Земли на второе изображение */
    dst_pix1 = img2.rpc.calc_NRPCI2D(dst_pt1);
    dst_pix3 = img2.rpc.calc_NRPCI2D(dst_pt3);
//    dst_pix4 = img2.rpc.calc_NRPC2D(dst_pt4);

    /** интересующий нас прямоугол'ник : */
    dst_rect.x          =   dst_pix1(0);
    dst_rect.y          =   dst_pix1(1);

    dst_rect.width      =   dst_pix3(0) - dst_pix1(0);
    dst_rect.height     =   dst_pix3(1) - dst_pix1(1);
}



void SatelliteImagesPair::project_normalizedRectFromImg2ToImg1(const ImageRectangleD &src_rect, double height, ImageRectangleD &dst_rect) {
    /**
     * Повторная проекция (reprojection) прямоугольной области первого изображения  this->img2 на второе изображение this->img1.
     * Предполагается, что прямоугольник src_rect в нормированных координатах, то есть левый верхний угол из [-1,+1]x[-1,+1], длиные сторон из [0,2]
     * */

    /** делаем простую проверку того, что  длина сторон нормированного прямоугольлника должна быть в окрестности двух*/

    if ( src_rect.width > 2.5 || src_rect.height > 2.5){ std::cout << "Rectangle sould be normalized " << std::endl;}

    /** нумерация точек происходит от левого верхнего угла по часовой стрелке */
    Eigen::Vector2d         src_pix1,   src_pix3,   src_pix4;
    Eigen::Vector3d         dst_pt1,    dst_pt3,    dst_pt4;
    Eigen::Vector2d         dst_pix1,   dst_pix3,   dst_pix4;

    /** получаем нормированные координаты  на изображении  трёх из  4х тчочек  прямоугольника */
    src_pix1 = Eigen::Vector2d( src_rect.x,                  src_rect.y                  );
    src_pix3 = Eigen::Vector2d( src_rect.x+src_rect.width,   src_rect.y+src_rect.height  );

    /** проецируем  полученные точки в систему координат Земли в нормированных координатах второго изображения */
    dst_pt1 = img2.rpc.calc_NRPC3D(src_pix1, height);
    dst_pt3 = img2.rpc.calc_NRPC3D(src_pix3, height);

    /** координаты полученных точек в системе координат Долгота-Широта-Высота*/
    dst_pt1 = img2.rpc.unnormalize_groundCoordinates(dst_pt1);
    dst_pt3 = img2.rpc.unnormalize_groundCoordinates(dst_pt3);

    /** координаты точек dst_pt1 and dst_pt2  Земли в нормированной системе координат первого изображения */
    dst_pt1 = img1.rpc.normalize_groundCoordinates(dst_pt1);
    dst_pt3 = img1.rpc.normalize_groundCoordinates(dst_pt3);


    /** отображаем точки Земли на второе изображение */
    dst_pix1 = img1.rpc.calc_NRPCI2D(dst_pt1);
    dst_pix3 = img1.rpc.calc_NRPCI2D(dst_pt3);


    /** интересующий нас прямоугол'ник : */
    dst_rect.x          =   dst_pix1(0);
    dst_rect.y          =   dst_pix1(1);

    dst_rect.width      =   dst_pix3(0) - dst_pix1(0);
    dst_rect.height     =   dst_pix3(1) - dst_pix1(1);

}




void SatelliteImagesPair::calc_img1PatchSizeNum(float overlap, int min_res, int max_res) {

    /** Для использования алгоритмов  нахождения соответствий (карты сдвигов)  для стереопарных изображений  (или приведённых к таковым) необходимо сделать несколько шагов:
     *
     * 1) разбить первое изображение на пересекающиеся прямоугольники ( степень пересечения -- параметр overlap ). Каждый прямоугольник будет имеет разрешение от min_res до max_res (по каждой стороне).
     *    Если сузить диапозон  [min_res,max_res], то получаемое разбиение не будет покрывать изображение целиком (а только его часть). Такоим образом, решается простая двумерная
     *    задача, которая разбивается на две одномерных
     *
     * 2) для каждого прямоугольника на первом изображении находится прямоугольник на втором изображении такой, что все точки  прямоугольника первого изображения с любыми высотами
     *    лежат на  прямоугольнике второго изображения. Далее разрешение обоих прямоугольников  делается одинаковым (т. е. увеличивается первый ).
     *
     *  размер  куска SatteliteImagePairData.img1_patch_size  вычисляется в  (дробных) пикселях
     * */


    float width, height, overlap_;
    int num_of_segments;

//    overlap_ = std::clamp(overlap, 0.0f, 0.5f); // error: ‘clamp’ is not a member of ‘std’
    overlap_ = std::max(0.0f, std::min(overlap, 0.5f));

    if ( overlap_ != overlap ) { std::cout << " Overlap clamped" << std::endl; }



    int     patches_num_width,  patches_num_height;
    float   patches_res_width,  patches_res_height;

//     data.img1_working_area

    ImageRectangleI img1_wa = img1.convert_rectangleD2I( data.img1_working_area );

    calc_fitResolution(overlap_, min_res, max_res, img1_wa.width  /*img1.dim.number_of_columns*/, patches_num_width,  patches_res_width  );
    calc_fitResolution(overlap_, min_res, max_res, img1_wa.height /*img1.dim.number_of_rows*/,    patches_num_height, patches_res_height );

    data.img1_patch_size         =  Eigen::Vector2f ( patches_res_width,  patches_res_height );
    data.img1_patches_num        =  Eigen::Vector2i ( patches_num_width,  patches_num_height );

}




void SatelliteImagesPair::calc_fitResolution(float overlap, int min, int max, int length, int &segments_num, float &out_resolution) {

    /**
     * Решается одномерная задача о нахождении размера отрезков и их количества, которые покрывают заданную длину с данным перехлёстом
     *
     *  1) находим среднее между min и max разрешением    mid =  0,5*(min+max)
     *  2) находим нецелове количество отрезков со средним разрешением mid
     *  3) берём  целую часть  от нецелого кол-ва отрезков   [mid]
     *  4) находим  длину отрезков, которые покрыавют всю длину
     *  5) если длина отрезка больше, чем max,  берём  ( целую часть +1 ) от нецелого кол-ва отрезков
     *  6) находим  длину отрезков, которые покрыавют всю длину
     *  7) если длина отрезка меньше, чем min,  полагаем  количество отрезков   целой части от нецелого кол-ва отрезков и длину отрезков как  0,5*(min+max)
     *
     * */

    data.patches_overlap = overlap;

    float   num_of_segments, mid, res;
    int     num_of_segments_;


    mid  = (float) 0.5* ( min + max);


    num_of_segments     =  (length - mid) / ((1.0-overlap)*mid) + 1.0;

    num_of_segments_    =  std::floor(num_of_segments);
    res                 =   length/( (num_of_segments_-1)*(1.0-overlap) + 1 );


    if ( res > max ){

        num_of_segments_    =  std::ceil(num_of_segments);
        res                 =  length/( (num_of_segments_-1)*(1.0-overlap) + 1 );
    }
    if ( res < min ){

        num_of_segments_    =  std::floor(num_of_segments);
        res                 =  mid;
    }


    out_resolution  = res;
    segments_num    = num_of_segments_;

}



void SatelliteImagesPair::get_patchesStereo_old(int idx, PatchesStereoPair &pair) {



/*
    Eigen::Vector2d     patch1_rect_ul_normalized  = pair.patch1.rpc.normalize_imageCoordinates( pair.patch1.rect_src.get_ul().cast<double>()   );
    Eigen::Vector2d     patch1_rect_br_normalized  = pair.patch1.rpc.normalize_imgCoordinates( pair.patch1.rect_src.get_br().cast<double>()   );


    Eigen::Vector3d            bbox_corner_1    =   pair.patch1.rpc.calc_NRPC3D( patch1_rect_ul_normalized, -1 );
    Eigen::Vector3d            bbox_corner_2    =   pair.patch1.rpc.calc_NRPC3D( patch1_rect_br_normalized, +1 );
*/


    ImageRectangleI     patch1_rect, patch2_rect;
    ImageRectangleD     rect_img2_height_m1,  rect_img2_height_p1;
    ImageRectangleD     rect_img1_height_m1,  rect_img1_height_p1;
    ImageRectangleI     test_rect_1, test_rect_2;
    ImageRectangleI     test_rect_3, test_rect_4;
    ImageRectangleD     normalized_patch1;

    get_img1PatchRect(idx, patch1_rect);

    pair.patch1.rect_src =  patch1_rect;
    pair.patch2.rect_src =  ImageRectangleI();

    normalized_patch1 = img1.normalize_rectangleI2D(patch1_rect);

    project_normalizedRectFromImg1ToImg1       ( normalized_patch1, -1.0, rect_img1_height_m1 );
    project_normalizedRectFromImg1ToImg1       ( normalized_patch1, +1.0, rect_img1_height_p1 );

    project_normalizedRectFromImg1ToImg2       ( normalized_patch1, -1.0, rect_img2_height_m1 );
    project_normalizedRectFromImg1ToImg2       ( normalized_patch1, +1.0, rect_img2_height_p1 );

    rect_img1_height_m1.circumscribe_inPlace(rect_img1_height_p1);
    rect_img2_height_m1.circumscribe_inPlace(rect_img2_height_p1);

    pair.patch1.rect = img1.unnormalize_rectangleD2I(rect_img1_height_m1);
    pair.patch2.rect = img2.unnormalize_rectangleD2I(rect_img2_height_m1);

    make_equalRectangles(pair.patch1.rect, pair.patch2.rect);

    img1.get_imgFromRect( pair.patch1.rect, pair.patch1.imgPN);
    img2.get_imgFromRect( pair.patch2.rect, pair.patch2.imgPN);

    pair.patch1.rpc = img1.rpc;
    pair.patch2.rpc = img2.rpc;
}



void SatelliteImagesPair::get_patchesStereo(int idx, PatchesStereoPair &pair) {

    ImageRectangleI     patch1_rect, patch2_rect;
    ImageRectangleD     rect_img2_height_m1,  rect_img2_height_p1;
    ImageRectangleD     rect_img1_height_m1,  rect_img1_height_p1;
    ImageRectangleI     test_rect_1, test_rect_2;
    ImageRectangleI     test_rect_3, test_rect_4;
    ImageRectangleD     normalized_patch1;
    Eigen::Vector2d     shift;

    get_img1PatchRect(idx, patch1_rect);

    pair.patch1.rect_src =  patch1_rect;
    pair.patch2.rect_src =  ImageRectangleI();

    normalized_patch1 = img1.normalize_rectangleI2D(patch1_rect);


    project_normalizedRectFromImg1ToImg2(normalized_patch1, -1.0, rect_img2_height_m1);
    project_normalizedRectFromImg1ToImg2(normalized_patch1, +1.0, rect_img2_height_p1);

    rect_img2_height_m1.circumscribe_inPlace(rect_img2_height_p1);
    rect_img2_height_m1.pushInside(this->img2.img_area);

    project_normalizedRectFromImg2ToImg1(rect_img2_height_m1, -1.0, rect_img1_height_m1);
    project_normalizedRectFromImg2ToImg1(rect_img2_height_m1, +1.0, rect_img1_height_p1);

    normalized_patch1 = rect_img1_height_m1.overlap(rect_img1_height_p1);


    ///--------------------------------------------------------------------------------------------------



    pair.patch1.rect = img1.unnormalize_rectangleD2I(normalized_patch1);
    pair.patch2.rect = img2.unnormalize_rectangleD2I(rect_img2_height_m1);

    make_equalRectangles(pair.patch1.rect, pair.patch2.rect);


    img1.get_imgFromRect( pair.patch1.rect, pair.patch1.imgP);
    img2.get_imgFromRect( pair.patch2.rect, pair.patch2.imgP);

    pair.patch1.rpc = img1.rpc;
    pair.patch2.rpc = img2.rpc;


    pair.patch1.calc_intensityRange();
    pair.patch2.calc_intensityRange();

    pair.patch1.normalize_byTruncatedIntensityRange(0.1);
    pair.patch2.normalize_byTruncatedIntensityRange(0.1);
}



void SatelliteImagesPair::get_img1PatchRect(int idx, ImageRectangleI &rect) {

    int                 row_rect, col_rect;
    ImageRectangleI     img1_wa;

    /** just idx value clamping */
    idx             =   std::max( 0,  std::min( idx,  data.img1_patches_num(0)*data.img1_patches_num(1) ) );
    img1_wa         =   img1.convert_rectangleD2I( data.img1_working_area);

    row_rect        =  (float) idx/data.img1_patches_num(0);
    col_rect        =   idx - row_rect*data.img1_patches_num(0);

    rect.x          =   std::round ( data.img1_patch_size(0) * (  (col_rect)*(1-data.patches_overlap ) /*+ 1 */) );
    rect.y          =   std::round ( data.img1_patch_size(1) * (  (row_rect)*(1-data.patches_overlap ) /*+ 1 */) );

    rect.x         +=   img1_wa.x;
    rect.y         +=   img1_wa.y;


    rect.width      =   std::round ( data.img1_patch_size(0) );
    rect.height     =   std::round ( data.img1_patch_size(1) );

}

Eigen::Vector2d SatelliteImagesPair::swap_coordinates2D(const Eigen::Vector2d &v)  const {

    /** вспомогательнаый метод для перехода от  X-Y координат к row-column или обратно. Первые две составляющие  вектора v меняются местами  */
    return Eigen::Vector2d( v(1), v(0) );
}

Eigen::Vector3d SatelliteImagesPair::swap_coordinates3D(const Eigen::Vector3d &v)  const {

    /** вспомогательнаый метод для перехода от  X-Y координат к row-column или обратно. Первые две составляющие  вектора v меняются местами  */
    return Eigen::Vector3d( v(1), v(0), v(2));
}

void SatelliteImagesPair::project_normalizedRectCornersToGroundImg1(const ImageRectangleD &src, double height, Eigen::Matrix<double, 3, 4> &corners) {


    Eigen::Matrix<double, 2, 4>     src_corners = src.get_corners();
    Eigen::Vector2d                 pix;
    Eigen::Vector3d                 P;


    for (int idx = 0; idx < 4; ++idx) {

        pix                 =       src_corners.col(idx);
        P                   =       img1.rpc.calc_NRPC3D(pix,height);
        P                   =       img1.rpc.unnormalize_groundCoordinates(P);
        corners.col(idx)    =       P;
    }

}

void SatelliteImagesPair::make_equalRectangles(ImageRectangleI &rect1, ImageRectangleI &rect2) {


    int  width_new      =   std::max( rect1.width, rect2.width);
    int  height_new     =   std::max( rect1.height, rect2.height);

    rect1.fitSizeTo(width_new, height_new);
    rect2.fitSizeTo(width_new, height_new);
}


#endif // SSV_SATELLITEIMAGEPAIR_H

