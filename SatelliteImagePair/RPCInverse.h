

#ifndef SSV_SATELLITERPCINVERSE_H
#define SSV_SATELLITERPCINVERSE_H

#include <array>

class RPCInverse {
public:

  std::array< double,20 >       sample_numerator,       sample_denumerator;
  std::array< double,20 >       line_numerator,         line_denumerator;

  double                        error_bias_row,     error_bias_column;
  double                        first_longtitude,   last_longtitude;
  double                        first_latitude,     last_latitude;
  double                        scale_longtitude,   offset_longtitude;
  double                        offset_latitude,    scale_latitude;
  double                        scale_height, offset_height;

  void print();
};

void RPCInverse::print() {


    std::cout << "Inverse RPC: " << std::endl;


    for (int i = 0; i < 20; ++i) {

        std::cout <<    std::setprecision(17)   << std::setw(25) << sample_numerator[i] << " " << std::setw(25)  << sample_denumerator[i] << " "
                  <<    std::setw(25)           << line_numerator[i] << " " << std::setw(25)  << line_denumerator[i] << std::endl;
    }

    std::cout << std::endl;

    std::cout <<  "Error Bias Row: " << error_bias_row << "; Error Bias Column : " << error_bias_column  << std::endl;

    std::cout << "First Longitude: " << first_longtitude << " ; First Latitude: " << first_latitude <<
              "; Last Longitude: " << last_longtitude << "; Last Latitude: " << last_latitude << std::endl;

    std::cout << "-------------------------------------------------------------------------------------------------------- \n" << std::endl;
}


#endif //  SSSV_SATELLITERPCINVERSE_H