//
// Created by morzh on 13.11.17.
//

#ifndef SSV_VER_0_1_SATTELITEIMAGEPAIRDATA_H
#define SSV_VER_0_1_SATTELITEIMAGEPAIRDATA_H

#include <Common/ImageRectangle.h>

class SatteliteImagePairData{
public:

    ImageRectangleD             img1_working_area,  img2_working_area;
//    ImageRectangleD             img1_patch_size;


    Eigen::Vector2f             img1_patch_size;  /** X-Y format  or  the same column-row order */
    Eigen::Vector2i             img1_patches_num; /** X-Y format */
    float                       patches_overlap;
};

#endif //SSV_VER_0_1_SATTELITEIMAGEPAIRDATA_H
