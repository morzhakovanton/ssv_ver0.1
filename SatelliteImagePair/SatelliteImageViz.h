/// class SatImgVizUitlity -

#ifndef SSV_SATELLITEIMAGEVIZUTILITY_H
#define SSV_SATELLITEIMAGEVIZUTILITY_H

#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"

#include "boost/multi_array.hpp"
#include "gdal/gdal_priv.h"


#include "SatelliteImageRPC.h"
#include "SatelliteImageInfo.h"
#include "Common/ImageRectangle.h"



class SatelliteImageViz {

public:

                SatelliteImageViz    (  SatelliteImageRPC* p_rpc,  SatelliteImageInfo* p_info);

    void        get                 ( cv::Mat &img, const ImageRectangleI &roi, const Eigen::Vector2i &out_size);

    void        show_imageScaled    ( const  cv::Mat& img);
    void        show_image          ( const  cv::Mat& image);
    void        read_image          ( const  std::string& file_name, cv::Mat& img);
    void        save_imageScaled    ( const  cv::Mat& img, const std::string& file_name);
    void        save_image          ( const  std::string& file_name);


    SatelliteImageRPC*  p_rpc;
    SatelliteImageInfo* p_info;
};



SatelliteImageViz::SatelliteImageViz(  SatelliteImageRPC* p_rpc,  SatelliteImageInfo* p_info ) : p_rpc(p_rpc), p_info(p_info) {

}


void SatelliteImageViz::get(cv::Mat &img, const ImageRectangleI &roi, const Eigen::Vector2i &out_size) {

    if ( roi.check_isEmpty() ) { std::cerr <<  "SatelliteImageViz::get; " << "ROI is empty"; return;}


    img = cv::Mat::zeros(  cv::Size ( out_size(0), out_size(1) ),  CV_32FC1);

    GDALDataset     *dataset;
    GDALRasterBand  *poBand;

    GDALAllRegister();
    dataset = (GDALDataset *) GDALOpen( ( p_info->get_pathNameImage()).c_str(), GA_ReadOnly );

    if( dataset == NULL ) { std::cerr << "GDAL cannot read this f***ing file from the specified  path  \n";      return;     }

    poBand = dataset->GetRasterBand(1);

    CPLErr error = poBand->RasterIO(GF_Read, roi.x, roi.y, roi.width, roi.height, img.data,  out_size(0), out_size(1) , GDT_Float32 , 0, 0);

}


#endif // SSV_SATELLITEIMAGEVIZUTILITY_H