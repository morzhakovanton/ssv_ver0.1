//
// Created by morzh on 24.08.16.
//

#ifndef AVX2_TESTS_SETTINGS_H
#define AVX2_TESTS_SETTINGS_H

#define PYRAMID_LEVELS                  4
#define FRAME_THRESHOLD_EDGES           35
#define FRAME_THRESHOLD_CORNERS         20e+4

#define GRADIENT_THETA                  0.09f
#define ENCC_EDGES_EPSILON              0.6f
#define ENCC_EDGES_MAXCLEARNESS         0.015f

#define ENCC_CORNERS_EPSILON            0.5f
#define CORNERS_MAXCLEARNESS            0.02f

#endif //AVX2_TESTS_SETTINGS_H
