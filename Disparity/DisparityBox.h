//
// Created by morzh on 24.09.18.
//

#ifndef SSV_VER_0_1_DISPARITYBOX_H
#define SSV_VER_0_1_DISPARITYBOX_H


#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>

#include "MeasureZMNCC.h"
#include "../SatellitePatches/PatchesStereoPair.h"
#include "Frame.h"
#include "FrameViz.h"
#include "DisparityBoxDebug.h"

//#define MATCHDEBUG



struct DisparityBoxOptions{

    float               epsilon                     =       0.85;
    float               min_clearness               =       0.05;
    bool                consistency_check           =       false;
    Eigen::Vector2i     windowHalfSize              =       Eigen::Vector2i(3,3);
    int                 num_pyramid_levels          =       4;
    int                 search_range                =       9;
    int                 search_range_init_guess     =       0;
    int                 consistency_check_range     =       5;
    float               mappable_points_treshold    =       0.15;
};




class DisparityBox{

public:
                DisparityBox                   ( );
    void        estimate_disparityPair              ( PatchesStereoPair &pair, const DisparityBoxOptions &options = DisparityBoxOptions());
    void        estimate_disparityImages            ( const cv::Mat &img1, const cv::Mat &img2, cv::Mat &disparity, cv::Mat &mask, bool use_init_guess = true);
    float       estimate_disparityAtPoint           ( const cv::Mat &left_stripe, const cv::Mat &right_stripe, float d);

    void        build_frames                        ( const PatchesStereoPair &pair);

    void        get_leftStripe                      ( int row, int col, const cv::Mat &img1, cv::Mat &left_stripe);
    void        get_rightStripe                     ( int row, int col, float d, const cv::Mat &img2, cv::Mat &right_stripe);

    bool        check_matchingSizes                 ( const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &disparity, const cv::Mat &mask);
    bool        check_matchingImageTypes            ( const cv::Mat &img1, const cv::Mat &img2, cv::Mat disparity, cv::Mat mask);
    template<typename T>
    bool        check_consistency                   ( const cv::Mat &left_stripe, const cv::Mat &right_stripe, int d);
    bool        check_selfSimilarity                ( const cv::Mat &left_stripe, const cv::Mat &right_stripe, int d);
    template<typename T>
    int         find_disparityInt                   ( const cv::Mat &left_block, const cv::Mat &right_stripe);
    template<typename T>
    float       refine_subpixelAccuracy             ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);


    Frame                       frame1, frame2;
    DisparityBoxOptions             options;
    DisparityBoxDebug      debug;
};



DisparityBox::DisparityBox() : debug(&frame1, &frame2) {

}


void DisparityBox::estimate_disparityPair(PatchesStereoPair &pair, const DisparityBoxOptions &options) {

    this->options = options;

    build_frames(pair);


//    frame1.viz.show_imagePyramid(opts.num_pyramid_levels);
//    frame1.viz.show_gradientsPyramid();
//    frame1.viz.show_edgesPyramid(false);
//    frame1.viz.show_masksPyramid();


    int cols        = frame1.data.images[frame1.active_lvl].cols;
    int rows        = frame1.data.images[frame1.active_lvl].rows;
    int num_lvls    = options.num_pyramid_levels;


    for ( int lvl=num_lvls-1; lvl>=0 ;--lvl){

        estimate_disparityImages(frame1.data.images[lvl], frame2.data.images[lvl], frame1.data.disparities[lvl], frame1.data.masks[lvl]);

        if ( lvl == num_lvls-1)
            frame1.fill_disparitiesHoles(lvl);

        frame1.proppagate_diaprityToLvlvDownFrom(lvl);
    }

    frame1.data.disparities[0].copyTo(pair.disparity_local.ref2target);

}


void DisparityBox::estimate_disparityImages(const cv::Mat &img1, const cv::Mat &img2, cv::Mat &disparity, cv::Mat &mask, bool use_init_guess) {

    if ( !check_matchingSizes(img1, img2, disparity, mask) || !check_matchingImageTypes(img1, img2, disparity, mask))
        return;

    cv::Mat     leftStripe, rightStripe;

    for ( int row=0; row < mask.rows; ++row){
        for (int col=0; col<mask.cols; ++col){

            if ( !mask.at<bool>(row,col))
                continue;

            try{

                float d =  std::round(disparity.at<float>(row,col));

                get_leftStripe (row, col,    img1, leftStripe);
                get_rightStripe(row, col, d, img2, rightStripe);
#ifdef  MATCHDEBUG
                debug.show_stripes();
#endif

                disparity.at<float>(row,col) = estimate_disparityAtPoint(leftStripe, rightStripe, d);
#ifdef  MATCHDEBUG
                debug.show_disparity();
#endif
            }
            catch(const std::exception& e){

                mask.at<bool>(row,col) = false;
                continue;
            }

        }

    }
}

void DisparityBox::build_frames(const PatchesStereoPair &pair) {

    frame1.data.clear();
    frame2.data.clear();

    frame1.set_image(pair.patch1.imgPNT);
    frame2.set_image(pair.patch2.imgPNT);


    frame1.build_keyFrame(options.num_pyramid_levels, options.mappable_points_treshold);
    frame2.build_frame(options.num_pyramid_levels);
}

bool DisparityBox::check_matchingSizes(const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &disparity, const cv::Mat &mask) {


    bool rows_check = (img1.rows == img2.rows) && (img1.rows == disparity.rows) && (img1.rows == mask.rows);
    bool cols_check = (img1.cols == img2.cols) && (img1.cols == disparity.cols) && (img1.rows == mask.cols);

    if (  rows_check && cols_check ) {

        std::cerr << "DisparityBox:sizes of  data for stereo  are not matched" << std::endl;
        return false;
    }

    return true;
}

bool DisparityBox::check_matchingImageTypes(const cv::Mat &img1, const cv::Mat &img2, cv::Mat disparity, cv::Mat mask) {


    if ( img1.type() != CV_32FC1){

        std::cerr << "DisparityBox: Image 1 type is not one channel float grayscale (32FC1)" << std::endl;
        return false;
    }


    if ( img2.type() != CV_32FC1){

        std::cerr << "DisparityBox: Image 2 type is not one channel float grayscale (32FC1)" << std::endl;
        return false;
    }


    if ( disparity.type() != CV_32FC1){

        std::cerr << "DisparityBox: ref2target type is not one channel float grayscale (32FC1)" << std::endl;
        return false;
    }


    if ( mask.type() != IPL_DEPTH_1U){

        std::cerr << "DisparityBox: mask type is not one channel bool (IPL_DEPTH_1U)" << std::endl;
        return false;
    }


    return true;
}

void DisparityBox::get_leftStripe(int row, int col, const cv::Mat &img1, cv::Mat &left_stripe) {

    int         sz_half_x   =   options.windowHalfSize(0);
    int         sz_half_y   =   options.windowHalfSize(1);

    int         range_x1    =   options.consistency_check_range;
    int         range_x2    =   options.consistency_check_range;

    cv::Rect    rect_block  =   cv::Rect(col-sz_half_x-range_x1, row-sz_half_y, 2*sz_half_x+1+2*range_x2, 2*sz_half_y+1 );
    cv::Rect    rect_img    =   cv::Rect(0, 0, img1.cols, img1.rows);

#ifdef  MATCHDEBUG
    debug.rect_leftStripe = rect_block;
    img1.copyTo(debug.img1);
    debug.center_leftStripe = Eigen::Vector2i(col, row);
#endif

    if ( (rect_block & rect_img) == rect_block) {

        img1(rect_block).copyTo(left_stripe);

#ifdef  MATCHDEBUG
        debug.left_stripe_isValid   = false;
        debug.left_stripe           = left_stripe;
#endif
    }
    else{

#ifdef  MATCHDEBUG
        debug.left_stripe_isValid   = false;
#endif
        throw std::runtime_error("get_leftStripe failed, requested rectangle is out of bounds");
    }

}

void DisparityBox::get_rightStripe(int row, int col, float d, const cv::Mat &img2, cv::Mat &right_stripe) {

    int         sz_half_x   = options.windowHalfSize(0);
    int         sz_half_y   = options.windowHalfSize(1);

    int         range_x1    = options.search_range;
    int         range_x2    = options.search_range;

    cv::Rect    rect_block  = cv::Rect(col-sz_half_x-range_x1+d, row-sz_half_y, 2*sz_half_x+1 + 2*range_x2, 2*sz_half_y+1);
    cv::Rect    rect_img    = cv::Rect(0, 0, img2.cols, img2.rows);

#ifdef  MATCHDEBUG
    debug.rect_rightStripe = rect_block;
    img2.copyTo(debug.img2);
    debug.center_rightStripe = Eigen::Vector2i(col, row);
#endif

    if ( (rect_block & rect_img) == rect_block) {

        img2(rect_block).copyTo(right_stripe);

#ifdef  MATCHDEBUG
        debug.right_stripe          = right_stripe;
        debug.right_stripe_isValid  = false;
#endif
    }
    else{

#ifdef  MATCHDEBUG
        debug.right_stripe_isValid  = false;
#endif
        throw std::runtime_error("get_rightStripe failed, requested rectangle is out of bounds");
    }
}



float DisparityBox::estimate_disparityAtPoint(const cv::Mat &left_stripe, const cv::Mat &right_stripe, float d){

    cv::Mat     left_block, right_block;
    cv::Rect2i  left_block_rect, right_block_rect;

    int         d_L2R       = d;
    int         sz_half_x   = options.windowHalfSize(0);
    int         sz_half_y   = options.windowHalfSize(1);


    cv::Rect2i rect_left_block = cv::Rect2i(  options.consistency_check_range, 0, 2*sz_half_x+1, 2*sz_half_y+1);

    left_stripe(rect_left_block).copyTo(left_block);

    d_L2R = find_disparityInt<MeasureZMNCC>(left_block, right_stripe);

    if ( !check_consistency<MeasureZMNCC>(left_stripe, right_stripe, d_L2R)  ){
//    if ( !check_consistency<MeasureZMNCC>(left_stripe, right_stripe, d_L2R) || !check_selfSimilarity(left_stripe, right_stripe, d_L2R) ){

        throw std::runtime_error("check_consistency or check_selfSimilarity failed");
    }

//    filter_flatPatchDetection();
    return  refine_subpixelAccuracy<MeasureZMNCC>(left_block, right_stripe, d_L2R);
}


template<typename T>
bool DisparityBox::check_consistency(const cv::Mat &left_stripe, const cv::Mat &right_stripe, int d) {

    int                             consistency_range       =   options.consistency_check_range;
    int                             sz_half_x               =   options.windowHalfSize(0);
    int                             sz_half_y               =   options.windowHalfSize(1);
    int                             left_blocks_num         =   left_stripe.cols - (2*sz_half_x+1);
    cv::Mat                         left_block, right_block;
    std::vector<float>              costs(left_blocks_num);
    T                               measure;
    std::vector<float>::iterator    max_cost;
    int                             d_R2L;


    cv::Rect2i right_block_rect = cv::Rect2i(d, 0, 2*sz_half_x+1, 2*sz_half_y+1);
    right_stripe(right_block_rect).copyTo(right_block);


    for( int idx=0; idx <= left_blocks_num; idx++){

        cv::Rect left_block_rect = cv::Rect2i(idx, 0, 2*sz_half_x+1, 2*sz_half_y+1);
        left_stripe(left_block_rect).copyTo(left_block);

        costs[idx] = measure.calc_cost( left_block, right_block);
    }


    max_cost = std::max_element(costs.begin(), costs.end());
    d_R2L    = (int)std::distance( costs.begin(), max_cost );

    if ( d_R2L != 0 || *max_cost < options.epsilon) /// !!!!! here could be a bug !!!!
        return false;
    else
        return  true;
}

bool DisparityBox::check_selfSimilarity(const cv::Mat &left_stripe, const cv::Mat &right_stripe, int d) {

    return false;
}

template<typename T>
int DisparityBox::find_disparityInt(const cv::Mat &left_block, const cv::Mat &right_stripe) {

    int                             sz_half_x        =   options.windowHalfSize(0);
    int                             sz_half_y        =   options.windowHalfSize(1);
    int                             right_blocks_num =   right_stripe.cols - (2*sz_half_x+1);
    cv::Mat                         right_block;
    std::vector<float>              costs(right_blocks_num);
    T                               measure;
    std::vector<float>::iterator    max_cost;


    for( int idx=0; idx <= right_blocks_num; idx++){

        cv::Rect right_block_rect = cv::Rect2i(idx, 0, 2*sz_half_x+1, 2*sz_half_y+1);
        right_stripe(right_block_rect).copyTo(right_block);

        costs[idx] = measure.calc_cost( left_block, right_block);
    }

    max_cost = std::max_element(costs.begin(), costs.end());


    return  (int)std::distance( costs.begin(), max_cost );
}

template<typename T>
float DisparityBox::refine_subpixelAccuracy(const cv::Mat &left_block, const cv::Mat &right_stripe, int d) {

    T measure;

    return measure.calc_subpix(left_block, right_stripe, d);
}

#endif //SSV_VER_0_1_DISPARITYBOX_H
