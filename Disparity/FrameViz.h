//
// Created by morzh on 25.09.18.
//

#ifndef SSV_VER_0_1_FRAMEVIZ_H
#define SSV_VER_0_1_FRAMEVIZ_H


#include <string>
#include <vector>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/imgproc.hpp>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <fstream>

#include "FrameData.h"
#include "../Common/VizBase.h"


class FrameViz: public  VizBase{

public:

    void        get_gradCornesCV(int lvl, cv::Mat &outPoints, int threshold);
    void        get_edgesPointsList                 ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);

    void        write_imagePyramid(const std::string &imPath, const std::string &imName);
    void        write_imageGradsPyramid(const std::string &imPath, const std::string &imName);
    void        write_imageMaxGradsPyramid(const std::string &imPath, const std::string &imName);
    void        write_imageMaxGradsTresholdPyramid(const std::string &imPath, const std::string &imName);
    void        write_cornerPtsList(const std::string &imPath, const std::string &imName);
    void        write_cornerListCV(const std::string &imPath, const std::string &imName);


    void        show_imagePyramid                   ( int num_lvls);
    void        show_gradientsPyramid               (  );
    void        show_edgesPyramid                   (bool scaled = true);
    void        show_cornersPyramid                 (  );
    void        show_propagatedcornersPyramid       ( int num_lvls);
    void        show_masksPyramid                   ( int num_lvls=5);
    void        show_cornerPointsPyramid            ( int num_lvls);
    void        show_disparityMap                   ( bool reverse );


    void        print_mappablePointsCoverage        (   );
    
    FrameData* data;

};


void FrameViz::get_gradCornesCV(int lvl, cv::Mat &outPoints, int threshold) {


//	if (  outPoints.cols != 0 ) 	outPoints.release();

    outPoints = cv::Mat(0,0, CV_32FC2);

    for(int i=0; i<data->edges[lvl].cols; i++)
        for(int j=0; j<data->edges[lvl].rows; j++)

            if ( data->edges[lvl].at<float>(j,i)  > threshold ){ // = (float) i/data.edges[lvl].rows; //(float) j / data.edges[lvl].cols;

                cv::Vec2f vec(i, j);
                outPoints.push_back(vec);
            }


//	std::cout << "get_gradCornesCV: " << outPoints.rows << "x" <<  outPoints.cols << std::endl;


}




void FrameViz::write_imagePyramid(const std::string &imPath, const std::string &imName) {

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    std::string fullPath;

    for (int idx=0; idx < data->images.size(); ++idx){

        fullPath = imPath + "//" + imName + "_" + std::to_string(idx) +".png";
        try { cv::imwrite(fullPath, data->images[idx], compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Image pyramid level " << std::to_string(idx) << "::exception converting image to PNG format: %s\n" << ex.what(); }
    }

}

void FrameViz::write_imageGradsPyramid(const std::string &imPath, const std::string &imName) {


    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

    std::string fullPath;

    double min,  max;
    cv::Mat adjMap;

    for (int idx=0; idx < data->gradsX.size(); ++idx){

        fullPath = imPath + "//" + imName + "X_" + std::to_string(idx) +".png";

        cv::minMaxIdx(data->gradsX[idx], &min, &max);
        cv::convertScaleAbs(data->gradsX[idx]-min, adjMap, 255 / ( max-min) );
        try{cv::imwrite(fullPath , adjMap, compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image gradient X pyramid level " << std::to_string(idx) << "::exception converting image to PNG format: %s\n" << ex.what(); }

        cv::minMaxIdx(data->gradsY[idx], &min, &max);
        cv::convertScaleAbs(data->gradsY[idx]-min, adjMap, 255 / ( max-min) );
        fullPath = imPath + "//" + imName + "Y_" + std::to_string(idx) +".png";
        try{cv::imwrite(fullPath , adjMap, compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image gradient Y pyramid level " << std::to_string(idx) << "::xception converting image to PNG format: %s\n" << ex.what(); }
    }


}

void FrameViz::write_imageMaxGradsPyramid(const std::string &imPath, const std::string &imName) {

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    std::string fullPath;

    for (int idx=0; idx < data->edges.size(); ++idx){

        fullPath = imPath + "//" + imName + "_" + std::to_string(idx) +".png";
        try{cv::imwrite(fullPath , data->edges[idx], compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image max gradient pyramid level " << std::to_string(idx)<<"::xception converting image to PNG format: %s\n"<< ex.what(); }
    }

}

void FrameViz::write_cornerPtsList(const std::string &imPath, const std::string &imName){



//	for (int idx=0; idx < data->imPyramid_K.size(); ++idx){

    std::ofstream    fs_cornerPtList    ( imPath+"//"+imName+".txt", std::ofstream::out);

    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");

    Eigen::Matrix2Xf pts;
    get_edgesPointsList(/*idx*/0, pts, 45);
    std::cout << fs_cornerPtList.is_open() << std::endl;

//		fs_cornerPtList << "level " << idx  << ": " << std::endl;
    fs_cornerPtList << (pts.transpose()).format(CleanFmt);
//		fs_cornerPtList << std::endl;
//		fs_cornerPtList << std::endl;
//	}

//	fs_cornerPtList.close();
}

void FrameViz::write_cornerListCV(const std::string &imPath, const std::string &imName){


//	for (int idx=0; idx < data->imPyramid_K.size(); ++idx){

    std::ofstream    fs_cornerPtList    ( imPath+"//"+ imName + ".txt", std::ofstream::out);

    cv::Mat corners;
    get_gradCornesCV(0, corners, 50);

    fs_cornerPtList << corners;

//		fs_cornerPtList << std::endl;
//		fs_cornerPtList << std::endl;
//	}

}




void FrameViz::show_imagePyramid(int num_lvls) {

    if ( num_lvls < 1) return;


    if ( data->images.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame. \n";    return; }

    int w = data->images[0].cols,   h = data->images[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    data->images[0].copyTo(imToShow);

    if ( num_lvls > 2 ) {


        for (int lvl = 1; lvl < std::min( (int)data->images.size(), num_lvls); ++lvl) {

            int w_lvl = data->images[lvl].cols, h_lvl = data->images[lvl].rows;

            cv::Mat currentImToAdd = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm = data->images[lvl];

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));

            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }

    show_imageScaled("Image Pyramid", imToShow);
    cv::waitKey(-1);

}



void FrameViz::show_disparityMap(bool reverse) {

    if ( data->disparities.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame. \n";    return; }

    int w = data->disparities[0].cols,   h = data->disparities[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    data->disparities[0].copyTo(imToShow);


    for (int lvl = 1; lvl < data->disparities.size(); ++lvl) {

        int w_lvl = data->disparities[lvl].cols,   h_lvl = data->disparities[lvl].rows;

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,     w_lvl, CV_32FC1 );
        cv::Mat currentIm       = data->disparities[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));

        cv::hconcat(imToShow,  currentImToAdd, imToShow);

    }

    if ( reverse) imToShow *= -1.0f;

    show_imageScaled("Disparity Pyramid", imToShow);
    cv::waitKey(-1);



}




void  FrameViz::show_gradientsPyramid(  ){

    if ( ( data->gradsX.size() == 0 ) || ( data->gradsY.size() == 0 )  ) {  std::cout << "There are NO IMAGE PYRAMID in this Frame. \n";    return; }

    int w = data->images[0].cols,   h = data->images[0].rows;

    cv::Mat gradToShowX    = cv::Mat( h, w, CV_32FC1 );
    cv::Mat gradToShowY    = cv::Mat( h, w, CV_32FC1 );

    data->gradsX[0].copyTo(gradToShowX);
    data->gradsY[0].copyTo(gradToShowY);


    for (int lvl = 1; lvl < data->gradsX.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,   data->gradsX[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = data->gradsX[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(gradToShowX,  currentImToAdd, gradToShowX);
    }



    for (int lvl = 1; lvl < data->gradsY.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,   data->gradsY[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = data->gradsY[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(gradToShowY,  currentImToAdd, gradToShowY);
    }



    show_imageScaled("Image Gradient X", gradToShowX);
    show_imageScaled("Image Gradient Y", gradToShowY);
    cv::waitKey(-1);
}

void FrameViz::show_edgesPyramid(bool scaled) {

    if ( data == nullptr){

        std::cerr << "FrameViz::data pointer is NULL" << std::endl;
        return;
    }


    if ( data->edges.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame. \n";    return; }

    int w = data->edges[0].cols,   h = data->edges[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    data->edges[0].copyTo(imToShow);


    for (int lvl = 1; lvl < data->edges.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,  data->edges[lvl].cols, CV_32FC1 );
        cv::Mat currentIm = cv::Mat::zeros( data->edges[lvl].rows,  data->edges[lvl].cols, CV_32FC1 );

        data->edges[lvl].copyTo(currentIm);

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(imToShow,  currentImToAdd, imToShow);
    }

    if ( scaled)

        show_imageScaled("Smeared Edges Pyramid Scaled", imToShow);

    else{
        cv::imshow("Smeared Edges Pyramid Non Scaled", imToShow);
        cv::waitKey(-1);
    }
}


void  FrameViz::show_cornersPyramid(  ){


    if ( data->corners.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame. \n";    return; }

    int w = data->corners[0].cols,   h = data->corners[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    data->corners[0].copyTo(imToShow);


    for (int lvl = 1; lvl < data->corners.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,  data->corners[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = data->corners[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(imToShow,  currentImToAdd, imToShow);
    }

    show_imageScaled("Corners Pyramid", imToShow);
    cv::waitKey(-1);
}


void FrameViz::show_propagatedcornersPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;
    if ( data->cornersPropagatedPyramid.size() == 0) {  std::cout << "There are NO BIT MASK PYRAMID in this Frame. \n";    return; }

    int w = data->cornersPropagatedPyramid[0].cols,   h = data->cornersPropagatedPyramid[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  data->cornersPropagatedPyramid[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)data->cornersPropagatedPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = data->cornersPropagatedPyramid[lvl].cols, h_lvl = data->cornersPropagatedPyramid[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);


            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = data->cornersPropagatedPyramid[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask Propagated Corners Pyramid", imToShow);
    cv::waitKey(-1);


}



void FrameViz::show_masksPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;


    if ( data->masks.size() == 0) {  std::cout << "There are NO BIT MASK PYRAMID in this Frame. \n";    return; }

    int w = data->masks[0].cols,   h = data->masks[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  data->masks[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)data->masks.size(), num_lvls); ++lvl) {

            int w_lvl = data->masks[lvl].cols, h_lvl = data->masks[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);


            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = data->masks[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask Edges Pyramid", imToShow);
    cv::waitKey(-1);
}

void FrameViz::show_cornerPointsPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;


    if ( data->cornerPointsMaskPyramid.size() == 0) {  std::cout << "There are NO BIT CORNER MASK PYRAMID in this Frame. \n";    return; }

    int w = data->cornerPointsMaskPyramid[0].cols,   h = data->cornerPointsMaskPyramid[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  data->cornerPointsMaskPyramid[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)data->cornerPointsMaskPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = data->cornerPointsMaskPyramid[lvl].cols, h_lvl = data->cornerPointsMaskPyramid[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);

            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = data->cornerPointsMaskPyramid[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask  Corners Pyramid", imToShow);
    cv::waitKey(-1);
}

void FrameViz::get_edgesPointsList(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {

    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int i=0; i<data->edges[lvl].cols; i++)
        for(int j=0; j<data->edges[lvl].rows; j++)

            if ( data->edges[lvl].at<float>(j,i)  >= threshold ){ // = (float) i/data.edges[lvl].rows; //(float) j / data.edges[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}


#endif //SSV_VER_0_1_FRAMEVIZ_H
