//
// Created by morzh on 19.04.19.
//

#ifndef SSV_VER_0_1_MEASURECENSUS_H
#define SSV_VER_0_1_MEASURECENSUS_H


#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"
#include "MeasureBase.h"

class MeasureCensus: public MeasureBase{
public:

    virtual float               calc_cost                           ( const cv::Mat &img1, const cv::Mat &img_2);
    virtual float               calc_subpix                         ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);
    float                       calc_HammingDistance                ( );

};

float MeasureCensus::calc_cost(const cv::Mat &img1, const cv::Mat &img2) {

    cv::Mat         img_bin1, img_bin2;
    cv::Mat         imgs_xor;

    cv::Point2i     center1     = cv::Point2i( 0.5*img1.cols, 0.5*img1.rows);
    cv::Point2i     center2     = cv::Point2i( 0.5*img2.cols, 0.5*img2.rows);

    float           center1_val = img1.at<float>(center1);
    float           center2_val = img2.at<float>(center2);



    cv::threshold( img1, img_bin1, center1_val, 1, CV_THRESH_BINARY);
    cv::threshold( img2, img_bin2, center2_val, 1, CV_THRESH_BINARY);

    cv::bitwise_xor(img1, img2, imgs_xor);

    return cv::sum(imgs_xor)[0];
}

#endif //SSV_VER_0_1_MEASURECENSUS_H
