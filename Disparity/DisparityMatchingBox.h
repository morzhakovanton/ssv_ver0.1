
#ifndef SSV_VER_0_1_DISPARITYMATCHINGBOX_H
#define SSV_VER_0_1_DISPARITYMATCHINGBOX_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>

#include "MeasureZMNCC.h"
#include "../SatellitePatches/PatchesStereoPair.h"
#include "Frame.h"
#include "FrameViz.h"


class DisparityMatchingBox{
public:

    void            estimate_disparity              ( const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &init_val, int box_half_size, int range, cv::Mat &disparity, cv::Mat *errors);
    void            get_refBlock                    ( const cv::Mat &img1, int x, int y, int sz, cv::Mat &block);
    void            get_targetStripe                ( const cv::Mat &img2, int col, int y, float d, int hsize, cv::Mat &stripe, int range);
    int             find_match                      ( cv::Mat block, cv::Mat stripe, cv::Vec2f *error);

    MeasureBase*     measure  =  nullptr;

private:
    bool            check_data                      ( const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &init_vals, cv::Mat *errors);
};



void DisparityMatchingBox::estimate_disparity(const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &init_val, int box_half_size, int range, cv::Mat &disparity, cv::Mat *errors) {

    if ( !check_data(img1, img2, init_val, errors) )
        return;


    ImageRectangleI     working_area    =   ImageRectangleI( box_half_size+range, box_half_size+range,  img1.cols-(2*box_half_size+1+range),  img1.cols-(2*box_half_size+1+range) );
    cv::Mat             block,   stripe;
    float               d_init;
    cv::Vec2f           error;


    for(    int x = working_area.x; x < working_area.width;  ++x ){
        for(int y = working_area.y; y < working_area.height; ++y ){

            d_init = init_val.at<float>(y,x);

            try {

                get_refBlock       ( img1, x, y, box_half_size, block);
                get_targetStripe   ( img2, x, y, d_init, box_half_size, stripe, range);


                disparity.at<float>(y, x) = find_match(block, stripe, &error) - range;

                if (errors != nullptr)
                    (*errors).at<cv::Vec2f>(y, x) = error;
            }
            catch(const std::exception& e){

                disparity.at<float>(y, x) = 0.0;

                if (errors != nullptr)
                    (*errors).at<cv::Vec2f>(y, x) = cv::Vec2f(1.0,1.0);

                continue;
            }
        }
    }

}



int DisparityMatchingBox::find_match(cv::Mat block, cv::Mat stripe, cv::Vec2f *error = nullptr) {

    int                             sz_x                =   block.cols;
    int                             sz_y                =   block.rows;
    int                             right_blocks_num    =   stripe.cols - sz_x;
    int                             disparity;

    cv::Mat                         right_block;

    std::vector<int>                costs(right_blocks_num);
    std::vector<int>::iterator      max_cost;



    for( int idx=0; idx <= right_blocks_num; idx++){

        cv::Rect right_block_rect = cv::Rect2i(idx, 0,sz_x, sz_y);
        stripe(right_block_rect).copyTo(right_block);
        costs[idx] = measure->calc_cost( block, right_block);
    }

    max_cost    =   std::max_element(costs.begin(), costs.end());
    disparity   =   std::distance( costs.begin(), max_cost );

    if ( error != nullptr){
        std::sort( costs.begin(), costs.end(), std::greater<float>());
        *error = cv::Vec2f(costs[0], costs[1]);
    }


    return  disparity;
}

void DisparityMatchingBox::get_refBlock(const cv::Mat &img1, int x, int y, int sz, cv::Mat &block) {

    cv::Rect    rect_block  =   cv::Rect(x-sz, y-sz, 2*sz+1, 2*sz+1 );
    cv::Rect    rect_img    =   cv::Rect(0, 0, img1.cols, img1.rows);

#ifdef  MATCHDEBUG
    debug.rect_leftStripe = rect_block;
    img1.copyTo(debug.img1);
    debug.center_leftStripe = Eigen::Vector2i(col, row);
#endif

    if ( (rect_block & rect_img) == rect_block) {

        img1(rect_block).copyTo(block);

#ifdef  MATCHDEBUG
        debug.left_stripe_isValid   = false;
        debug.left_stripe           = left_stripe;
#endif
    }
    else{

#ifdef  MATCHDEBUG
        debug.left_stripe_isValid   = false;
#endif
        throw std::runtime_error("get_leftStripe failed, requested rectangle is out of bounds");
    }
}



void DisparityMatchingBox::get_targetStripe(const cv::Mat &img2, int col, int row, float d, int hsize, cv::Mat &stripe, int range) {

    cv::Rect    rect_block  = cv::Rect(col-hsize-range+d, row-hsize, 2*hsize+1 + 2*range, 2*hsize+1);
    cv::Rect    rect_img    = cv::Rect(0, 0, img2.cols, img2.rows);

#ifdef  MATCHDEBUG
    debug.rect_rightStripe = rect_block;
    img2.copyTo(debug.img2);
    debug.center_rightStripe = Eigen::Vector2i(col, row);
#endif

    if ( (rect_block & rect_img) == rect_block) {

        img2(rect_block).copyTo(stripe);

#ifdef  MATCHDEBUG
        debug.right_stripe          = right_stripe;
        debug.right_stripe_isValid  = false;
#endif
    }
    else{

#ifdef  MATCHDEBUG
        debug.right_stripe_isValid  = false;
#endif
        throw std::runtime_error("get_rightStripe failed, requested rectangle is out of bounds");
    }

}

bool DisparityMatchingBox::check_data(const cv::Mat &img1, const cv::Mat &img2, const cv::Mat &init_val, cv::Mat *errors) {

    if ( measure == nullptr){
        std::cerr << "DisparityMatchingBox::estimate_disparity()::measure is set to bull pointer" << std::endl;
        return false;
    }
    if ( img1.cols != img2.cols || img1.rows != img2.rows){
        std::cerr << "DisparityMatchingBox::estimate_disparity()::images are not of the same size" << std::endl;
        return false;
    }
    if ( img1.cols != init_val.cols || img1.rows != init_val.rows){
        std::cerr << "DisparityMatchingBox::estimate_disparity()::init_val matrix is or the wrong size" << std::endl;
        return false;
    }
    if ( errors != nullptr){
        *errors = cv::Mat::zeros(img1.rows, img1.cols, CV_32FC2);
    }


    return true;
}

#endif //SSV_VER_0_1_DISPARITYMATCHINGBOX_H
