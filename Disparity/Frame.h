//
// Created by morzh on 1/23/16.
//

#ifndef SOP_PCFROMSTEREOPAIR_FRAME_HPP
#define SOP_PCFROMSTEREOPAIR_FRAME_HPP

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <fstream>
#include <iostream>
#include <algorithm>

#include "Settings.h"
#include "FrameData.h"
#include "FrameViz.h"
//#define MIN_EPL_ANGLE_SQUARED (0.3f*0.3f)

class Frame {
public:
                Frame                               ();
                Frame                               ( const cv::Mat &im);
                Frame                               ( const std::string &imPath);
                ~Frame                              ( );

    void        set_image                           ( const cv::Mat &im);

    void        build_keyFrame(int num_lvls, const cv::Mat &mask);
    void        build_keyFrame(int num_lvls, float mappable_points_threshold);
    void        build_frame(int num_lvls);
    void        build_imagePyramid                  ( int numLayers);
    void        build_imageGradients                ( int numLayers);
    void        build_imageEdgesSmeared             ( int numLayers);
    void        build_imageCorners                  ( int numLayers);
    void        build_masksPyramid(int numLayers, float threshold);
    void        build_cornersPointsMask             ( int numLayers);
    void        build_cornersPropagationMask        ( int numLayers);
    void        build_cornerPointsMaskAtLvl         ( int lvl, int threshold);
    void        build_mappablePointsMaskAtLvl(int lvl, float threshold);
    void        build_nextPyramidImgLvl             ( const cv::Mat &kernel);
    void        build_nextPyrLvlGradients           ( );
    void        build_nextPyrLvlEdges               ( );
    void        build_nextPyrLvlCorners             ( );

    void        get_edgesPointsList                 ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_cornerPointsList                ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_listOfEdgesCoordinates          ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_gradCornesCV                    ( int lvl, cv::Mat &outPoints, int threshold);
    void        get_HarrisCornersCameraFrame        ( int lvl, Eigen::Matrix2Xf &outPoints, int treshold, int blockSize, int apertureSize, float k);
    void        get_gradCornersSubPx                ( cv::Mat &corners, int lvl, int wSize, int zSize, int threshold);


    void        calc_imZeroLvlShiftScale            (   );
    float       calc_numMappableNeighbours          ( int row, int col, int lvl, int do_interp_count, const cv::Mat &mapPtsMask);
    void        calc_minMaxDisparities              ( const std::pair<int, int> &minMaxDispLvlZero, int numlevels );
    void        calc_minMaxDisparitiesForMatching   ( const std::pair<int, int> &minMaxDispLvlZero, int numlevels);

    void        fill_holesDisparitytMap             ();
    void        fill_disparitiesHolesIteration      ( int lvl, int min_num_neighbours);
    void        fill_disparitiesHoles               ( int lvl );

    void        multiplyMaxGradByMaskAtLvl          (const cv::Mat &mask, int lvl = 0);
    void        multiplyCornersByMaskAtLvl          (const cv::Mat &mask, int lvl = 0);
    void        multiplyMaxGradByMask               (const cv::Mat &mask);
    void        multiplyCornersByMask               (const cv::Mat &mask);
    void        deleteOddRowOddColumn               ( const cv::Mat & imSrc, cv::Mat &imDst);


    void        setNumOfPyrLvls                     ( int n );


    void        medianFilterDisparityMap            ( int lvl );
    void        medianFilterDisparityMap_std        ( int lvl );
    void        setChannelToConstValue              ( cv::Mat &mat, unsigned int channel, unsigned char value) const;

    void        propagateCornersToTheNextLvl        ( int lvl);
    void        propagateCornersToTheNextLvl2       ( int lvl);
    void        set_activeLvl( int lvl);


    void        fill_GaussKernel();
    void        fill_BoxKernel();
    void        fill_disparityMaps(int layers);
    void        fill_cornerDisparityMaps(int layers);
    void        fill_PropagatedCornersDisparityMaps(int layers);

    void        smearMaxGrads(cv::Mat &maxgrads);
    void        calcNumMappablePoints(int numLayers);
    void        check_and_resolve_numLvls_division(int num_levels);
    void        switch_toLvlDown();
    void        set_activeLayer(int lvl);
    void        proppagate_diaprityToLvlvDownFrom(int lvl);

    FrameData       data;
    FrameViz        viz;

    cv::Mat         gaussKernel;
    cv::Mat         boxKernel;
    cv::Point       anchor;

    double          delta;
    int             ddepth;

    int             numOfPyrLvls;

    int             active_lvl;
};


Frame::Frame() {

    viz.data = &data;
}

Frame::Frame(const std::string &imPath) : anchor(cv::Point(-1, -1)), delta(0), ddepth(-1), gaussKernel(cv::Mat(3, 3, CV_32FC1)), boxKernel(cv::Mat(2, 2, CV_32FC1)){

    cv::Mat im = cv::imread( imPath, CV_LOAD_IMAGE_GRAYSCALE);
    im.convertTo(im, CV_32FC1);
    data.images.push_back( im );

    fill_GaussKernel();
    fill_BoxKernel();

    data.mappablePointsMask = cv::Mat::zeros(im.rows, im.cols, IPL_DEPTH_1U);
}

Frame::Frame(const cv::Mat &im) : anchor(cv::Point(-1, -1)), delta(0), ddepth(-1), gaussKernel(cv::Mat(3, 3, CV_32FC1)), boxKernel(cv::Mat(2, 2, CV_32FC1)){

    cv::Mat imFloat;
    im.convertTo(imFloat, CV_32FC1);
    data.images.push_back( imFloat);

    fill_GaussKernel();
    fill_BoxKernel();

    data.mappablePointsMask = cv::Mat::zeros(im.rows, im.cols, IPL_DEPTH_1U);
}




Frame::~Frame() {

    for (int idx = 0; idx < data.images.size();    ++idx) { 	data.images[idx].release();       }
    for (int idx = 0; idx < data.gradsX.size(); ++idx) { 	data.gradsX[idx].release();    }
    for (int idx = 0; idx < data.gradsY.size(); ++idx) { 	data.gradsY[idx].release();    }
    for (int idx = 0; idx < data.edges.size();++idx){ 	data.edges[idx].release();  }
}

void Frame::build_keyFrame(int num_lvls, const cv::Mat &mask) {

    check_and_resolve_numLvls_division(num_lvls); ///  our image should have dimension ( width and height) that are divisable by num_layers number

    build_imagePyramid(num_lvls);
    build_imageGradients(num_lvls);
    build_imageEdgesSmeared(num_lvls);
    build_imageCorners(num_lvls);

    multiplyMaxGradByMask          ( mask );
//    multiplyCornersByMask          ( mask );
    build_masksPyramid(num_lvls, 0);
//    build_cornersPointsMask        ( num_lvls );
    calcNumMappablePoints          ( num_lvls );
    fill_disparityMaps             ( num_lvls );
//    fill_cornerDisparityMaps       ( num_lvls );
//    build_cornersPropagationMask( num_lvls);
}


void Frame::build_keyFrame(int num_lvls, float mappable_points_threshold) {

    check_and_resolve_numLvls_division(num_lvls); ///  our image should have dimension ( width and height) that are divisable by num_layers number

    build_imagePyramid(num_lvls);
    build_imageGradients(num_lvls);
    build_imageEdgesSmeared(num_lvls);
//    build_imageCorners(num_lvls);

    build_masksPyramid(num_lvls, mappable_points_threshold);
//    build_cornersPointsMask        ( num_lvls );
    calcNumMappablePoints          ( num_lvls );
    fill_disparityMaps             ( num_lvls );
//    fill_cornerDisparityMaps       ( num_lvls );
//    build_cornersPropagationMask( num_lvls);
}



void Frame::build_frame(int num_lvls) {

    check_and_resolve_numLvls_division( num_lvls ); ///  our image should have dimension ( width and height) that are divisable by num_layers number

    build_imagePyramid(num_lvls);
/*
    build_imageGradients(num_lvls);
    build_imageEdgesSmeared(num_lvls);
    build_masksPyramid(num_lvls);
    fill_disparityMaps(num_lvls);
*/
}



void Frame::build_imagePyramid(int numLayers) {

    for (int idx = 0; idx < numLayers-1; ++idx)
        build_nextPyramidImgLvl(gaussKernel /*boxKernel*/);
}


void Frame::build_nextPyramidImgLvl(const cv::Mat &kernel) {

    cv::Mat imFiltered, newPyrLvl;
    cv::Mat curPyrLvl =  data.images[ data.images.size()-1 ];

//    filter2D( data.images[ data.images.size()-1 ], imFiltered, ddepth, kernel, anchor, delta, cv::BORDER_DEFAULT );
//    deleteOddRowOddColumn(imFiltered, newPyrLvl);
//	data.images.push_back(imFiltered);

    cv::resize( curPyrLvl, newPyrLvl, cv::Size( 0.5*curPyrLvl.cols,  0.5*curPyrLvl.rows) ) ;
    data.images.push_back(newPyrLvl/*.clone()*/);
}

void Frame::deleteOddRowOddColumn(const cv::Mat &imSrc, cv::Mat &imDst) {

    int rows = imSrc.rows / 2;
    int cols = imSrc.cols / 2;

    int intermCols = imSrc.cols;
    cv::Mat imInterm = cv::Mat (imSrc.rows, intermCols, imSrc.type() );

    for (int idx1=0; idx1 < rows; ++idx1){

        cv::Rect inRect (0, idx1*2+1,  intermCols, 1);
        cv::Rect outRect(0, idx1    , intermCols, 1);
        imSrc(inRect).copyTo(imInterm(outRect));
    }

    imDst.release();
    imDst = cv::Mat(rows, cols, imSrc.type() );

    for (int idx2=0; idx2 < cols; ++idx2){

        cv::Rect inRect (idx2*2+1,0, 1, rows);
        cv::Rect outRect(idx2, 0, 1, rows);
        imInterm(inRect).copyTo(imDst(outRect));

    }


}


void Frame::fill_BoxKernel() {

    boxKernel = cv::Mat::zeros(2,2, CV_32FC1);

    boxKernel.at<float>(0, 0) = 0.25;  boxKernel.at<float>(0, 1) = 0.25;
    boxKernel.at<float>(1, 0) = 0.25;  boxKernel.at<float>(1, 1) = 0.25;
}

void Frame::fill_GaussKernel() {

    gaussKernel = cv::Mat::zeros(3,3, CV_32FC1);

    gaussKernel.at<float>(0, 0) = 0.0625;   gaussKernel.at<float>(0, 1) = 0.125;   gaussKernel.at<float>(0, 2) = 0.0625;
    gaussKernel.at<float>(1, 0) = 0.1250;   gaussKernel.at<float>(1, 1) = 0.250;   gaussKernel.at<float>(1, 2) = 0.1250;
    gaussKernel.at<float>(2, 0) = 0.0625;   gaussKernel.at<float>(2, 1) = 0.125;   gaussKernel.at<float>(2, 2) = 0.0625;
}

void Frame::setNumOfPyrLvls(int n) {    	numOfPyrLvls = n;       }

void Frame::build_imageGradients(int numLayers) {

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlGradients();
}

void Frame::build_nextPyrLvlGradients() {

    int pyrLevel = data.gradsX.size();

    int imCols = data.images[pyrLevel].cols;
    int imRows = data.images[pyrLevel].rows;
    cv::Rect inRect, outRect;

    cv::Mat left(imRows, imCols, CV_32FC1, cv::Scalar(0) ),     right(imRows, imCols, CV_32FC1, cv::Scalar(0) );
    cv::Mat top (imRows, imCols, CV_32FC1, cv::Scalar(0) ),     botom(imRows, imCols, CV_32FC1, cv::Scalar(0) );


    inRect  = cv::Rect(0,0, imCols-1, imRows); // left
    outRect = cv::Rect(1,0, imCols-1, imRows); // left
    data.images[pyrLevel](inRect).copyTo(left(outRect));

    inRect  = cv::Rect(1,0, imCols-1, imRows); // right
    outRect = cv::Rect(0,0, imCols-1, imRows); // right
    data.images[pyrLevel](inRect).copyTo(right(outRect));

    //----------------------------------------------------------

    inRect  = cv::Rect(0,0, imCols, imRows-1); // top
    outRect = cv::Rect(0,1, imCols, imRows-1); // top
    data.images[pyrLevel](inRect).copyTo(top(outRect));

    inRect  = cv::Rect(0,1, imCols, imRows-1); // botom
    outRect = cv::Rect(0,0, imCols, imRows-1); // botom
    data.images[pyrLevel](inRect).copyTo(botom(outRect));

    //-----------------------------------------------------------

    cv::Mat gradX(imRows, imCols, CV_32FC1);
    cv::Mat gradY(imRows, imCols, CV_32FC1);

    gradX = 0.5*(right - left);
    gradY = 0.5*(botom - top);

    data.gradsX.push_back( gradX/*.clone() */);
    data.gradsY.push_back( gradY/*.clone() */);
}


void Frame::build_imageEdgesSmeared(int numLayers) {


    if (  data.edges.size() != 0){

        data.edges.clear();
    }

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlEdges();
}


void Frame::build_imageCorners(int numLayers) {

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlCorners();
}


void Frame::build_masksPyramid(int numLayers, float threshold) {

    for (int lvl = 0; lvl < numLayers; ++lvl)

        build_mappablePointsMaskAtLvl(lvl, threshold);
}

void Frame::build_cornersPointsMask(int numLayers) {

    for (int lvl = 0; lvl < numLayers; ++lvl)

        build_cornerPointsMaskAtLvl(lvl, FRAME_THRESHOLD_CORNERS);
}


void Frame::build_nextPyrLvlEdges() {

    int     pyrLevel = data.edges.size();
    cv::Mat gradX_square, gradY_square, gradLength_square, gradLength;

    cv::multiply( data.gradsX[pyrLevel], data.gradsX[pyrLevel], gradX_square);
    cv::multiply( data.gradsY[pyrLevel], data.gradsY[pyrLevel], gradY_square);

    gradLength_square = gradX_square + gradY_square;

    sqrt(gradLength_square, gradLength);

    smearMaxGrads(gradLength);

//    viz.show_imageScaled("asdasdfasd", gradLength);

    data.edges.push_back(gradLength/*.clone()*/);
}

void Frame::build_nextPyrLvlCorners() {

    int pyrLevel = data.corners.size();
    cv::Mat gradX_abs, gradY_abs, corner, gradLength;

    cv::multiply( data.gradsX[pyrLevel], data.gradsX[pyrLevel], gradX_abs);
    cv::multiply( data.gradsY[pyrLevel], data.gradsY[pyrLevel], gradY_abs);

    gradX_abs  = cv::abs(data.gradsX[pyrLevel] );
    gradY_abs  = cv::abs(data.gradsY[pyrLevel] );

    cv::multiply(gradX_abs, gradY_abs, corner  );

    data.corners.push_back(corner/*.clone()*/);
}


void Frame::get_edgesPointsList(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int i=0; i<data.edges[lvl].cols; i++)
        for(int j=0; j<data.edges[lvl].rows; j++)

            if ( data.edges[lvl].at<float>(j,i)  >= threshold ){ // = (float) i/data.edges[lvl].rows; //(float) j / data.edges[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}


void Frame::get_cornerPointsList(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int i=0; i<data.edges[lvl].cols; i++)
        for(int j=0; j<data.edges[lvl].rows; j++)

            if ( data.edges[lvl].at<float>(j,i)  >= threshold ){ // = (float) i/data.edges[lvl].rows; //(float) j / data.edges[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}


void Frame::get_gradCornesCV(int lvl, cv::Mat &outPoints, int threshold) {


//	if (  outPoints.cols != 0 ) 	outPoints.release();

    outPoints = cv::Mat(0,0, CV_32FC2);

    for(int i=0; i<data.edges[lvl].cols; i++)
        for(int j=0; j<data.edges[lvl].rows; j++)

            if ( data.edges[lvl].at<float>(j,i)  > threshold ){ // = (float) i/data.edges[lvl].rows; //(float) j / data.edges[lvl].cols;

                cv::Vec2f vec(i, j);
                outPoints.push_back(vec);
            }


//	std::cout << "get_gradCornesCV: " << outPoints.rows << "x" <<  outPoints.cols << std::endl;


}

void Frame::get_gradCornersSubPx(cv::Mat &corners2, int lvl, int wSize, int zSize, int threshold) {


    get_gradCornesCV(lvl, corners2, threshold);
    cv::cornerSubPix( data.images[lvl], corners2, cv::Size( 5, 5 ), cv::Size( -1, -1 ), cv::TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001 ));
}

void Frame::get_HarrisCornersCameraFrame(int lvl, Eigen::Matrix2Xf &outPoints, int treshold, int blockSize, int apertureSize, float k) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    cv::Mat hCorners;
    cv::cornerHarris( data.images[lvl], hCorners, blockSize, apertureSize, k, cv::BORDER_DEFAULT );  /// Detecting corners
    normalize( hCorners, hCorners, 0, 255, cv::NORM_MINMAX, CV_32FC1);    /// Normalizing
    convertScaleAbs( hCorners, hCorners );



    for(int i=0; i< hCorners.cols; i++)
        for(int j=0; j< hCorners.rows; j++)

            if ( hCorners.at<float>(j,i)  > (float) treshold/255 ){
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}

void Frame::smearMaxGrads(cv::Mat &maxgrads) {


//    if(data_.maxGradientsValid[level]) return;

//    if(enablePrintDebugInfo && printFrameBuildDebugInfo)    printf("CREATE AbsGrad lvl %d for frame %d\n", lvl, id());

    int width   = maxgrads.cols;
    int height  = maxgrads.rows;


    float* maxGradTemp = new float [width * height];


    // 1. write abs gradients in real data_.
    Eigen::Vector4f*    gradxyii_pt         =   ( Eigen::Vector4f* ) maxgrads.data  + width;
    float*              maxgrad_pt          =   ( float* ) maxgrads.data  + width +1 ;
    float*              maxgrad_pt_max      =   ( float* ) maxgrads.data + width*(height-1) -1;


    // 2. smear up/down direction into temp buffer
    float* maxgrad_t_pt     =   maxGradTemp + width+1;

    for( ;   maxgrad_pt<maxgrad_pt_max;  maxgrad_pt++, maxgrad_t_pt++ )
    {
        float g1 = maxgrad_pt[-width];
        float g2 = maxgrad_pt[0];
        if(g1 < g2) g1 = g2;

        float g3 = maxgrad_pt[width];
        if(g1 < g3)
            *maxgrad_t_pt = g3;
        else
            *maxgrad_t_pt = g1;
    }

    float numMappablePixels = 0;

    // 2. smear left/right direction into real data_
    maxgrad_pt              =  ( float* ) maxgrads.data  + width+1;
    maxgrad_pt_max          =  ( float* ) maxgrads.data  + width*(height-1)-1;
    maxgrad_t_pt            =   maxGradTemp + width+1;

    for( ;  maxgrad_pt<maxgrad_pt_max;  maxgrad_pt++, maxgrad_t_pt++ )
    {
        float g1 = maxgrad_t_pt[-1];
        float g2 = maxgrad_t_pt[0];

        if(g1 < g2) g1 = g2;

        float g3 = maxgrad_t_pt[1];
        if(g1 < g3)
        {
            *maxgrad_pt = g3;
            if(g3 >= FRAME_THRESHOLD_EDGES)
                numMappablePixels++;
        }
        else
        {
            *maxgrad_pt = g1;
            if(g1 >= FRAME_THRESHOLD_EDGES)
                numMappablePixels++;
        }
    }

//    if(level==0)        this->stats_.numMappablePixels = numMappablePixels;

//    FrameMemory::getInstance().returnBuffer(maxGradTemp);
    delete[] maxGradTemp;
//    data_.maxGradientsValid[level] = true;

}


void  Frame::multiplyMaxGradByMaskAtLvl(const cv::Mat &mask, int lvl) {

    cv::multiply( data.edges[lvl], mask, data.edges[lvl] );
}

void  Frame::multiplyCornersByMaskAtLvl(const cv::Mat &mask, int lvl) {

    cv::multiply( data.corners[lvl], mask, data.corners[lvl] );
}



void  Frame::multiplyMaxGradByMask  (const cv::Mat &mask){

    if ( (mask.cols != data.edges[0].cols)  || (mask.cols != data.edges[0].cols) ) {  std::cout << " mask size do not consize with edges size" << std::endl; return ;}

    cv::Mat mask__;

    for (int lvl = 0; lvl < data.edges.size(); ++lvl) {

        cv::Size  size__ = cv::Size(data.edges[lvl].cols , data.edges[lvl].rows );

        cv::resize(mask, mask__,  size__ );

        multiplyMaxGradByMaskAtLvl(mask__, lvl);
    }

}



void  Frame::multiplyCornersByMask  (const cv::Mat &mask){

    if ( (mask.cols != data.corners[0].cols)  || (mask.cols != data.corners[0].cols) ) {  std::cout << " mask size do not consize with edges size" << std::endl; return ;}

    cv::Mat mask__;

    for (int lvl = 0; lvl < data.corners.size(); ++lvl) {

        cv::Size  size__ = cv::Size( data.corners[lvl].cols , data.corners[lvl].rows );

        cv::resize(mask, mask__,  size__ );

        multiplyCornersByMaskAtLvl(mask__, lvl);
    }

}

void Frame::get_listOfEdgesCoordinates(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int col=0; col<data.edges[lvl].cols; col++)
        for(int row=0; row<data.edges[lvl].rows; row++)

            if ( data.edges[lvl].at<float>(row,col)  >= threshold ){ // = (float) col/data.edges[lvl].rows; //(float) row / data.edges[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << col, row;
                outPoints.col( outPoints.cols()-1 ) = curCoords;
            }

}


void Frame::build_mappablePointsMaskAtLvl(int lvl, float threshold) {

    cv::Mat mappablePointsMask_ =  cv::Mat::zeros( data.edges[lvl].rows, data.edges[lvl].cols, IPL_DEPTH_1U);

    for(int col=0; col < data.edges[lvl].cols; col++) {
        for (int row = 0; row < data.edges[lvl].rows; row++)

            if (data.edges[lvl].at<float>(row, col) >= threshold)
                mappablePointsMask_.at<bool>(row, col) = true;  // data.mappablePointsMask.at<bool>(row, col) = true;
            else
                mappablePointsMask_.at<bool>(row, col) = false; //  data.mappablePointsMask.at<bool>(row, col) = false;
    }

    data.masks.push_back(mappablePointsMask_);

}



void Frame::build_cornerPointsMaskAtLvl(int lvl, int threshold) {

    cv::Mat cornerPointsMask_ =  cv::Mat::zeros( data.corners[lvl].rows, data.corners[lvl].cols, IPL_DEPTH_1U);

    for(int col=0; col<data.corners[lvl].cols; col++) {
        for (int row = 0; row < data.corners[lvl].rows; row++)

            if (data.corners[lvl].at<float>(row, col) >= threshold) cornerPointsMask_.at<bool>(row, col) = true;  // data.mappablePointsMask.at<bool>(row, col) = true;
            else                                                      cornerPointsMask_.at<bool>(row, col) = false; //  data.mappablePointsMask.at<bool>(row, col) = false;
    }

    data.cornerPointsMaskPyramid.push_back(cornerPointsMask_);

}

void Frame::fill_disparityMaps(int layers) {

    data.disparities.clear();

    for (int lvl = 0; lvl < layers; ++lvl) {

        data.disparities.push_back( cv::Mat::zeros( data.images[lvl].rows, data.images[lvl].cols, CV_32FC1) );
    }

}

void Frame::fill_cornerDisparityMaps(int layers) {

    data.cornersDisparityPyramid.clear();

    for (int lvl = 0; lvl < layers; ++lvl) {

        data.disparities.push_back( cv::Mat::zeros( data.images[lvl].rows, data.images[lvl].cols, CV_32FC1) );
    }

}


void Frame::calc_imZeroLvlShiftScale() {

    double min, max;
    cv::minMaxIdx(data.images[0], &min, &max);
    data.scaleShiftZeroLvlPyramid = std::make_pair( min, 255/( max-min) );
}


void Frame::calc_minMaxDisparities(const std::pair<int, int> &minMaxDispLvlZero, int numlevels) {

    data.minMaxDisparities.clear();

    data.minMaxDisparities.push_back( minMaxDispLvlZero) ;

    for (int i = 1; i < numlevels; ++i) {

        float min  = std::round( (float) minMaxDispLvlZero.first  / (i+1) );
        float max  = std::round( (float) minMaxDispLvlZero.second / (i+1) );

        data.minMaxDisparities.push_back( std::make_pair( min, max) );
    }

}

void Frame::calc_minMaxDisparitiesForMatching(const std::pair<int, int> &minMaxDispLvlZero, int numlevels) {

    calc_minMaxDisparities( minMaxDispLvlZero, numlevels);

    for (int lvl = 0; lvl < numlevels - 1; ++lvl) {

        data.minMaxPyramidMatchingDisparities.push_back( std::make_pair( -13, +13) );
    }

    data.minMaxPyramidMatchingDisparities.push_back(   data.minMaxDisparities[numlevels-1] );
}

void Frame::calcNumMappablePoints(int numLayers) {

    data.numOfMappablePointsPyramid.clear();

    for (int lvl = 0; lvl < numLayers; ++lvl) {

        int rows            = data.images[lvl].rows;
        int cols            = data.images[lvl].cols;
        int numOfMapPts     = 0;

        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {

                if ( data.masks[lvl].at<bool>(row, col) ) numOfMapPts++;

            }

        }

        data.numOfMappablePointsPyramid.push_back(numOfMapPts);

    }
}

void Frame::fill_disparitiesHoles(int lvl){

    for (int idx = 0; idx < 20; ++idx) {

        fill_disparitiesHolesIteration(lvl, 3);
    }
}

void Frame::fill_disparitiesHolesIteration(int lvl, int min_num_neighbours) {

    int cols = data.images[lvl].cols;
    int rows = data.images[lvl].rows;

    cv::Mat mappablePointsMask_ =  cv::Mat::zeros( rows, cols, IPL_DEPTH_1U);

    data.masks[lvl].copyTo(mappablePointsMask_);

    for (int col = 0; col < cols; ++col) {
        for (int row = 0; row < rows; ++row) {

            if ( !mappablePointsMask_.at<bool>(row, col)) {

                float pix_value = calc_numMappableNeighbours(row, col, lvl, min_num_neighbours, mappablePointsMask_);

                if (std::abs(pix_value) > 1e-2) {
                    data.disparities[lvl].at<float>(row, col)           = pix_value;
                    data.masks[lvl].at<bool>(row, col)   = true;
                }
            }
        }

    }
}


float Frame::calc_numMappableNeighbours(int in_row, int in_col, int lvl, int do_interp_count, const cv::Mat &mapPtsMask) {

    int     count   =   0;
    float   val     =   0;

    for (int col = in_col-1; col <= in_col+1; ++col) {
        for (int row = in_row-1; row <= in_row+1; ++row) {

            if ( (row < 0 )  ||  (row > data.images[lvl].rows-1) || (col < 0 )  ||  (col > data.images[lvl].cols-1)  ) continue;
            if ( !mapPtsMask.at<bool>(row,col) ) continue;

            count++;
            val += data.disparities[lvl].at<float>(row,col);
        }
    }



    if ( count >= do_interp_count )   return    (float) val/count;
    else                              return    0.0f;

}

void Frame::medianFilterDisparityMap(int lvl) {

    float window[9];
    cv::Mat disparity_temp;


    data.disparities[lvl].copyTo( disparity_temp);

    for(int y = 1; y < data.disparities[lvl].rows - 1; y++){
        for(int x = 1; x < data.disparities[lvl].cols - 1; x++){

            window[0] = data.disparities[lvl].at<float>( y - 1 , x - 1 );
            window[1] = data.disparities[lvl].at<float>( y     , x - 1 );
            window[2] = data.disparities[lvl].at<float>( y + 1 , x - 1 );
            window[3] = data.disparities[lvl].at<float>( y - 1 , x );
            window[4] = data.disparities[lvl].at<float>( y     , x );
            window[5] = data.disparities[lvl].at<float>( y + 1 , x );
            window[6] = data.disparities[lvl].at<float>( y - 1 , x + 1 );
            window[7] = data.disparities[lvl].at<float>( y     , x + 1 );
            window[8] = data.disparities[lvl].at<float>( y + 1 , x + 1 );

            // sort the window to find median
            int temp, i , j;

            for(i = 0; i < 9; i++){
                temp = window[i];
                for(j = i-1; j >= 0 && temp < window[j]; j--){
                    window[j+1] = window[j];
                }
                window[j+1] = temp;
            }

            // assign the median to centered element of the matrix
            disparity_temp.at<float>(y,x) = window[4];
        }
    }


    disparity_temp.copyTo( data.disparities[lvl] );

}


void Frame::medianFilterDisparityMap_std(int lvl) {

    std::vector<float>  window(9);
    cv::Mat             disparity_temp;

    data.disparities[lvl].copyTo( disparity_temp);

    for(int y = 1; y < data.disparities[lvl].rows - 1; y++){
        for(int x = 1; x < data.disparities[lvl].cols - 1; x++){

            window[0] = data.disparities[lvl].at<float>( y - 1 , x - 1 );
            window[1] = data.disparities[lvl].at<float>( y     , x - 1 );
            window[2] = data.disparities[lvl].at<float>( y + 1 , x - 1 );
            window[3] = data.disparities[lvl].at<float>( y - 1 , x );
            window[4] = data.disparities[lvl].at<float>( y     , x );
            window[5] = data.disparities[lvl].at<float>( y + 1 , x );
            window[6] = data.disparities[lvl].at<float>( y - 1 , x + 1 );
            window[7] = data.disparities[lvl].at<float>( y     , x + 1 );
            window[8] = data.disparities[lvl].at<float>( y + 1 , x + 1 );

            // sort the window to find median
            std::sort(window.begin(), window.end());
            disparity_temp.at<float>(y,x) = window[4];
        }
    }


    disparity_temp.copyTo( data.disparities[lvl] );

}

void Frame::check_and_resolve_numLvls_division(int num_levels) {

    int factor                  = std::pow(2, num_levels);

    int im_type                 = data.images[0].type();
    int width                   = data.images[0].cols;
    int height                  = data.images[0].rows;
    int width_delta             = std::ceil( (float) width  / factor ) * factor   -    width  ;
    int height_delta            = std::ceil( (float) height / factor ) * factor   -    height ;

    if ( width_delta  ) {

        cv::Mat add_cols = cv::Mat::zeros(height, width_delta, im_type);
        cv::hconcat( data.images[0], add_cols, data.images[0]);
    }

    if ( height_delta ) {

        width = data.images[0].cols;
        cv::Mat add_rows = cv::Mat::zeros(height_delta, width, im_type);
        cv::vconcat( data.images[0], add_rows, data.images[0]);
    }



}

void Frame::setChannelToConstValue( cv::Mat &mat, unsigned int channel, unsigned char value) const
{
    // make sure have enough channels
    if (mat.channels() < channel+1)
        return;

    // check_isEmpty mat is continuous or not
    if (mat.isContinuous())
        mat.reshape(1, mat.rows*mat.cols).col(channel).setTo( cv::Scalar(value) );
    else{
        for (int i = 0; i < mat.rows; i++)
            mat.row(i).reshape(1, mat.cols).col(channel).setTo( cv::Scalar(value));
    }
}

void Frame::propagateCornersToTheNextLvl(int lvl) {

    if (lvl == PYRAMID_LEVELS) { printf("Frame::buildNextLvlBitMask(): top level pyramid reached. \n");     return;   }


    cv::Mat         im              =   cv::Mat::zeros( data.images[lvl].rows, data.images[lvl].cols,  IPL_DEPTH_1U ) ;

    int             width           =   data.images[lvl-1].cols;
    int             height          =   data.images[lvl-1].rows;
/*

    for ( int row=0; row < data.images[lvl].rows; ++row){
        for (int col = 0; col < data.images[lvl].rows; ++col) {

            im.at<bool>( row, col ) = data.images[lvl-1].at<bool>( row, col );
        }
    }
*/


    const bool*     source          =   (bool*) data.cornersPropagatedPyramid[lvl-1].data;
    const bool*     source_end      =   source + (width-1)*height;
    bool*           dest            =   (bool*) im.data;

    int i=0;

    for (  ; source < source_end ; ) {

        *dest =  (  *source ||  *(source+1) || *(source+width)  ||  *(source+width+1) );

        source  +=  2;
        i       +=  2;

        if ( i == width){ source += width; i=0;}

        dest++;
    }

    data.cornersPropagatedPyramid.push_back( im );

}

void Frame::propagateCornersToTheNextLvl2(int lvl) {

    cv::Mat im;

    cv::resize( data.cornersPropagatedPyramid[lvl-1], im,  cv::Size( data.images[lvl].rows, data.images[lvl].rows ), 0,0, CV_INTER_CUBIC );

    data.cornersPropagatedPyramid.push_back( im );

}

void Frame::build_cornersPropagationMask(int numLayers) {

    data.cornersPropagatedPyramid.clear();

    if ( !data.cornerPointsMaskPyramid.size() ) return;

    cv::Mat im;
    data.cornerPointsMaskPyramid[0].copyTo(im);
    data.cornersPropagatedPyramid.push_back(im);

    for (int lvl = 1; lvl < numLayers; ++lvl) {

        propagateCornersToTheNextLvl(lvl);

    }

}

void Frame::switch_toLvlDown() {

    active_lvl -= 1;
    active_lvl = std::max(0, std::min(active_lvl, numOfPyrLvls-1));

}

void Frame::set_activeLayer(int lvl) {

    active_lvl =  lvl;

}

void Frame::set_image(const cv::Mat &im) {

    cv::Mat imFloat;
    im.convertTo(imFloat, CV_32FC1);
    data.images.push_back(im);
//    fill_GaussKernel();
//    fill_BoxKernel();
    data.mappablePointsMask = cv::Mat::zeros(im.rows, im.cols, IPL_DEPTH_1U);
}


void Frame::proppagate_diaprityToLvlvDownFrom(int lvl) {

    if ( lvl == 0){
        std::cerr << "DisparityBox: ref2target propagation is impossible, level index is zero" << std::endl;
        return;
    }

    cv::Mat disparity_map;

    cv::resize( data.disparities[lvl], disparity_map,  cv::Size( data.images[lvl-1].cols, data.images[lvl-1].rows ), 0,0, CV_INTER_CUBIC );
    disparity_map *=2.0;
    data.disparities[lvl-1] = disparity_map;
}


#endif //SOP_PCFROMSTEREOPAIR_FRAME_HPPFrame::Frame(const std::string& imPath){