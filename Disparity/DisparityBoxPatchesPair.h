//
// Created by morzh on 19.04.19.
//

#ifndef SSV_VER_0_1_DISPARITYBOXPATCHESPAIR_H
#define SSV_VER_0_1_DISPARITYBOXPATCHESPAIR_H


#include "MeasureZMNCC.h"
#include "../SatellitePatches/PatchesStereoPair.h"
#include "DisparityMatchingBox.h"
#include "MeasureNSSD.h"
#include "MeasureCensus.h"
#include "MeasureCensusModified.h"
#include "MeasureZMNCCModified.h"


struct  DisparityBoxPatchesPairOptions {

    std::vector<int>    half_window_sizes           =   {7, 11, 19};
    int                 disparity_search_range      =   6;
    float               confidence_epsilon          =   0.01;

};




class  DisparityBoxPatchesPair{

    void        estimate_disparityPair              ( PatchesStereoPair &pair );
    void        calc_confidence                     ( DisparityData &data );
    void        fuse_disparityData                  ( const std::vector<DisparityData> &data, DisparityData &fused_data);
    void        reverse_disparity                   ( const cv::Mat &d, cv::Mat &d_reversed);
    void        get_XShiftsIntFromFlow(const cv::Mat &flow, cv::Mat &disparity);
    bool        check_disparityData                 ( const std::vector<DisparityData> &data);


    double                              tolerance  = 1e-3;
    DisparityBoxPatchesPairOptions      opts;
};



void DisparityBoxPatchesPair::estimate_disparityPair(PatchesStereoPair &pair) {

    std::vector<DisparityData>      data_aggregation;
    DisparityData                   data_fused;
    std::vector<MeasureBase*>       measures(3);
    DisparityMatchingBox            box_matching;
    int                             size;

    measures[0] = new MeasureZMNCCModified();
    measures[1] = new MeasureNSSD();
    measures[2] = new MeasureCensusModified();


    for ( int idx_measure=0; idx_measure< measures.size(); ++idx_measure) {

        box_matching.measure = measures[idx_measure];

        for (int idx = 0; idx < opts.half_window_sizes.size(); ++idx) {

            DisparityData   data;
            cv::Mat         ref2target_init, target2ref_init;

            reverse_disparity(pair.disparity_local.ref2target, target2ref_init);
            get_XShiftsIntFromFlow(pair.flow, ref2target_init);

            size = opts.half_window_sizes[idx];

            box_matching.estimate_disparity(pair.patch1.imgPNT, pair.patch2.imgPNT, ref2target_init, size, opts.disparity_search_range, data.ref2target, &data.errors_ref2target);
            box_matching.estimate_disparity(pair.patch2.imgPNT, pair.patch1.imgPNT, target2ref_init, size, opts.disparity_search_range, data.target2ref, &data.errors_target2ref);

            calc_confidence( data );
            data_aggregation.push_back( data );
        }
    }

    fuse_disparityData(data_aggregation, pair.disparity_local);
}


void DisparityBoxPatchesPair::fuse_disparityData(const std::vector<DisparityData> &data, DisparityData &fused_data) { /// NEED TO BE CHECKED !!!
    /// using "Stereo Similarity Metric Fusion Using StereoConfidence" paper ::: https://lvdmaaten.github.io/publications/papers/ICPR_2014b.pdf

    if ( !check_disparityData(data))
        return;

    int min_d(+1e6), max_d(-1e6);


    for (auto datum: data) {

        double min, max;
        cv::minMaxLoc(datum.ref2target, &min, &max);
        min_d = std::min( (int)min, min_d);
        max_d = std::min( (int)max, max_d);
    }


    std::vector<float>::iterator      max_cost;
    int                               d_winner;


    for (int col=0; col < data[0].ref2target.cols; ++col){
        for (int row=0; row < data[0].ref2target.rows; ++row) {

            std::vector<float>      U(max_d-min_d);
            std::vector<float>      conf(max_d-min_d);
            std::fill(conf.begin(), conf.end(), 0.0);


            for (int idx_d = min_d; idx_d < max_d; ++idx_d) {
                for ( auto datum:data) {

                    if( datum.ref2target.at<int>(row, col) == idx_d){

                        U[idx_d]     = datum.ref2target.at<int>(row, col) * datum.confidence.at<float>(row, col);
                        conf[idx_d] += datum.confidence.at<float>(row, col);
                    }
                }
            }

            max_cost    =   std::max_element ( U.begin(), U.end()  );
            d_winner    =   std::distance    ( U.begin(), max_cost );

            fused_data.ref2target.at<int>(row,col)      =   d_winner;
            fused_data.confidence.at<float>(row,col)    =   conf[d_winner];
        }
    }

}



void DisparityBoxPatchesPair::calc_confidence(DisparityData &data) {
    /// using "Stereo Similarity Metric Fusion Using StereoConfidence" paper ::: https://lvdmaaten.github.io/publications/papers/ICPR_2014b.pdf

    /// \frac{c_1 - c_2}{| c_1 - \min_{d'} ( c'(x-d_1, y, d'))| - \epsilon}

    for(    int row=0; row < data.ref2target.rows; ++row ){
        for(int col=0; col < data.ref2target.cols; ++col){

            cv::Vec2f   error_ref2target        = data.errors_ref2target.at<cv::Vec2f>(row, col);
            cv::Vec2f   error_target2ref        = data.errors_target2ref.at<cv::Vec2f>(row, col);

            float       disparity_ref2target    =  data.ref2target.at<float>(row,                      col);
            float       disparity_target2ref    =  data.target2ref.at<float>(row+disparity_ref2target, col);
            float       min                     =  disparity_ref2target - disparity_target2ref;

            data.confidence.at<float>(row, col) =  (error_ref2target(0) - error_ref2target(1)) /( std::abs(error_ref2target(0) - min) - opts.confidence_epsilon) ;
        }
    }
}


void DisparityBoxPatchesPair::reverse_disparity(const cv::Mat &d, cv::Mat &d_reversed) {

    cv::Mat     xx      =   cv::Mat(d.rows, d.cols, CV_32FC1);
    cv::Mat     column  =   cv::Mat(d.rows, 1,      CV_32FC1);

    cv::Mat map( d.size(), CV_32FC2);


    for (int y = 0; y < map.rows; ++y){
        for (int x = 0; x < map.cols; ++x){

            float d__  = d.at<float>(y,x);
            map.at<cv::Point2f>(y, x) = cv::Point2f(y, x+d__);
        }
    }

    cv::remap(d, d_reversed, map);
    d_reversed *= -1.0;
}


void DisparityBoxPatchesPair::get_XShiftsIntFromFlow( const cv::Mat &flow, cv::Mat &disparity ) { ///NOT  DONE YET !!!!!

    cv::Mat xy[2];
    cv::split(flow, xy);
//    disparity = xy[1];

    xy[1].convertTo(disparity, CV_32SC1, 1.0, 0.5);
}


bool DisparityBoxPatchesPair::check_disparityData(const std::vector<DisparityData> &data) {


    int conf_cols= data[0].confidence.cols;
    int conf_rows= data[0].confidence.rows;

    int disparity_cols = data[0].ref2target.cols;
    int disparity_rows = data[0].ref2target.rows;

    for (int idx =1; idx<data.size(); ++idx){

        int ccols = data[idx].confidence.cols;
        int crows = data[idx].confidence.cols;

        int dcols = data[0].ref2target.cols;
        int drows = data[0].ref2target.rows;


        if ( conf_cols != ccols || conf_rows != crows || disparity_cols != dcols || disparity_rows != drows){

            std::cerr <<  "DisparityBoxPatchesPair::check_disparityData()::some elements in 'std::vector<DisparityData>& data' have  unmatched dimensons" << std::endl;
            return  false;
        }

    }

    return true;
}


#endif //SSV_VER_0_1_DISPARITYBOXPATCHESPAIR_H
