//
// Created by morzh on 10.04.19.
//

#ifndef SSV_VER_0_1_DISPARITYPOCBLOCKDEBUG_H
#define SSV_VER_0_1_DISPARITYPOCBLOCKDEBUG_H


#include "../Common/TiffWriter.h"

class DisparityPOCBlockDebug{
public:

    void show_divisionRectangles(const PatchesStereoPair &pair, std::array<cv::Rect, 4> rects);

    void write_alignedSubImages(const std::array<cv::Mat, 4> &imgs1, const std::array<cv::Mat, 4> &imgs2, const std::array<Eigen::Matrix<double, 3, 3>, 4> &xforms,
                                std::string filename_base);
};




void DisparityPOCBlockDebug::show_divisionRectangles(const PatchesStereoPair &pair, std::array<cv::Rect, 4> rects) {

    cv::Mat img1, img2, img2show;


    img1 = 0.5*pair.patch1.imgPNT+0.5;
    img2 = 0.5*pair.patch2.imgPNT+0.5;


    cv::cvtColor(img1, img1, cv::COLOR_GRAY2BGR);
    cv::cvtColor(img2, img2, cv::COLOR_GRAY2BGR);


    for (int i=0; i<4; i++){

        cv::rectangle(img1, rects[i], cv::Scalar(1,0,0), CV_INTER_CUBIC );
        cv::rectangle(img2, rects[i], cv::Scalar(1,0,0), CV_INTER_CUBIC );
    }


    cv::imshow("Image 1 Rectangles",  img1);
    cv::imshow("Image 2 Rectangles",  img2);
    cv::waitKey(-1);

}

void DisparityPOCBlockDebug::write_alignedSubImages(const std::array<cv::Mat, 4> &imgs1, const std::array<cv::Mat, 4> &imgs2, const std::array<Eigen::Matrix<double, 3, 3>, 4> &xforms,
                                                    std::string filename_base) {

    std::array<cv::Mat, 4> imgs2_xformed;
    TiffWriter writer;


    for ( int idx=0; idx<4; ++idx){

        cv::Mat M = cv::Mat::eye(2,3, CV_64FC1);

        M.at<double>(0,2) =  xforms[idx](0,2);
        M.at<double>(1,2) =  xforms[idx](1,2);

        cv::Size dsize  = cv::Size(imgs2[idx].cols, imgs2[idx].rows);

        cv::warpAffine( imgs2[idx], imgs2_xformed[idx], M, dsize );

//        cv::imshow("Svdsdv", imgs2_xformed[idx]);
//        cv::waitKey(-1);

        std::string filename = filename_base + "_" + std::to_string(idx) + ".tiff";

        std::vector<cv::Mat>    imgs(2);

//        cv::cvtColor( imgs1[idx],         imgs[0], cv::COLOR_GRAY2BGR );
//        cv::cvtColor( imgs2_xformed[idx], imgs[1], cv::COLOR_GRAY2BGR );

        imgs[0] = 255*imgs1[idx];
        imgs[1] = 255*imgs2_xformed[idx];

        imgs[0].convertTo( imgs[0], CV_8UC1 );
        imgs[1].convertTo( imgs[1], CV_8UC1 );

        cv::imshow("adasdf", imgs[0]);
        cv::imshow("adasdfasda", imgs[1]);
        cv::waitKey(-1);


//        imgs.push_back( imgs1[idx].clone() );
//        imgs.push_back( imgs2_xformed[idx].clone() );

        writer.write(filename, imgs);
    }




}


#endif //SSV_VER_0_1_DISPARITYPOCBLOCKDEBUG_H
