//
// Created by morzh on 25.09.18.
//

#ifndef SSV_VER_0_1_FRAMEDATA_H
#define SSV_VER_0_1_FRAMEDATA_H


#include <vector>


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


struct  FrameData{

    void        clear();


    std::vector<cv::Mat>                images;
    std::vector<cv::Mat>                gradsX;
    std::vector<cv::Mat>                gradsY;
    std::vector<cv::Mat>                edges;
    std::vector<cv::Mat>                corners;
    std::vector<cv::Mat>                cornersPropagatedPyramid;
    std::vector<cv::Mat>                disparities;
    std::vector<cv::Mat>                cornersDisparityPyramid;
    std::vector<cv::Mat>                masks;
    std::vector<cv::Mat>                cornerPointsMaskPyramid;
    std::vector<std::pair<int,int>>     minMaxDisparities;
    std::vector<std::pair<int,int>>     minMaxPyramidMatchingDisparities;
    std::vector<std::pair<float,float>> scaleShiftInrensitiesPyramid;

    std::pair<float, float>             scaleShiftZeroLvlPyramid;



    std::vector<int>                    numOfMappablePointsPyramid;

    cv::Mat                             mappablePointsMask;
};


void FrameData::clear() {

    images.clear();
    gradsX.clear();
    gradsY.clear();
    edges.clear();
    corners.clear();
    cornersPropagatedPyramid.clear();
    disparities.clear();
    cornersDisparityPyramid.clear();
    masks.clear();
    cornerPointsMaskPyramid.clear();
    minMaxDisparities.clear();
    minMaxPyramidMatchingDisparities.clear();
    scaleShiftInrensitiesPyramid.clear();
    numOfMappablePointsPyramid.clear();

}

#endif //SSV_VER_0_1_FRAMEDATA_H
