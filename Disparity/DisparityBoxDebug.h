//
// Created by morzh on 05.04.19.
//

#ifndef SSV_VER_0_1_DISPARITYBOXDEBUG_H
#define SSV_VER_0_1_DISPARITYBOXDEBUG_H


#include "Frame.h"


class DisparityBoxDebug{
public:

    DisparityBoxDebug(Frame *frame1, Frame *frame2);

    cv::Rect2i          rect_leftStripe, rect_rightStripe;
    cv::Rect2i          rect_match, rect_consistency;

    int                 disparity, consistecny;
    cv::Mat             left_stripe, right_stripe;

    cv::Mat             img1, img2;
    Eigen::Vector2i     center_leftStripe, center_rightStripe;

    Frame               *frame1, *frame2;

    bool                left_stripe_isValid =true;
    bool                right_stripe_isValid =true;

    void show_stripes();
    void show_disparity();
};




DisparityBoxDebug::DisparityBoxDebug(Frame *frame1, Frame *frame2) : frame1(frame1), frame2(frame2) {

}


void DisparityBoxDebug::show_stripes() {


    cv::Mat img2show, img1__, img2__;
    cv::Point2i center1, center2;

    img1__ = 0.5*img1+0.5;
    img2__ = 0.5*img2+0.5;

    cv::cvtColor(img1__, img1__, cv::COLOR_GRAY2BGR);
    cv::cvtColor(img2__, img2__, cv::COLOR_GRAY2BGR);


    if ( !left_stripe_isValid || !right_stripe_isValid){

        cv::hconcat( img1__, img2__, img2show );

        cv::Mat reddish = cv::Mat(img2show.rows, img2show.cols, CV_32FC3);
        cv::Mat img2show_reddish;


        reddish.setTo( cv::Scalar(0.7, 0.7, 1.05));
        img2show_reddish = img2show.mul(reddish);

        left_stripe_isValid     = true;
        right_stripe_isValid    = true;

        cv::imshow("Stripes Rectangles Fail", img2show_reddish);
        cv::waitKey(-1);
        return;
    }


    center1 = cv::Point2i( center_leftStripe(0),  center_leftStripe(1) );
    center2 = cv::Point2i( center_rightStripe(0), center_rightStripe(1));

    img1__.at<cv::Vec3f>(center1) = cv::Vec3f(1,1,1);
    img2__.at<cv::Vec3f>(center2) = cv::Vec3f(1,1,1);

    cv::rectangle(img1__, rect_leftStripe.tl(),  rect_leftStripe.br(),  cv::Scalar(0, 0, 1), 1 );
    cv::rectangle(img2__, rect_rightStripe.tl(), rect_rightStripe.br(), cv::Scalar(0, 0, 1), 1 );


    cv::hconcat( img1__, img2__, img2show );

    cv::imshow("Stripes Rectangles", img2show);
    cv::waitKey(-1);
}

void DisparityBoxDebug::show_disparity() {

    cv::Mat     left_stripe_4x, right_stripe_4x, right_stripe_4x_border;
    cv::Mat     img2show;

    left_stripe  = 0.5*left_stripe  + 0.5;
    right_stripe = 0.5*right_stripe + 0.5;


    cv::resize(left_stripe,  left_stripe_4x,  cv::Size(), 4.0, CV_INTER_NN);
    cv::resize(right_stripe, right_stripe_4x, cv::Size(), 4.0, CV_INTER_NN);

    /// make  images-stripes one size and v concatenate
    int             borderType = cv::BORDER_CONSTANT;
    int             top(0), bottom(0), left(0), right(right_stripe.cols-left_stripe.cols);
    cv::Scalar      color_border( 0,0,0 );

    cv::copyMakeBorder( right_stripe_4x, right_stripe_4x_border, top, bottom, left, right, borderType, color_border );
    cv::vconcat(right_stripe_4x_border, left_stripe_4x, img2show);

    /// show results
    cv::imshow("Stripes Box Matching", img2show);
    cv::waitKey(-1);
}


#endif //SSV_VER_0_1_DISPARITYBOXDEBUG_H
