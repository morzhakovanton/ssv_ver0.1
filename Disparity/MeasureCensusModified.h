//
// Created by morzh on 20.04.19.
//

#ifndef SSV_VER_0_1_MEASURECENSUSMODIFIED_H
#define SSV_VER_0_1_MEASURECENSUSMODIFIED_H


#include <opencv2/imgproc.hpp>
#include <iostream>
#include "opencv2/highgui.hpp"
#include "MeasureBase.h"


/**
 *      LOCAL STEREO MATCHING USING MOTION CUE AND MODIFIED CENSUS IN VIDEO DISPARITY ESTIMATION
 *  https://www.eurasip.org/Proceedings/Eusipco/Eusipco2012/Conference/papers/1569575901.pdf
 *
 * Здесь предполагается, что значения точек изображния находятся в переделах от -1 до +1
 * */


class MeasureCensusModified: public  MeasureBase{
public:

    float               calc_cost                           ( const cv::Mat &img1, const cv::Mat &img_2);
    float               calc_subpix                         ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);

private:

    float               get_alpha                           ( float val );
    bool                check_dimensions                    ( const cv::Mat &img1, const cv::Mat &img2);
    cv::Mat             calc_bitStringModified              ( const cv::Mat &block);
    float               calc_hammingModified                ( const cv::Mat &img1, const cv::Mat &img2);

    Eigen::Vector2f     alpha_range     =   Eigen::Vector2f(0, 0.02);
};

float MeasureCensusModified::calc_cost(const cv::Mat &img1, const cv::Mat &img2) {

    if ( !check_dimensions(img1, img2))
        return 0.0;

    cv::Mat img1__ = calc_bitStringModified(img1);
    cv::Mat img2__ = calc_bitStringModified(img2);


    return calc_hammingModified( img1__, img2__ );
}


float MeasureCensusModified::get_alpha(float val) {

    int interp = 0.5*val+0.5;

    return  interp*alpha_range(1) + (1-interp)*alpha_range(0);
}

bool MeasureCensusModified::check_dimensions(const cv::Mat &img1, const cv::Mat &img2) {

    if ( (img1.cols != img2.cols) || (img2.rows != img2.rows) ){

        std::cerr << " MeasureCensusModified::calc_cost()::one or both image dimensions not matched" << std::endl;
        return false;
    }
    if ( img1.cols %2 != 1 ||  img1.rows %2 != 1 ||  img2.cols %2 != 1 ||  img2.rows %2 != 1 ){

        std::cerr << " MeasureCensusModified::check_dimensions()::at least one of the images dimensions  isn't of odd size" << std::endl;
        return false;
    }


    return true;
}

cv::Mat MeasureCensusModified::calc_bitStringModified(const cv::Mat &block) {

    cv::Point2i         center      = cv::Point2i( (int)0.5*block.cols, (int)0.5*block.rows);
    float               center_val  = block.at<float>(center);
    float               alpha       = get_alpha(center_val);
    cv::Mat             compare = cv::Mat(block.rows, block.cols, CV_8UC1);


    for (    int col = 0; col < block.cols; ++col) {
        for (int row = 0; row < block.rows; ++row) {


            if ( row == center.y &&  col == center.x )
                compare.at<char>(row, col) = 0;

             float val = block.at<float>(row,col);

             if ( val  < center_val - alpha)
                 compare.at<char>(row, col) = 0;

             if ( val  >= center_val - alpha && val <= center_val + alpha)
                 compare.at<char>(row, col) = 1;

             if ( val  >  center_val + alpha)
                 compare.at<char>(row, col) = 2;
        }
    }


    return compare;
}


float MeasureCensusModified::calc_hammingModified(const cv::Mat &img1, const cv::Mat &img2) {

    if (!check_dimensions(img1, img2))
        return  1e3;

    float dist = -1.0;

    for (int row = 0; row < img1.rows; ++row) {
        for (int col = 0; col < img1.cols; ++col) {

            uchar val1 = img1.at<uchar>(row, col);
            uchar val2 = img2.at<uchar>(row, col);

            if ( val1 == val2)
                dist += 1.0;
        }
    }


    return 1.0 -  dist/(img1.rows*img1.cols - 1.0);
}


#endif //SSV_VER_0_1_MEASURECENSUSMODIFIED_H


