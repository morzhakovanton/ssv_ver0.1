//
// Created by morzh on 20.04.19.
//

#ifndef SSV_VER_0_1_MEASUREBASE_H
#define SSV_VER_0_1_MEASUREBASE_H


#include <vector>
#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"

#include <Eigen/Core>



class MeasureBase{
public:

    virtual ~MeasureBase() {    }

    virtual float               calc_cost                           ( const cv::Mat &img_1, const cv::Mat &img_2) = 0;
    virtual float               calc_subpix                         ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d) = 0;
};

#endif //SSV_VER_0_1_MEASUREBASE_H
