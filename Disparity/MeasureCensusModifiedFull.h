
#ifndef SSV_VER_0_1_MEASURECENSUSMODIFIEDFULL_H
#define SSV_VER_0_1_MEASURECENSUSMODIFIEDFULL_H


#include "opencv2/highgui.hpp"
#include <opencv2/imgproc.hpp>

#include "MeasureBase.h"


/**
 *      LOCAL STEREO MATCHING USING MOTION CUE AND MODIFIED CENSUS IN VIDEO DISPARITY ESTIMATION
 *  https://www.eurasip.org/Proceedings/Eusipco/Eusipco2012/Conference/papers/1569575901.pdf
 * */



class MeasureCensusModifiedFull : public MeasureBase {
public:

    float               calc_cost                           ( const cv::Mat &img1, const cv::Mat &img_2);


};




float MeasureCensusModifiedFull::calc_cost(const cv::Mat &img1, const cv::Mat &img_2) {

}


#endif //SSV_VER_0_1_MEASURECENSUSMODIFIEDFULL_H
