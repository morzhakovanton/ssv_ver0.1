//
// Created by morzh on 19.04.19.
//

#ifndef SSV_VER_0_1_MEASURENSSD_H
#define SSV_VER_0_1_MEASURENSSD_H

#include "MeasureBase.h"

#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"


class MeasureNSSD: public MeasureBase{
public:

    virtual float               calc_cost                           ( const cv::Mat &img_1, const cv::Mat &img_2);
    virtual float               calc_subpix                         ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);
    cv::Mat                     calc_zeroMeanNormalize              ( const cv::Mat &patch );
};



float MeasureNSSD::calc_cost(const cv::Mat &img_1, const cv::Mat &img_2) {

    cv::Mat             patch_1, patch_2, diff, diff_squared;

    patch_1 = calc_zeroMeanNormalize(img_1);
    patch_2 = calc_zeroMeanNormalize(img_2);

    diff = patch_1-patch_2;

    diff_squared = diff.mul(diff);

    return cv::sum(diff_squared)[0];
}


cv::Mat MeasureNSSD::calc_zeroMeanNormalize(const cv::Mat &patch) {

    cv::Scalar      meanVal             =   mean(patch);
//    float           mean                =   meanVal.val[0];
    cv::Mat         patch_centered;
    cv::Mat         patch_mean          = cv::Mat(patch.rows, patch.cols, CV_32FC1);


    patch_mean.setTo(meanVal);

    patch_centered  = patch - patch_mean;

    float normFactor = cv::norm(patch_centered);

    if (normFactor < 1e-6) {

        patch_centered.setTo(0);
        return patch_centered;
    }
    else{

        return patch_centered /= normFactor;
    }
}

#endif //SSV_VER_0_1_MEASURENSSD_H
