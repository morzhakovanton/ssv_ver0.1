//
// Created by morzh on 24.09.18.
//

#ifndef SSV_VER_0_1_SIMILARITYMEASURE_ZMNCC_H
#define SSV_VER_0_1_SIMILARITYMEASURE_ZMNCC_H

#include "MeasureBase.h"

#include <vector>
#include <opencv2/imgproc.hpp>
#include "opencv2/highgui.hpp"

#include <Eigen/Core>


/**
 * Мера zero mean normalized cross correlation выдаёт значения в отрезке [-1, 1]. Значение 1 соответствует полному совпадению.
 *
 * */


class MeasureZMNCC: public  MeasureBase{
public:
                        MeasureZMNCC();

    virtual float       calc_cost                           ( const cv::Mat &img_1, const cv::Mat &img_2);
    float               calc_dotProduct                     ( const std::vector<float> &v1, const std::vector<float> &v2);
    float               calc_dotProduct                     ( const cv::Mat &patch1, const cv::Mat &patch2);
    cv::Mat             calc_zeroMeanNormalize              ( const cv::Mat &patch );
    virtual float       calc_subpix                         ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);
    float               calc_subpixQuadratic                ( const cv::Mat &left_block, const cv::Mat &right_stripe, int d);
    void                calc_tau_nom_denom                  ( std::vector<std::pair<cv::Mat, float>> &rBoxes);
    void                calc_quadraticInterpolation         ( int disparity);
    void                prepare_quadraticInterpolation      ( int d);
    void                calc_centeredAndNormFactor          ( cv::Mat &patch, float &normFactor);


    bool            interpNotPossible;

    int             L2R, R2L;                        /** disparity 'left'  frame to 'right' frame ( L2R ) and 'right' frame  to 'left'( R2L ) */
    int             numOfPtsWithPositiveDenominator,   totalNumOfPts,    numOfQuadricInterp, halfLeftStripeSize;

    float           lambda, r, tau0, tau0_nom,   tau0_denom;
    float           rho_d0,    rho_d0m1,    rho_d0p1;
    float           L2R_subpx;
//    const float 	epsilon, minClearness;

};


MeasureZMNCC::MeasureZMNCC() { }


float MeasureZMNCC::calc_dotProduct(const cv::Mat &patch1, const cv::Mat &patch2) {

    if ( patch1.size != patch2.size ) return 0;

    float sum = 0;

    for (int i = 0; i < patch1.rows; ++i) {
        for (int j = 0; j < patch1.cols; ++j)

            sum += patch1.at<float>(i, j) * patch2.at<float>(i, j);
    }

    return sum;
}


cv::Mat MeasureZMNCC::calc_zeroMeanNormalize(const cv::Mat &patch) {

    cv::Scalar      meanVal             =   mean(patch);
//    float           mean                =   meanVal.val[0];
    cv::Mat         patch_centered;
    cv::Mat         patch_mean          = cv::Mat(patch.rows, patch.cols, CV_32FC1);


    patch_mean.setTo(meanVal);

    patch_centered  = patch - patch_mean;

    float normFactor = cv::norm(patch_centered);

    if (normFactor < 1e-6) {

        patch_centered.setTo(0);
        return patch_centered;
    }
    else{

        return patch_centered /= normFactor;
    }
}


float MeasureZMNCC::calc_cost(const cv::Mat &img_1, const cv::Mat &img_2) {

    cv::Mat             patch_1, patch_2;

    patch_1 = calc_zeroMeanNormalize(img_1);
    patch_2 = calc_zeroMeanNormalize(img_2);

    return calc_dotProduct(patch_1, patch_2);
}


void MeasureZMNCC::calc_centeredAndNormFactor(cv::Mat &patch, float &normFactor) {


//    std::cout << patch << std::endl;

    cv::Scalar  meanVal     = cv::mean( patch );
    float       mean        = static_cast<float>(meanVal.val[0]);
    cv::Mat     lPatchMean  = cv::Mat(patch.rows, patch.cols, CV_32FC1);

    lPatchMean.setTo(mean);
    patch -= lPatchMean;
    normFactor = (float)cv::norm(patch);
//    normFactor = static_cast<float>(cv::norm(patch));

    if (normFactor  < 1e-6 ){

        patch.setTo(0);
        return;
    }

    patch /= normFactor;
}


float MeasureZMNCC::calc_dotProduct(const std::vector<float> &v1, const std::vector<float> &v2) {


    if ( v1.size() != v2.size() ) return 0;

    float sum = 0;

    for (int i = 0; i < v1.size(); ++i) {   sum += v1[i] * v2[i] ;     }
//    std::cout << "sum value is: "  << sum << std::endl;
    return sum;
}

float MeasureZMNCC::calc_subpix(const cv::Mat &left_block, const cv::Mat &right_stripe, int d) {

    float   d_L2R_1, d_L2R_2;
    int     sz                  =   left_block.rows;
    int     num_right_blocks    =   right_stripe.cols - sz;
    cv::Mat lBlock_NCC;
    cv::Mat rBlock1, rBlock2;


    if ( d == num_right_blocks) {
        return d;
    }



    right_stripe(cv::Rect2i(d,0,sz,sz)).copyTo(rBlock1);
    right_stripe(cv::Rect2i(d+1,0,sz,sz)).copyTo(rBlock2);

//    std::cout << left_block << std::endl;


    rho_d0   =   calc_cost(left_block, rBlock1);
    rho_d0p1 =   calc_cost(left_block, rBlock2);

//    std::cout << left_block << std::endl;

    calc_centeredAndNormFactor(rBlock1, d_L2R_1);
    calc_centeredAndNormFactor(rBlock2, d_L2R_2);


    std::vector<std::pair<cv::Mat, float>>      rBoxes(2);
    std::vector<float>                          costs(3);

    rBoxes[0]  = std::make_pair(rBlock1, d_L2R_1);
    rBoxes[1]  = std::make_pair(rBlock2, d_L2R_2);




/*
    std::cout << "----------------------------------------------" << std::endl;
    std::cout <<  rBoxes[0].first  << std::endl;
    std::cout << rBoxes[0].second << std::endl;

    std::cout << "----------------------------------------------" << std::endl;

    std::cout <<  rBoxes[1].first  << std::endl;
    std::cout << rBoxes[1].second << std::endl;
    std::cout << "----------------------------------------------" << std::endl;
*/


    calc_tau_nom_denom( rBoxes);

    if ( tau0_denom < 0 ){

        L2R_subpx =  tau0_nom / tau0_denom ;

        if (std::abs (L2R_subpx ) < 0.5 )
            L2R_subpx = d - L2R_subpx;
        else
            L2R_subpx = calc_subpixQuadratic(left_block, right_stripe, d);
    }
    else{
        numOfPtsWithPositiveDenominator++;
        L2R_subpx = calc_subpixQuadratic(left_block, right_stripe, d);
    }

    return L2R_subpx;
}


void MeasureZMNCC::calc_tau_nom_denom(std::vector<std::pair<cv::Mat, float>> &rBoxes) {

    lambda   = rBoxes[1].second  /   rBoxes[0].second;
    r        = calc_dotProduct(rBoxes[0].first, rBoxes[1].first);

//    std::cout << lambda << " " << r << std::endl;

    tau0_nom      =      rho_d0p1  -  r * rho_d0;
    tau0_denom    =      lambda * ( r * rho_d0p1 - rho_d0 )   +  r * rho_d0   -  rho_d0p1;

//    std::cout << tau0_nom << " " << tau0_denom << std::endl;

#ifdef OUTPUTPERPOINT
    std::cout << "rho_d0_: " << d_.rho_d0_ << "; rho_d0+1: " << d_.rho_d0p1_ << "; r: " <<  d_.r << "; lambda: " <<  d_.lambda << std::endl;
    std::cout << "tau0_nom: " << d_.tau0_nom << "; tau0_denom: " << d_.tau0_denom << std::endl;
#endif

}

void MeasureZMNCC::prepare_quadraticInterpolation(int d) {

    L2R		= rho_d0;
    L2R_subpx= L2R;

    if (L2R == 0 || L2R == ( d-1 )  ) {

        interpNotPossible = true;
    }
    else {



/*
        rho_d0m1  = costs[0];
        rho_d0    = costs[1];
        rho_d0p1  = costs[2];
*/

        L2R_subpx           = L2R;
        interpNotPossible   = false;
    }

#ifdef OUTPUT
    std::cout << "ref2target: " << d_.L2R << std::endl;
#endif
}


void MeasureZMNCC::calc_quadraticInterpolation(int disparity) {

    if ( interpNotPossible ){

        L2R_subpx = disparity;
        return;
    }

    numOfQuadricInterp++;

    float denom =  rho_d0m1  - 2*rho_d0 + rho_d0p1 ;

    if (std::abs(denom) < 0.001 )  {

        L2R_subpx = disparity;
        return;
    }

    L2R_subpx = L2R - 0.5 * ( rho_d0p1 - rho_d0m1 ) / denom  ;

}


float MeasureZMNCC::calc_subpixQuadratic(const cv::Mat &left_block, const cv::Mat &right_stripe, int d) {

    L2R		    = d;
    L2R_subpx   = d;

    int sz      = left_block.rows;
    cv::Mat rBlock;

    if ( d == 0 || d == right_stripe.cols- sz) {

        interpNotPossible = true;
    }
    else {

        right_stripe(cv::Rect2i(d-1,0,sz,sz)).copyTo(rBlock);

        rho_d0m1            = calc_cost(left_block, rBlock);
        interpNotPossible   = false;
    }

    calc_quadraticInterpolation(d);

    return L2R_subpx;
}


#endif //SSV_VER_0_1_SIMILARITYMEASURE_H
