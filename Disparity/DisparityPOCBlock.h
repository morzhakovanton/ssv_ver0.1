//
// Created by morzh on 10.04.19.
//

#ifndef SSV_VER_0_1_DISPARITYPOCBLOCK_H
#define SSV_VER_0_1_DISPARITYPOCBLOCK_H


#include "../SatellitePatches/PatchesStereoPair.h"
#include "../Rectification/ImageRegistrationPOC.h"

#include "DisparityPOCBlockDebug.h"

//#define DEBUG



struct OpticalFlowPOCOptions__{

    int min_side_res = 64;
    float overlap_ratio = 0.25;
    float  usefull_surface_ratio = 0.7;

};

class DisparityPOCBlock{
public:

    enum CGRectEdge { minXEdge, maxXEdge, minYEdge, maxYEdge};


    void                    estimate_dispraityPair          ( PatchesStereoPair &pair, const OpticalFlowPOCOptions__ &options = OpticalFlowPOCOptions__() );
    void                    estimate_dispraityImages        ( cv::Mat img1, cv::Mat img2, cv::Mat disparity, float*  estimate_shiftY  );
    void                    fill_imageColsWithGradient      ( cv::Mat &imgrad );
    std::vector<cv::Rect>   substract_rectFromRect          ( cv::Rect rect1, cv::Rect rect2);
    cv::Rect                rectBetween(cv::Rect rect1, cv::Rect rect2, cv::Rect *remainder, CGRectEdge edge);

    OpticalFlowPOCOptions__    options;
    DisparityPOCBlockDebug      debug;

    int CvRectGetMaxY(cv::Rect rect);
    int CvRectGetMaxX(cv::Rect rect);
    void CvRectDivide(cv::Rect rect, cv::Rect *pRectSlice, cv::Rect *pRectRemainder, int amount, CGRectEdge edge);
};




void DisparityPOCBlock::estimate_dispraityPair(PatchesStereoPair &pair, const OpticalFlowPOCOptions__ &opts) {


    this->options = opts;


    ImageRegistrationT2POC              imreg_T2;
    ImageRegistrationPOCOptions         options_translate;

    std::array<cv::Mat,         4>      imgs1,  imgs2, vector_fields;
    std::array<cv::Rect,        4>      rects, global_rects;
    std::array<Eigen::Matrix3d, 4>      shifts;

    int                                 cols = pair.patch1.imgPNT.cols;
    int                                 rows = pair.patch1.imgPNT.rows;


    ///========================================================================= define working rectangles

    cv::Rect                            img_rect  = cv::Rect(0, 0, pair.patch1.imgPNT.cols, pair.patch1.imgPNT.rows);
    cv::Rect                            quad_base = cv::Rect(0, 0, 0.5*(1+options.overlap_ratio)*img_rect.width, 0.5*(1+options.overlap_ratio)*img_rect.height);


    rects[0] = quad_base;

    quad_base.x = cols - quad_base.width;
    rects[1] = quad_base;

    quad_base.y = rows - quad_base.height;
    rects[2] = quad_base;

    quad_base.x = 0;
    rects[3] = quad_base;
//    debug.show_divisionRectangles(pair, rects);


    ///=================================================================================  align images using POC


    options_translate.use_Gabor_filter = false;
    options_translate.calc_confidence = false;
    options_translate.apply_highpass_filter = false;
    options_translate.fft_window = ImageRegistrationPOCOptions::FFTWindow::BLACKMAN;

    for (int idx=0; idx<4; idx++){

        pair.patch1.imgPNT(rects[idx]).copyTo(imgs1[idx]);

        shifts[idx] = imreg_T2.estimate_translation(imgs1[idx], pair.patch2.imgPNT(rects[idx]), options_translate, nullptr);

        vector_fields[idx] = cv::Mat::zeros(imgs1[idx].rows, imgs1[idx].cols, CV_32FC2);
        vector_fields[idx].setTo(cv::Scalar(shifts[idx](0), shifts[idx](1)));


#ifdef DEBUG
        std::cout  <<  "subimage POC translate " << std::to_string(idx) << std::endl;
        std::cout  <<  shifts[idx] << std::endl;

        pair.patch2.imgPNT(rects[idx]).copyTo(imgs2[idx]);


        imgs1[idx] *= 0.5;
        imgs2[idx] *= 0.5;

        imgs1[idx] += 0.5;
        imgs2[idx] += 0.5;
#endif

    }


#ifdef DEBUG
    debug.write_alignedSubImages(imgs1, imgs2, shifts, "/home/morzh/Pictures/subImgsTest");
//    debug.show_alignedSubImages();
#endif


    /// mix vector fields from 0-1 rectangles
    cv::Mat     img_mask_01             =   cv::Mat(rects[0].height, pair.patch1.imgPNT.cols, CV_32FC1);
    cv::Rect    rect_0_1_union          =   rects[0] | rects[1];
    cv::Rect    rect_0_1_intersection   =   rects[0] & rects[1];
    cv::Mat     blend_mask           =   cv::Mat::zeros(rect_0_1_intersection.height, rect_0_1_intersection.width, CV_32FC1);
    cv::Rect    rect_0m1, rect_1m0, rect___;


    float amount =  rect_0_1_intersection.width;

    fill_imageColsWithGradient(blend_mask);

    cv::Mat img1  = cv::Mat( vector_fields[0].rows, vector_fields[0].cols-amount,              CV_32FC2 );
    cv::Mat img21 = cv::Mat( rect_0_1_intersection.height, rect_0_1_intersection.width,        CV_32FC2 );
    cv::Mat img22 = cv::Mat( rect_0_1_intersection.height, rect_0_1_intersection.width,        CV_32FC2 );
    cv::Mat img3  = cv::Mat( vector_fields[1].rows, vector_fields[1].cols-amount,              CV_32FC2 );



    cv::Rect rect00 = cv::Rect(0,                0, img1.cols, img1.rows);
    cv::Rect rect01 = cv::Rect(img1.cols-amount, 0, amount,    img1.rows);

    double min, max;
    cv::minMaxLoc(vector_fields[0], &min, &max);

    vector_fields[0] -= min;
    vector_fields[0] *= 10;

    cv::minMaxLoc(vector_fields[1], &min, &max);

    vector_fields[1] -= min;
    vector_fields[1] *= 10;


    vector_fields[0](rect00).copyTo(img1);
    vector_fields[0](rect01).copyTo(img21);

/*
    cv::Mat rect00_chan = cv::Mat::ones(rect00.height, rect00.width, CV_32FC1);
    cv::Mat rect01_chan = cv::Mat::ones(rect01.height, rect01.width, CV_32FC1);


    std::vector<cv::Mat> rect00_img, rect01_img;

    rect00_img.push_back(img1);
    rect00_img.push_back(rect00_chan);


    rect01_img.push_back(img21);
    rect01_img.push_back(rect01_chan);

    cv::Mat fin_img1, fin_img2;

    cv::merge(rect00_img, fin_img1);
    cv::merge(rect01_img, fin_img2);


    cv::imshow("rect00", fin_img1);
    cv::imshow("rect01", fin_img2);
    cv::waitKey(-1);
*/

    cv::Rect rect10 = cv::Rect( 0,      0, amount,                         img3.rows );
    cv::Rect rect11 = cv::Rect( amount, 0,  vector_fields[1].cols-amount,  img3.rows );

    vector_fields[1](rect10).copyTo(img22);
    vector_fields[1](rect11).copyTo(img3);

    cv::Mat blend2;

    cv::merge( std::vector<cv::Mat>{blend_mask, blend_mask}, blend2);

    cv::Mat     img2            = blend2.mul(img22) + (1.0 - blend2).mul(img21);
    cv::Mat     img_vfield_top;

    std::cout << img2;

    cv::hconcat( std::vector<cv::Mat>{img1, img2, img3}, img_vfield_top );
    cv::Mat check_vfield_ones = cv::Mat::ones( img_vfield_top.rows, img_vfield_top.cols, CV_32FC1);

    cv::Mat img2show;
    cv::merge(std::vector<cv::Mat>{img_vfield_top, check_vfield_ones}, img2show);


    cv::imshow("vector field blend", img2show);
    cv::waitKey(-1);
/*
    img_mask_01(rect_0m1).setTo(0.0);
    img_mask_01(rect_1m0).setTo(1.0);

    blend_mask.copyTo( img_mask_01(rect_0_1_intersection));


    cv::imshow("fuse mask", img_mask_01);
    cv::waitKey(-1);
*/


    /// mix vector fields from 2-3 rectangles


    /// mix vector fields from fused 0-1 and fused 2-3 rectangles


}

void DisparityPOCBlock::fill_imageColsWithGradient(cv::Mat &imgrad) {

    /// works only for one channel float images CV_32FC1 !!!!

    float step = 1.0/(imgrad.cols-1);


    for (int c = 0; c < imgrad.cols; c++){

        imgrad.col(c).setTo(step*c);
    }

    cv::imshow("grad image", imgrad);
    cv::waitKey(-1);
}

std::vector<cv::Rect> DisparityPOCBlock::substract_rectFromRect(cv::Rect rect1, cv::Rect rect2) {
/**
    rectangle1 \setminus rectangle2
    -------------------------
    |      rectangle 1      |
    |                       |
    |     -------------     |
    |     |rectangle 2|     |
    |     -------------     |
    |                       |
    |                       |
    -------------------------

    If you subtract rectangle 2 from rectangle 1, you will get an area with a hole. This area can be decomposed into 4 rectangles
    -------------------------
    |          A            |
    |                       |
    |-----------------------|
    |  B  |   hole    |  C  |
    |-----------------------|
    |                       |
    |          D            |
    -------------------------
*/



    if ( rect1.area() == 0 ) {
        return std::vector<cv::Rect>();
    }

    cv::Rect intersectedRect = rect1 | rect2;

    /// No intersection
    if (intersectedRect.area() ==0 ) {

        std::vector<cv::Rect>  rect(1);
        rect[0] = rect1;
        return rect;
    }


    std::vector<cv::Rect>   results;
    cv::Rect                remainder, subtractedArea;

    subtractedArea = rectBetween(rect1, intersectedRect, &remainder, CGRectEdge::maxYEdge);
    if ( subtractedArea.area() != 0 ) {
        results.push_back(subtractedArea);
    }

    subtractedArea = rectBetween(remainder, intersectedRect, &remainder, CGRectEdge::minYEdge);
    if (subtractedArea.area() != 0 ) {
        results.push_back(subtractedArea);
    }

    subtractedArea = rectBetween(remainder, intersectedRect, &remainder, CGRectEdge::maxXEdge);
    if (subtractedArea.area() != 0 ) {
        results.push_back(subtractedArea);
    }

    subtractedArea = rectBetween(remainder, intersectedRect, &remainder, CGRectEdge::minXEdge);
    if (subtractedArea.area() != 0 ) {
        results.push_back(subtractedArea);
    }

    return results;
}



cv::Rect DisparityPOCBlock::rectBetween(cv::Rect rect1, cv::Rect rect2, cv::Rect *remainder, CGRectEdge edge){
    /// returns the area between rect1 and rect2 along the edge

    cv::Rect intersectedRect = rect1 | rect2;

    if ( intersectedRect.area() == 0) {
        return cv::Rect();
    }

    cv::Rect    rect3;
    float       chop_amount = 0;

    switch (edge) {

        case CGRectEdge::maxYEdge:
            chop_amount = rect1.height - (intersectedRect.y - rect1.y);
            if (chop_amount > rect1.height) { chop_amount = rect1.height; }
            break;

        case CGRectEdge::minYEdge:
            chop_amount = rect1.height - (CvRectGetMaxY(rect1) - CvRectGetMaxY(intersectedRect));
            if (chop_amount > rect1.height) { chop_amount = rect1.height; }
            break;

        case CGRectEdge::maxXEdge:
            chop_amount = rect1.width - (intersectedRect.x - rect1.x);
            if (chop_amount > rect1.width) { chop_amount = rect1.width; }
            break;

        case CGRectEdge::minXEdge:
            chop_amount = rect1.width - (CvRectGetMaxX(rect1) - CvRectGetMaxX(intersectedRect));
            if (chop_amount > rect1.width) { chop_amount = rect1.width; }
            break;

        default:
            break;
    }

    CvRectDivide(rect1, remainder, &rect3, chop_amount, edge);

    return rect3;
}

void DisparityPOCBlock::CvRectDivide(cv::Rect rect, cv::Rect *pRectSlice, cv::Rect *pRectRemainder, int amount, CGRectEdge edge) {


    switch (edge){
        /// =================================================== X EDGES
        case CGRectEdge::minXEdge:

            if ( amount <= 0){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect(0,0,0,0);
                if ( pRectRemainder != nullptr)      *pRectRemainder     = rect;
            }
            if ( amount > 0 && amount < rect.width ){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect( rect.x,        rect.y, amount,            rect.height );
                if ( pRectRemainder != nullptr)      *pRectRemainder     = cv::Rect( rect.x+amount, rect.y, rect.width-amount, rect.height );
            }
            if ( amount >= rect.width ){

                if ( pRectSlice     != nullptr)      *pRectSlice         = rect;
                if ( pRectRemainder != nullptr)      *pRectRemainder     = cv::Rect(0,0,0,0);
            }
            break;

        case CGRectEdge::maxXEdge:

            if ( amount <= 0){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect(0,0,0,0);
                if ( pRectRemainder != nullptr)      *pRectRemainder     = rect;
            }
            if ( amount > 0 && amount < rect.width ){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect( rect.x,                     rect.y, rect.width-amount, rect.height );
                if ( pRectRemainder != nullptr)      *pRectRemainder     = cv::Rect( rect.x+rect.width-amount,   rect.y, amount,            rect.height );
            }
            if ( amount >= rect.width ){

                if ( pRectSlice     != nullptr)      *pRectSlice          = cv::Rect(0,0,0,0);
                if ( pRectRemainder != nullptr)      *pRectRemainder      = rect;
            }
            break;

            /// ================================================== Y EDGES
        case CGRectEdge::minYEdge:

            if ( amount <= 0){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect(0,0,0,0);
                if ( pRectRemainder != nullptr)      *pRectRemainder     = rect;
            }
            if ( amount > 0 && amount < rect.height ){

                if ( pRectSlice     != nullptr)      *pRectSlice         = cv::Rect( rect.x, rect.y,        rect.width, amount             );
                if ( pRectRemainder != nullptr)      *pRectRemainder     = cv::Rect( rect.x, rect.y+amount, rect.width, rect.height-amount );
            }
            if ( amount >= rect.height ){

                if ( pRectSlice     != nullptr)      *pRectSlice         = rect;
                if ( pRectRemainder != nullptr)      *pRectRemainder     = cv::Rect(0,0,0,0);
            }
            break;

        case CGRectEdge::maxYEdge:

            if ( amount <= 0){

                if ( pRectSlice     != nullptr)      *pRectSlice          = rect;
                if ( pRectRemainder != nullptr)      *pRectRemainder      = cv::Rect(0,0,0,0);
            }
            if ( amount > 0 && amount < rect.height ){

                if ( pRectSlice     != nullptr)      *pRectSlice          = cv::Rect( rect.x, rect.y,        rect.width, amount             );
                if ( pRectRemainder != nullptr)      *pRectRemainder      = cv::Rect( rect.x, rect.y+amount, rect.width, rect.height-amount );
            }
            if ( amount >= rect.height ){

                if ( pRectSlice     != nullptr)      *pRectSlice          = cv::Rect(0,0,0,0);
                if ( pRectRemainder != nullptr)      *pRectRemainder      = rect;
            }
            break;
    }

}

int DisparityPOCBlock::CvRectGetMaxY(cv::Rect rect) {

    return rect.y + rect.height;
}

int DisparityPOCBlock::CvRectGetMaxX(cv::Rect rect) {

    return rect.x + rect.width;
}

#endif //SSV_VER_0_1_DISPARITYPOCBLOCK_H
