#ifndef SSV_VER_0_1_CONVEXFOURGON_H
#define SSV_VER_0_1_CONVEXFOURGON_H


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <vector>
#include <type_traits>
#include <iomanip>
#include <Eigen/Dense>

#include "ImageRectangle.h"
#include "Segment.h"



/**
 * Класс являет собой выпуклый четырёхугольник. Вершины нумеруются по часовой с трелки, начиная с верхней. Верхней значит, что y-координата  (верхней) верщины >= y-координат всех
 * остальных вершин. При преобразовании точек происходит перенумерация вершин так, что первая  всегда "на верху".
 *
 **/


class ConvexTetragon{
public:

/*
    struct Segment{

        Segment(Eigen::Vector2d _pt1, Eigen::Vector2d _pt2): pt1(_pt1), pt2(_pt2){}

        Eigen::Vector2d pt1, pt2;
    };
*/

                                    ConvexTetragon              ( const Eigen::Vector2d& pt1, const Eigen::Vector2d& pt2,  const Eigen::Vector2d& pt3,  const Eigen::Vector2d& pt4 );
                                    ConvexTetragon              ( const Eigen::Matrix2Xd& );
                                    ConvexTetragon              ( const ImageRectangleI& );
                                    ConvexTetragon              ( const ImageRectangleD& );
                                    ConvexTetragon              (  );

    enum class DegenerateType { NONE, TRIANGLE, SEGMENT, POINT };


    void                            transform                   ( const Eigen::Matrix3d &xform );
    void                            shift                       ( const Eigen::Vector2d &t);
    ConvexTetragon                  offset                      ( double offset_val );
    void                            offset_inPlace              ( );

    Eigen::Vector2d                 get_verticesAverage         ( ) const;
    template <typename  Type>
    ImageRectangle<Type>            get_boundingBox             () const;
    void                            get_regularGrid             ( Eigen::Vector2i divs, Eigen::Matrix<double, 2, Eigen::Dynamic> &out) const ;
    Eigen::Vector2d                 get_orthgonal               ( const Eigen::Vector2d &vec);
    Eigen::Vector2d                 get_diagonalsIntersection   ();
    std::vector<cv::Point>          get_veticesCV               ( );
    std::vector<Segment>            get_sidesList() const;

    double                          calc_area                   () const;

    ConvexTetragon                  intersect                   ( const ConvexTetragon& tetra);
    ConvexTetragon                  circumscribe                ( const ConvexTetragon& tetra);
    void                            intersectInPlace            ( const ConvexTetragon& tetra);
    void                            circumscribeInPlace         ( const ConvexTetragon& tetra);
    template<class T>
    double                          intersect_area              ( const ImageRectangle<T>& rect ) const;


    template<class T>
    std::vector<Eigen::Vector2d>    intersection_points         ( const ImageRectangle<T> &rect) const;
    template<class T>
    std::vector<Eigen::Vector2d>    find_insidePoints(const ImageRectangle<T> &rect) const;


    bool                            check_isDegenerate          ( );
    bool                            check_isConvex              ( ) const;
    bool                            check_isNumericallyUnstable ( ) const;
    bool                            check_isInside              ( const Eigen::Vector2d &pt/*, const Eigen::Vector2d* p_center = nullptr*/) const;
    template<class T>
    bool                            check_isInside              ( const ImageRectangle<T>& rect ) const;
    bool                            check_isHomoFinite          ( const Eigen::Vector3d &v) const;
    template<class T>
    bool                            check_hasInside             ( const ImageRectangle<T>& rect ) const;
    template<class T>
    bool                            check_hasInside             ( const Eigen::Matrix<T,2,1>& pt ) const;
    DegenerateType                  check_hasDegenerateSides    ( );
    void                            make_ordered                ( );
    void                            process_points              ( );
    void                            cyclic_permutation          ( int init_idx );
    int                             find_firstPointIdx          ( );



    void                            make_convex                 ( );
    void                            print_vertices              ( );
    std::vector<Eigen::Vector2d>    verts;
    bool                            isDegenerate                = true;
    bool                            isConvex                    = false;
    bool                            isOrdered                   = false;

    bool                            isNumericallyUnstable       = false;
    DegenerateType                  typeDegenerate              = DegenerateType::NONE;
    double                          condition_number_threshold  = 1e-5;
    double                          degenerate_side_threshold   = 1e-4;
    double                          degenerate_side_length      = 1e-5;

    double                          finite_condition_tolerance  = 1e-5;

private:

    Eigen::Vector2d                 remove_homogenity           ( const Eigen::Vector3d &v) const;
    double                          calc_area                   ( const std::vector<Eigen::Vector2d> verts) const;
    double                          calc_areaTriangle           ( const std::vector<Eigen::Vector2d> &verts) const;
    void                            sort_clockwise              ( const std::vector<Eigen::Vector2d> &verts_src, std::vector<Eigen::Vector2d> &verts_dst) const;
    Eigen::Vector2d                 get_average                 ( const std::vector<Eigen::Vector2d> &verts) const;
    std::vector<Eigen::Vector2d>    form_triangle               ( int idx, const std::vector<Eigen::Vector2d> &verts, const Eigen::Vector2d &center) const;
};


ConvexTetragon::ConvexTetragon(const Eigen::Vector2d &pt1, const Eigen::Vector2d &pt2, const Eigen::Vector2d &pt3, const Eigen::Vector2d &pt4) {

    verts.clear();

    verts.push_back(pt1);
    verts.push_back(pt2);
    verts.push_back(pt3);
    verts.push_back(pt4);

    process_points();
}


ConvexTetragon::ConvexTetragon(const Eigen::Matrix2Xd & xform) {

    verts.clear();

    for (int idx = 0; idx < 4; ++idx) {

        Eigen::Vector2d v = xform.col(idx);
//        std::cout << v <<std::endl;
        verts.push_back(v);
    }

    process_points();
}



ConvexTetragon::ConvexTetragon(const ImageRectangleI & rect) {

    verts.clear();

    verts.push_back( Eigen::Vector2d( rect.x,            rect.y ) );
    verts.push_back( Eigen::Vector2d( rect.x+rect.width, rect.y ) );
    verts.push_back( Eigen::Vector2d( rect.x+rect.width, rect.y+rect.height ) );
    verts.push_back( Eigen::Vector2d( rect.x,            rect.y+rect.height ) );

    isDegenerate    = false;
    isConvex        = true;
}



ConvexTetragon::ConvexTetragon(const ImageRectangleD & rect) {

    verts.clear();

    verts.push_back( Eigen::Vector2d( rect.x,            rect.y ) );
    verts.push_back( Eigen::Vector2d( rect.x+rect.width, rect.y ) );
    verts.push_back( Eigen::Vector2d( rect.x+rect.width, rect.y+rect.height ) );
    verts.push_back( Eigen::Vector2d( rect.x,            rect.y+rect.height ) );

    isDegenerate    = false;
    isConvex        = true;
}


ConvexTetragon::ConvexTetragon() {


}


void ConvexTetragon::process_points() {

    isDegenerate            = check_isDegenerate();
    isConvex                = check_isConvex();
    isNumericallyUnstable   = check_isNumericallyUnstable();

    if ( !isDegenerate && !isConvex ){   make_convex();  }
    if ( !isDegenerate &&  isConvex) {   make_ordered(); }

}


void ConvexTetragon::make_ordered() {

    /// упорядочиваем веришны по часовой стрелке
    cv::Mat     points = cv::Mat(4,2,CV_32FC1);
    cv::Mat     convex_hull__;
    int         idx_init;

    std::vector<int> verts_indices;
    std::vector<Eigen::Vector2d>    verts_temp(4);

    points.at<float>(0,0) = verts[0](0);  points.at<float>(0,1) = verts[0](1);
    points.at<float>(1,0) = verts[1](0);  points.at<float>(1,1) = verts[1](1);
    points.at<float>(2,0) = verts[2](0);  points.at<float>(2,1) = verts[2](1);
    points.at<float>(3,0) = verts[3](0);  points.at<float>(3,1) = verts[3](1);

    cv::convexHull ( points, verts_indices, false, false );


    for (int idx = 0; idx < 4; ++idx) {

        verts_temp[idx] = verts[ verts_indices[idx]];

    }

    verts = verts_temp;

    idx_init =  find_firstPointIdx( );
    cyclic_permutation(idx_init);

    isOrdered = true;
}



void ConvexTetragon::transform(const Eigen::Matrix3d &xform) {

    if ( xform.determinant() < 0.001 ){ std::cerr << " determinant is near zero" << std::endl;}
    /// TODO: add badly condition matrix  check

    for (int idx = 0; idx < 4; ++idx) {

        Eigen::Vector3d v;

        v << verts[idx], 1;
        v = xform *v;
        v /= v(2);

        verts[idx](0) = v(0);
        verts[idx](1) = v(1);
    }


    int idx_init = find_firstPointIdx();

    cyclic_permutation(idx_init);
}



bool ConvexTetragon::check_isDegenerate() {

    /**
     * Проверка на невырожденность четырёхугольника. Проверяется попарное несовпадение вершин
     * */


    for (       int idx1 = 0;       idx1 < 4; ++idx1) {
        for (   int idx2 = idx1+1;  idx2 < 4; ++idx2) {

            Eigen::Vector2d   res = verts[idx1] - verts[idx2];

            if ( res.norm() < 0.0001) return true;
        }
    }

    return false;
}

bool ConvexTetragon::check_isConvex() const {

    /**
     * * Для проверки 4х угольника на выпцклость берем  прямую, проходящую чере соседние вершины (уже предполагается, что вершины идут по часовой стрелке) и
     * проверяем лежат ли оставшиеся две точки по одну и ту же сторону от прямой ( лежат в одной полуплоскости, образованной прямой line).
     * Координаты прямой получаем как векторное произведение точек, через которые она проходит
     * */

/*    if ( !isOrdered)       make_ordered();


    for (int idx = 0; idx < 4; ++idx) {

        Eigen::Vector3d pt1, pt2, line;

        pt1 << verts[ idx     ], 1;
        pt2 << verts[(idx+1)%4], 1;

        line = pt1.cross(pt2);

        Eigen::Vector2d     check_pt1, check_pt2;

        check_pt1 << verts[ (idx+2)%4 ], 1;
        check_pt2 << verts[ (idx+3)%4 ], 1;


        if (  check_pt1.dot(line) * check_pt1.dot(line)  < 0   ) return false;

    }


    return true;*/

    cv::Mat points = cv::Mat(4,2,CV_32FC1);

    points.at<float>(0,0) = verts[0](0);  points.at<float>(0,1) = verts[0](1);
    points.at<float>(1,0) = verts[1](0);  points.at<float>(1,1) = verts[1](1);
    points.at<float>(2,0) = verts[2](0);  points.at<float>(2,1) = verts[2](1);
    points.at<float>(3,0) = verts[3](0);  points.at<float>(3,1) = verts[3](1);


    std::vector<int>     convex_hull;
    cv::convexHull       ( points, convex_hull, false, false );

    return convex_hull.size() == 4;
}


template <typename Type>
ImageRectangle<Type> ConvexTetragon::get_boundingBox() const{

    std::vector<Type> x_coords;
    std::vector<Type> y_coords;


    if ( !(std::is_same<Type, double>::value || std::is_same<Type, float>::value  || std::is_same<Type, int>::value)   ){

/*
        std::cout << std::is_same<Type, double>::value << std::endl;
        std::cout << std::is_same<Type, float>::value << std::endl;
        std::cout << std::is_same<Type, int>::value << std::endl;
*/
        std::cerr << " Type should be float, double or int" << std::endl;
    }


    if ( std::is_same<Type, double>::value || std::is_same<Type, float>::value   ){

        for (int idx = 0; idx < 4; ++idx){

            x_coords.push_back(verts[idx](0));
            y_coords.push_back(verts[idx](1));
        }
    }


    if ( std::is_same<Type, int>::value ){

        for (int idx = 0; idx < 4; ++idx){

            x_coords.push_back( std::round(verts[idx](0)) );
            y_coords.push_back( std::round(verts[idx](1)) );
        }
    }


    typename std::vector<Type>::iterator min_x, max_x, min_y, max_y;

    min_x = std::min_element(x_coords.begin(), x_coords.end());
    max_x = std::max_element(x_coords.begin(), x_coords.end());
    min_y = std::min_element(y_coords.begin(), y_coords.end());
    max_y = std::max_element(y_coords.begin(), y_coords.end());


/*
    Type rect_x = std::distance(x_coords.begin(), min_x);
    Type rect_y = std::distance(y_coords.begin(), min_y);
    Type rect_width = std::distance(x_coords.begin(), max_x) - rect_x;
    Type rect_height = std::distance(y_coords.begin(), max_y) - rect_y;

    return ImageRectangle<Type>( rect_x, rect_y, rect_width, rect_height);

*/

    return ImageRectangle<Type>( *min_x, *min_y, *max_x-*min_x, *max_y-*min_y);
}


void ConvexTetragon::make_convex() {

    /**
     * если 4х угольник не выпуклый, то его можно мыслить как треугольник +  внутренняя точка. Далее внутренняя точка выдавливается из треугольника так, что все 4е точки образуют параллелограм
     * */



    std::vector<int>                convex_hull, convex_hull_sorted;
    std::vector<double>             triangle_lengths(3);
    std::vector<Eigen::Vector2d>    triangle_sides(3);
    cv::Mat                         points = cv::Mat(4,2,CV_32FC1);

    points.at<float>(0,0) = verts[0](0);  points.at<float>(0,1) = verts[0](1);
    points.at<float>(1,0) = verts[1](0);  points.at<float>(1,1) = verts[1](1);
    points.at<float>(2,0) = verts[2](0);  points.at<float>(2,1) = verts[2](1);
    points.at<float>(3,0) = verts[3](0);  points.at<float>(3,1) = verts[3](1);


    cv::convexHull       ( points, convex_hull, true, false );

    convex_hull_sorted = convex_hull;

    std::sort( convex_hull_sorted.begin(), convex_hull_sorted.end());

    for ( auto el:convex_hull_sorted){ std::cout << el <<" ";}


    int idx_interior_point(3);

    for (int idx = 0; idx < convex_hull_sorted.size(); ++idx) {

        if (idx != convex_hull_sorted[idx] ){  idx_interior_point = idx; break;}
    }

    std::cout << "  interior point idx: " << idx_interior_point << std::endl;

    for (int idx = 0; idx < 3; ++idx) {

        int idx_1 =  convex_hull[idx];
        int idx_2 =  convex_hull[(idx+1)%3];

        triangle_lengths[idx]   =  (verts[idx_2] - verts[idx_1]).norm();
        triangle_sides[idx]     =   verts[idx_2] - verts[idx_1];
    }


    std::vector<double>::iterator   max_length_iter     =   std::max_element( triangle_lengths.begin(), triangle_lengths.end() );
    int                             max_length_idx      =   std::distance( triangle_lengths.begin(), max_length_iter ) ;

    verts[idx_interior_point] = verts[convex_hull[max_length_idx]]  - triangle_sides[( max_length_idx+1 )%3];

    isConvex = true;
}



int ConvexTetragon::find_firstPointIdx() {

    auto min_y_coord = std::min_element(verts.begin(), verts.end(), [] (Eigen::Vector2d const& lhs, Eigen::Vector2d const& rhs) {return lhs(1) <= rhs(1);});

    auto distance = std::distance(verts.begin(), min_y_coord);

//    std::cout << distance << std::endl;

    return distance;
}


void ConvexTetragon::cyclic_permutation(int init_idx) {

    std::rotate(verts.begin(), verts.begin() + init_idx, verts.end());

}

void ConvexTetragon::shift(const Eigen::Vector2d &t) {


    for (int i = 0; i < 4; ++i) {

        verts[i] += t;
    }

}

Eigen::Vector2d ConvexTetragon::get_verticesAverage()  const {

    Eigen::Vector2d average;

    average.setZero();

    for (int idx = 0; idx < 4; ++idx) {

        average += verts[idx];
    }

    average /= 4.0;

    return  average;


}

void ConvexTetragon::print_vertices() {

    std::cout << "Tetragon vertices: " << std::endl;

    std::cout << std::setw(8) << verts[0](0)<< std::setw(8) << verts[1](0) << std::setw(8) << verts[2](0) << std::setw(8) << verts[3](0) << std::endl;
    std::cout << std::setw(8) << verts[0](1)<< std::setw(8) << verts[1](1) << std::setw(8) << verts[2](1) << std::setw(8) << verts[3](1) << std::endl;

    std::cout << "------------------------------------------------------------------" << std::endl;

}

void ConvexTetragon::get_regularGrid(Eigen::Vector2i divs, Eigen::Matrix<double, 2, Eigen::Dynamic> &out) const {

    ImageRectangleD         bbox = get_boundingBox<double>();
    Eigen::Matrix2Xd        bbox_grid, tetragon_grid;

    bbox.get_regularGrid( divs, bbox_grid);

    std::vector<bool>       labels(bbox_grid.cols());
    Eigen::Vector2d          center = get_verticesAverage();
//    Eigen::Vector2d          center = Eigen::Vector2d(0,0);

    std::fill (labels.begin(), labels.begin()+ labels.size(), true);
/*
    for (int i = 0; i < 4; ++i)
        center+=verts[i];

    center /= 4;
*/
    Eigen::Vector3d     center_homo;

    center_homo << center, 1;


    for (int i = 0; i < 4; ++i){

        Eigen::Vector3d     pt1, pt2;
        Eigen::Vector3d     line;
        double              center_sign;

        pt1 << verts[i], 1;
        pt2 << verts[(i+1)%4], 1;

        line = pt1.cross(pt2);

        center_sign = line.dot(center_homo);

        for(int idx=0; idx <  bbox_grid.cols(); ++idx ){

            if (labels[idx] == false) continue;

            Eigen::Vector3d grid_pt;

            grid_pt << bbox_grid.col(idx), 1;

            if ( line.dot( grid_pt ) * center_sign < 0)
                labels[idx] =false;
        }
    }


    int tetragon_grid_pt_num(0);

    for ( auto el:labels){

        if ( el )
        tetragon_grid_pt_num++;

    }

    out.conservativeResize(Eigen::NoChange, tetragon_grid_pt_num);


    int idx_out(0);

    for ( int idx_bbox_grid=0; idx_bbox_grid < bbox_grid.cols(); ++idx_bbox_grid){

        if (labels[idx_bbox_grid]){

            out.col(idx_out) = bbox_grid.col(idx_bbox_grid);
            idx_out++;
        }
    }


}

std::vector<cv::Point> ConvexTetragon::get_veticesCV() {

    std::vector<cv::Point> verts_out;

    for (int idx = 0; idx < 4; ++idx) {

        cv::Point pt = cv::Point( verts[idx](0), verts[idx](1));
        verts_out.push_back(pt);
    }

    return verts_out;
}

ConvexTetragon ConvexTetragon::offset(double offset_val) {

    /// возвращает 4х угольник с отступом

    /**
     * 1) сдвигаем 4х угольник так, чтобы центр был в начале координат
     * 2) находим координаты прямых, проходящих через стороны х4 угольника
     * 3) сдвигаем прямые к  нулю на значение offset_val
     * 4) находим пересечения прямых. Это будут  центрированные вершины нового 4х угольника
     * 5) относим  новый 4х угольник обратно (на значение center)
     * */


    if ( isNumericallyUnstable){

        std::cerr << "offset result will be unstable" << std::endl;
    }

    Eigen::Vector2d                 center = get_verticesAverage();
    std::vector<Eigen::Vector3d>    verts_centered(4);
    std::vector<Eigen::Vector3d>    tetragon_lines(4); /// координаты прямых, которые образуют 4х угольник
    std::vector<Eigen::Vector2d>    tetragon_verts_new(4);


    /// Шаг 1)
    for (int idx = 0; idx < 4; ++idx) {

        verts_centered[idx] << verts[idx] - center, 1;
    }


    /// Шаг 2)-3)
    for (int idx=0; idx < 4; ++idx){

        tetragon_lines[idx]         =   verts_centered[idx%4].cross( verts_centered[(idx+1)%4] );
        double line_norm_factor     =   tetragon_lines[idx].head(2).norm();
        tetragon_lines[idx]         /=  line_norm_factor;
        double d                    =   std::max(1e-6, tetragon_lines[idx](2)+offset_val);
        tetragon_lines[idx](2)      =   d;
    }
    /// Шаг 4)-5)
    for (int idx = 0; idx < 4; ++idx) {

        Eigen::Vector3d intersection_point = tetragon_lines[idx].cross( tetragon_lines[(idx+1)%4] );
        intersection_point /= intersection_point(2);
        tetragon_verts_new[idx] = intersection_point.head(2)+center;

    }

    return ConvexTetragon( tetragon_verts_new[0],tetragon_verts_new[1],tetragon_verts_new[2],tetragon_verts_new[3]);
}

Eigen::Vector2d ConvexTetragon::get_orthgonal(const Eigen::Vector2d &vec) {

    return Eigen::Vector2d(vec(1), -vec(0));
}


Eigen::Vector2d ConvexTetragon::get_diagonalsIntersection() {


    std::vector<Eigen::Vector3d>    diagonals(2);
    Eigen::Vector3d                 cross_point;

    for (int idx = 0; idx < 2; ++idx) {

        Eigen::Vector3d pt1, pt2;

        pt1 << verts[idx], 1;
        pt2 << verts[idx+2], 1;

        diagonals[idx] = pt1.cross(pt2);

    }

    cross_point = diagonals[0].cross(diagonals[1]);

    cross_point /= cross_point(2);

    return cross_point.head(2);
}

bool ConvexTetragon::check_isNumericallyUnstable() const {


    Eigen::Vector2d                 center = get_verticesAverage();
    Eigen::Matrix<double, 2,4>      verts_centered;
    Eigen::Matrix2d                 cov;
    Eigen::Vector2cd                eigen_values_complex;
    Eigen::Vector2d                 eigen_values_real;
    double                          max_value, min_value;

    for (int idx = 0; idx < 4; ++idx) {

        verts_centered.col(idx) = verts[idx] - center;
    }


    cov = verts_centered * verts_centered.transpose();

    Eigen::EigenSolver<Eigen::Matrix2d> es(cov);

    eigen_values_complex  = es.eigenvalues();

    eigen_values_real << std::abs(eigen_values_complex(0)), std::abs(eigen_values_complex(1));

    if ( eigen_values_real(0) > eigen_values_real(1)){

        max_value = eigen_values_real(0);
        min_value = eigen_values_real(1);

    }else{

        max_value = eigen_values_real(1);
        min_value = eigen_values_real(0);

    }


    if ( min_value / max_value < condition_number_threshold)
        return false;
    else
        return  true;
}

ConvexTetragon ConvexTetragon::intersect(const ConvexTetragon &tetra) {

    /**
     * 1) Находим кол-во  вершин tetra, лежащих в данном 4х угольнике. Если ноль, возщвращаем пустой 4х угольник
     * */


    if ( isNumericallyUnstable){   std::cerr << "intersect result will be unstable" << std::endl;  }

    int num_points_inside(0);

    for (int idx = 0; idx < 4; ++idx) {

        if (check_isInside(tetra.verts[idx]))
            num_points_inside++;
    }


    if ( num_points_inside == 0){

        ConvexTetragon  degen_tetra;
        degen_tetra.isDegenerate == true;
        return degen_tetra;
    }


    if ( num_points_inside == 1){


    }


    if ( num_points_inside == 2){


    }


    if ( num_points_inside == 3){


    }


    if ( num_points_inside == 4){

        return tetra;
    }


    return ConvexTetragon();
}

bool ConvexTetragon::check_isInside(const Eigen::Vector2d &pt/*, const Eigen::Vector2d* p_center*/) const {

    Eigen::Vector3d     center_homo;
    Eigen::Vector3d     pt_homo;
    int                 counter(0);


    center_homo << get_verticesAverage(), 1;

/*
    if ( p_center == nullptr)
        center_homo << get_verticesAverage(), 1;
    else
        center_homo << *p_center, 1;
*/


    pt_homo << pt, 1;

    for (int idx = 0; idx < 4; ++idx) {

        Eigen::Vector3d     pt1, pt2;
        Eigen::Vector3d     line;

        pt1 << verts[idx], 1;
        pt2 << verts[(idx+1)%4], 1;

        line = pt1.cross(pt2);

        if ( line.dot(pt_homo) * line.dot(center_homo) > 0.0 )
//        if ( line.dot(pt) * line.dot(center_homo) > 0.0 )

            counter ++;
    }


    if ( counter == 4 )
        return true;
    else
        return false;
}


ConvexTetragon::DegenerateType ConvexTetragon::check_hasDegenerateSides() {

    std::vector<bool>               sides_isDegenerate(4);
    int                             counter(0);

    for (int idx = 0; idx < 4; ++idx) {

        double side_length = ( verts[(idx+1)%4] - verts[idx]).norm();

        if ( side_length < degenerate_side_threshold ){

            sides_isDegenerate[idx] = true;
            counter++;
        }
    }


    switch (counter) {
        case 0:
            return DegenerateType::NONE;
        case 1:
            return DegenerateType::TRIANGLE;
        case 2:
            return DegenerateType::SEGMENT;
        case 3:
            return DegenerateType::POINT;
    }


    return DegenerateType::NONE;
}

double ConvexTetragon::calc_area() const {

    double              area    = 0.0;
    Eigen::Vector3d     pt1H, pt2H, pt3H;
    Eigen::Vector3d     line;
    double              base, height,  denom;

    /// сalculate triangle 1
    pt1H << verts[0], 1;
    pt2H << verts[1], 1;
    pt3H << verts[2], 1;


    line    =   pt1H.cross(pt2H);
    base    =   (pt1H - pt2H).norm();
    denom   =   std::sqrt( line(0)*line(0) + line(1)*line(1) );
    height  =   line.dot(pt3H) / denom;

    area += 0.5*base*height;

    /// сalculate triangle 2
    pt1H << verts[2], 1;
    pt2H << verts[3], 1;
    pt3H << verts[0], 1;

    line    =   pt1H.cross(pt2H);
    base    =   (pt1H - pt2H).norm();
    denom   =   std::sqrt( line(0)*line(0) + line(1)*line(1) );
    height  =   line.dot(pt3H) / denom;

    area += 0.5*base*height;

    return area;
}

template<class T>
bool ConvexTetragon::check_isInside(const ImageRectangle<T> &rect) const{

    return  rect.check_hasInside(verts[0]) &&
            rect.check_hasInside(verts[1]) &&
            rect.check_hasInside(verts[2]) &&
            rect.check_hasInside(verts[3]);
}

template<class T>
bool ConvexTetragon::check_hasInside(const Eigen::Matrix<T,2,1> &pt) const {

    return false;

}

template<class T>
bool ConvexTetragon::check_hasInside(const ImageRectangle<T> &rect) const {

    return  check_hasInside( rect.get_ul() ) &&
            check_hasInside( rect.get_ur() ) &&
            check_hasInside( rect.get_br() ) &&
            check_hasInside( rect.get_bl() );
}

template<class T>
double ConvexTetragon::intersect_area(const ImageRectangle<T> &rect) const{

    double area = 0;

    if ( check_isInside(rect))
        return calc_area();

    if ( check_hasInside(rect))
        return rect.calc_area();


    std::vector<Eigen::Vector2d>    intersection_verts;
    std::vector<Eigen::Vector2d>    inside_verts;
    std::vector<Eigen::Vector2d>    all_verts;


    intersection_verts  =   intersection_points(rect);
    inside_verts        =   find_insidePoints(rect);



    all_verts.reserve( intersection_verts.size() + inside_verts.size() ); // preallocate memory
    all_verts.insert( all_verts.end(), inside_verts.begin(), intersection_verts.end() );
    all_verts.insert( all_verts.end(), inside_verts.begin(), inside_verts.end() );


    calc_area(all_verts);

    return area;
}

template<class T>
std::vector<Eigen::Vector2d> ConvexTetragon::intersection_points(const ImageRectangle<T> &rect) const {

    std::vector<Eigen::Vector2d>    intersection_pts;
    std::vector<Segment>            tetragon_sides, rectangle_sides;

    tetragon_sides  = get_sidesList();
    rectangle_sides = rect.get_sidesList();


    for (int idx1=0; idx1< 4; ++idx1){
        for ( int idx2; idx2 < 4; ++idx2){

            Eigen::Vector3d  intersect_pt = tetragon_sides[idx1].intersect(rectangle_sides[idx2]);

            if (check_isHomoFinite(intersect_pt))
                intersection_pts.push_back( remove_homogenity(intersect_pt) );
        }
    }

    return intersection_pts;
}


std::vector<Segment> ConvexTetragon::get_sidesList() const {

    std::vector<Segment> segments(4);

    segments[0] = Segment(verts[0], verts[1]);
    segments[1] = Segment(verts[1], verts[2]);
    segments[2] = Segment(verts[2], verts[3]);
    segments[3] = Segment(verts[3], verts[0]);

    return segments;
}

bool ConvexTetragon::check_isHomoFinite(const Eigen::Vector3d &v) const {
    /// check if point in homogeneous representation is near infinity

    return v(2) > finite_condition_tolerance;
}

Eigen::Vector2d ConvexTetragon::remove_homogenity(const Eigen::Vector3d &v) const{

    return Eigen::Vector2d( v(0)/v(2), v(1)/v(2));
}

double ConvexTetragon::calc_area(const std::vector<Eigen::Vector2d> verts) const  {

    if (verts.size() == 3)
        return calc_areaTriangle(verts);


    Eigen::Vector2d                 center;
    std::vector<Eigen::Vector2d>    triangle;
    double                          area=0;
    std::vector<Eigen::Vector2d>    verts__;

    sort_clockwise(verts, verts__ );

    center = get_average(verts__);

    for (int idx = 0; idx < verts.size() - 1; ++idx) {

        triangle = form_triangle(idx, verts, center);

        area += calc_areaTriangle(triangle);
    }

    return area;
}

double ConvexTetragon::calc_areaTriangle(const std::vector<Eigen::Vector2d> &verts) const {

    if ( verts.size() != 3){
        std::cerr << "num of vertices is not 3" << std::endl;
    }

    Eigen::Vector3d     pt1H, pt2H, pt3H;
    Eigen::Vector3d     line;
    double              base, height,  denom;

    pt1H << verts[0], 1;
    pt2H << verts[1], 1;
    pt3H << verts[2], 1;


    line    =   pt1H.cross(pt2H);
    base    =   (pt1H - pt2H).norm();
    denom   =   std::sqrt( line(0)*line(0) + line(1)*line(1) );
    height  =   line.dot(pt3H) / denom;

    return  0.5*base*height;
}

void ConvexTetragon::sort_clockwise(const std::vector<Eigen::Vector2d> &verts_src, std::vector<Eigen::Vector2d> &verts_dst) const {

    int vnum = verts_src.size();

    if ( vnum <= 2)  return;

    /// упорядочиваем веришны по часовой стрелке
    cv::Mat             points      = cv::Mat(vnum,2,CV_32FC1);
    cv::Mat             convex_hull__;
    std::vector<int>    verts_indices;

    verts_dst.resize(vnum);

    for ( int idx=0; idx < vnum; ++idx){

        points.at<float>(idx,0) = verts_src[idx](0);
        points.at<float>(idx,1) = verts_src[idx](1);
    }

    cv::convexHull( points, verts_indices, false, false );


    for (int idx = 0; idx < vnum; ++idx)

        verts_dst[idx] = verts_src[ verts_indices[idx]];

}

Eigen::Vector2d ConvexTetragon::get_average(const std::vector<Eigen::Vector2d> &verts) const {

    Eigen::Vector2d avg = Eigen::Vector2d(0,0);

    for (auto v : verts)
        avg += v;

    return avg / verts.size();
}

std::vector<Eigen::Vector2d> ConvexTetragon::form_triangle(int idx, const std::vector<Eigen::Vector2d> &verts, const Eigen::Vector2d &center) const {

    int vnum = verts.size();

    std::vector<Eigen::Vector2d> sorted_verts( vnum );

    int j=0;
    for (int i = idx; i < vnum + idx; ++i, ++j)

        sorted_verts[j] = verts[i%vnum];

    return sorted_verts;
}

template<class T>
std::vector<Eigen::Vector2d> ConvexTetragon::find_insidePoints(const ImageRectangle<T> &rect) const {

    std::vector<Eigen::Vector2d> inside_points;


    if ( check_hasInside( rect.get_ul()) )

        inside_points.push_back(  Eigen::Vector2d( rect.get_ul()(0), rect.get_ul()(1) )   );


    if ( check_hasInside( rect.get_ur()) )
        inside_points.push_back(  Eigen::Vector2d( rect.get_ur()(0), rect.get_ur()(1) )   );


    if ( check_hasInside( rect.get_bl()) )
        inside_points.push_back(  Eigen::Vector2d( rect.get_bl()(0), rect.get_bl()(1) )   );


    if ( check_hasInside( rect.get_br()) )
        inside_points.push_back(  Eigen::Vector2d( rect.get_br()(0), rect.get_br()(1) )   );


    return inside_points;
}


#endif //SSV_VER_0_1_CONVEXFOURGON_H
