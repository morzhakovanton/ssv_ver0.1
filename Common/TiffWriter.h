//
// Created by morzh on 23.02.18.
//

#ifndef SSV_VER_0_1_TIFFWRITER_H
#define SSV_VER_0_1_TIFFWRITER_H


#include <string>
#include <tiffio.h>

class TiffWriter
{
public:

    void write( const std::string& filename, std::vector<cv::Mat>& images);


};

void TiffWriter::write(const std::string &filename, std::vector<cv::Mat> &imgs) {

    /// now works only with CV_8UC1 and CV_8UC3


    cv::Mat     im1, im2;


    if ( imgs[0].type() != imgs[1].type()                             ){   std::cerr << "images are not of the same type" << std::endl;  }
    if ( imgs[0].cols != imgs[1].cols || imgs[0].rows != imgs[1].rows ){   std::cerr << "images are not of the same size" << std::endl;    }


    if ( imgs[0].channels() == 3) {
        cv::cvtColor(imgs[0], im1, CV_BGR2RGB);
        cv::cvtColor(imgs[1], im2, CV_BGR2RGB);
    }else{
        im1 = imgs[0];
        im2 = imgs[1];
    }


    uint32 image_width = im1.cols, image_height = im1.rows;
    uint16 spp, bpp, photo, res_unit;
    TIFF *out;
    int i, j;
    uint16 page;
    uint8  NPAGES;


    int                 num_channels        =       im1.channels();
    int                 im_square           =       image_width*image_height;
    unsigned  char*     ims                 =       new unsigned char [ 2*im_square*num_channels ];

    std::memcpy( ims                         , im1.data, sizeof(unsigned char)*im_square*num_channels);
    std::memcpy( (ims+im_square*num_channels), im2.data, sizeof(unsigned char)*im_square*num_channels);


    out = TIFFOpen( filename.c_str(), "w");

    if (!out) {       std::cerr << "Can't open "<<  filename << " for writing \n";        return;     }


    spp             =   num_channels; /* Samples per pixel */
    bpp             =   8; /* Bits per sample */
    NPAGES          =   imgs.size();


    if ( num_channels == 1)
        photo           =   PHOTOMETRIC_MINISBLACK;
    else if ( num_channels == 3)
        photo           =   PHOTOMETRIC_RGB;
    else
        std::cerr << "can't write this file format so far" << std::endl;


    for (page = 0; page < NPAGES; page++){

        TIFFSetField(out, TIFFTAG_IMAGEWIDTH, image_width );
        TIFFSetField(out, TIFFTAG_IMAGELENGTH, image_height);
        TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bpp);
        TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);
        TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photo);
        TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(out, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
        TIFFSetField(out, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        TIFFSetField(out, TIFFTAG_PAGENUMBER, page, NPAGES);

        for (j = 0; j < image_height; j++)
            TIFFWriteScanline(out, &ims[j * image_width*num_channels + im_square*num_channels*page], j, 0);

        TIFFWriteDirectory(out);
    }

}


#endif //SSV_VER_0_1_TIFFWRITER_H
