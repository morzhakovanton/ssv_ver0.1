//
// Created by morzh on 09.03.18.
//

#ifndef SSV_VER_0_1_CONVEXTETRAGONVIZ_H
#define SSV_VER_0_1_CONVEXTETRAGONVIZ_H


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <Eigen/Dense>
#include <Eigen/Geometry>


#include <vector>
#include <random>
#include <iostream>

#include "ConvexTetragon.h"


class ConvexTetragonTest{

public:
                            ConvexTetragonTest          (   );

    void                    test_fillRandom             ( ConvexTetragon& tetragon);
    void                    test_initialization         ( ConvexTetragon &tetragon);
    Eigen::Matrix3d         test_randomTransform        ( double rad_min, double rad_max, double t_min, double t_max);
    void                    test2                       ( ConvexTetragon& tetragon);
    void                    test_offset                 ( int num_times = 100);
    void                    test_intersection           ( int num_times = 100);
    void                    test_isPointInside          ( int num_times = 100);

    void                    create_gpaph                ( cv::Mat& img, ConvexTetragon& tetragon);
    void                    create_gpaph2               ( cv::Mat& img, ConvexTetragon& tetragon);
    void                    create_randomPoints         ( Eigen::Matrix2Xd &M);
    void                    make_graphsAll              (   );


    Eigen::Affine2d         create_randomTransform      (   );

    cv::Mat                 draw_tetragon(ConvexTetragon tetragon, cv::Mat* img= nullptr);
    cv::Mat                 draw_tetragons(std::vector<ConvexTetragon> tetragons, cv::Mat *img= nullptr);

    void                    show_tetragon(ConvexTetragon tetragon);
    void                    show_tetragons(std::vector<ConvexTetragon> tetragons);


    std::vector<Eigen::Vector2d>    temp_verts, temp_verts_dump;
    cv::Mat                         verts_src, verts_processed, verts_transformed,  verts_all_stages;
    int rand_range_min = 150;
    int rand_range_max = 350;
    int graph_size = 600;
};



ConvexTetragonTest::ConvexTetragonTest() {

}


void ConvexTetragonTest::test_fillRandom( ConvexTetragon& tetragon ) {

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(rand_range_min, rand_range_max);


    for (int idx = 0; idx < 4; ++idx) {

        tetragon.verts[idx](0) = dist(gen);
        tetragon.verts[idx](1) = dist(gen);
    }

    tetragon.isDegenerate = true;
    tetragon.isConvex = false;


}

void ConvexTetragonTest::test_initialization(ConvexTetragon &tetragon) {


    for (int i = 0; i < 500; ++i) {

        test_fillRandom(tetragon);
        temp_verts_dump = tetragon.verts;
        create_gpaph(verts_src, tetragon);
        tetragon.process_points();
        create_gpaph(verts_processed, tetragon);
        Eigen::Matrix3d xform = test_randomTransform(0, 3.14159265, -5,+5);
//        std::cout << xform << std::endl;
        Eigen::Vector2d shift = tetragon.get_verticesAverage();
        tetragon.shift(  -shift);
        tetragon.transform(xform);
        tetragon.shift( shift);
        create_gpaph(verts_transformed, tetragon);
        make_graphsAll();

        cv::imshow("tetragon test_initialization", verts_all_stages);
        int key = cv::waitKey(-1);

        if ( key == 99 )  break;

        if ( key == 100 || key == 86 ){ /// 'D' and 'd' keys

            for ( auto v:temp_verts_dump ) { std::cout <<  v << std::endl;  std::cout <<  "-----------" << std::endl;}
        }

    }

}
void ConvexTetragonTest::test2(ConvexTetragon& tetragon) {


        create_gpaph(verts_src, tetragon);
        tetragon.process_points();
        create_gpaph(verts_processed, tetragon);
        Eigen::Matrix3d xform = test_randomTransform(0, 3.14159265, -5,+5);
        std::cout << xform << std::endl;
        Eigen::Vector2d shift = tetragon.get_verticesAverage();
        tetragon.shift(  -shift);
        tetragon.transform(xform);
        tetragon.shift( shift);
        create_gpaph(verts_transformed, tetragon);
        make_graphsAll();

        cv::imshow("tetragon test_initialization", verts_all_stages);
        int key = cv::waitKey(-1);
        std::cout << key << std::endl;

}


void ConvexTetragonTest::make_graphsAll() {

    verts_all_stages = cv::Mat::zeros(graph_size, 3*graph_size, CV_8UC3);

    verts_src.copyTo         ( verts_all_stages(cv::Rect(0,             0, graph_size, graph_size)) );
    verts_processed.copyTo   ( verts_all_stages(cv::Rect(graph_size,    0, graph_size, graph_size)) );
    verts_transformed.copyTo ( verts_all_stages(cv::Rect(2*graph_size,  0, graph_size, graph_size)) );

}

void ConvexTetragonTest::create_gpaph(cv::Mat &img, ConvexTetragon &tetragon) {

    img = cv::Mat::ones( graph_size, graph_size, CV_8UC3);

    img.setTo(cv::Scalar(255,255,255));

    for (int idx = 0; idx < 4; ++idx) {

        cv::Point  pt   = cv::Point( tetragon.verts[idx](0), tetragon.verts[idx](1) );

        cv::circle(img, pt, 2, cv::Scalar(20,20,20), 1);
        putText(img, std::to_string(idx), pt, cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(200,20,20), 1.0);
    }


}

void ConvexTetragonTest::create_gpaph2(cv::Mat &img, ConvexTetragon &tetragon) {

    img = cv::Mat::ones( graph_size, graph_size, CV_8UC3);

    std::vector<cv::Scalar> colors;

    colors.push_back(cv::Scalar(255,255,0));
    colors.push_back(cv::Scalar(0,0,255));
    colors.push_back(cv::Scalar(0,255,0));
    colors.push_back(cv::Scalar(255,0,0));

    img.setTo(cv::Scalar(255,255,255));

    for (int idx = 0; idx < 4; ++idx) {

        cv::Point  pt   = cv::Point( tetragon.verts[idx](0), tetragon.verts[idx](1) );

        cv::circle(img, pt, 2, cv::Scalar(20,20,20), 1);
        putText(img, std::to_string(idx), pt, cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(200,20,20), 1.0);
    }


}

void ConvexTetragonTest::create_randomPoints(Eigen::Matrix2Xd &M) {


    M.conservativeResize( Eigen::NoChange, 4);


    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(rand_range_min, rand_range_max);


    for (int idx = 0; idx < 4; ++idx) {

        M(0,idx) = dist(gen);
        M(1,idx) = dist(gen);
    }

}

Eigen::Matrix3d ConvexTetragonTest::test_randomTransform(double rad_min, double rad_max, double t_min, double t_max) {

    std::random_device                  rd;
    std::mt19937                        gen_rad(rd()),  gen_t(rd());
    std::uniform_real_distribution<>    dist_rad(rad_min, rad_max);
    std::uniform_real_distribution<>    dist_t(t_min, t_max);

    Eigen::Translation2d                t ( dist_rad(gen_t), dist_rad(gen_t) );
    Eigen::Rotation2Dd                  R ( dist_rad(gen_rad) );
    Eigen::Matrix3d                     T = Eigen::Matrix3d::Identity();


    T.block<2,2>(0,0) = R.toRotationMatrix();
    T.block<2,1>(0,2) = t.vector();

    return T;
}

Eigen::Affine2d ConvexTetragonTest::create_randomTransform() {

    return Eigen::Affine2d();
}


cv::Mat ConvexTetragonTest::draw_tetragon(ConvexTetragon tetragon, cv::Mat* img) {

    cv::Mat img__;

    if ( img == nullptr){
        img__ = cv::Mat::ones( graph_size, graph_size, CV_8UC3);
        img__.setTo(cv::Scalar(255,255,255));
    } else {
        img->copyTo(img__);
    }



    for (int idx = 0; idx < 4; ++idx) {

        cv::line( img__, cv::Point(tetragon.verts[idx](0), tetragon.verts[idx](1)), cv::Point(tetragon.verts[(idx+1)%4](0), tetragon.verts[(idx+1)%4](1)), cv::Scalar(0,0,0), 1, CV_AA );
    }

    return img__;

}

cv::Mat ConvexTetragonTest::draw_tetragons(std::vector<ConvexTetragon> tetragons, cv::Mat *img) {

    cv::Mat img__;

    if ( img == nullptr){
        img__ = cv::Mat::ones( graph_size, graph_size, CV_8UC3);
        img__.setTo(cv::Scalar(255,255,255));
    } else {
        img->copyTo(img__);
    }

    for ( auto tetra : tetragons){

        img__ = this->draw_tetragon(tetra, &img__);
    }

    return  img__;
}

void ConvexTetragonTest::show_tetragon(ConvexTetragon tetragon) {

    cv::Mat img = draw_tetragon(tetragon);

    cv::imshow("Tetragon", img);
    cv::waitKey(-1);
}

void ConvexTetragonTest::show_tetragons(std::vector<ConvexTetragon> tetragons) {

    cv::Mat img = draw_tetragons(tetragons);

    cv::imshow("Tetragon", img);
    cv::waitKey(-1);
}

void ConvexTetragonTest::test_offset(int num_times) {

    Eigen::Matrix2Xd        tetra_points;

    for (int i = 0; i < num_times; ++i) {

        create_randomPoints(tetra_points);

        ConvexTetragon tetragon(tetra_points);
        ConvexTetragon tetragon_offset = tetragon.offset(10);

        tetragon.print_vertices();
        tetragon_offset.print_vertices();
        show_tetragons({tetragon, tetragon_offset});
    }


}


#endif //SSV_VER_0_1_CONVEXTETRAGONVIZ_H
