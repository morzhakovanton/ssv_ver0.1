//
// Created by morzh on 25.09.18.
//

#ifndef SSV_VER_0_1_VIZBASE_H
#define SSV_VER_0_1_VIZBASE_H

#include <string>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>


class VizBase{

public:


    void        show_imageScaled    ( const std::string window_name, const  cv::Mat& img);
    void        show_image          ( const std::string window_name, const  cv::Mat& image);
    void        show_imageNormalized( const std::string window_name, const  cv::Mat& image);

    void        read_image          ( const  std::string& file_name, cv::Mat& img);

    void        save_imageScaled    ( const  cv::Mat& img, const std::string& file_name);
    void        save_image          ( const  std::string& file_name);


};

void VizBase::show_imageScaled(const std::string window_name, const cv::Mat &img) {

    double      min, max;
    cv::Mat     adjMap;

    cv::minMaxIdx(img, &min, &max);
    cv::convertScaleAbs(img-min, adjMap, 255 / ( max-min) );

    cv::imshow( window_name, adjMap);
    cv::waitKey(-1);

}



#endif //SSV_VER_0_1_VIZBASE_H
