//
// Created by morzh on 17.04.19.
//

#ifndef SSV_VER_0_1_SEGMENT_H
#define SSV_VER_0_1_SEGMENT_H


#include <Eigen/Dense>


struct Segment{

    Segment(Eigen::Vector2d _pt1, Eigen::Vector2d _pt2);
    Segment();


    Eigen::Vector3d         intersect       (Segment seg2);
    Eigen::Vector3d         get_homoPt1     ( );
    Eigen::Vector3d         get_homoPt2     ( );

    bool                    check_hasInside( const Eigen::Vector2d& point);
    bool                    check_hasInside( const Eigen::Vector3d& point);

    Eigen::Vector2d         pt1, pt2;
    double                  tolerance = 1e-5;
};


Segment::Segment(Eigen::Vector2d _pt1, Eigen::Vector2d _pt2) : pt1(_pt1), pt2(_pt2){}
Segment::Segment() { }

Eigen::Vector3d Segment::intersect(Segment seg2) {

    Eigen::Vector3d line1 =     get_homoPt1().cross(    get_homoPt2() );
    Eigen::Vector3d line2 = seg2.get_homoPt1().cross(seg2.get_homoPt2() );

    Eigen::Vector3d intersect_point = line1.cross(line2);

    if (  intersect_point(2) < tolerance)
        return  Eigen::Vector3d(0,0,0);

    if ( check_hasInside( intersect_point) && seg2.check_hasInside(intersect_point)) {

        intersect_point /= intersect_point(2);
        return intersect_point;
    }
    else
        return Eigen::Vector3d(0,0,0);
}

Eigen::Vector3d Segment::get_homoPt1() {

    Eigen::Vector3d  pt1H;
    pt1H << pt1, 1;

    return pt1H;
}

Eigen::Vector3d Segment::get_homoPt2() {

    Eigen::Vector3d  pt2H;
    pt2H << pt2, 1;

    return pt2H;
}

bool Segment::check_hasInside(const Eigen::Vector2d &point) {

    double  length  =   (pt1-pt2).norm();
    double  dist1   =   (point-pt1).norm();
    double  dist2   =   (point-pt2).norm();

    if ( (dist1+dist2 - length) > tolerance)

        return false;
    else
        return  true;
}

bool Segment::check_hasInside(const Eigen::Vector3d &point) {

    if ( std::abs(point(2))  < tolerance)
        return  false;

    Eigen::Vector3d  point__ = point / point(2);
    Eigen::Vector2d  pt;

    pt << point__(0), point__(1);

    return  check_hasInside(pt);
}

#endif //SSV_VER_0_1_SEGMENT_H
