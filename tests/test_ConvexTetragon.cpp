

#include "Common/ConvexTetragonTest.h"
#include "Common/ConvexTetragon.h"



int main(){


    ConvexTetragonTest      tetra_test;
    Eigen::Matrix2Xd        tetra_points;


//    std::cout << tetra_points << std::endl;

    for (int i = 0; i < 100; ++i) {

        tetra_test.create_randomPoints(tetra_points);

        ConvexTetragon tetragon(tetra_points);

//    tetra_test.test_initialization(tetragon);
//    tetra_test.draw_tetragon(tetragon);

        ConvexTetragon tetragon_offset = tetragon.offset(-10);

//    tetra_test.show_tetragon(tetragon);
        tetragon.print_vertices();
        tetragon_offset.print_vertices();
        tetra_test.show_tetragons({tetragon, tetragon_offset});
    }


/*
    Eigen::Matrix2Xd   points;

    points.conservativeResize( Eigen::NoChange, 4);

    points(0,0) = 211.602;  points(1,0) = 220.455;
    points(0,1) = 235.761;   points(1,1) = 284.789;
    points(0,2) = 205.3;  points(1,2) = 301.247;
    points(0,3) = 308.518;  points(1,3) = 327.604;

    ConvexTetragon          tetra2(points);

    tetra_test.test2( tetra2);
*/
    return 0;
}
