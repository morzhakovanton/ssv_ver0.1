//
// Created by morzh on 20.12.17.
//



#include "SatelliteImagePair/SatelliteImagePair.h"


int main(){


    SatelliteImagesPair     sat_pair;


    int         segm_num;
    float       segm_length, length = 212, overlap = 0.1, check;
    int         min = 20, max=30;

    sat_pair.calc_fitResolution( overlap, min, max, length, segm_num, segm_length );


    std::cout << "segm num: " << segm_num << " segm length: " << segm_length  << std::endl;

    check = segm_length + (1-overlap)*segm_length*(segm_num-1);

    std::cout << check << std::endl;
}