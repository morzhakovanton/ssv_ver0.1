

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>


#include "../Common/TiffWriter.h"


int main(){

    std::string     im1_filename    ( "/home/morzh/Pictures/libtiff_test_1.png" );
    std::string     im2_filename    ( "/home/morzh/Pictures/libtiff_test_2.png" );
    std::string     im_dst_filename ( "/home/morzh/Pictures/libtiff_test_multipage.tiff" );


    cv::Mat     im1_src     = cv::imread( im1_filename,  CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat     im2_src     = cv::imread( im2_filename,  CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat     im1, im2;


    if ( im1.cols != im2.cols || im1.rows != im2.rows){       std::cerr << "images are not of the same size" << std::endl;    }


    if ( im1_src.channels() == 3) {
        cv::cvtColor(im1_src, im1, CV_BGR2RGB);
        cv::cvtColor(im2_src, im2, CV_BGR2RGB);
    }else{
        im1_src.copyTo(im1);
        im2_src.copyTo(im2);
    }


    uint32 image_width = im1.cols, image_height = im1.rows;
    uint16 spp, bpp, photo, res_unit;
    TIFF *out;
    int i, j;
    uint16 page;
    uint8  NPAGES;



    int                 num_channels        =       im1.channels();
    int                 im_square           =       image_width*image_height;
    unsigned  char*     ims                 =       new unsigned char [ im_square*2*num_channels ];

    std::memcpy( ims,             im1.data, sizeof(unsigned char)*im_square*num_channels);
    std::memcpy( (ims+im_square*num_channels), im2.data, sizeof(unsigned char)*im_square*num_channels);


/*
    std::memcpy( im1.data, ims,              sizeof(unsigned char)*im_square*num_channels);
    std::memcpy( im2.data, (ims+im_square*num_channels), sizeof(unsigned char)*im_square*num_channels);
    cv::imshow("1", im1);
    cv::imshow("2", im2);
    cv::waitKey(-1);
*/


    out = TIFFOpen(im_dst_filename.c_str(), "w");

    if (!out) {       fprintf (stderr, "Can't open %s for writing\n");        return 1;     }


    spp             =   num_channels; /* Samples per pixel */
    bpp             =   8; /* Bits per sample */
    NPAGES          =   2;


    if ( num_channels == 1)
        photo           =   PHOTOMETRIC_MINISBLACK;
    else if ( num_channels == 3)
        photo           =   PHOTOMETRIC_RGB;
    else
        std::cerr << "can't write this file format" << std::endl;

    for (page = 0; page < NPAGES; page++){

        TIFFSetField(out, TIFFTAG_IMAGEWIDTH, image_width );
        TIFFSetField(out, TIFFTAG_IMAGELENGTH, image_height);
        TIFFSetField(out, TIFFTAG_BITSPERSAMPLE, bpp);
        TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL, spp);
        TIFFSetField(out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(out, TIFFTAG_PHOTOMETRIC, photo);
        TIFFSetField(out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
        TIFFSetField(out, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
        TIFFSetField(out, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        TIFFSetField(out, TIFFTAG_PAGENUMBER, page, NPAGES);

        for (j = 0; j < image_height; j++)
            TIFFWriteScanline(out, &ims[j * image_width*num_channels + im_square*num_channels*page], j, 0);

        TIFFWriteDirectory(out);
    }


    return 0;
}


