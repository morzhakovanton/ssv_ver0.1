//
// Created by morzh on 17.03.19.
//



#include <iostream>
#include <random>

#include <Eigen/Core>


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


#include "Disparity/DisparityBox.h"
//#include "../Disparity/ZMNCC.h"
#include "ENCCStereoMatching.hpp"
#include "ENCC_params.h"


int main(){

    l_img = read_img();
    r_img = read_img();


    int block_half = 7;
    int stripe_len = 31;
    int shift;

    cv::Mat             left_block, right_stripe;
    StereoBoxMatching   boxMatching;

    boxMatching.options.windowHalfSize = Eigen::Vector2i(block_half, block_half);
    boxMatching.options.search_range = stripe_len;
}