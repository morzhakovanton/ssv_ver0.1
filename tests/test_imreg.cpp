//
// Created by morzh on 03.06.18.
//



#include <Rectification/ImageRegistrationPOC.h>


int main(){


    ImageRegistrationPOC   imreg;
    cv::Mat             fixed, moved;
    Eigen::Vector4f     xform_sim2_, xform_sim2, xform_scalesT2;
    cv::Mat T;

    fixed = cv::imread("/home/morzh/CLionProjects/ssv_ver0.1/face01.png", CV_LOAD_IMAGE_GRAYSCALE);
    fixed.convertTo(fixed, CV_32FC1);
//    cv::imshow("fixed image", fixed);
//    cv::waitKey(-1);


    /**======================== scale - rotation test =================================*/
/*
    float angle = 15;
    float scale = 1.2;
    T = cv::getRotationMatrix2D( cv::Point2f(0,0), angle, scale );

    std::cout << T << std::endl;

    cv::warpAffine( fixed, moved, T, cv::Size( fixed.cols, fixed.rows) );
//    cv::imshow("moved image", moved);
//    cv::waitKey(-1);


    xform_sim2 = imreg.estimate_SIM2_old( fixed, moved);
    xform_sim2_ = imreg.estimate_SIM2( fixed, moved);

    std::cout << xform_sim2 << std::endl;
    std::cout << xform_sim2_ << std::endl;
*/

    /**================================ non prop scale test =====================================*/

    float scale_x = 0.85;   float scale_y = 1.2;

    T = cv::Mat::eye(2,3,CV_64FC1);

    T.at<double>(0,0) = scale_x;    T.at<double>(1,1) = scale_y;

    std::cout << T << std::endl;

    cv::warpAffine( fixed, moved, T, cv::Size( fixed.cols, fixed.rows) );

    xform_scalesT2 = imreg.estimate_nonPropScaleT2(fixed, moved, 0, nullptr);
    std::cout << xform_scalesT2 << std::endl;
}