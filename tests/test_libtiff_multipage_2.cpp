//
// Created by morzh on 28.02.18.
//

int numberOfPages = 1000;
int width = 256;
int height = 256;

string fileName = "test_initialization.tif";

ushort[] image = new ushort[width * height];
byte[] buffer = new byte[width * height * sizeof(ushort)];


using (Tiff output = Tiff.Open(fileName, "w"))
{
if (output == null)
{
return;
}
stopWatch.Start();
for (int i = 0; i < numberOfPages; i++)
{
Buffer.BlockCopy(image, 0, buffer, 0, buffer.Length);

output.SetField(TiffTag.IMAGEWIDTH, width);
output.SetField(TiffTag.IMAGELENGTH, height);
output.SetField(TiffTag.SAMPLESPERPIXEL, 1);
output.SetField(TiffTag.BITSPERSAMPLE, 16);
output.SetField(TiffTag.ORIENTATION, Orientation.TOPLEFT);
output.SetField(TiffTag.XRESOLUTION, 96);
output.SetField(TiffTag.YRESOLUTION, 96);
output.SetField(TiffTag.PLANARCONFIG, PlanarConfig.CONTIG);
output.SetField(TiffTag.PHOTOMETRIC, Photometric.MINISBLACK);
output.SetField(TiffTag.COMPRESSION, Compression.NONE);
output.SetField(TiffTag.FILLORDER, FillOrder.MSB2LSB);
output.SetField(TiffTag.SUBFILETYPE, FileType.PAGE);
output.SetField(TiffTag.PAGENUMBER, i + 1, numberOfPages);

output.WriteEncodedStrip(0, buffer, buffer.Length);

output.WriteDirectory();