//
// Created by morzh on 22.02.19.
//

#include <iostream>
#include <random>

#include <Eigen/Core>


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


#include "Disparity/DisparityBox.h"
//#include "../Disparity/MeasureZMNCC.h"
#include "ENCCStereoMatching.hpp"
#include "ENCC_params.h"


int fill_image_cols(cv::Mat& img,  int col_width, bool show_image) {


    int rows = img.rows;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float> dist(0, rows-1);

    int shift = (int)std::round(dist(mt));

    cv::Rect2i black_cols(shift,0,col_width, rows);

    img.setTo(1.0);
    img(black_cols).setTo(0.0);


    if (show_image) {

        cv::imshow("columns", img);
        cv::waitKey(-1);
    }

    return shift;


}

void fill_image_rand(cv::Mat& img, int rows, int cols){

    double low = 0.0;
    double high = 1.0;

    img = cv::Mat::zeros(rows, cols, CV_32FC1);

    cv::randu( img, cv::Scalar(low), cv::Scalar(high));
}


cv::Mat translateImg(cv::Mat &img, float offsetx, float offsety);

int test_disparity_1();
int test_disparity_2();
int test_disparity_3();


int main(){

    return test_disparity_1();
//    return test_disparity_2();
}


int test_disparity_1(){

    int block_half = 7;
    int stripe_len = 31;

    cv::Mat             left_block, right_stripe, right_stripe_subpix;
    DisparityBox   boxMatching;

    boxMatching.options.windowHalfSize = Eigen::Vector2i(block_half, block_half);
    boxMatching.options.search_range = stripe_len;


    int shift_low       =   0;
    int shift_high      =   stripe_len - (2*block_half+1)-1;
    int num_0_1         =   0;
    int num_0_25        =   0;
    int num_0_50        =   0;
    int num_of_tests    =   1000;


    for (int i=0; i<num_of_tests; i++){

        cv::Mat ddd;

        fill_image_rand(right_stripe, 2*block_half+1, stripe_len);

        cv::resize(right_stripe, ddd, cv::Size(), 8, 8);
        cv::blur(ddd, right_stripe, cv::Size(2,2));
        cv::resize(right_stripe, right_stripe, cv::Size(), 0.5, 0.5);

        right_stripe(cv::Rect2i(0,0, stripe_len, 2*block_half+1)).copyTo(ddd);

        right_stripe = ddd;

//        cv::imshow("right stripe", ddd);
//        cv::imshow("right stripe", right_stripe);
//        cv::waitKey(-1);

        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_real_distribution<float> dist(shift_low, shift_high+0.3);

        int     shift           = (int)std::round(dist(mt));
        float   shift_subpix    = 0.45;

        right_stripe_subpix = translateImg(right_stripe, -shift_subpix, 0);
        right_stripe_subpix(  cv::Rect2i( shift, 0,  2*block_half+1, 2*block_half+1) ).copyTo(left_block);

        int     d               = boxMatching.find_disparityInt<MeasureZMNCC>(left_block, right_stripe);
        float   d_subpx         = boxMatching.refine_subpixelAccuracy<MeasureZMNCC>(left_block, right_stripe, d);

//        std::cout << shift+shift_subpix << " " << d_subpx << std::endl;

        if      ( std::abs(shift+shift_subpix-d_subpx) < 0.1 ){      /*std::cout << "ref2target success value is: " << d << std::endl; */ num_0_1++;   }
        else if ( std::abs(shift+shift_subpix-d_subpx) < 0.25){      /*std::cout << "ref2target success value is: " << d << std::endl; */ num_0_25++;  }
        else if ( std::abs(shift+shift_subpix-d_subpx) < 0.51){      /*std::cout << "ref2target success value is: " << d << std::endl; */ num_0_50++;  }
    }


    std::cout << num_0_1  << " successful tests  with 0.10 out of " << num_of_tests << std::endl;
    std::cout << num_0_25 << " successful tests  with 0.25 out of " << num_of_tests << std::endl;
    std::cout << num_0_50 << " successful tests  with 0.51 out of " << num_of_tests << std::endl;


    return  0;
}

int test_disparity_2() {

    int block_half = 7;
    int stripe_len = 31;
    int shift;

    cv::Mat             left_block, right_stripe;
    DisparityBox   boxMatching;

    boxMatching.options.windowHalfSize = Eigen::Vector2i(block_half, block_half);
    boxMatching.options.search_range = stripe_len;

    right_stripe = cv::Mat::zeros(2*block_half+1, stripe_len, CV_32FC1);

    shift =  fill_image_cols(right_stripe,  4, false);

    right_stripe(  cv::Rect2i( shift, 0,  2*block_half+1, 2*block_half+1) ).copyTo(left_block);
/*
    ENCCStereoMatching boxMatching_old( Eigen::Vector2i(block_half, block_half)  );
    int     d_old       =   boxMatching_old.findL2RCorrespondence_test(left_block, right_stripe);
    float   d_subpx_old =   boxMatching_old.calc_ENCCSubPx_test(boxMatching_old.correspData_.rBoxes);
*/

    int     d           =   boxMatching.find_disparityInt<MeasureZMNCC>(left_block, right_stripe);
    float   d_subpx     =   boxMatching.refine_subpixelAccuracy<MeasureZMNCC>(left_block, right_stripe, d);

    std::cout << shift << " " << d_subpx << std::endl;

    return 0;
}

int test_disparity_3() {


    return 0;
}


cv::Mat translateImg(cv::Mat &img, float offsetx, float offsety){

    cv::Mat img_out;

    cv::Mat trans_mat = (cv::Mat_<double>(2,3) << 1, 0, offsetx, 0, 1, offsety);

    warpAffine(img,img_out,trans_mat,img.size());

    return img_out;
}