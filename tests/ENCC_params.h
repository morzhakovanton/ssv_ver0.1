//
// Created by morzh on 3/24/17.
//

#ifndef SATELLITESTEREOVISION_ENCC_PARAMS_H
#define SATELLITESTEREOVISION_ENCC_PARAMS_H


class ENCC_params{

public:

    ENCC_params(float eps, float min_clr, bool c_check);
    ENCC_params(float eps, float min_clr, bool c_check, Eigen::Vector2i wndwSz);

    enum { SINGLE_LEVEL=0, PYRAMID=1 };

    float               epsilon                 =   0.85;
    float               min_clearness           =   0.05;
    bool                consistency_check       =   false;
    Eigen::Vector2i     windowSize              =   Eigen::Vector2i(3,3);

};


ENCC_params::ENCC_params(float eps, float min_clr, bool c_check) : epsilon(eps), min_clearness(min_clr), consistency_check(c_check) { }


ENCC_params::ENCC_params(float eps, float min_clr, bool c_check, Eigen::Vector2i wndwSz) : epsilon(eps), min_clearness(min_clr), consistency_check(c_check), windowSize(wndwSz) { }



#endif //SATELLITESTEREOVISION_ENCC_PARAMS_H
