///////////////////////////////////////////////////////////////////////////////////////////////////////
///
///
///    An Enhanced Correlation-Based Method for Stereo Correspondence  with Sub-Pixel Accuracy
///
///
/////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef SOP_PCFROMSTEREOPAIR_ENCC_HPP
#define SOP_PCFROMSTEREOPAIR_ENCC_HPP


#include <fstream>
#include <iostream>
#include <cassert>
#include <math.h>
#include <numeric>


#include <Eigen/Core>
#include <Eigen/Dense>
#include <eigen3/unsupported/Eigen/FFT>
#include "Frame_old.h"

#include "ENCC_params.h"


//#define OUTPUTPERPOINT
//#define OUTPUT



class ENCCStereoMatching{

public:

    ENCCStereoMatching( const Eigen::Vector2i& halfWindowSize ) :
            halfWindowVec_(halfWindowSize), subpxError(true), statsSuccess(-1), statsMinClearness(0), statsEpsilon(1), statsConsistencyCheck(2), statsEpipolarCheck(3), statsOutOfBounds(4)
    {

        halfWindowWidth_ = halfWindowVec_(0);
        halfWindowHeight_ = halfWindowVec_(1);
    }
    ~ENCCStereoMatching(){}



    struct correspodenceStats{

        int 				totalNumOfPoints;
        int 				numMeanClearness;
        int 				numEpsilon;
        int 				numConsistency;
        int 				numOutOfBounds;

        float 				percentClearness;
        float  				percentEpsilon;
        float				percentConsistency;
        float				percentOutOfBounds;


        void				clearStatistics(){
            totalNumOfPoints=0; numMeanClearness=0; numEpsilon=0; numConsistency=0; numOutOfBounds=0;
            percentClearness=0; percentEpsilon=0; percentConsistency=0;percentOutOfBounds=0;
        }

        void				calcPercents(){
            percentClearness 	= (float)numMeanClearness/ totalNumOfPoints;
            percentEpsilon		= (float)numEpsilon/totalNumOfPoints;
            percentConsistency 	= (float)numConsistency/totalNumOfPoints;
            percentOutOfBounds 	= (float)numOutOfBounds/totalNumOfPoints;
        }
        void				printNumbers();
        void				printPercents(){ std::cout <<"clearness: " << percentClearness << "; epsilon: " << percentEpsilon << "; consistency: " <<percentConsistency << std::endl; }

    }statsCorresp_;



    struct  EpipolarData{

        Eigen::Vector2f epilineDirL, epilineDirR, epilineDirNormalL, epilineDirNormalR;
        Eigen::Vector2f	ptLeft,  ptRight_min,  	ptRight_max;
        float 			episegmentR_norm;
        bool            epipolesAreAtInfinity;
        int             epiLineRLength;

        friend std::ostream& operator<<( std::ostream& os, const EpipolarData& ed){

//            Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");

            os << "point left: "                << "[ "  << ed.ptLeft.transpose()            << " ]" <<
                  "; point right max: "         << "[ "  << ed.ptRight_max.transpose()       << " ]" <<
                  "; point right min: "         << "[ "  << ed.ptRight_min.transpose()       << " ]" <<  "\n";

            os << "epiline dir left: "          << "[ "  << ed.epilineDirL.transpose()       << " ]" <<
                  "; epiline dir right: "       << "[ "  << ed.epilineDirR.transpose()       << " ]" <<
                  "; epiline left normal: "     << "[ "  << ed.epilineDirNormalL.transpose() << " ]" <<
                  "; epiline right normal: "    << "[ "  << ed.epilineDirNormalR.transpose() << " ]" <<
                  "; norm of right episegment: "<<          ed.episegmentR_norm              << "\n";
        }

    }epiData_;



    struct DisparityData{

        float           lambda, r;
        int             L2R, R2L;
        float           L2R_subpx;
        int             num_of_levels;

        float           tau0_nom,   tau0_denom;
        float           rho_d0_,    rho_d0m1_,    rho_d0p1_;

        bool            interpNotPossible;

        int             numOfPtsWithPositiveDenominator;
        int             totalNumOfPts;
        int             numOfQuadricInterp;

        int 			halfLeftStripeSize;
        float 			epsilon, minClearness;


        float           test_rBoxes_L2R_idx, test_rBoxes_L2Rp1_idx,     test_rBoxes_L2R_val, test_rBoxes_L2Rp1_val;

        int             min_disparity, max_disparity;
        bool            use_init_value;


        void            printDataMembers(){

            std::cout << "ref2target value L2R: "<< L2R <<  "; ref2target value R2L:  " <<  R2L << "; ref2target subpixel value: " << L2R_subpx << "\n";
            std::cout << "interpolation NOT possible; " << interpNotPossible << "; \n sub-pixel data: "  << "; lambda: " << lambda << "; r: " << r <<
                         "; tau_0 nominator: " << tau0_nom << "; tau_0 denominator: " << tau0_denom << "ж subpix value is: " << L2R_subpx <<  "\n" <<
                         "; rBoxes L2R value: " << test_rBoxes_L2R_val << "; rBoxes L2R + 1 value: " << test_rBoxes_L2Rp1_val << "\n";
        }



        friend std::ostream& operator<<( std::ostream& os, const DisparityData& d){

            os << "ref2target value L2R: "<< d.L2R <<  "; ref2target value R2L:  " <<  d.R2L << "; ref2target subpixel value: " << d.L2R_subpx << "\n";
            os << "sub-pixel data: "  << " lambda: " << d.lambda /*<< " ; tau_0: " << d.tau0 */ << "; tau_0 nominator: " << d.tau0_nom << "; tau_0 denominator: " << d.tau0_denom << "\n";
        }


        float imL_shift, imL_scale;
        float imR_shift, imR_scale;

        cv::Mat   init_disparity;

    } d_;


    std::vector<int> 	integerDisparity_;
    std::vector<int> 	floatDisparity_;

    const int			statsSuccess, statsMinClearness, statsEpsilon, statsConsistencyCheck, statsEpipolarCheck, statsOutOfBounds ;


    struct CorrespondenceData{

        cv::Mat 							                        lPatch, rPatch, rightStripe, leftStripe;
        std::vector<std::pair<int, float>> 	                        lrPatchesDiffs, rlPatchesDiffs  ;
        std::vector<float>                                          rNormFactors    ;
        std::vector< std::pair< std::vector< float>, float> >       rBoxes;
        int                                                         currentPtIdx;


        void print_rBoxes(){  for (int j = 0; j < rBoxes.size(); ++j) {  std::cout << rBoxes[j].second << " ";  }        }


    }correspData_;



    /*
    struct disparityData{

        int 			 	L2R;
        float 			 	L2R_subpx;
        int 			 	R2L;
        int 			 	halfLeftStripeSize;
        int                 minClearness;
        std::vector<float> 	valsForInterp;
        bool 				interpNotPossible;
        int 				epsilon;

    }d_;
*/

    const bool          subpxError;
    Eigen::Vector2i     halfWindowVec_;
    int                 halfWindowWidth_;
    int                 halfWindowHeight_;

    void                do_stereoMatching                   ( Frame_old &imL, const Frame_old &imR, int lvl, const ENCC_params &params, bool use_init_value);
    void                do_stereoMatchingCorners            ( Frame_old &imL, const Frame_old &imR, int lvl, const ENCC_params &params, bool use_init_value);
    void                do_stereoMatchingPyramid            ( Frame_old &imL, const Frame_old &imR, int num_lvls, const std::vector<ENCC_params> &params);
    void                do_stereoMatchingCornersPyramid     ( Frame_old &imL, const Frame_old &imR, int num_lvls, const std::vector<ENCC_params> &params);

    void                calc_r                              ( std::vector< std::pair< std::vector< float>, float> >& );
    void                calc_lambda                         ( std::vector< std::pair< std::vector< float>, float> >& );


    void                calc_tau_nom_denom                  ( std::vector< std::pair< std::vector< float>, float> >& );
    void                resetArrays                         (   Eigen::Matrix2Xf &rPoints, int size         );
    cv::Mat             getLeftStripe                       (   const cv::Mat &imL                          );
    cv::Mat 	        getRightStripe                      (   const cv::Mat &imR                          );
    void                zeroMeanNormalize                   (   cv::Mat &patch                              );
    void                zeroMeanNormalizeNormFactor         (   cv::Mat &patch, float &normFactor           );
    void		        findL2RCorrespondence  		        (   cv::Mat& leftStripe, cv::Mat& rightStripe   );
    float findL2RCorrespondence_test(cv::Mat &leftBlock, cv::Mat &rightStripe);
    void		        consistencyCheck   			        (   cv::Mat& leftStripe, cv::Mat& rightStripe   );
    void 		        collectStats				        (   int     );
    std::vector<float>  patchToStdVec                       ( cv::Mat& patch    );

    template<typename T>
    bool 	            is_valid				            (   const T &value ){	return ! std::isinf(value) && ! std::isnan(value);	}

    float               calc_DotProduct                     ( const std::vector<float> &v1, const std::vector<float> &v2);
    float               calc_dotProduct                     ( const cv::Mat &patch1, const cv::Mat &patch2);
    void                calc_ENCCSubPx                      ( std::vector<std::pair<std::vector<float>, float> > &rBoxes);
    float calc_ENCCSubPx_test(std::vector<std::pair<std::vector<float>, float> > &rBoxes);
    void                calc_QuadricSubpx                   ( );
    void                calc_shiftAndScaleForImages         ( const Frame_old &L, const Frame_old &R, int lvl);
    void 		        calc_LeftVector                     ( std::vector<float> &lSample, EpipolarData &epiData, const Frame_old &imL);
    void 		        calc_RightVector                    ( std::pair<std::vector<float>, float> &rBox, EpipolarData &epiData, const cv::Mat &imR, int idx2);
    bool 		        checkEpipolarData			        ( const Frame_old &imL, EpipolarData& data);
    bool 		        checkSizes					        ( const cv::Mat& left,const cv::Mat&  right );
    float 		        getGraySubpixFloat                  ( const cv::Mat &img, cv::Point2f pt);
    uchar               getGraySubpixUchar                  ( const cv::Mat &img, cv::Point2f pt );

    bool                consisitencyCheck                   ( float epsilon );
    void                prepareForSubpxInterpolation        ( std::vector<std::pair<int, float>> &blockDiffs_);

    void                fitImageTo8U                        ( cv::Mat &im);
    void                show_imageScaled                    ( std::string imName, cv::Mat im);
    void                show_singleMatchWholeImage          ( const Frame_old &imL, const Frame_old &imR, int lvl);
    void                show_singleCornerMatchWholeImage    ( const Frame_old &imL, const Frame_old &imR, int lvl);
    void                show_leftStripe                     ( const cv::Mat &imL, int halfEpilineLength, int rect_x, int rect_y, int rect_w, int rect_h, bool showWholeImage, bool showStripe);
    void                show_rightStripe                    ( const cv::Mat &imR, int rect_x, int rect_y, int rect_w, int rect_h, bool showWholeImage, bool showStripe);
    void                show_mappablePointsShift            ( const Frame_old &lFrame, const Frame_old &rFrame, int lvl, bool write2disk, const std::string &file_path, const std::string &file_name);
    void                show_mappablePointsShiftLinearInterp(const Frame_old &lFrame, const Frame_old &rFrame, int lvl, bool write2disk, const std::string &file_path, const std::string &file_name, bool use_mask_as_transparency);
    void                propagate_dsiparityToPrevLvl(int lvl, Frame_old &imL);
    void                propagate_cornerDsiparityToPrevLvl(int lvl, Frame_old &imL);

    void                show_singleMatchOnStripes           ( );

    cv::Mat             disparityMap;

    void write_cornersCorrepondences(const Frame_old &lFrame, const Frame_old &rFrame, int lvl, const std::string &path, const std::string &file_name);
};


//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void ENCCStereoMatching::resetArrays(Eigen::Matrix2Xf &rPoints, int size) {

    if ( rPoints.cols()  != 0){  rPoints.resize(2, 0); }		rPoints.conservativeResize( Eigen::NoChange, size );

    integerDisparity_.clear();	floatDisparity_.clear();

}


void  ENCCStereoMatching::zeroMeanNormalize(cv::Mat &patch){

    cv::Scalar meanVal = mean( patch );
    float mean = meanVal.val[0];

    cv::Mat lPatchMean = cv::Mat(patch.rows, patch.cols, CV_32FC1);
    lPatchMean.setTo(meanVal);

    patch -= lPatchMean;

    float normFactor = cv::norm(patch);

    if (normFactor  < 1e-6 ){ patch.setTo(0); return;  }

    patch /= normFactor;
}

void ENCCStereoMatching::zeroMeanNormalizeNormFactor(cv::Mat &patch, float &normFactor){

    cv::Scalar  meanVal     = mean( patch );
    float       mean        = meanVal.val[0];
    cv::Mat     lPatchMean  = cv::Mat(patch.rows, patch.cols, CV_32FC1);

    lPatchMean.setTo(meanVal);
    patch -= lPatchMean;
    normFactor = cv::norm(patch);

    if (normFactor  < 1e-6 ){ patch.setTo(0); return;  }

    patch /= normFactor;
}



void  ENCCStereoMatching::findL2RCorrespondence( cv::Mat& leftStripe, cv::Mat& rightStripe){


    correspData_.lrPatchesDiffs.resize  (  rightStripe.cols-( 2*halfWindowWidth_ )   );
    correspData_.rNormFactors.resize    (  rightStripe.cols-( 2*halfWindowWidth_ )   );
    correspData_.rBoxes.resize          (  rightStripe.cols-( 2*halfWindowWidth_ )   );

    float rNormFactor;

    leftStripe( cv::Rect(2, 0, 2*halfWindowWidth_+1, 2*halfWindowHeight_+1) ).copyTo(correspData_.lPatch);

    zeroMeanNormalize(correspData_.lPatch);
//    if (correspData_.currentPtIdx == 125) std::cout << correspData_.lPatch << std::endl;


    for (int idx2 = 0; idx2 < epiData_.epiLineRLength /*rightStripe.cols-( 2*halfWindowWidth_ )*/; ++idx2) {


        rightStripe( cv::Rect( idx2, 0, 2*halfWindowWidth_+1, 2*halfWindowHeight_+1) ).copyTo(correspData_.rPatch);

        zeroMeanNormalizeNormFactor(correspData_.rPatch, rNormFactor);

        correspData_.lrPatchesDiffs [idx2]  = std::make_pair( idx2, calc_dotProduct(correspData_.lPatch, correspData_.rPatch) );
        correspData_.rBoxes[idx2]           = std::make_pair( patchToStdVec( correspData_.rPatch ), rNormFactor) ;
    }



    std::sort(correspData_.lrPatchesDiffs.begin(), correspData_.lrPatchesDiffs.end(),  [](const std::pair<int, float> &a, const std::pair<int, float> &b) { return a.second > b.second;  });

    if ( std::abs(correspData_.lrPatchesDiffs[0].second - correspData_.lrPatchesDiffs[1].second ) < d_.minClearness ) 	 throw statsMinClearness;
    if ( correspData_.lrPatchesDiffs[0].second  <  d_.epsilon ) 								                         throw statsEpsilon;

    d_.L2R 		= correspData_.lrPatchesDiffs[0].first;

    prepareForSubpxInterpolation(correspData_.lrPatchesDiffs);

}



float ENCCStereoMatching::findL2RCorrespondence_test(cv::Mat &leftBlock, cv::Mat &rightStripe){


    correspData_.lrPatchesDiffs.resize  (  rightStripe.cols-( 2*halfWindowWidth_ )   );
    correspData_.rNormFactors.resize    (  rightStripe.cols-( 2*halfWindowWidth_ )   );
    correspData_.rBoxes.resize          (  rightStripe.cols-( 2*halfWindowWidth_ )   );

    float rNormFactor;

    leftBlock.copyTo(correspData_.lPatch);

    zeroMeanNormalize(correspData_.lPatch);
//    if (correspData_.currentPtIdx == 125) std::cout << correspData_.lPatch << std::endl;

    epiData_.epiLineRLength =  rightStripe.cols - (2*halfWindowWidth_);


    for (int idx2 = 0; idx2 < epiData_.epiLineRLength /*rightStripe.cols-( 2*halfWindowWidth_ )*/; ++idx2) {


        rightStripe( cv::Rect( idx2, 0, 2*halfWindowWidth_+1, 2*halfWindowHeight_+1) ).copyTo(correspData_.rPatch);

        zeroMeanNormalizeNormFactor(correspData_.rPatch, rNormFactor);

        correspData_.lrPatchesDiffs [idx2]  = std::make_pair( idx2, calc_dotProduct(correspData_.lPatch, correspData_.rPatch) );
        correspData_.rBoxes[idx2]           = std::make_pair( patchToStdVec( correspData_.rPatch ), rNormFactor) ;
    }



    std::sort(correspData_.lrPatchesDiffs.begin(), correspData_.lrPatchesDiffs.end(),  [](const std::pair<int, float> &a, const std::pair<int, float> &b) { return a.second > b.second;  });

    if ( std::abs(correspData_.lrPatchesDiffs[0].second - correspData_.lrPatchesDiffs[1].second ) < d_.minClearness ) 	 throw statsMinClearness;
    if ( correspData_.lrPatchesDiffs[0].second  <  d_.epsilon ) 								                         throw statsEpsilon;

    d_.L2R 		= correspData_.lrPatchesDiffs[0].first;

    prepareForSubpxInterpolation(correspData_.lrPatchesDiffs);


    return d_.L2R;
}



void  ENCCStereoMatching::consistencyCheck ( cv::Mat& leftStripe, cv::Mat& rightStripe){

    cv::Mat                                 lPatch, rPatch;
    std::vector<std::pair<int, float>> 	    rlPatchesDiffs( leftStripe.cols - ( 2*halfWindowWidth_ ) );

    rightStripe( cv::Rect( d_.L2R, 0, 2*halfWindowWidth_+1, 2*halfWindowHeight_+1) ).copyTo(rPatch);
    zeroMeanNormalize(rPatch);


    for (int idx = 0; idx < leftStripe.cols- 2*halfWindowWidth_ ; ++idx) {

        leftStripe( cv::Rect( idx, 0, 2*halfWindowWidth_+1, 2*halfWindowHeight_+1) ).copyTo(lPatch);
        zeroMeanNormalize(lPatch);

        rlPatchesDiffs[idx]  = std::make_pair( idx, calc_dotProduct(lPatch, rPatch) );
    }


    std::sort(rlPatchesDiffs.begin(), rlPatchesDiffs.end(),  [](const std::pair<int, float> &a, const std::pair<int, float> &b) { return a.second > b.second;  });

    d_.R2L 		= rlPatchesDiffs[0].first;

    if ( d_.R2L != 2) throw statsConsistencyCheck /*somethingWrong*/;
}


cv::Mat	 ENCCStereoMatching::getRightStripe(const cv::Mat &imR){

    cv::Mat     rImWindow 		        = cv::Mat::zeros( 2*halfWindowHeight_+1,  d_.max_disparity  - d_.min_disparity + 2*halfWindowWidth_ +1, CV_32FC1 );
    float       init_d_val              = 0;

    int         im_w                    = imR.cols;
    int         im_h                    = imR.rows;

    if ( d_.use_init_value )
        init_d_val = d_.init_disparity.at<float>( epiData_.ptLeft(1),epiData_.ptLeft(0) );


    int         rect_x                  = epiData_.ptLeft(0) + d_.min_disparity + init_d_val - halfWindowWidth_;
    int         rect_y                  = epiData_.ptLeft(1) - halfWindowHeight_;
    int         rect_w                  = (d_.max_disparity  - d_.min_disparity) + 2*halfWindowWidth_;
    int         rect_h                  = 2*halfWindowHeight_+1 ;


    if ( (rect_x <0) || (rect_y < 0) || ( rect_x + rect_w > im_w-1 ) ||  ( rect_y + rect_h > im_h-1 )  ) return rImWindow;


    imR( cv::Rect( rect_x, rect_y, rect_w, rect_h)).copyTo(rImWindow);
//    if ( /*correspData_.currentPtIdx == 1025 */ true) {  show_rightStripe(imR, rect_x, rect_y, rect_w, rect_h, true, false);   cv::waitKey(-1);   }
    return rImWindow;
}

void ENCCStereoMatching::show_rightStripe(const cv::Mat &imR, int rect_x, int rect_y, int rect_w, int rect_h, bool showWholeImage, bool showStripe) {

    cv::Mat     im_copy;
    cv::Mat 	rImWindow_2 = cv::Mat::zeros(2 * halfWindowHeight_ + 1, std::abs(d_.max_disparity - d_.min_disparity) + 2 * halfWindowWidth_ + 1, CV_32FC1 );

    imR.copyTo(im_copy);
    fitImageTo8U(im_copy);

    rectangle(im_copy, cv::Point(rect_x, rect_y), cv::Point(rect_x + rect_w - 1, rect_y + rect_h - 1), cv::Scalar(255, 0.2, 0.2)  );
    im_copy.at<unsigned char > ( epiData_.ptLeft(1), epiData_.ptLeft(0) ) = 255;
    im_copy( cv::Rect(rect_x, rect_y, rect_w, rect_h) ).copyTo(rImWindow_2 );

    if ( showWholeImage)    imshow  ( "Right Image", im_copy    );
    if ( showStripe )       imshow  ( "Right Stripe", rImWindow_2 );
}


cv::Mat  ENCCStereoMatching::getLeftStripe(const cv::Mat &imL){

    int         halfEpilineLength        = 2;
    cv::Mat 	lImWindow 		         = cv::Mat::zeros( 2*halfWindowHeight_+1, 2*(halfEpilineLength + halfWindowWidth_) +1, CV_32FC1 );

    int         im_w                     = imL.cols;
    int         im_h                     = imL.rows;

    int         rect_x                   = epiData_.ptLeft(0) - halfWindowWidth_ - halfEpilineLength;
    int         rect_y                   = epiData_.ptLeft(1) - halfWindowHeight_;
    int         rect_w                   = 2*(halfEpilineLength + halfWindowWidth_) +1;
    int         rect_h                   = 2*halfWindowHeight_+1 ;

    if ( (rect_x <0) || (rect_y < 0) || ( rect_x + rect_w > im_w-1 ) ||  ( rect_y + rect_h > im_h-1 )  ) return lImWindow;

    imL( cv::Rect( rect_x, rect_y, rect_w, rect_h) ).copyTo( lImWindow );


//    if ( correspData_.currentPtIdx == 1025 ) {

//    show_leftStripe(imL, halfEpilineLength, rect_x, rect_y, rect_w, rect_h, true, false);

//        cv::waitKey(-1);
//    }

    return lImWindow;
}

void ENCCStereoMatching::show_leftStripe(const cv::Mat &imL, int halfEpilineLength, int rect_x, int rect_y, int rect_w, int rect_h, bool showWholeImage, bool showStripe) {
    cv::Mat     im_copy;
    cv::Mat 	lImWindow_2 = cv::Mat::zeros(2 * halfWindowHeight_ + 1, 2 * (halfEpilineLength + halfWindowWidth_) + 1, CV_32FC1 );

    imL.copyTo(im_copy);
    fitImageTo8U(im_copy);

    rectangle(im_copy, cv::Point(rect_x, rect_y), cv::Point(rect_x + rect_w - 1, rect_y + rect_h - 1), cv::Scalar(255, 0.2, 0.2)  );
    im_copy.at<unsigned char > ( epiData_.ptLeft(1), epiData_.ptLeft(0) ) = 255;
    im_copy( cv::Rect(rect_x, rect_y, rect_w, rect_h) ).copyTo(lImWindow_2 );

    if (showWholeImage) imshow  ( "Left Image", im_copy    );
    if (showStripe)     imshow  ( "Left Stripe", lImWindow_2 );
}


void ENCCStereoMatching::collectStats( int type ){


    if (type == 0) statsCorresp_.numMeanClearness++;
    if (type == 1) statsCorresp_.numEpsilon++;
    if (type == 2) statsCorresp_.numConsistency++;
}


std::vector<float> ENCCStereoMatching::patchToStdVec ( cv::Mat& patch){

    std::vector<float> vec;

    if ( patch.rows == 0 ||  patch.cols == 0 ) return vec;

    for (int i = 0; i < patch.rows; ++i) {
        for (int j = 0; j < patch.cols; ++j)
            vec.push_back(patch.at<float>(i, j));
    }
}


void ENCCStereoMatching::do_stereoMatchingPyramid(Frame_old &imL, const Frame_old &imR, int num_lvls, const std::vector<ENCC_params> &params) {

    int debug__lvl_min = 0;

    d_.num_of_levels = num_lvls;

    for (int lvl = num_lvls-1; lvl >= debug__lvl_min ; --lvl) {

        if ( lvl == num_lvls-1) {
            do_stereoMatching(imL, imR, lvl, params[lvl], false);
            imL.fill_disparitiesHoles(lvl);
        }
        else {
            do_stereoMatching(imL, imR, lvl, params[lvl], true);
            imL.medianFilterDisparityMap_std(lvl);
        }

        propagate_dsiparityToPrevLvl(lvl, imL);

//        show_imageScaled("Disparity", imL.disparityPyramid[ std::abs(lvl-1) ]);
//        cv::waitKey(-1);

        std::string path        =  "/home/morzh/ClionProjects/satellitestereovision/OUT";
        std::string file_name   =  "match_lvl_"         + std::to_string( lvl );
        std::string file_name_2 =  "match_lvlm1_prop_"  + std::to_string( lvl );

        bool use_transp = true;
        if ( lvl != num_lvls-1 ) { use_transp = false;}

        show_mappablePointsShiftLinearInterp(imL, imR, lvl,               true, path, file_name,   use_transp);
        show_mappablePointsShiftLinearInterp(imL, imR, std::abs(lvl - 1), true, path, file_name_2, false);

    }
}




void ENCCStereoMatching::do_stereoMatchingCornersPyramid( Frame_old &imL, const Frame_old &imR, int num_lvls, const std::vector<ENCC_params> &params) {

    int debug__lvl_min = 0;

    d_.num_of_levels = num_lvls;

    for (int lvl = num_lvls-1; lvl >= debug__lvl_min ; --lvl) {

        imL.show_propagatedCornersPyramid(3);

        if ( lvl == num_lvls-1) {
            do_stereoMatchingCorners         ( imL, imR, lvl, params[lvl], false);
        }
        else {
            do_stereoMatchingCorners         ( imL, imR, lvl, params[lvl], true);
        }

        propagate_cornerDsiparityToPrevLvl(lvl, imL);
        imL.show_propagatedCornersPyramid(3);


/*
        std::string path        =  "/home/morzh/ClionProjects/satellitestereovision/OUT";
        std::string file_name   =  "cornet_match_lvl_"         + std::to_string( lvl );

        write_cornersCorrepondences(imL, imR, lvl, path, file_name);
*/
/*
        bool use_transp = true;
        if ( lvl != num_lvls-1 ) { use_transp = false;}

        show_mappablePointsShiftLinearInterp(imL, imR, lvl,               true, path, file_name,   use_transp);
        show_mappablePointsShiftLinearInterp(imL, imR, std::abs(lvl - 1), true, path, file_name_2, false);
*/
    }
}



void ENCCStereoMatching::do_stereoMatching(Frame_old &imL, const Frame_old &imR, int lvl, const ENCC_params &params, bool use_init_value)
{

    d_.minClearness 	= params.min_clearness;
    d_.epsilon 		    = params.epsilon;

    statsCorresp_.clearStatistics();
    statsCorresp_.totalNumOfPoints = imL.numOfMappablePointsPyramid[lvl];
    correspData_.currentPtIdx = 0;



    /**----------------------------------------------------------------------------------- assign data from left image imL ---------*/

    if ( !use_init_value ) {

        d_.min_disparity        =   imL.minMaxDisparities[lvl].first;
        d_.max_disparity        =   imL.minMaxDisparities[lvl].second;
        d_.use_init_value       =   false;
    }

    if ( use_init_value ){

        d_.init_disparity       =   imL.disparityPyramid[lvl];
        d_.min_disparity        =   imL.minMaxPyramidMatchingDisparities[lvl].first;
        d_.max_disparity        =   imL.minMaxPyramidMatchingDisparities[lvl].second;
        d_.use_init_value       =   true;
    }

    epiData_.epiLineRLength =   std::abs(d_.max_disparity - d_.min_disparity);

//    disparityMap = cv::Mat::zeros( imL.imPyramid[lvl].rows, imL.imPyramid[lvl].cols,  CV_32FC1 );

    calc_shiftAndScaleForImages(imL, imR, lvl);


    /**--------------------------------------------------------------------------------------------------*/

    for (     int idx_x = 0; idx_x < imL.imPyramid[lvl].cols; ++idx_x) {
        for ( int idx_y = 0; idx_y < imL.imPyramid[lvl].rows; ++idx_y) {

            if  ( imL.mappablePointsMaskPyramid[lvl].at<bool>(idx_y, idx_x) == false ) continue;

            epiData_.ptLeft = Eigen::Vector2f(idx_x, idx_y);


            try {

                correspData_.leftStripe     = getLeftStripe   ( imL.imPyramid[lvl] );
                correspData_.rightStripe    = getRightStripe  ( imR.imPyramid[lvl] );

/*
                if ( (idx_x==422) && (idx_y==170) ){

                    show_imageScaled("left stripe" , correspData_.leftStripe);
                    show_imageScaled("right stripe", correspData_.rightStripe);
                    cv::waitKey(-1);
                }
*/

                findL2RCorrespondence                         ( correspData_.leftStripe, correspData_.rightStripe);
                if (params.consistency_check) consistencyCheck( correspData_.leftStripe, correspData_.rightStripe);

                correspData_.currentPtIdx++;


//                if ( (idx_x==422) && (idx_y==170) ){ show_singleMatchOnStripes(); /*show_singleMatchWholeImage(imL, imR, lvl)*/; }

            }
            catch (int ex)
            {
                if ((ex >= 0) && (ex <= 4)) {

                    if ( !use_init_value ){   imL.disparityPyramid[lvl].at<float>(idx_y, idx_x) = 0; }
                    imL.mappablePointsMaskPyramid[lvl].at<bool>(idx_y, idx_x) = false;
                    collectStats(ex);
                    continue;
                }
            }


            if ( lvl == 0 ) {  calc_ENCCSubPx(correspData_.rBoxes);  }

            /// OpenCV, like may other libraries, treat matrices (and images) in row-major order. That means every acces is defined as (ROW, column)

            imL.disparityPyramid[lvl].at<float>(idx_y, idx_x) +=   d_.min_disparity + d_.L2R_subpx ;

//            disparityMap.at<float>(idx_y, idx_x) = d_.max_disparity + d_.L2R_subpx;
//            show_singleMatchOnStripes();
//            cv::waitKey(-1);


/*

            if ( false )
            {
                show_singleMatchOnStripes ( );
                show_singleMatchWholeImage(imL, imR, 0);
                cv::waitKey(-1);

            }
*/
        }
    }

    statsCorresp_.calcPercents();
    statsCorresp_.printPercents();


    std::cout << "num of successful correspondences: " << correspData_.currentPtIdx << std::endl;

}


void ENCCStereoMatching::do_stereoMatchingCorners(Frame_old &imL, const Frame_old &imR, int lvl, const ENCC_params &params, bool use_init_value)
{

    d_.minClearness 	= params.min_clearness;
    d_.epsilon 		    = params.epsilon;

    statsCorresp_.clearStatistics();
    statsCorresp_.totalNumOfPoints = imL.numOfMappablePointsPyramid[lvl];
    correspData_.currentPtIdx = 0;



    /**----------------------------------------------------------------------------------- assign data from left image imL ---------*/

    if ( !use_init_value ) {

        d_.min_disparity        =   imL.minMaxDisparities[lvl].first;
        d_.max_disparity        =   imL.minMaxDisparities[lvl].second;
        d_.use_init_value       =   false;
    }

    if ( use_init_value ){

        d_.init_disparity       =   imL.disparityPyramid[lvl];
        d_.min_disparity        =   imL.minMaxPyramidMatchingDisparities[lvl].first;
        d_.max_disparity        =   imL.minMaxPyramidMatchingDisparities[lvl].second;
        d_.use_init_value       =   true;
    }

    epiData_.epiLineRLength =   std::abs(d_.max_disparity - d_.min_disparity);

    calc_shiftAndScaleForImages(imL, imR, lvl);


    /**--------------------------------------------------------------------------------------------------*/

    for (     int idx_x = 0; idx_x < imL.imPyramid[lvl].cols; ++idx_x) {
        for ( int idx_y = 0; idx_y < imL.imPyramid[lvl].rows; ++idx_y) {

            if  ( imL.cornersPropagatedPyramid[lvl].at<bool>(idx_y, idx_x) == false ) continue;

            epiData_.ptLeft = Eigen::Vector2f(idx_x, idx_y);


            try {

                correspData_.leftStripe     = getLeftStripe   ( imL.imPyramid[lvl] );
                correspData_.rightStripe    = getRightStripe  ( imR.imPyramid[lvl] );

                findL2RCorrespondence                         ( correspData_.leftStripe, correspData_.rightStripe);
                if (params.consistency_check) consistencyCheck( correspData_.leftStripe, correspData_.rightStripe);

                correspData_.currentPtIdx++;


            }
            catch (int ex)
            {
                if ((ex >= 0) && (ex <= 4)) {

                    if ( !use_init_value ){   imL.cornersDisparityPyramid[lvl].at<float>(idx_y, idx_x) = 0; }
                    imL.cornersPropagatedPyramid[lvl].at<bool>(idx_y, idx_x) = false;
                    collectStats(ex);
                    continue;
                }
            }


            if ( lvl == 0 ) {  calc_ENCCSubPx(correspData_.rBoxes);  }

            /// OpenCV, like may other libraries, treat matrices (and images) in row-major order. That means every acces is defined as (ROW, column)

            imL.cornersDisparityPyramid[lvl].at<float>(idx_y, idx_x) +=   d_.min_disparity + d_.L2R_subpx ;

//            show_singleCornerMatchWholeImage(imL, imR, lvl);

        }
    }

    statsCorresp_.calcPercents();
    statsCorresp_.printPercents();


    std::cout << "num of successful correspondences: " << correspData_.currentPtIdx << std::endl;

}


void ENCCStereoMatching::prepareForSubpxInterpolation(  std::vector< std::pair<int, float> > &blockDiffs_  )  {

    d_.L2R		= blockDiffs_[0].first;
    d_.L2R_subpx= d_.L2R;

    if (d_.L2R == 0 || d_.L2R == ( blockDiffs_.size()-1 )  ) {

        d_.interpNotPossible = true;
    }
    else {

        int     d   = d_.L2R;
        auto    C1  = find_if( blockDiffs_.begin(), blockDiffs_.end(), [d](const std::pair<int, float> &element) { return element.first == (d - 1); });
        auto    C3  = find_if( blockDiffs_.begin(), blockDiffs_.end(), [d](const std::pair<int, float> &element) { return element.first == (d + 1); });

        d_.rho_d0_              = blockDiffs_[0].second;
        d_.rho_d0m1_            = (*C1).second;
        d_.rho_d0p1_            = (*C3).second;
        d_.L2R_subpx            = d_.L2R;
        d_.interpNotPossible    = false;
    }

#ifdef OUTPUT
    std::cout << "ref2target: " << d_.L2R << std::endl;
#endif
}


void ENCCStereoMatching::calc_LeftVector(std::vector<float> &lBox, EpipolarData &epiData, const Frame_old &imL) {

    Eigen::Vector2f ptSubPxLeft;
    float mean=0, norm, norm2=0;

    lBox.clear();

    for (int epiD = -halfWindowWidth_; epiD <= halfWindowWidth_; ++epiD) {
        for (int epiN = -halfWindowWidth_; epiN <= halfWindowWidth_; ++epiN) {

            ptSubPxLeft = epiData.ptLeft + epiD*epiData.epilineDirL + epiN*epiData.epilineDirNormalL;
            lBox.push_back(getGraySubpixFloat(imL.imPyramid[0], cv::Point2f(ptSubPxLeft(0), ptSubPxLeft(1))) );

        }
    }

    mean =  std::accumulate(lBox.begin(), lBox.end(), 0.0f) / lBox.size() ;

    for (float& val : lBox)     val     -=  mean;
    for (float val : lBox)      norm2   +=  val*val;
    norm = (float) sqrt(norm2);
    for (float& val : lBox)     val     /=  norm;
}



void ENCCStereoMatching::calc_RightVector(std::pair<std::vector<float>, float> &rBox, EpipolarData &epiData, const cv::Mat &imR, int idx2) {

    Eigen::Vector2f ptSubPxRight;
    float mean, norm, norm2=0;

    rBox.first.clear();

    for (int epiD = -halfWindowWidth_; epiD <= halfWindowWidth_; ++epiD) {
        for (int epiN = -halfWindowWidth_; epiN <= halfWindowWidth_; ++epiN) {

            ptSubPxRight = epiData.ptRight_max + idx2*epiData.epilineDirR  + epiD*epiData.epilineDirR + epiN*epiData.epilineDirNormalR;
            rBox.first.push_back(getGraySubpixFloat(imR, cv::Point2f(ptSubPxRight(0), ptSubPxRight(1))) );

        }
    }

//    for (auto i: rBox.first ) std::cout << i << " " ; std::cout << std::endl;

    mean =  std::accumulate(rBox.first.begin(), rBox.first.end(), 0.0f) / rBox.first.size() ;

    for ( float& val : rBox.first ) val      -=  mean;
    for ( float val  : rBox.first ) norm2    += val*val;

    norm = sqrt(norm2);

    for ( float& val : rBox.first ){

        if (norm > 0.0001)  val /= norm;
    }

    rBox.second = norm2;

}


float ENCCStereoMatching::getGraySubpixFloat( const cv::Mat &img, cv::Point2f pt )
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<float>(0,0);
}


uchar ENCCStereoMatching::getGraySubpixUchar( const cv::Mat &img, cv::Point2f pt )
{
    cv::Mat patch;
    cv::getRectSubPix(img, cv::Size(1,1), pt, patch);
    return patch.at<uchar>(0,0);


    cv::remap(img, patch, cv::Mat(1, 1, CV_32FC2, &pt), cv::noArray(),     cv::INTER_LINEAR, cv::BORDER_REFLECT_101);
}



float ENCCStereoMatching::calc_DotProduct(const std::vector<float> &v1, const std::vector<float> &v2)
{

    if ( v1.size() != v2.size() ) return 0;

    float sum = 0;

    for (int i = 0; i < v1.size(); ++i) {   sum += v1[i] * v2[i] ;     }
//    std::cout << "sum value is: "  << sum << std::endl;
    return sum;
}



float ENCCStereoMatching::calc_dotProduct(const cv::Mat &patch1, const cv::Mat &patch2)
{

    if ( patch1.size != patch2.size ) return 0;

    float sum = 0;

    for (int i = 0; i < patch1.rows; ++i) {
        for (int j = 0; j < patch1.cols; ++j)
            sum += patch1.at<float>(i, j) * patch2.at<float>(i, j);
    }

    return sum;
}




bool ENCCStereoMatching::checkEpipolarData(const Frame_old &imL, EpipolarData& data)
{

    if ( std::abs( data.ptRight_min(0) ) < 1e-6 || std::abs( data.ptRight_min(1) ) < 1e-6 || std::abs( data.ptRight_max(0) )  < 1e-6 || std::abs( data.ptRight_max(1) ) < 1e-6  )
        return false;

    return true;

}



void ENCCStereoMatching::calc_ENCCSubPx(std::vector<std::pair<std::vector<float>, float> > &rBoxes)
{

    d_.test_rBoxes_L2Rp1_val    =   rBoxes[d_.L2R+1].second;
    d_.test_rBoxes_L2R_val      =    rBoxes[d_.L2R].second ;


    if ( d_.interpNotPossible ) {  d_.L2R_subpx = d_.L2R;    return; }

//    try {

    calc_tau_nom_denom( rBoxes);

    if ( d_.tau0_denom < 0 ){

        d_.L2R_subpx =  d_.tau0_nom / d_.tau0_denom ;

        if (std::abs (d_.L2R_subpx ) < 0.5 ) d_.L2R_subpx = d_.L2R - d_.L2R_subpx;
        else calc_QuadricSubpx();
    }
    else{
        d_.numOfPtsWithPositiveDenominator++;
        calc_QuadricSubpx();
    }

}


float ENCCStereoMatching::calc_ENCCSubPx_test(std::vector<std::pair<std::vector<float>, float> > &rBoxes)
{

/*    std::cout << "----------------------------------------------" << std::endl;
    for (auto el : rBoxes[d_.L2R].first) std::cout << el << " "; std::cout << std::endl;
    std::cout << rBoxes[d_.L2R].second << std::endl;

    std::cout << "----------------------------------------------" << std::endl;

    for (auto el : rBoxes[d_.L2R+1].first) std::cout << el << " "; std::cout << std::endl;
    std::cout << rBoxes[d_.L2R+1].second << std::endl;
    std::cout << "----------------------------------------------" << std::endl;
*/



    d_.test_rBoxes_L2Rp1_val    =   rBoxes[d_.L2R+1].second;
    d_.test_rBoxes_L2R_val      =    rBoxes[d_.L2R].second ;



    if ( d_.interpNotPossible ) {

        d_.L2R_subpx = d_.L2R;
        return d_.L2R_subpx;
    }

//    try {

    calc_tau_nom_denom( rBoxes);

    if ( d_.tau0_denom < 0 ){

        d_.L2R_subpx =  d_.tau0_nom / d_.tau0_denom ;

        if (std::abs (d_.L2R_subpx ) < 0.5 ) d_.L2R_subpx = d_.L2R - d_.L2R_subpx;
        else calc_QuadricSubpx();
    }
    else{
        d_.numOfPtsWithPositiveDenominator++;
        calc_QuadricSubpx();
    }

    return  d_.L2R_subpx;

}



void ENCCStereoMatching::calc_QuadricSubpx()
{

    d_.numOfQuadricInterp++;

    float denom =  d_.rho_d0m1_  - 2*d_.rho_d0_ + d_.rho_d0p1_ ;

    if (std::abs(denom) < 0.001 )  {  d_.L2R_subpx = d_.L2R;    return; }

    d_.L2R_subpx = d_.L2R -   0.5 * ( d_.rho_d0p1_ - d_.rho_d0m1_ ) / denom  ;
}


void ENCCStereoMatching::calc_tau_nom_denom( std::vector< std::pair< std::vector< float>, float> >& rBoxes) {

//
//    check_rho_d_denom ( );
    calc_lambda       ( rBoxes );
    calc_r            ( rBoxes );


//    std::cout << d_.lambda << " " << d_.r << std::endl;

    d_.tau0_nom      =      d_.rho_d0p1_  -  d_.r * d_.rho_d0_;
    d_.tau0_denom    =      d_.lambda * ( d_.r * d_.rho_d0p1_ - d_.rho_d0_ )       +      d_.r * d_.rho_d0_   -   d_.rho_d0p1_;


//    std::cout << d_.tau0_nom << " " << d_.tau0_denom << std::endl;

#ifdef OUTPUTPERPOINT
    std::cout << "rho_d0_: " << d_.rho_d0_ << "; rho_d0+1: " << d_.rho_d0p1_ << "; r: " <<  d_.r << "; lambda: " <<  d_.lambda << std::endl;
    std::cout << "tau0_nom: " << d_.tau0_nom << "; tau0_denom: " << d_.tau0_denom << std::endl;
#endif
}

void ENCCStereoMatching::calc_lambda( std::vector< std::pair< std::vector< float>, float> >& rBoxes ){


    d_.lambda   =   rBoxes[d_.L2R+1].second    /   rBoxes[d_.L2R].second ;
}

void ENCCStereoMatching::calc_r( std::vector< std::pair< std::vector< float>, float> >& rBoxes ){

    d_.r  = calc_DotProduct(rBoxes[d_.L2R].first, rBoxes[d_.L2R + 1].first);

}

void ENCCStereoMatching::show_imageScaled(std::string imName, cv::Mat im) {

    double min, max;

    cv::minMaxIdx(im, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(im-min, adjMap, 255 / ( max-min) );
    cv::imshow(imName, adjMap);

}

void ENCCStereoMatching::fitImageTo8U(cv::Mat &im) {

    double min, max;

    cv::minMaxIdx(im, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(im-min, adjMap, 255 / ( max-min) );
    im = adjMap;
}

void ENCCStereoMatching::show_singleMatchWholeImage(const Frame_old &imL, const Frame_old &imR, int lvl) {

    cv::Mat     im1     =   imL.imPyramid[lvl],     im2     =   imR.imPyramid[lvl];
    cv::Size    sz1     =   im1.size(),             sz2     =   im2.size();

    int         x       =   epiData_.ptLeft(0),     y       =   epiData_.ptLeft(1);


    fitImageTo8U(im1);
    fitImageTo8U(im2);

    cv::Scalar color_white(222, 222, 222);

    /// draw cross on the first image

    cv::line(im1, cv::Point( x-7,y   ),   cv::Point( x-3,y   ), color_white);
    cv::line(im1, cv::Point( x+3,y   ),   cv::Point( x+7,y   ), color_white);
    cv::line(im1, cv::Point( x,  y-3 ),   cv::Point( x,  y-7 ), color_white);
    cv::line(im1, cv::Point( x,  y+3 ),   cv::Point( x,  y+7 ), color_white);

    /// draw cross on the second image

    int d = std::round( disparityMap.at<float>( epiData_.ptLeft(1), epiData_.ptLeft(0)) );

    cv::line(im2, cv::Point( d+x-7,y   ),   cv::Point( d+x-3,y   ), color_white);
    cv::line(im2, cv::Point( d+x+3,y   ),   cv::Point( d+x+7,y   ), color_white);
    cv::line(im2, cv::Point( d+x,  y-3 ),   cv::Point( d+x,  y-7 ), color_white);
    cv::line(im2, cv::Point( d+x,  y+3 ),   cv::Point( d+x,  y+7 ), color_white);


    /// concatenate images

    cv::Mat imToShow (sz1.height, sz1.width+sz2.width, CV_8UC1);

    imToShow.adjustROI(0, 0, 0, -sz2.width); // Move right boundary to the left.
    im1.copyTo(imToShow);

    imToShow.adjustROI(0, 0, -sz1.width, sz2.width); // Move the left boundary to the right, right boundary to the right.
    im2.copyTo(imToShow);

    imToShow.adjustROI(0, 0, sz1.width, 0); // restore original ROI


    show_imageScaled("Single Match", imToShow);
    cv::waitKey(-1);
}

void ENCCStereoMatching::show_singleCornerMatchWholeImage(const Frame_old &imL, const Frame_old &imR, int lvl) {

    cv::Mat     im1     =   imL.imPyramid[lvl],     im2     =   imR.imPyramid[lvl];
    cv::Size    sz1     =   im1.size(),             sz2     =   im2.size();

    int         x       =   epiData_.ptLeft(0),     y       =   epiData_.ptLeft(1);


    fitImageTo8U(im1);
    fitImageTo8U(im2);

    cv::Scalar color_white(222, 222, 222);

    /// draw cross on the first image

    cv::line(im1, cv::Point( x-7,y   ),   cv::Point( x-3,y   ), color_white);
    cv::line(im1, cv::Point( x+3,y   ),   cv::Point( x+7,y   ), color_white);
    cv::line(im1, cv::Point( x,  y-3 ),   cv::Point( x,  y-7 ), color_white);
    cv::line(im1, cv::Point( x,  y+3 ),   cv::Point( x,  y+7 ), color_white);

    /// draw cross on the second image

    int d = std::round( imL.cornersDisparityPyramid[lvl].at<float>( epiData_.ptLeft(1), epiData_.ptLeft(0)) );

    cv::line(im2, cv::Point( d+x-7,y   ),   cv::Point( d+x-3,y   ), color_white);
    cv::line(im2, cv::Point( d+x+3,y   ),   cv::Point( d+x+7,y   ), color_white);
    cv::line(im2, cv::Point( d+x,  y-3 ),   cv::Point( d+x,  y-7 ), color_white);
    cv::line(im2, cv::Point( d+x,  y+3 ),   cv::Point( d+x,  y+7 ), color_white);


    /// concatenate images

    cv::Mat imToShow (sz1.height, sz1.width+sz2.width, CV_8UC1);

    imToShow.adjustROI(0, 0, 0, -sz2.width); // Move right boundary to the left.
    im1.copyTo(imToShow);

    imToShow.adjustROI(0, 0, -sz1.width, sz2.width); // Move the left boundary to the right, right boundary to the right.
    im2.copyTo(imToShow);

    imToShow.adjustROI(0, 0, sz1.width, 0); // restore original ROI


    show_imageScaled("Single Match", imToShow);
    cv::waitKey(-1);
}

void ENCCStereoMatching::show_singleMatchOnStripes() {
    //    cv::Mat adjMap;
//    cv::convertScaleAbs(im-minL, adjMap, 255 / ( maxL-minL) );



    int         d           =   std::round( d_.L2R_subpx );

    cv::Mat     lStripe,  rStripe;

    cv::convertScaleAbs( correspData_.leftStripe -d_.imL_shift, lStripe, d_.imL_scale );
    cv::convertScaleAbs( correspData_.rightStripe-d_.imR_shift, rStripe, d_.imR_scale );

    cv::Scalar color_white(255, 255, 255);

    /// make markers on stripes

    lStripe.at<unsigned char> ( 0,                   halfWindowWidth_+2 ) = 255;
    lStripe.at<unsigned char> ( 2*halfWindowHeight_, halfWindowWidth_+2 ) = 255;

    rStripe.at<unsigned char> ( 0,                   halfWindowWidth_+d ) = 255;
    rStripe.at<unsigned char> ( 2*halfWindowHeight_, halfWindowWidth_+d ) = 255;


    /// concatenate images

    cv::Size    sz1 = lStripe.size(),           sz2 = rStripe.size();

    cv::Mat imToShow (sz1.height, sz1.width+sz2.width, CV_8UC1);

    imToShow.adjustROI(0, 0, 0, -sz2.width); // Move right boundary to the left.
    lStripe.copyTo(imToShow);

    imToShow.adjustROI(0, 0, -sz1.width, sz2.width); // Move the left boundary to the right, right boundary to the right.
    rStripe.copyTo(imToShow);

    imToShow.adjustROI(0, 0, sz1.width, 0); // restore original ROI


    cv::imshow("Single Match on Stripes", imToShow);
    cv::waitKey(-1);
}


void ENCCStereoMatching::calc_shiftAndScaleForImages(const Frame_old &L, const Frame_old &R, int lvl) {

    double minL, maxL, minR, maxR;

    cv::minMaxIdx(L.imPyramid[lvl], &minL, &maxL);
    cv::minMaxIdx(R.imPyramid[lvl], &minR, &maxR);

    d_.imL_shift = minL;    d_.imL_scale = 255 / ( maxL-minL);
    d_.imR_shift = minR;    d_.imR_scale = 255 / ( maxR-minR);

//    cv::Mat adjMap;
//    cv::convertScaleAbs(im-minL, adjMap, 255 / ( maxL-minL) );

}


void ENCCStereoMatching::show_mappablePointsShift(const Frame_old &lFrame, const Frame_old &rFrame, int lvl, bool write2disk, const std::string &file_path, const std::string &file_name) {


    int cols = lFrame.imPyramid[lvl].cols, rows = lFrame.imPyramid[lvl].rows;

    cv::Mat x_shift_image   = cv::Mat::zeros(rows, cols, CV_8UC4);
    cv::Mat left            = lFrame.imPyramid[lvl];
    cv::Mat right           = rFrame.imPyramid[lvl];

    fitImageTo8U(left);     fitImageTo8U(right);

    for (int idx_x = 0; idx_x < cols; ++idx_x) {
        for (int idx_y = 0; idx_y < rows; ++idx_y) {

            if ( lFrame.mappablePointsMaskPyramid[lvl].at<bool>(idx_y, idx_x)           == false )        continue;
            if ( std::abs( lFrame.disparityPyramid[lvl].at<float>(idx_y, idx_x) )       <  1e-2 )       continue;

            int             d       =   std::round( lFrame.disparityPyramid[lvl].at<float>(idx_y, idx_x) );
            uchar           upixel  =   left.at<uchar>(idx_y, idx_x);
            cv::Vec4b       pixel   =   cv::Vec4b(upixel, upixel, upixel, 255);

            x_shift_image.at<cv::Vec4b>(idx_y, idx_x + d) = pixel;

        }
    }


    cv::imshow("Shifted Image", x_shift_image);
    cv::waitKey(-1);


    /**--------------------- write to disc image files ------------------------*/

    if ( write2disk){

        std::vector<int> compression_params;
        compression_params.push_back( CV_IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(9);

        std::string fullPath_right      = file_path + "/" + file_name+".png";
        std::string fullPath_shifted    = file_path + "/" + file_name+"_shifted.png";


        try { cv::imwrite(fullPath_right, right, compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Right image write error:: %s\n" << ex.what(); }


        try { cv::imwrite(fullPath_shifted, x_shift_image, compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Shifted image write error:: %s\n" << ex.what(); }

    }
}

void ENCCStereoMatching::show_mappablePointsShiftLinearInterp(const Frame_old &lFrame, const Frame_old &rFrame, int lvl, bool write2disk, const std::string &file_path, const std::string &file_name,
                                                              bool use_mask_as_transparency) {


    int cols = lFrame.imPyramid[lvl].cols, rows = lFrame.imPyramid[lvl].rows;

    cv::Mat x_shift_image   = cv::Mat::zeros(rows, cols, CV_8UC4);
    cv::Mat left            = lFrame.imPyramid[lvl];
    cv::Mat right           = rFrame.imPyramid[lvl];

    fitImageTo8U(left);     fitImageTo8U(right);


    if ( !use_mask_as_transparency){   lFrame.setChannelToConstValue( x_shift_image, 3, 255);  }


    for (int idx_x = 0; idx_x < cols; ++idx_x) {
        for (int idx_y = 0; idx_y < rows; ++idx_y) {

//            if ( lFrame.mappablePointsMaskPyramid[lvl].at<bool>(idx_y, idx_x)   == false )          continue;
//            if ( std::abs( lFrame.disparityPyramid[lvl].at<float>(idx_y, idx_x) )  < 0.0001 )       continue;

            float             d           = lFrame.disparityPyramid[lvl].at<float>(idx_y, idx_x) ;
            float             shifted_x   = idx_x - d;

            if  ( (shifted_x < 0) ||  (shifted_x > cols ) || std::abs(d) < 1e-2 ) continue;

            uchar           upixel  =   getGraySubpixUchar(left, cv::Point2f(shifted_x, idx_y) );
            cv::Vec4b       pixel   =   cv::Vec4b       ( upixel, upixel, upixel, 255);

            x_shift_image.at<cv::Vec4b>(idx_y, idx_x) = pixel;
        }
    }


//    cv::imshow("Shifted Image", x_shift_image);
//    cv::waitKey(-1);




    if ( write2disk){

        std::vector<int> compression_params;
        compression_params.push_back( CV_IMWRITE_PNG_COMPRESSION);
        compression_params.push_back(9);

        std::string fullPath_right      = file_path + "/" + file_name+".png";
        std::string fullPath_shifted    = file_path + "/" + file_name+"_shifted.png";
        std::string fullPath_disparity = file_path + "/" + file_name+"_disparity.exr";


        try { cv::imwrite(fullPath_right, right, compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Right image write error:: %s\n" << ex.what(); }


        try { cv::imwrite(fullPath_shifted, x_shift_image, compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Shifted image write error:: %s\n" << ex.what(); }

        try { cv::imwrite(fullPath_disparity, lFrame.disparityPyramid[lvl]); }
        catch (std::runtime_error& ex){ std::cerr << "Shifted image write error:: %s\n" << ex.what(); }

    }
}

void ENCCStereoMatching::propagate_dsiparityToPrevLvl(int lvl, Frame_old &imL) {

    if ( lvl == 0) return;

    int w  = imL.disparityPyramid[lvl-1].cols;
    int h  = imL.disparityPyramid[lvl-1].rows;

    cv::resize( imL.disparityPyramid[lvl], imL.disparityPyramid[lvl-1], cv::Size(w,h ) );

    imL.disparityPyramid[lvl-1] *= 2;

}

void ENCCStereoMatching::propagate_cornerDsiparityToPrevLvl(int lvl, Frame_old &imL) {

    if ( lvl == 0) return;

    int w  = imL.disparityPyramid[lvl-1].cols;
    int h  = imL.disparityPyramid[lvl-1].rows;


    for (int row = 0; row < imL.cornersDisparityPyramid[lvl - 1].rows; ++row) {
        for (int col = 0; col < imL.cornersDisparityPyramid[lvl - 1].cols; ++col) {


            if (  imL.cornersPropagatedPyramid[lvl-1].at<bool>(row, col) == false) continue;

            imL.cornersDisparityPyramid[ lvl - 1].at<float>(row, col) = 2*imL.cornersDisparityPyramid[lvl].at<float>( row/2, col/2);
            imL.cornersPropagatedPyramid[lvl - 1].at<float>(row, col) =   imL.cornersPropagatedPyramid[lvl].at<float>( row/2, col/2);
        }
    }
}

void ENCCStereoMatching::write_cornersCorrepondences(const Frame_old &lFrame, const Frame_old &rFrame, int lvl, const std::string &path, const std::string &file_name) {

    cv::Mat imL, imR;

    lFrame.imPyramid[lvl].copyTo(imL);
    rFrame.imPyramid[lvl].copyTo(imR);

    for (int row = 0; row < imL.rows; ++row) {
        for (int col = 0; col < imL.cols; ++col) {

            if ( lFrame.cornersPropagatedPyramid[lvl].at<bool>(row, col) ){

//                draw_crossCorrespondence( imL, cv::Point(col, row));

            }
        }
    }

}

//--------------------------------------------------------------------

#endif //SOP_PCFROMSTEREOPAIR_ENCC_HPP