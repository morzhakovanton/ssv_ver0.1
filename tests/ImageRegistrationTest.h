//
// Created by morzh on 7/2/17.
//

#ifndef IMAGEREGISTRATION_IMAGEREGISTRATIONTEST_H
#define IMAGEREGISTRATION_IMAGEREGISTRATIONTEST_H

#include "ImageRegistrationStatistics.h"
#include "ImageRegistrationT2POC.h"
#include <Eigen/Core>


class ImageRegistrationTest{
public:

    void check_shiftT2_32x32(const cv::Mat &fixed, Eigen::Vector2f start, Eigen::Vector2f end, Eigen::Vector2f step);

    ImageRegistrationStatistics     stats;
    ImageRegistrationT2POC          imreg_poc;
};

void ImageRegistrationTest::check_shiftT2_32x32(const cv::Mat &fixed, Eigen::Vector2f start, Eigen::Vector2f end, Eigen::Vector2f step) {

    cv::Mat             xform  = cv::Mat::eye( 2, 3, CV_32FC1);
    cv::Mat             moved;
    Eigen::Vector2f     shift_estimate, shift_truth;
    float               confidence;
    int                 row, col;


    stats.error_matrix          =       cv::Mat::zeros(  (float) ( end(0)-start(0) )/step(0) + 1, (float) ( end(1)-start(1) )/step(1) + 1, CV_32FC1 );
    stats.confidence_matrix     =       cv::Mat::zeros(  (float) ( end(0)-start(0) )/step(0) + 1, (float) ( end(1)-start(1) )/step(1) + 1, CV_32FC1 );

    for (       float idx1 = start(0); idx1 < end(0); idx1 += step(0) ) {
        for (   float idx2 = start(1); idx2 < end(1); idx2 += step(1) ) {

            xform.at<float>(0,2) = idx1;   xform.at<float>(1,2) = idx2;
            cv::warpAffine( fixed, moved,  xform,  fixed.size()  );

            shift_truth    = Eigen::Vector2f(idx1, idx2);
            shift_estimate = imreg_poc.estimate_shift32x32(fixed, moved, ImageRegistrationT2POC::Options::UPSAMPLING, &confidence);

            row =  std::round( -start(0)/step(0)  + idx1/step(0) );
            col =  std::round( -start(1)/step(1) + idx2/step(1) );

//            stats.collect_statsT2( Eigen::Vector2f(idx1, idx2), shift_estimate, confidence);

            stats.error_matrix.at<float>     ( row,  col  )         =   (shift_truth - shift_estimate).norm();
            stats.confidence_matrix.at<float>( row,  col  )         =   confidence;

        }
    }

}

#endif //IMAGEREGISTRATION_IMAGEREGISTRATIONTEST_H
