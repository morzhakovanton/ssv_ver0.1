//
// Created by morzh on 12.09.18.
//


#include "Rectification/ImageRegistrationPOCOptions.h"
#include "../Rectification/ImageRegistrationPOCBase.h"




int main(){

    ImageRegistrationPOCBase    poc_base;

    cv::Mat                     window_1d = cv::Mat::zeros(  1, 21, CV_32FC1 );
    cv::Mat                     window_2d = cv::Mat::zeros( 42, 68, CV_32FC1 );

    poc_base.create_TukeyWindow1D(window_1d);
    poc_base.create_TukeyWindow2D(window_2d);

    std::cout << window_1d << std::endl;
//    std::cout << window_2d << std::endl;

/*
    cv::imshow("Tukey Window", window_2d);
    cv::waitKey(-1);
*/


    ///------------------------------------------------------------------------
    cv::Mat X, Y;
    poc_base.meshgrid(cv::Range(1,5), cv::Range(10, 14), X, Y);

    std::cerr << X << std::endl;
    std::cerr << Y << std::endl;


    ///-------------------------------------------------------------------------------

    ImageRegistrationPOCOptions options;

    options.print();

    cv::Mat gb = poc_base.get_GaborKernel(options.gabor_options);

    std::cout << gb << std::endl;


    return 0;
}