//
// Created by morzh on 1/23/16.
//

#ifndef SOP__Frame_old_HPP
#define SOP__Frame_old_HPP

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <fstream>
#include <iostream>
#include <algorithm>

#include "Settings.h"

//#define MIN_EPL_ANGLE_SQUARED (0.3f*0.3f)

class Frame_old {
public:
                Frame_old                               ( const cv::Mat &im);
                Frame_old                               ( const std::string &imPath);
                ~Frame_old                              ( );

    void        build_keyFrame_old(int num_lvls, const cv::Mat &mask);
    void        build_Frame_old(int num_lvls);
    void        build_imagePyramid                  ( int numLayers);
    void        build_imageGradients                ( int numLayers);
    void        build_imageEdges                    ( int numLayers);
    void        build_imageCorners                  ( int numLayers);
    void        build_mappablePointsMask            ( int numLayers);
    void        build_cornersPointsMask             ( int numLayers);
    void        build_cornersPropagationMask        ( int numLayers);
    void        build_cornerPointsMaskAtLvl         ( int lvl, int threshold);
    void        build_mappablePointsMaskAtLvl       ( int lvl, int threshold);
    void        build_nextPyramidLvl                ( const cv::Mat &kernel);
    void        build_nextPyrLvlGradients           ( );
    void        build_nextPyrLvlEdges               ( );
    void        build_nextPyrLvlCorners             ( );

    void        get_edgesPointsList                 ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_cornerPointsList                ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_listOfEdgesCoordinates          ( int lvl, Eigen::Matrix2Xf &outPoints, int threshold);
    void        get_gradCornesOCV                   ( int lvl, cv::Mat &outPoints, int threshold);
    void        get_HarrisCornersCameraFrame_old        ( int lvl, Eigen::Matrix2Xf &outPoints, int treshold, int blockSize, int apertureSize, float k);
    void        get_gradCornersSubPx                ( cv::Mat &corners, int lvl, int wSize, int zSize, int threshold);

    void        reduceDisparitytMap                 ( int  lvl );
    void        expandDisparitytMap                 ( );


    void        calc_imZeroLvlShiftScale            (   );
    float       calc_numMappableNeighbours          ( int row, int col, int lvl, int do_interp_count, const cv::Mat &mapPtsMask);
    void        calc_minMaxDisparities              ( const std::pair<int, int> &minMaxDispLvlZero, int numlevels );
    void        calc_minMaxDisparitiesForMatching   ( const std::pair<int, int> &minMaxDispLvlZero, int numlevels);

    void        fill_holesDisparitytMap             ();
    void        fill_disparitiesHolesIteration      ( int lvl, int min_num_neighbours);
    void        fill_disparitiesHoles               ( int lvl );

    void        multiplyMaxGradByMaskAtLvl          (const cv::Mat &mask, int lvl = 0);
    void        multiplyCornersByMaskAtLvl          (const cv::Mat &mask, int lvl = 0);
    void        multiplyMaxGradByMask               (const cv::Mat &mask);
    void        multiplyCornersByMask               (const cv::Mat &mask);
    void        deleteOddRowOddColumn               ( const cv::Mat & imSrc, cv::Mat &imDst);


    void        setNumOfPyrLvls                     ( int n );
    void        test_writeImagePyramid              ( const std::string &imPath, const std::string &imName);
    void        test_writeImageGradsPyramid         ( const std::string &imPath, const std::string &imName);
    void        test_writeImageMaxGradsPyramid      ( const std::string &imPath, const std::string &imName);
    void        test_writeImageMaxGradsTresholdPyramid (const std::string &imPath, const std::string &imName);
    void        test_writeCornerPtsList             ( const std::string &imPath, const std::string &imName);


    void        test_writeCornerListOCV		        ( const std::string &imPath, const std::string &imName);
    void        show_imagePyramid                   ( int num_lvls);
    void        show_gradientsPyramid               (  );
    void        show_edgesPyramid                   (  );
    void        show_cornersPyramid                 (  );
    void        show_propagatedCornersPyramid       ( int num_lvls);
    void        show_mappablePointsPyramid          ( int num_lvls);
    void        show_cornerPointsPyramid            ( int num_lvls);
    void        show_disparityMap                   ( bool reverse );


    void        print_mappablePointsCoverage        (   );
    void        medianFilterDisparityMap            ( int lvl );
    void        medianFilterDisparityMap_std        ( int lvl );
    void        imshowScaled                        ( std::string imName, cv::Mat &im);
    void        setChannelToConstValue              ( cv::Mat &mat, unsigned int channel, unsigned char value) const;

    void        propagateCornersToTheNextLvl        ( int lvl);
    void        propagateCornersToTheNextLvl2       ( int lvl);

    std::vector<cv::Mat>                imPyramid;
    std::vector<cv::Mat>                gradXPyramid;
    std::vector<cv::Mat>                gradYPyramid;
    std::vector<cv::Mat>                edgesPyramid;
    std::vector<cv::Mat>                cornersPyramid;
    std::vector<cv::Mat>                cornersPropagatedPyramid;
    std::vector<cv::Mat>                disparityPyramid;
    std::vector<cv::Mat>                cornersDisparityPyramid;
    std::vector<cv::Mat>                mappablePointsMaskPyramid;
    std::vector<cv::Mat>                cornerPointsMaskPyramid;
    std::vector<std::pair<int,int>>     minMaxDisparities;
    std::vector<std::pair<int,int>>     minMaxPyramidMatchingDisparities;
    std::vector<std::pair<float,float>> scaleShiftInrensitiesPyramid;

    std::pair<float, float>             scaleShiftZeroLvlPyramid;



    std::vector<int>                    numOfMappablePointsPyramid;

    cv::Mat                             mappablePointsMask;
private:

    cv::Mat     gaussKernel_;
    cv::Mat     boxKernel_;
    cv::Point   anchor_;

    double      delta_;
    int         ddepth_;
    int         numOfPyrLvls;

    void    fill_GaussKernel();
    void    fill_BoxKernel();
    void    fill_disparityMaps(int layers);
    void    fill_cornerDisparityMaps(int layers);
    void    fill_PropagatedCornersDisparityMaps(int layers);

    void smearMaxGrads(cv::Mat &maxgrads);

    void calcNumMappablePoints(int numLayers);

    void check_and_resolve_numLvls_division(int num_levels);
};


Frame_old::Frame_old(const std::string &imPath) : anchor_(cv::Point(-1, -1)), delta_(0), ddepth_(-1), gaussKernel_(cv::Mat(3, 3, CV_32FC1)), boxKernel_(cv::Mat(2, 2, CV_32FC1)){

    cv::Mat im = cv::imread( imPath, CV_LOAD_IMAGE_GRAYSCALE);
    im.convertTo(im, CV_32FC1);
    imPyramid.push_back( im );
    fill_GaussKernel();
    fill_BoxKernel();

    mappablePointsMask = cv::Mat::zeros(im.rows, im.cols, IPL_DEPTH_1U);
}

Frame_old::Frame_old(const cv::Mat &im) : anchor_(cv::Point(-1, -1)), delta_(0), ddepth_(-1), gaussKernel_(cv::Mat(3, 3, CV_32FC1)), boxKernel_(cv::Mat(2, 2, CV_32FC1)){

    cv::Mat imFloat;
    im.convertTo(imFloat, CV_32FC1);
    imPyramid.push_back( imFloat);
    fill_GaussKernel();
    fill_BoxKernel();
    mappablePointsMask = cv::Mat::zeros(im.rows, im.cols, IPL_DEPTH_1U);
}

Frame_old::~Frame_old() {

    for (int idx = 0; idx < imPyramid.size();    ++idx) { 	imPyramid[idx].release();       }
    for (int idx = 0; idx < gradXPyramid.size(); ++idx) { 	gradXPyramid[idx].release();    }
    for (int idx = 0; idx < gradYPyramid.size(); ++idx) { 	gradYPyramid[idx].release();    }
    for (int idx = 0; idx < edgesPyramid.size();++idx){ 	edgesPyramid[idx].release();  }
}

void Frame_old::build_keyFrame_old(int num_lvls, const cv::Mat &mask) {

    check_and_resolve_numLvls_division(num_lvls); ///  our image should have dimension ( width and height) that are divisable by num_layers number

    build_imagePyramid(num_lvls);
    build_imageGradients(num_lvls);
    build_imageEdges(num_lvls);
    build_imageCorners(num_lvls);

    multiplyMaxGradByMask          ( mask );
    multiplyCornersByMask          ( mask );
    build_mappablePointsMask       ( num_lvls );
    build_cornersPointsMask        ( num_lvls );
    calcNumMappablePoints          ( num_lvls );
    fill_disparityMaps             ( num_lvls );
    fill_cornerDisparityMaps       ( num_lvls );

    build_cornersPropagationMask( num_lvls);
}


void Frame_old::build_Frame_old(int num_lvls) {

    check_and_resolve_numLvls_division( num_lvls ); ///  our image should have dimension ( width and height) that are divisable by num_layers number

    build_imagePyramid(num_lvls);
    build_imageGradients(num_lvls);
    build_imageEdges(num_lvls);

    build_mappablePointsMask(num_lvls);
    fill_disparityMaps(num_lvls);
}



void Frame_old::build_imagePyramid(int numLayers) {

    for (int idx = 0; idx < numLayers-1; ++idx) build_nextPyramidLvl(gaussKernel_ /*boxKernel_*/);
}



void Frame_old::build_nextPyramidLvl(const cv::Mat &kernel) {

    cv::Mat imFiltered, newPyrLvl;
    cv::Mat curPyrLvl =  imPyramid[ imPyramid.size()-1 ];

//    filter2D( imPyramid[ imPyramid.size()-1 ], imFiltered, ddepth_, kernel, anchor_, delta_, cv::BORDER_DEFAULT );
//    deleteOddRowOddColumn(imFiltered, newPyrLvl);
//	imPyramid.push_back(imFiltered);

    cv::resize( curPyrLvl, newPyrLvl, cv::Size( 0.5*curPyrLvl.cols,  0.5*curPyrLvl.rows) ) ;
    imPyramid.push_back(newPyrLvl);
}


void Frame_old::deleteOddRowOddColumn(const cv::Mat &imSrc, cv::Mat &imDst) {

    int rows = imSrc.rows / 2;
    int cols = imSrc.cols / 2;

    int intermCols = imSrc.cols;
    cv::Mat imInterm = cv::Mat (imSrc.rows, intermCols, imSrc.type() );

    for (int idx1=0; idx1 < rows; ++idx1){

        cv::Rect inRect (0, idx1*2+1,  intermCols, 1);
        cv::Rect outRect(0, idx1    , intermCols, 1);
        imSrc(inRect).copyTo(imInterm(outRect));
    }

    imDst.release();
    imDst = cv::Mat(rows, cols, imSrc.type() );

    for (int idx2=0; idx2 < cols; ++idx2){

        cv::Rect inRect (idx2*2+1,0, 1, rows);
        cv::Rect outRect(idx2, 0, 1, rows);
        imInterm(inRect).copyTo(imDst(outRect));

    }


}

void Frame_old::fill_BoxKernel() {

    boxKernel_.at<float>(0, 0) = 0.25; 	boxKernel_.at<float>(0, 1) = 0.25;
    boxKernel_.at<float>(1, 0) = 0.25;  boxKernel_.at<float>(1, 1) = 0.25;
}


void Frame_old::fill_GaussKernel() {
    gaussKernel_.at<float>(0, 0) = 0.0625;   gaussKernel_.at<float>(0, 1) = 0.125;   gaussKernel_.at<float>(0, 2) = 0.0625;
    gaussKernel_.at<float>(1, 0) = 0.1250;   gaussKernel_.at<float>(1, 1) = 0.250;   gaussKernel_.at<float>(1, 2) = 0.1250;
    gaussKernel_.at<float>(2, 0) = 0.0625;   gaussKernel_.at<float>(2, 1) = 0.125;   gaussKernel_.at<float>(2, 2) = 0.0625;
}

void Frame_old::setNumOfPyrLvls(int n) {    	numOfPyrLvls = n;       }

void Frame_old::build_imageGradients(int numLayers) {

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlGradients();
}

void Frame_old::build_nextPyrLvlGradients() {

    int pyrLevel = gradXPyramid.size();

    int imCols = imPyramid[pyrLevel].cols;
    int imRows = imPyramid[pyrLevel].rows;
    cv::Rect inRect, outRect;

    cv::Mat left(imRows, imCols, CV_32FC1, cv::Scalar(0) ),     right(imRows, imCols, CV_32FC1, cv::Scalar(0) );
    cv::Mat top (imRows, imCols, CV_32FC1, cv::Scalar(0) ),     botom(imRows, imCols, CV_32FC1, cv::Scalar(0) );


    inRect  = cv::Rect(0,0, imCols-1, imRows); // left
    outRect = cv::Rect(1,0, imCols-1, imRows); // left
    imPyramid[pyrLevel](inRect).copyTo(left(outRect));

    inRect  = cv::Rect(1,0, imCols-1, imRows); // right
    outRect = cv::Rect(0,0, imCols-1, imRows); // right
    imPyramid[pyrLevel](inRect).copyTo(right(outRect));

    //----------------------------------------------------------

    inRect  = cv::Rect(0,0, imCols, imRows-1); // top
    outRect = cv::Rect(0,1, imCols, imRows-1); // top
    imPyramid[pyrLevel](inRect).copyTo(top(outRect));

    inRect  = cv::Rect(0,1, imCols, imRows-1); // botom
    outRect = cv::Rect(0,0, imCols, imRows-1); // botom
    imPyramid[pyrLevel](inRect).copyTo(botom(outRect));

    //-----------------------------------------------------------

    cv::Mat gradX(imRows, imCols, CV_32FC1);
    cv::Mat gradY(imRows, imCols, CV_32FC1);

    gradX = 0.5*(right - left);
    gradY = 0.5*(botom - top);

    gradXPyramid.push_back( gradX);
    gradYPyramid.push_back( gradY );
}

void Frame_old::build_imageEdges(int numLayers) {

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlEdges();
}


void Frame_old::build_imageCorners(int numLayers) {

    for (int idx = 0; idx < numLayers; ++idx)

        build_nextPyrLvlCorners();
}


void Frame_old::build_mappablePointsMask(int numLayers) {

    for (int lvl = 0; lvl < numLayers; ++lvl)

        build_mappablePointsMaskAtLvl(lvl, FRAME_THRESHOLD_EDGES);
}


void Frame_old::build_cornersPointsMask(int numLayers) {

    for (int lvl = 0; lvl < numLayers; ++lvl)

        build_cornerPointsMaskAtLvl(lvl, FRAME_THRESHOLD_CORNERS);
}

void Frame_old::build_nextPyrLvlEdges() {

    int pyrLevel = edgesPyramid.size();
    cv::Mat gradX_square, gradY_square, gradLength_square, gradLength;

    cv::multiply( gradXPyramid[pyrLevel], gradXPyramid[pyrLevel], gradX_square);
    cv::multiply( gradYPyramid[pyrLevel], gradYPyramid[pyrLevel], gradY_square);

    gradLength_square = gradX_square + gradY_square;

    sqrt(gradLength_square, gradLength);

    smearMaxGrads( gradLength );

    edgesPyramid.push_back(gradLength);
}


void Frame_old::build_nextPyrLvlCorners() {

    int pyrLevel = cornersPyramid.size();
    cv::Mat gradX_abs, gradY_abs, corner, gradLength;

    cv::multiply( gradXPyramid[pyrLevel], gradXPyramid[pyrLevel], gradX_abs);
    cv::multiply( gradYPyramid[pyrLevel], gradYPyramid[pyrLevel], gradY_abs);

    gradX_abs  = cv::abs(gradXPyramid[pyrLevel] );
    gradY_abs  = cv::abs(gradYPyramid[pyrLevel] );

    cv::multiply(gradX_abs, gradY_abs, corner  );

    cornersPyramid.push_back(corner);
}

void Frame_old::test_writeImagePyramid(const std::string &imPath, const std::string &imName) {

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    std::string fullPath;

    for (int idx=0; idx < imPyramid.size(); ++idx){

        fullPath = imPath + "//" + imName + "_" + std::to_string(idx) +".png";
        try { cv::imwrite(fullPath, imPyramid[idx], compression_params); }
        catch (std::runtime_error& ex){ std::cerr << "Image pyramid level " << std::to_string(idx) << "::exception converting image to PNG format: %s\n" << ex.what(); }
    }

}

void Frame_old::test_writeImageGradsPyramid(const std::string &imPath, const std::string &imName) {


    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);

    std::string fullPath;

    double min,  max;
    cv::Mat adjMap;

    for (int idx=0; idx < gradXPyramid.size(); ++idx){

        fullPath = imPath + "//" + imName + "X_" + std::to_string(idx) +".png";

        cv::minMaxIdx(gradXPyramid[idx], &min, &max);
        cv::convertScaleAbs(gradXPyramid[idx]-min, adjMap, 255 / ( max-min) );
        try{cv::imwrite(fullPath , adjMap, compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image gradient X pyramid level " << std::to_string(idx) << "::exception converting image to PNG format: %s\n" << ex.what(); }

        cv::minMaxIdx(gradYPyramid[idx], &min, &max);
        cv::convertScaleAbs(gradYPyramid[idx]-min, adjMap, 255 / ( max-min) );
        fullPath = imPath + "//" + imName + "Y_" + std::to_string(idx) +".png";
        try{cv::imwrite(fullPath , adjMap, compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image gradient Y pyramid level " << std::to_string(idx) << "::xception converting image to PNG format: %s\n" << ex.what(); }
    }


}

void Frame_old::test_writeImageMaxGradsPyramid(const std::string &imPath, const std::string &imName) {

    std::vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    compression_params.push_back(9);
    std::string fullPath;

    for (int idx=0; idx < edgesPyramid.size(); ++idx){

        fullPath = imPath + "//" + imName + "_" + std::to_string(idx) +".png";
        try{cv::imwrite(fullPath , edgesPyramid[idx], compression_params);}
        catch (std::runtime_error& ex){ std::cerr << "Image max gradient pyramid level " << std::to_string(idx)<<"::xception converting image to PNG format: %s\n"<< ex.what(); }
    }

}

void Frame_old::test_writeCornerPtsList(const std::string &imPath, const std::string &imName){



//	for (int idx=0; idx < imPyramid_K.size(); ++idx){

    std::ofstream    fs_cornerPtList    ( imPath+"//"+imName+".txt", std::ofstream::out);

    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");

    Eigen::Matrix2Xf pts;
    get_edgesPointsList(/*idx*/0, pts, 45);
    std::cout << fs_cornerPtList.is_open() << std::endl;

//		fs_cornerPtList << "level " << idx  << ": " << std::endl;
    fs_cornerPtList << (pts.transpose()).format(CleanFmt);
//		fs_cornerPtList << std::endl;
//		fs_cornerPtList << std::endl;
//	}

//	fs_cornerPtList.close();
}

void Frame_old::test_writeCornerListOCV(const std::string &imPath, const std::string &imName){


//	for (int idx=0; idx < imPyramid_K.size(); ++idx){

    std::ofstream    fs_cornerPtList    ( imPath+"//"+ imName + ".txt", std::ofstream::out);

    cv::Mat corners;
    get_gradCornesOCV(0, corners, 50);

    fs_cornerPtList << corners;

//		fs_cornerPtList << std::endl;
//		fs_cornerPtList << std::endl;
//	}

}

void Frame_old::get_edgesPointsList(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int i=0; i<edgesPyramid[lvl].cols; i++)
        for(int j=0; j<edgesPyramid[lvl].rows; j++)

            if ( edgesPyramid[lvl].at<float>(j,i)  >= threshold ){ // = (float) i/edgesPyramid[lvl].rows; //(float) j / edgesPyramid[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}


void Frame_old::get_cornerPointsList(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int i=0; i<edgesPyramid[lvl].cols; i++)
        for(int j=0; j<edgesPyramid[lvl].rows; j++)

            if ( edgesPyramid[lvl].at<float>(j,i)  >= threshold ){ // = (float) i/edgesPyramid[lvl].rows; //(float) j / edgesPyramid[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}


void Frame_old::get_gradCornesOCV(int lvl, cv::Mat &outPoints, int threshold) {


//	if (  outPoints.cols != 0 ) 	outPoints.release();

    outPoints = cv::Mat(0,0, CV_32FC2);

    for(int i=0; i<edgesPyramid[lvl].cols; i++)
        for(int j=0; j<edgesPyramid[lvl].rows; j++)

            if ( edgesPyramid[lvl].at<float>(j,i)  > threshold ){ // = (float) i/edgesPyramid[lvl].rows; //(float) j / edgesPyramid[lvl].cols;

                cv::Vec2f vec(i, j);
                outPoints.push_back(vec);
            }


//	std::cout << "get_gradCornesOCV: " << outPoints.rows << "x" <<  outPoints.cols << std::endl;


}


void Frame_old::get_gradCornersSubPx(cv::Mat &corners2, int lvl, int wSize, int zSize, int threshold) {


    get_gradCornesOCV(lvl, corners2, threshold);
    cv::cornerSubPix( imPyramid[lvl], corners2, cv::Size( 5, 5 ), cv::Size( -1, -1 ), cv::TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001 ));
}

void Frame_old::get_HarrisCornersCameraFrame_old(int lvl, Eigen::Matrix2Xf &outPoints, int treshold, int blockSize, int apertureSize, float k) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    cv::Mat hCorners;
    cv::cornerHarris( imPyramid[lvl], hCorners, blockSize, apertureSize, k, cv::BORDER_DEFAULT );  /// Detecting corners
    normalize( hCorners, hCorners, 0, 255, cv::NORM_MINMAX, CV_32FC1);    /// Normalizing
    convertScaleAbs( hCorners, hCorners );



    for(int i=0; i< hCorners.cols; i++)
        for(int j=0; j< hCorners.rows; j++)

            if ( hCorners.at<float>(j,i)  > (float) treshold/255 ){
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << i, j;
                outPoints.col(outPoints.cols()-1) = curCoords;
            }

}

void Frame_old::smearMaxGrads(cv::Mat &maxgrads) {


//    if(data_.maxGradientsValid[level]) return;

//    if(enablePrintDebugInfo && printFrame_oldBuildDebugInfo)    printf("CREATE AbsGrad lvl %d for Frame_old %d\n", lvl, id());

    int width   = maxgrads.cols;
    int height  = maxgrads.rows;


    float* maxGradTemp = new float [width * height];


    // 1. write abs gradients in real data_.
    Eigen::Vector4f*    gradxyii_pt         =   ( Eigen::Vector4f* ) maxgrads.data  + width;
    float*              maxgrad_pt          =   ( float* ) maxgrads.data  + width +1 ;
    float*              maxgrad_pt_max      =   ( float* ) maxgrads.data + width*(height-1) -1;


    // 2. smear up/down direction into temp buffer
    float* maxgrad_t_pt     =   maxGradTemp + width+1;

    for( ;   maxgrad_pt<maxgrad_pt_max;  maxgrad_pt++, maxgrad_t_pt++ )
    {
        float g1 = maxgrad_pt[-width];
        float g2 = maxgrad_pt[0];
        if(g1 < g2) g1 = g2;

        float g3 = maxgrad_pt[width];
        if(g1 < g3)
            *maxgrad_t_pt = g3;
        else
            *maxgrad_t_pt = g1;
    }

    float numMappablePixels = 0;

    // 2. smear left/right direction into real data_
    maxgrad_pt              =  ( float* ) maxgrads.data  + width+1;
    maxgrad_pt_max          =  ( float* ) maxgrads.data  + width*(height-1)-1;
    maxgrad_t_pt            =   maxGradTemp + width+1;

    for( ;  maxgrad_pt<maxgrad_pt_max;  maxgrad_pt++, maxgrad_t_pt++ )
    {
        float g1 = maxgrad_t_pt[-1];
        float g2 = maxgrad_t_pt[0];

        if(g1 < g2) g1 = g2;

        float g3 = maxgrad_t_pt[1];
        if(g1 < g3)
        {
            *maxgrad_pt = g3;
            if(g3 >= FRAME_THRESHOLD_EDGES)
                numMappablePixels++;
        }
        else
        {
            *maxgrad_pt = g1;
            if(g1 >= FRAME_THRESHOLD_EDGES)
                numMappablePixels++;
        }
    }

//    if(level==0)        this->stats_.numMappablePixels = numMappablePixels;

//    Frame_oldMemory::getInstance().returnBuffer(maxGradTemp);
    delete[] maxGradTemp;
//    data_.maxGradientsValid[level] = true;

}

void  Frame_old::multiplyMaxGradByMaskAtLvl(const cv::Mat &mask, int lvl) {

    cv::multiply( edgesPyramid[lvl], mask, edgesPyramid[lvl] );
}


void  Frame_old::multiplyCornersByMaskAtLvl(const cv::Mat &mask, int lvl) {

    cv::multiply( cornersPyramid[lvl], mask, cornersPyramid[lvl] );
}

void  Frame_old::multiplyMaxGradByMask  (const cv::Mat &mask){

    if ( (mask.cols != edgesPyramid[0].cols)  || (mask.cols != edgesPyramid[0].cols) ) {  std::cout << " mask size do not consize with edges size" << std::endl; return ;}

    cv::Mat mask__;

    for (int lvl = 0; lvl < edgesPyramid.size(); ++lvl) {

        cv::Size  size__ = cv::Size(edgesPyramid[lvl].cols , edgesPyramid[lvl].rows );

        cv::resize(mask, mask__,  size__ );

        multiplyMaxGradByMaskAtLvl(mask__, lvl);
    }

}



void  Frame_old::multiplyCornersByMask  (const cv::Mat &mask){

    if ( (mask.cols != cornersPyramid[0].cols)  || (mask.cols != cornersPyramid[0].cols) ) {  std::cout << " mask size do not consize with edges size" << std::endl; return ;}

    cv::Mat mask__;

    for (int lvl = 0; lvl < cornersPyramid.size(); ++lvl) {

        cv::Size  size__ = cv::Size( cornersPyramid[lvl].cols , cornersPyramid[lvl].rows );

        cv::resize(mask, mask__,  size__ );

        multiplyCornersByMaskAtLvl(mask__, lvl);
    }

}



void Frame_old::get_listOfEdgesCoordinates(int lvl, Eigen::Matrix2Xf &outPoints, int threshold) {


    if (  outPoints.cols() != 0 ) 	outPoints.resize(2, 0);

    for(int col=0; col<edgesPyramid[lvl].cols; col++)
        for(int row=0; row<edgesPyramid[lvl].rows; row++)

            if ( edgesPyramid[lvl].at<float>(row,col)  >= threshold ){ // = (float) col/edgesPyramid[lvl].rows; //(float) row / edgesPyramid[lvl].cols;
                outPoints.conservativeResize(Eigen::NoChange, outPoints.cols()+1);
                Eigen::Vector2f curCoords;
                curCoords << col, row;
                outPoints.col( outPoints.cols()-1 ) = curCoords;
            }

}

void Frame_old::build_mappablePointsMaskAtLvl(int lvl, int threshold) {

    cv::Mat mappablePointsMask_ =  cv::Mat::zeros( edgesPyramid[lvl].rows, edgesPyramid[lvl].cols, IPL_DEPTH_1U);

    for(int col=0; col<edgesPyramid[lvl].cols; col++) {
        for (int row = 0; row < edgesPyramid[lvl].rows; row++)

            if (edgesPyramid[lvl].at<float>(row, col) >= threshold) mappablePointsMask_.at<bool>(row, col) = true;  // mappablePointsMask.at<bool>(row, col) = true;
            else                                                    mappablePointsMask_.at<bool>(row, col) = false; //  mappablePointsMask.at<bool>(row, col) = false;
    }

    mappablePointsMaskPyramid.push_back(mappablePointsMask_);

}


void Frame_old::build_cornerPointsMaskAtLvl(int lvl, int threshold) {

    cv::Mat cornerPointsMask_ =  cv::Mat::zeros( cornersPyramid[lvl].rows, cornersPyramid[lvl].cols, IPL_DEPTH_1U);

    for(int col=0; col<cornersPyramid[lvl].cols; col++) {
        for (int row = 0; row < cornersPyramid[lvl].rows; row++)

            if (cornersPyramid[lvl].at<float>(row, col) >= threshold) cornerPointsMask_.at<bool>(row, col) = true;  // mappablePointsMask.at<bool>(row, col) = true;
            else                                                      cornerPointsMask_.at<bool>(row, col) = false; //  mappablePointsMask.at<bool>(row, col) = false;
    }

    cornerPointsMaskPyramid.push_back(cornerPointsMask_);

}


void Frame_old::show_imagePyramid(int num_lvls) {

    if ( num_lvls < 1) return;


    if ( imPyramid.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame_old. \n";    return; }

    int w = imPyramid[0].cols,   h = imPyramid[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    imPyramid[0].copyTo(imToShow);

    if ( num_lvls > 2 ) {


        for (int lvl = 1; lvl < std::min( (int)imPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = imPyramid[lvl].cols, h_lvl = imPyramid[lvl].rows;

            cv::Mat currentImToAdd = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm = imPyramid[lvl];

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));

            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }

    imshowScaled("Image Pyramid", imToShow);
    cv::waitKey(-1);

}



void Frame_old::show_disparityMap(bool reverse) {

    if ( disparityPyramid.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame_old. \n";    return; }

    int w = disparityPyramid[0].cols,   h = disparityPyramid[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    disparityPyramid[0].copyTo(imToShow);


    for (int lvl = 1; lvl < disparityPyramid.size(); ++lvl) {

        int w_lvl = disparityPyramid[lvl].cols,   h_lvl = disparityPyramid[lvl].rows;

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,     w_lvl, CV_32FC1 );
        cv::Mat currentIm       = disparityPyramid[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));

        cv::hconcat(imToShow,  currentImToAdd, imToShow);

    }

    if ( reverse) imToShow *= -1.0f;

    imshowScaled("Disparity Pyramid", imToShow);
    cv::waitKey(-1);



}




void  Frame_old::show_gradientsPyramid(  ){

    if ( ( gradXPyramid.size() == 0 ) || ( gradYPyramid.size() == 0 )  ) {  std::cout << "There are NO IMAGE PYRAMID in this Frame_old. \n";    return; }

    int w = imPyramid[0].cols,   h = imPyramid[0].rows;

    cv::Mat gradToShowX    = cv::Mat( h, w, CV_32FC1 );
    cv::Mat gradToShowY    = cv::Mat( h, w, CV_32FC1 );

    gradXPyramid[0].copyTo(gradToShowX);
    gradYPyramid[0].copyTo(gradToShowY);


    for (int lvl = 1; lvl < gradXPyramid.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,   gradXPyramid[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = gradXPyramid[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(gradToShowX,  currentImToAdd, gradToShowX);
    }



    for (int lvl = 1; lvl < gradYPyramid.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,   gradYPyramid[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = gradYPyramid[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(gradToShowY,  currentImToAdd, gradToShowY);
    }



    imshowScaled("Image Gradient X", gradToShowX);
    imshowScaled("Image Gradient Y", gradToShowY);
    cv::waitKey(-1);
}

void   Frame_old::show_edgesPyramid(  ){


    if ( edgesPyramid.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame_old. \n";    return; }

    int w = edgesPyramid[0].cols,   h = edgesPyramid[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    edgesPyramid[0].copyTo(imToShow);


    for (int lvl = 1; lvl < edgesPyramid.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,  edgesPyramid[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = edgesPyramid[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(imToShow,  currentImToAdd, imToShow);
    }

    imshowScaled("Edges Pyramid", imToShow);
    cv::waitKey(-1);
}


void  Frame_old::show_cornersPyramid(  ){


    if ( cornersPyramid.size() == 0) {  std::cout << "There are NO IMAGE PYRAMID in this Frame_old. \n";    return; }

    int w = cornersPyramid[0].cols,   h = cornersPyramid[0].rows;

    cv::Mat imToShow    = cv::Mat( h, w, CV_32FC1 );

    cornersPyramid[0].copyTo(imToShow);


    for (int lvl = 1; lvl < cornersPyramid.size(); ++lvl) {

        cv::Mat currentImToAdd  = cv::Mat::zeros( h,  cornersPyramid[lvl].cols, CV_32FC1 );
        cv::Mat currentIm       = cornersPyramid[lvl];

        currentIm.copyTo( currentImToAdd(cv::Rect(0,0, currentIm.cols, currentIm.rows )));
        cv::hconcat(imToShow,  currentImToAdd, imToShow);
    }

    imshowScaled("Corners Pyramid", imToShow);
    cv::waitKey(-1);
}


void Frame_old::show_propagatedCornersPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;
    if ( cornersPropagatedPyramid.size() == 0) {  std::cout << "There are NO BIT MASK PYRAMID in this Frame_old. \n";    return; }

    int w = cornersPropagatedPyramid[0].cols,   h = cornersPropagatedPyramid[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  cornersPropagatedPyramid[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)cornersPropagatedPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = cornersPropagatedPyramid[lvl].cols, h_lvl = cornersPropagatedPyramid[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);


            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = cornersPropagatedPyramid[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask Propagated Corners Pyramid", imToShow);
    cv::waitKey(-1);


}



void Frame_old::show_mappablePointsPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;


    if ( mappablePointsMaskPyramid.size() == 0) {  std::cout << "There are NO BIT MASK PYRAMID in this Frame_old. \n";    return; }

    int w = mappablePointsMaskPyramid[0].cols,   h = mappablePointsMaskPyramid[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  mappablePointsMaskPyramid[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)mappablePointsMaskPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = mappablePointsMaskPyramid[lvl].cols, h_lvl = mappablePointsMaskPyramid[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);


            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = mappablePointsMaskPyramid[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask Edges Pyramid", imToShow);
    cv::waitKey(-1);
}

void Frame_old::show_cornerPointsPyramid(int num_lvls) {

    if ( num_lvls == 0 ) return;


    if ( cornerPointsMaskPyramid.size() == 0) {  std::cout << "There are NO BIT CORNER MASK PYRAMID in this Frame_old. \n";    return; }

    int w = cornerPointsMaskPyramid[0].cols,   h = cornerPointsMaskPyramid[0].rows;

    cv::Mat imToShow = cv::Mat::zeros( cv::Size(w, h) , CV_32FC1);

    for (int row = 0; row < h; ++row) {
        for (int col = 0; col < w; ++col) {

            imToShow.at<float>(row,col) =  cornerPointsMaskPyramid[0].at<bool>(row,col);
        }
    }


    if ( num_lvls > 1 ) {

        for (int lvl = 1; lvl < std::min( (int)cornerPointsMaskPyramid.size(), num_lvls); ++lvl) {

            int w_lvl = cornerPointsMaskPyramid[lvl].cols, h_lvl = cornerPointsMaskPyramid[lvl].rows;

            cv::Mat currentImToAdd  = cv::Mat::zeros(h, w_lvl, CV_32FC1);
            cv::Mat currentIm       = cv::Mat::zeros(h_lvl, w_lvl, CV_32FC1);

            for (int row = 0; row < h_lvl; ++row) {
                for (int col = 0; col < w_lvl; ++col) {

                    currentIm.at<float>(row, col) = cornerPointsMaskPyramid[lvl].at<bool>(row, col);
                }
            }

            currentIm.copyTo(currentImToAdd(cv::Rect(0, 0, currentIm.cols, currentIm.rows)));
            cv::hconcat(imToShow, currentImToAdd, imToShow);

        }
    }


    cv::imshow("Bit Mask  Corners Pyramid", imToShow);
    cv::waitKey(-1);
}

void Frame_old::imshowScaled(std::string imName, cv::Mat &im)
{

    double min;
    double max;

    cv::minMaxIdx(im, &min, &max);
    cv::Mat adjMap;
    cv::convertScaleAbs(im-min, adjMap, 255 / ( max-min) );
    cv::imshow(imName, adjMap);
}

void Frame_old::fill_disparityMaps(int layers) {

    disparityPyramid.clear();

    for (int lvl = 0; lvl < layers; ++lvl) {

        disparityPyramid.push_back( cv::Mat::zeros( imPyramid[lvl].rows, imPyramid[lvl].cols, CV_32FC1) );
    }

}

void Frame_old::fill_cornerDisparityMaps(int layers) {

    cornersDisparityPyramid.clear();

    for (int lvl = 0; lvl < layers; ++lvl) {

        cornersDisparityPyramid.push_back( cv::Mat::zeros( imPyramid[lvl].rows, imPyramid[lvl].cols, CV_32FC1) );
    }

}

void Frame_old::calc_imZeroLvlShiftScale() {

    double min, max;
    cv::minMaxIdx(imPyramid[0], &min, &max);
    scaleShiftZeroLvlPyramid = std::make_pair( min, 255/( max-min) );
}


void Frame_old::calc_minMaxDisparities(const std::pair<int, int> &minMaxDispLvlZero, int numlevels) {

    minMaxDisparities.clear();

    minMaxDisparities.push_back( minMaxDispLvlZero) ;

    for (int i = 1; i < numlevels; ++i) {

        float min  = std::round( (float) minMaxDispLvlZero.first  / (i+1) );
        float max  = std::round( (float) minMaxDispLvlZero.second / (i+1) );

        minMaxDisparities.push_back( std::make_pair( min, max) );
    }

}


void Frame_old::calc_minMaxDisparitiesForMatching(const std::pair<int, int> &minMaxDispLvlZero, int numlevels) {

    calc_minMaxDisparities( minMaxDispLvlZero, numlevels);

    for (int lvl = 0; lvl < numlevels - 1; ++lvl) {

        minMaxPyramidMatchingDisparities.push_back( std::make_pair( -13, +13) );
    }

    minMaxPyramidMatchingDisparities.push_back(   minMaxDisparities[numlevels-1] );
}

void Frame_old::calcNumMappablePoints(int numLayers) {

    numOfMappablePointsPyramid.clear();

    for (int lvl = 0; lvl < numLayers; ++lvl) {

        int rows            = imPyramid[lvl].rows;
        int cols            = imPyramid[lvl].cols;
        int numOfMapPts     = 0;

        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {

                if ( mappablePointsMaskPyramid[lvl].at<bool>(row, col) ) numOfMapPts++;

            }

        }

        numOfMappablePointsPyramid.push_back(numOfMapPts);

    }
}

void Frame_old::fill_disparitiesHoles(int lvl){

    for (int idx = 0; idx < 20; ++idx) {

        fill_disparitiesHolesIteration(lvl, 3);
    }
}

void Frame_old::fill_disparitiesHolesIteration(int lvl, int min_num_neighbours) {

    int cols = imPyramid[lvl].cols;
    int rows = imPyramid[lvl].rows;

    cv::Mat mappablePointsMask_ =  cv::Mat::zeros( rows, cols, IPL_DEPTH_1U);

    mappablePointsMaskPyramid[lvl].copyTo(mappablePointsMask_);

    for (int col = 0; col < cols; ++col) {
        for (int row = 0; row < rows; ++row) {

            if ( !mappablePointsMask_.at<bool>(row, col)) {

                float pix_value = calc_numMappableNeighbours(row, col, lvl, min_num_neighbours, mappablePointsMask_);

                if (std::abs(pix_value) > 1e-2) {
                    disparityPyramid[lvl].at<float>(row, col)           = pix_value;
                    mappablePointsMaskPyramid[lvl].at<bool>(row, col)   = true;
                }
            }
        }

    }
}

float Frame_old::calc_numMappableNeighbours(int in_row, int in_col, int lvl, int do_interp_count, const cv::Mat &mapPtsMask) {

    int     count   =   0;
    float   val     =   0;

    for (int col = in_col-1; col <= in_col+1; ++col) {
        for (int row = in_row-1; row <= in_row+1; ++row) {

            if ( (row < 0 )  ||  (row > imPyramid[lvl].rows-1) || (col < 0 )  ||  (col > imPyramid[lvl].cols-1)  ) continue;
            if ( !mapPtsMask.at<bool>(row,col) ) continue;

            count++;
            val += disparityPyramid[lvl].at<float>(row,col);
        }
    }



    if ( count >= do_interp_count )   return    (float) val/count;
    else                              return    0.0f;

}


void Frame_old::medianFilterDisparityMap(int lvl) {

    float window[9];
    cv::Mat disparity_temp;


    disparityPyramid[lvl].copyTo( disparity_temp);

    for(int y = 1; y < disparityPyramid[lvl].rows - 1; y++){
        for(int x = 1; x < disparityPyramid[lvl].cols - 1; x++){

            window[0] = disparityPyramid[lvl].at<float>( y - 1 , x - 1 );
            window[1] = disparityPyramid[lvl].at<float>( y     , x - 1 );
            window[2] = disparityPyramid[lvl].at<float>( y + 1 , x - 1 );
            window[3] = disparityPyramid[lvl].at<float>( y - 1 , x );
            window[4] = disparityPyramid[lvl].at<float>( y     , x );
            window[5] = disparityPyramid[lvl].at<float>( y + 1 , x );
            window[6] = disparityPyramid[lvl].at<float>( y - 1 , x + 1 );
            window[7] = disparityPyramid[lvl].at<float>( y     , x + 1 );
            window[8] = disparityPyramid[lvl].at<float>( y + 1 , x + 1 );

            // sort the window to find median
            int temp, i , j;

            for(i = 0; i < 9; i++){
                temp = window[i];
                for(j = i-1; j >= 0 && temp < window[j]; j--){
                    window[j+1] = window[j];
                }
                window[j+1] = temp;
            }

            // assign the median to centered element of the matrix
            disparity_temp.at<float>(y,x) = window[4];
        }
    }


    disparity_temp.copyTo( disparityPyramid[lvl] );

}

void Frame_old::medianFilterDisparityMap_std(int lvl) {

    std::vector<float>  window(9);
    cv::Mat             disparity_temp;

    disparityPyramid[lvl].copyTo( disparity_temp);

    for(int y = 1; y < disparityPyramid[lvl].rows - 1; y++){
        for(int x = 1; x < disparityPyramid[lvl].cols - 1; x++){

            window[0] = disparityPyramid[lvl].at<float>( y - 1 , x - 1 );
            window[1] = disparityPyramid[lvl].at<float>( y     , x - 1 );
            window[2] = disparityPyramid[lvl].at<float>( y + 1 , x - 1 );
            window[3] = disparityPyramid[lvl].at<float>( y - 1 , x );
            window[4] = disparityPyramid[lvl].at<float>( y     , x );
            window[5] = disparityPyramid[lvl].at<float>( y + 1 , x );
            window[6] = disparityPyramid[lvl].at<float>( y - 1 , x + 1 );
            window[7] = disparityPyramid[lvl].at<float>( y     , x + 1 );
            window[8] = disparityPyramid[lvl].at<float>( y + 1 , x + 1 );

            // sort the window to find median
            std::sort(window.begin(), window.end());
            disparity_temp.at<float>(y,x) = window[4];
        }
    }


    disparity_temp.copyTo( disparityPyramid[lvl] );

}


void Frame_old::check_and_resolve_numLvls_division(int num_levels) {

    int factor                  = std::pow(2, num_levels);

    int im_type                 = imPyramid[0].type();
    int width                   = imPyramid[0].cols;
    int height                  = imPyramid[0].rows;
    int width_delta             = std::round( (float) width  / factor ) * factor   -    width  ;
    int height_delta            = std::round( (float) height / factor ) * factor   -    height ;

    if ( width_delta  ) {

        cv::Mat add_cols = cv::Mat::zeros(height, width_delta, im_type);
        cv::hconcat( imPyramid[0], add_cols, imPyramid[0]);
    }

    if ( height_delta ) {

        cv::Mat add_rows = cv::Mat::zeros(height_delta, width, im_type);
        cv::vconcat( imPyramid[0], add_rows, imPyramid[0]);
    }



}

void Frame_old::setChannelToConstValue( cv::Mat &mat, unsigned int channel, unsigned char value) const
{
    // make sure have enough channels
    if (mat.channels() < channel+1)
        return;

    // check_isEmpty mat is continuous or not
    if (mat.isContinuous())
        mat.reshape(1, mat.rows*mat.cols).col(channel).setTo( cv::Scalar(value) );
    else{
        for (int i = 0; i < mat.rows; i++)
            mat.row(i).reshape(1, mat.cols).col(channel).setTo( cv::Scalar(value));
    }
}

void Frame_old::propagateCornersToTheNextLvl(int lvl) {

    if (lvl == PYRAMID_LEVELS) { printf("Frame_old::buildNextLvlBitMask(): top level pyramid reached. \n");     return;   }


    cv::Mat         im              =   cv::Mat::zeros( imPyramid[lvl].rows, imPyramid[lvl].cols,  IPL_DEPTH_1U ) ;

    int             width           =   imPyramid[lvl-1].cols;
    int             height          =   imPyramid[lvl-1].rows;
/*

    for ( int row=0; row < imPyramid[lvl].rows; ++row){
        for (int col = 0; col < imPyramid[lvl].rows; ++col) {

            im.at<bool>( row, col ) = imPyramid[lvl-1].at<bool>( row, col );
        }
    }
*/


    const bool*     source          =   (bool*) cornersPropagatedPyramid[lvl-1].data;
    const bool*     source_end      =   source + (width-1)*height;
    bool*           dest            =   (bool*) im.data;

    int i=0;

    for (  ; source < source_end ; ) {

        *dest =  (  *source ||  *(source+1) || *(source+width)  ||  *(source+width+1) );

        source  +=  2;
        i       +=  2;

        if ( i == width){ source += width; i=0;}

        dest++;
    }

    cornersPropagatedPyramid.push_back( im );

}

void Frame_old::propagateCornersToTheNextLvl2(int lvl) {

    cv::Mat im;

    cv::resize( cornersPropagatedPyramid[lvl-1] ,im,  cv::Size( imPyramid[lvl].rows, imPyramid[lvl].rows ), 0,0, CV_INTER_NN );

    cornersPropagatedPyramid.push_back( im );

}

void Frame_old::build_cornersPropagationMask(int numLayers) {

    cornersPropagatedPyramid.clear();

    if ( !cornerPointsMaskPyramid.size() ) return;

    cv::Mat im;
    cornerPointsMaskPyramid[0].copyTo(im);
    cornersPropagatedPyramid.push_back(im);

    for (int lvl = 1; lvl < numLayers; ++lvl) {

        propagateCornersToTheNextLvl(lvl);

    }

}


#endif //SOP_PCFROMSTEREOPAIR_Frame_old_HPPFrame_old::Frame_old(const std::string& imPath){