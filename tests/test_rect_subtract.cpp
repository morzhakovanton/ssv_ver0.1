//
// Created by morzh on 14.04.19.
//



#include "../Disparity/DisparityPOCBlock.h"


int main(){

    DisparityPOCBlock disp_poc;

    cv::Rect rect = cv::Rect(0,0, 20, 14);

    std::cout << "source rect" << rect << std::endl;

    cv::Rect slice, remainder;

    disp_poc.CvRectDivide(rect, &slice, &remainder, 10, disp_poc.maxXEdge);


    std::cout << "slice rect" << slice<< std::endl;
    std::cout << "remainder rect" << remainder << std::endl;

    return 0;
}