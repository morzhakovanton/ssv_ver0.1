//
// Created by morzh on 16.04.19.
//


#include <OpticalFlow/QuadTree__.h>

using namespace std;

int main()
{
    QuadTree__            center( Point(0, 0), Point(8, 8));
    QuadTreeNode        a     ( Point(1, 1), 1);
    QuadTreeNode        b     ( Point(2, 5), 2);
    QuadTreeNode        c     ( Point(7, 6), 3);

    center.insert(&a);
    center.insert(&b);
    center.insert(&c);

    cout << "Node a:            " <<  center.search(Point(1, 1))->data << "\n";
    cout << "Node b:            " <<  center.search(Point(2, 5))->data << "\n";
    cout << "Node c:            " <<  center.search(Point(7, 6))->data << "\n";
    cout << "Non-existing node: " << center.search(Point(5, 5));

    return 0;
}