//
//  test_initialization of Rectangle<T> class
//


#include "Common/ImageRectangle.h"

int main(){



    ImageRectangleI     rect1( 2,  2, 10, 20);
    ImageRectangleI     rect2( 3, 13,  5, 10);
    ImageRectangleI     rect3;

//    rect1.overlap_inPlace(rect2);

    rect3 = rect1.overlap(rect2);


    rect2.pushInside(rect1, nullptr);
    rect2.print();



    return 0;

}